# E-Staffing
E Staffing App (Web and Mobile Apps)

## Changelog

### Version 1.9.18 - CompileVersion 300

- Add new talent module.
- Add new feedback module.
- Add apk: app-V.1.9.18-QA.apk

## Changelog

### Version 1.9.2 - CompileVersion 293

- Add personalization preferences.
- Add apk: app-V.1.9.2-QA.apk

### Version 1.8.23 - CompileVersion 141

- Add persistence to tokens for login
- Add configuration class to save preferences encrypted
- Add apk: app-V.1.8.23-QA.apk

### Version 1.8.22 - CompileVersion 140
- ES-2276 -> Se cambio el servicio de talent para usar "findByHierarchy"

### version 1.8.21 {Dev && Prod} - CompileVersion 139
-  se terminaron todas las pantallas del modulo correspondiente

### Version 1.8.5 - CompileVersion 123

- Fix agendado de entrevistas, para soportar codigo  dude después 201
- Rester
-- Se agrego sopore para codigos "success" que no sean solo 200

### Version 1.8.3 - CompileVersion 121

- Fix de lenguajes que no tiene URL de video para que no se puedan ver
- US-13 parte inicial

### version 1.8.1 - CompileVersion 119

- Fix error de que no mostraba video en pantalla completa(hueco de lado derecho) [ES-2174]
- Fix error de no permitía seleccionar opción de idioma desde el listado [Es-21756]
- Fix error de quitar loader por que se queda ciclado cuando el video tenia problemas [ES-2182]
- Fix de limpiar la caja después de ir a otro video

### Version 1.7.103 - CompileVersion 118

- Refactor "setHeader" en Cada fragment por ponerla en "ABFragment" como función padre

### Version 1.7.102 - CompileVersion 117

- Se regreso el popup de idiomas en 

### Version 1.7.101 - CompileVersion 116 "RecruitmentRequestCandidateSingleFragment"

- Se creo fragmento de info del lenguaje
-- Se remplazo el popup de lenguaje por este fragmento
- Fix en "RecruitmentRequestCandidateEducationSingleFragment" para regresar con la flecha

### Version 1.7.100 - CompileVersion 115

- Fix de separadores en "RecruitmentRequestSingleFragment" in skills
- Fix de mayusculas en "RequestCandidateWorkExperienceAdapter" en nombre y puesto
- Fix de color y CamelCase en "RequestCandidateWorkExperienceAdapter" para la duración

### Version 1.7.99 - CompileVersion 114

- Agregada US-11
-- Incluye maqueado del Modal y conexión de servicio

### Version 1.7.40

- Agregago boton para sitio  web "RegisterThanksController"
- Cambio en el icono de cerrar sesion

Jira stories:

- ES-1306
- ES-1284
- ES-1276
- ES-1275

### Version 1.7.38

- Refactor "layout" of "HomeController"
- Refactor "layout" of "Supportcontroller"
- Create "layout" of "RecruitmentConttroller" to be disabled
- Refactor "layout" of "PrivacityController"

Jira stories.

- ES-1152
- ES-1155
- ES-1154
- ES-1383
- ES-1382
- ES-1381
- ES-1300


### Version 1.7.37

- Refactor "layout" of "RecoverController"
- Refactor "layout" of "ResetController"

Jira stories:

- ES-1156
- ES-1146

### Version 1.7.36

- Refactor new version of AndroidX
- Refactor "layout" of "LoginController"
  - Introduce button "Create account"
- Create Controller "Register"
- Create Controller "registerThanks"

Jira stories:

- ES-1106
- ES-1108
- ES-1132
- ES-1133
- ES-1137