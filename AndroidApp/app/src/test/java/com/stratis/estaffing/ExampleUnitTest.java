package com.stratis.estaffing;

import android.content.Context;

import com.stratis.estaffing.Core.App;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void addition_inCorrect() {
        assertNotEquals(5, 2 + 2);
    }

    @Test
    public void isEmailValid() {
        assertTrue(App.isEmailValid("erick@gmail.com"));
    }

    @Test
    public void isEmailInvalid() {
        assertFalse(App.isEmailValid("erick"));
    }
}