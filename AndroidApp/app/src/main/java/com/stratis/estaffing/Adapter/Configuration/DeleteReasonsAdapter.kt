package com.stratis.estaffing.Adapter.Configuration

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Model.CatalogItem
import com.stratis.estaffing.R
import com.stratis.estaffing.View.Configuration.DeleteReasonsView

open class DeleteReasonsAdapter(context: Context?, items: List<CatalogItem>, selectedItem: CatalogItem?) : RecyclerView.Adapter<DeleteReasonsView>() {

    private var items: List<CatalogItem>? = null
    private var context: Context? = null
    private var selectedItem: CatalogItem? = null

    private var listener: DeleteReasonsAdapterListener? = null

    init {
        this.context = context
        this.items = items
        this.selectedItem = selectedItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeleteReasonsView {
        val layoutView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.fragment_delete_reason, parent, false)
        return DeleteReasonsView(layoutView)
    }

    override fun onBindViewHolder(holder: DeleteReasonsView, position: Int) {
        holder.setIsRecyclable(false)

        val reason : CatalogItem = this.items!![position]

        holder.content?.setOnClickListener {
            listener?.onClick(reason)
        }

        holder.textView?.text = reason.description
        val image: Int = if (selectedItem?.catalogItemId == reason.catalogItemId) R.drawable.checkbox_square_selected else R.drawable.checkbox_square_unselected
        holder.reasonIconImageView?.setImageResource(image)
        val background: Int = if (selectedItem?.catalogItemId == reason.catalogItemId) R.drawable.talent_feedback_skill_selected else R.drawable.talent_feedback_skill_unselected
        holder.content?.setBackgroundResource(background)
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    fun setListener(listener: DeleteReasonsAdapterListener) {
        this.listener = listener
    }

    interface DeleteReasonsAdapterListener {

        fun onClick(catalogItem: CatalogItem)
    }

}