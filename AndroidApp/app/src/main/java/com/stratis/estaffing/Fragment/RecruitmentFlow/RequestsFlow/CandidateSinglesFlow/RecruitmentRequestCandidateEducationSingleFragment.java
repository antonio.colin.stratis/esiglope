package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandiateEducationModel;
import com.stratis.estaffing.R;

public class RecruitmentRequestCandidateEducationSingleFragment extends ABFragment {

    private CandiateEducationModel educationModel;

    public RecruitmentRequestCandidateEducationSingleFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_candidate_education_single, container, false);
        this.logEvent();
        this.setHeader("");
        this.initViews();

        return this.mView;
    }

    private void logEvent(){
        switch(educationModel.getCategoryId()) {

            case CandiateEducationModel.typeCertification:
            case CandiateEducationModel.typeDiploma:
            default:
                FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat("cert"));
                break;

            case CandiateEducationModel.typeGraded:
            case CandiateEducationModel.typeMaster:
            case CandiateEducationModel.typePhD:
                FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat("ced"));
                break;
        }
    }

    private void initViews() {

        final ImageView educationImage = this.mView.findViewById(R.id.educationImage);
        if (educationModel.getHasCertificate()) {

            educationImage.setImageBitmap(null);
            Glide.with(this)
                    .load( educationModel.getCertificateFileUrl())
                    .centerCrop()
                    .thumbnail(.40f)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    // .placeholder(R.drawable.loading_spinner)
                    .into( educationImage );
        }

        final TextView title = this.mView.findViewById(R.id.title);
        title.setText( educationModel.getCourseName() );

        final TextView schoolContainerContent = this.mView.findViewById(R.id.schoolContainerContent);
        schoolContainerContent.setText( educationModel.getCourseSchool() );

        final TextView dateContainerContent = this.mView.findViewById(R.id.dateContainerContent);
        dateContainerContent.setText( educationModel.getValidityDate() );

        final TextView certificateNumberContent = this.mView.findViewById(R.id.certificateNumberContent);
        certificateNumberContent.setText( educationModel.getCertificateNumber() );

        final TextView educationDescription = this.mView.findViewById(R.id.educationDescription);
        educationDescription.setText( educationModel.getCourseDescription() );
    }

    public void setEducationModel(CandiateEducationModel educationModel) {
        this.educationModel = educationModel;
    }
}