package com.stratis.estaffing.View;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 29/07/20
 */

public class FeedbackView extends RecyclerView.ViewHolder {

    //private ImageView feedbackOwnerInitials = null;
    private TextView feedbackOwnerInitials = null;
    private AutoResizeTextView feedbackOwner = null;
    private TextView feedbackDate = null;
    private TextView feedbackStrengths = null;
    private TextView feedbackOpportunities = null;

    public FeedbackView(View itemView) {

        // --
        super(itemView);

        // --
        this.feedbackOwnerInitials = itemView.findViewById(R.id.feedbackOwnerInitials);
        this.feedbackOwner = itemView.findViewById(R.id.feedbackOwner);
        this.feedbackDate = itemView.findViewById(R.id.feedbackDate);
        this.feedbackStrengths = itemView.findViewById(R.id.feedbackStrengths);
        this.feedbackOpportunities = itemView.findViewById(R.id.feedbackOpportunities);
    }

    /*
    public ImageView getFeedbackOwnerInitials() {
        return feedbackOwnerInitials;
    }
    */
    public TextView getFeedbackOwnerInitials(){
        return feedbackOwnerInitials;
    }

    public AutoResizeTextView getFeedbackOwner() {
        return feedbackOwner;
    }

    public TextView getFeedbackDate() {
        return feedbackDate;
    }

    public TextView getFeedbackStrengths() {
        return feedbackStrengths;
    }

    public TextView getFeedbackOpportunities() {
        return feedbackOpportunities;
    }
}