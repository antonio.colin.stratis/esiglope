package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate.RequestCandidateEducationAdapter;
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.RequestCandidateMessagesAdapter;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Interview.RequestCandidateInterview;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidateMessage;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.CandidateService;

import org.json.JSONArray;

import java.util.ArrayList;

public class RecruitmentRequestCandidateMessagesFragment extends ABFragment
        implements View.OnClickListener {

    private RequestCandidateInterview interview;
    private ArrayList<RequestCandidateMessage> messages;
    private RequestCandidateMessagesAdapter messagesAdapter;

    private EditText inputChatContainerInput;
    private RecyclerView messagesList;

    public RecruitmentRequestCandidateMessagesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_candidate_messages, container, false);

        this.setHeader("");
        this.initViews();

        if (this.messages == null) {
            this.getMessages();
        } else {
            this.paintMessages();
            this.readMessages();
        }

        return this.mView;
    }

    private void initViews() {

        // -- Name
        final TextView title = this.mView.findViewById(R.id.title);
        final String name = String.format(getString(R.string.recruitment_request_candidate_messages_title),
                this.interview.getCandidate().getAccountManagerShort());
        title.setText( name );

        // -- Input message
        this.inputChatContainerInput = this.mView.findViewById(R.id.inputChatContainerInput);

        final ImageView inputChatContainerIcon = this.mView.findViewById(R.id.inputChatContainerIcon);
        inputChatContainerIcon.setTag("1");
        inputChatContainerIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        final String tag = String.valueOf(v.getTag());
        switch (tag) {

            case "1":

                this.sendMessage();
                break;
        }
    }

    private void getMessages() {

        CandidateService.getMessages(this.interview.getCandidate(), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                messages = RequestCandidateMessage.parse(res,
                        Session.get(getContext()).getDataSessionId());
                paintMessages();
                readMessages();
            }

            @Override
            public void onError(ReSTResponse response) {
                messages = new ArrayList<>();
                paintMessages();
            }
        });
    }

    private void sendMessage() {

        final String messageToSend = String.valueOf(this.inputChatContainerInput.getText());
        final RequestCandidateMessage message = new RequestCandidateMessage();
        message.setVacancyCandidateId(this.interview.getCandidate().getVacancyCandidateId());
        message.setMessage(messageToSend);
        message.setAuthorId(Session.get(getContext()).getDataSessionId());
        message.setViewed(true);
        message.setType(RequestCandidateMessage.RequestCandidateMessageType.out);

        if (!messageToSend.equals("")) {

            CandidateService.sendMessage(message, new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {

                    inputChatContainerInput.setText("");
                    inputChatContainerInput.clearFocus();

                    // -- Add message
                    messages.add(message);
                    messagesAdapter.notifyDataSetChanged();
                    messagesList.postDelayed(() ->
                            messagesList.scrollToPosition(messagesList.getAdapter().getItemCount() - 1),
                            1000);
                }

                @Override
                public void onError(ReSTResponse response) {

                    final Notification notification = new Notification(getContext(), R.layout.notification_light_error);
                    notification.setMessage("Error - Vuelve a intentar");
                    notification.show(3000);
                }
            });

        }
    }

    private void readMessages() {

        final ArrayList<RequestCandidateMessage> notReadMessages = RequestCandidateMessage.howManyWithoutRead(this.messages);
        for (RequestCandidateMessage message : notReadMessages) {

            CandidateService.readMessage(message, new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {}

                @Override
                public void onError(ReSTResponse response) {}
            });
        }
    }

    private void paintMessages() {

        // -- Messages list
        this.messagesList = this.mView.findViewById(R.id.messagesList);
        App.createVerticalRecyclerList(messagesList, getContext());

        this.messagesAdapter = new RequestCandidateMessagesAdapter(this.messages);
        messagesList.setAdapter(messagesAdapter);
    }

    public void setInterview(RequestCandidateInterview interview) {
        this.interview = interview;
    }

    public void setMessages(ArrayList<RequestCandidateMessage> messages) {
        this.messages = messages;
    }
}