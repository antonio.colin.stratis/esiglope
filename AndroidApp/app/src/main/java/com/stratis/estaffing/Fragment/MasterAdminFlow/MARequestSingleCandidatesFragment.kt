package com.stratis.estaffing.Fragment.MasterAdminFlow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.MasterAdminFlow.MACandidatesAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Control.Loader
import com.stratis.estaffing.Control.Notification
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Listener.NotificationCenterFragmentListener
import com.stratis.estaffing.Model.MasterAdminFlow.MARequestCandidate
import com.stratis.estaffing.Model.Profile
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.MasterAdmin.MasterAdminService
import org.json.JSONArray
import java.util.*

open class MARequestSingleCandidatesFragment : ABFragment(),
        MACandidatesAdapter.MARequestCandidateClickListener, MARequestSingleCandidateSingleListener {

    open var profile: Profile? = null
    open var loader: Loader? = null
    open var notificationCenterListener: NotificationCenterFragmentListener? = null

    private var candidatesNoReviewed: RecyclerView? = null
    private var candidatesReviewed: RecyclerView? = null
    private var candidatesDiscarded: RecyclerView? = null

    private var candidatesNoReviewedAdapter: MACandidatesAdapter? = null
    private var candidatesReviewedAdapter: MACandidatesAdapter? = null
    private var candidatesDiscardedAdapter: MACandidatesAdapter? = null

    //private var currentFilterId = "0"
    private var candidatesNoReviewedVisible = false
    private var candidatesReviewedVisible = false
    private var candidatesDiscardedVisible = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_m_a_request_single_candidates, container, false)

        // -- Set header
        setHeader("")

        // -- Init views
        this.initViews()

        // -- Get talents
        this.getCandidates()

        // -- Show loader
        loader = Loader(mContext)
        loader?.show()

        return this.mView
    }

    private fun initViews() {

        this.candidatesNoReviewed = mView.findViewById(R.id.gradedList)
        App.createVerticalRecyclerList(this.candidatesNoReviewed, context)

        this.candidatesReviewed = mView.findViewById(R.id.recyclerView)
        App.createVerticalRecyclerList(this.candidatesReviewed, context)

        this.candidatesDiscarded = mView.findViewById(R.id.elements_list)
        App.createVerticalRecyclerList(this.candidatesDiscarded, context)
    }

    open fun getCandidates() {

        // -- Get talents
        MasterAdminService.getCandidates(profile!!.provisionalId, object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {

                // --
                val res = JsonBuilder.stringToJsonArray(response.body)
                parseRequests(res)

                // -- Hide loader
                loader!!.cancel()
            }

            override fun onError(response: ReSTResponse) {

                mView.findViewById<View>(R.id.filtersContainer).visibility = View.GONE
                loader!!.cancel()
                if (notificationCenterListener != null) {
                    notificationCenterListener?.onCandidateListNotAvailable()
                }
            }
        })
    }

    private fun parseRequests(res: JSONArray) {

        val candidatesNoReviewedItems = ArrayList<MARequestCandidate>()
        val candidatesReviewedItems = ArrayList<MARequestCandidate>()
        val candidatesDiscardedItems = ArrayList<MARequestCandidate>()

        val items = MARequestCandidate.parse(res)
        for (item in items) {

            if (item.statusId != MARequestCandidate.statusCandidateDiscarded) {
                if (item.isReviewed) {
                    candidatesReviewedItems.add(item)
                } else {
                    candidatesNoReviewedItems.add(item)
                }
            } else {
                candidatesDiscardedItems.add(item)
            }
        }

        // -- Painting
        this.candidatesNoReviewedAdapter = MACandidatesAdapter(
                MARequestCandidate.getWithDatesTitles(candidatesNoReviewedItems), context)
        this.candidatesNoReviewedAdapter?.setListener(this)
        this.candidatesNoReviewed?.setAdapter(candidatesNoReviewedAdapter)
        if (candidatesNoReviewedItems.size > 0) {
            this.candidatesNoReviewedVisible = true
            mView.findViewById<View>(R.id.candidatesNoReviewedGroup).visibility = View.VISIBLE
        }

        this.candidatesReviewedAdapter = MACandidatesAdapter(MARequestCandidate.getWithDatesTitles(candidatesReviewedItems), context)
        this.candidatesReviewedAdapter?.setListener(this)
        this.candidatesReviewed?.setAdapter(candidatesReviewedAdapter)
        if (candidatesReviewedItems.size > 0) {
            this.candidatesReviewedVisible = true
            mView.findViewById<View>(R.id.candidatesReviewedGroup).visibility = View.VISIBLE
        }


        this.candidatesDiscardedAdapter = MACandidatesAdapter(MARequestCandidate.getWithDatesTitles(candidatesDiscardedItems), context)
        this.candidatesDiscardedAdapter?.setListener(this)
        this.candidatesDiscardedAdapter?.setNotInterested(true)
        this.candidatesDiscarded?.setAdapter(candidatesDiscardedAdapter)
        if (candidatesDiscardedItems.size > 0) {
            this.candidatesDiscardedVisible = true
            mView.findViewById<View>(R.id.candidatesDiscardedGroup).visibility = View.VISIBLE
        }
    }

    /** Implementations **/

    override fun onDetail(item: MARequestCandidate?) {

        val candidateSingleFragment = MARequestSingleCandidateSingleFragment()
        candidateSingleFragment.profile = profile
        candidateSingleFragment.candidate = item
        candidateSingleFragment.listener = this
        this.performSegue(candidateSingleFragment)
    }

    override fun onDiscardCandidate() {

        val notification = Notification(mContext, R.layout.notification_light)
        notification.setMessage(mContext.getString(R.string.recruitment_request_notification_candidate_reviewed))
        notification.show(5000)
    }
}

open interface MARequestSingleCandidateSingleListener {
    fun onDiscardCandidate()
}