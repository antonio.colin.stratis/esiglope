package com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Control.ScreenView;
import com.stratis.estaffing.Control.eStaffingFragment;
import com.stratis.estaffing.Controller.RecruitmentFlow.RequestVacancyLocationController;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.Model.RecruitmentFlow.LocationPlace;
import com.stratis.estaffing.R;

import java.util.Calendar;
import java.util.List;

public class RequestVacancyModalityFragment extends eStaffingFragment implements View.OnClickListener {

    private  RelativeLayout wireBox;
    private  RelativeLayout wirelessBox;
    private  RelativeLayout mouseBox;
    private  RelativeLayout calendarBox;

    private Boolean wireSelected = false;
    private Boolean wirelessSelected = false;
    private Boolean mouseSelected = false;

    private DatePickerDialog picker = null;

    private RelativeLayout timeAssignationInput;
    private EditText timeAssignationInputEdit;
    private Button timeAssignationUndefined;
    private Boolean timeAssignationUndefinedBool = null;

    private TextView labelLocation;
    private RelativeLayout rlLocation;
    private TextView inLocation;
    private RelativeLayout locationSearchBox;
    private ImageView locationSearchBoxIcon;

    public RequestVacancyModalityFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        this.mView = inflater.inflate(R.layout.fragment_request_vacancy_modality, container, false);
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName());
        this.initViews();

        // --
        this.canActivateFooterButton();

        // --
        return this.mView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 4152) {
            setProfile((Profile) data.getSerializableExtra("profile"));
            setLocationView();
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        this.checkProfile();
    }

    @SuppressLint("SetTextI18n")
    private void initViews() {

        // --
        this.wireBox = this.mView.findViewById(R.id.wireBox);
        ((TextView) this.wireBox.findViewById(R.id.title)).setText(getString(R.string.request_vacancy_modality_onsite));
        this.wireBox.setTag("11");
        this.wireBox.setOnClickListener(this);

        this.wirelessBox = this.mView.findViewById(R.id.wirelessBox);
        ((TextView) this.wirelessBox.findViewById(R.id.title)).setText(getString(R.string.request_vacancy_modality_remote));
        this.wirelessBox.setTag("12");
        this.wirelessBox.setOnClickListener(this);

        this.mouseBox = this.mView.findViewById(R.id.mouseBox);
        ((TextView) this.mouseBox.findViewById(R.id.title)).setText(getString(R.string.request_vacancy_modality_hybrid));
        this.mouseBox.setTag("13");
        this.mouseBox.setOnClickListener(this);

        // --
        this.checkButtonsState();

        // --
        this.initCalendarButton();

        // --
        this.initTimeAssignationButtons();

        // --
        this.initLocationView();
    }

    private void initLocationView() {

        this.labelLocation = findView(R.id.locationText);
        this.rlLocation = findView(R.id.locationSearchBox);
        this.locationSearchBoxIcon = findView(R.id.locationSearchBoxIcon);
        inLocation = findView(R.id.locationSearchBoxLabel);
        locationSearchBox = findView(R.id.locationSearchBox);
        locationSearchBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(getActivity(), RequestVacancyLocationController.class);
                intent.putExtra("profile", getProfile());
                startActivityForResult(intent, 1234);

            }
        });

    }

    private void checkProfile() {

        if (this.getProfile() != null) {

            if (!this.getProfile().getModalityId().equals("null")) {
                Log.d("profile", this.getProfile().getModalityId());
                this.turnOffModalities();
                switch (this.getProfile().getModalityId()) {

                    case "1000100001":
                        this.wireBox.performClick();
                        break;

                    case "1000100002":
                        this.wirelessBox.performClick();
                        break;

                    case "1000100003":
                        this.mouseBox.performClick();
                        break;
                }

            }

            if ((this.getProfile().getJobLocation() != null && !App.isEmpty(this.getProfile().getJobLocation().getName()))
                || !App.isEmpty(this.getProfile().getRemoteJobCountries())) {
                setLocationView();
            }

            if (!App.isEmpty(this.getProfile().getVacancyDate())) {
                this.setDateLabels(getProfile().getVacancyDate());
            }

            if (this.getProfile().getHiringTime() != null) {

                if (this.getProfile().getHiringTime() == 0) {
                    this.changeAssignButtonState(false);

                } else {

                    this.changeAssignButtonState(true);
                    timeAssignationInputEdit.setText(String.valueOf(this.getProfile().getHiringTime()));
                    timeAssignationInputEdit.setFocusableInTouchMode(true);
                    timeAssignationInputEdit.setFocusable(true);
                }
            }
        }
    }

    private void checkButtonsState() {

        // -- Turn off all
        this.changeStateButton(this.wireBox, this.wireSelected, "wire_mouse");
        this.changeStateButton(this.wirelessBox, this.wirelessSelected, "wireless_mouse");
        this.changeStateButton(this.mouseBox, this.mouseSelected, "mouse");
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeStateButton(RelativeLayout box, Boolean state, String iconName) {

        if (state) {

            box.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(box);

            ((ImageView) box.findViewById(R.id.icon)).setImageBitmap( App.getImage(mContext, iconName + "_white") );
            ((TextView) box.findViewById(R.id.title)).setTextColor(  ContextCompat.getColor(mContext, R.color.white) );

        } else {

            box.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            ((ImageView) box.findViewById(R.id.icon)).setImageBitmap( App.getImage(mContext, iconName) );
            ((TextView) box.findViewById(R.id.title)).setTextColor(  ContextCompat.getColor(mContext, R.color.recruitment_request_vacancy_detail_unselected_text_button) );
        }
    }

    private void initCalendarButton() {

        this.calendarBox = this.mView.findViewById(R.id.calendarBox);
        this.calendarBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (picker == null) {

                    final Calendar sCalendar = Calendar.getInstance();
                    final ScreenView screenView = ScreenView.getInstance(getContext());
                    picker = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {

                        month = month + 1;
                        String date = String.format("%s/%s/%s", dayOfMonth,  month, year);
                        //Log.d("DXGOP", "MES SELECCIONADO ::; " + date);

                        // -- Set data lables in format and saving
                        getProfile().setVacancyDate(date);
                        setDateLabels(date);
                        //setDateLabels( App.parseDate(dt) );
                        // --
                        final Notification notification = new Notification(getContext(), R.layout.notification_light);
                        notification.setMessage(getContext().getString(R.string.request_vacancy_modality_notification_text));
                        notification.show(1500);

                    }, sCalendar.get(Calendar.YEAR), sCalendar.get(Calendar.MONTH), sCalendar.get(Calendar.DAY_OF_MONTH));
                    picker.getWindow().setLayout( screenView.getX() - screenView.threeRuleX(5), screenView.getY()/5);
                    picker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                }
                picker.show();
            }
        });
    }

    private void setLocationView(int backDrawable, String text, int heigth, boolean secondary,
                                 int textColor, int fontStyle, int fontSize,
                                 int left, int top, int rigth, int bottom, String icon) {
        this.rlLocation.setBackground(ContextCompat.getDrawable(mContext, backDrawable));
        final float scale = mContext.getResources().getDisplayMetrics().density;
        this.rlLocation.getLayoutParams().height = (int) (heigth * scale + 0.5f);
        if(secondary)
            this.setViewAsSecondaryColor(this.rlLocation);

        this.inLocation.setText(text);
        this.inLocation.setTextColor(ContextCompat.getColor(mContext, textColor));
        Typeface fontFamily = ResourcesCompat.getFont(mContext, fontStyle);
        this.inLocation.setTypeface(fontFamily);
        this.inLocation.setTextSize(fontSize);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout
                .LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins((int) (left * scale + 0.5f), (int) (top * scale + 0.5f), (int) (rigth * scale + 0.5f), (int) (bottom * scale + 0.5f));
        this.inLocation.setLayoutParams(layoutParams);
        this.locationSearchBoxIcon.setImageBitmap(App.getImage(mContext, icon));
    }

    private void setLocationView() {

        removeLocationDetail();

        if(getProfile().getModalityId().equals("1000100002") && !App.isEmpty(getProfile().getRemoteJobCountries())) {
            setLocationView(R.drawable.request_vacancy_selected_button, getProfile().getRemoteJobCountries(), 70, true,
                    R.color.white, R.font.os_bold, 15, 20, 24, 19, 26, "white_pencil");
        } else if(!getProfile().getModalityId().equals("1000100002")
                && getProfile().getJobLocation() != null && !App.isEmpty(getProfile().getJobLocation().getName())) {
            setLocationView(R.drawable.request_vacancy_selected_button, getProfile().getJobLocation().getName(), 70, true,
                    R.color.white, R.font.os_bold, 15, 20, 24, 19, 26, "white_pencil");

            this.inLocation.setTextSize(14);
            final float scale = mContext.getResources().getDisplayMetrics().density;
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout
                    .LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins((int) (20 * scale + 0.5f), (int) (16 * scale + 0.5f), (int) (19 * scale + 0.5f), (int) (35 * scale + 0.5f));
            this.inLocation.setLayoutParams(layoutParams);

            TextView locationDetail = new TextView(mContext);
            locationDetail.setId(R.id.locationSearchBoxLabelDetail);
//                locationDetail.setText(getProfile().getAddress());
            locationDetail.setText(getProfile().getJobLocation().getDescription());
            locationDetail.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            locationDetail.setTextSize(16);
            Typeface fontFamily = ResourcesCompat.getFont(mContext, R.font.os_regular);
            locationDetail.setTypeface(fontFamily);
            layoutParams = new RelativeLayout
                    .LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins((int) (20 * scale + 0.5f), (int) (34 * scale + 0.5f), (int) (91 * scale + 0.5f), (int) (15 * scale + 0.5f));
            locationDetail.setLayoutParams(layoutParams);
            this.rlLocation.addView(locationDetail);
        }

        // ONM : Esto no funciono
        ((TextView) this.calendarBox.findViewById(R.id.calendarBoxText)).requestFocus();
    }

    private void removeLocationDetail() {
        TextView locationDetail = mView.findViewById(R.id.locationSearchBoxLabelDetail);
        if(locationDetail != null) {
            this.rlLocation.removeView(locationDetail);
        }
    }

    private void setDateLabels(String date) {

        if (!date.equals("")) {

            date = App.getDateFormatted(date, "dd 'de' MMMM yyyy", "dd/M/yyyy");
            this.calendarBox.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.calendarBox);

            ((TextView) this.calendarBox.findViewById(R.id.calendarBoxText)).setTextColor(  ContextCompat.getColor(mContext, R.color.white) );
            ((TextView) this.calendarBox.findViewById(R.id.calendarBoxText)).setText( date );
            ((ImageView) this.calendarBox.findViewById(R.id.calendarBoxIcon)).setImageBitmap( App.getImage(mContext, "time_white") );

        } else {

            this.calendarBox.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            ((TextView) this.calendarBox.findViewById(R.id.calendarBoxText)).setTextColor(  ContextCompat.getColor(mContext, R.color.recruitment_request_vacancy_modality_gray) );
            ((TextView) this.calendarBox.findViewById(R.id.calendarBoxText)).setText("");
            ((ImageView) this.calendarBox.findViewById(R.id.calendarBoxIcon)).setImageBitmap( App.getImage(mContext, "time_gray") );
        }
        // --
        this.canActivateFooterButton();
    }

    // --
    private void initTimeAssignationButtons() {

        //  --
        this.timeAssignationInput = this.mView.findViewById(R.id.timeAssignationInput);
        this.timeAssignationInput.setOnClickListener(v -> {

            changeAssignButtonState(true);
            timeAssignationInputEdit.setFocusableInTouchMode(true);
            timeAssignationInputEdit.setFocusable(true);
            timeAssignationInputEdit.requestFocus();
        });

        // --
        this.timeAssignationUndefined = this.mView.findViewById(R.id.timeAssignationUndefined);
        this.timeAssignationUndefined.setOnClickListener(v -> {
            changeAssignButtonState(false);
        });

        this.timeAssignationInputEdit = this.timeAssignationInput.findViewById(R.id.timeAssignationInputEdit);
        this.timeAssignationInputEdit.setFocusable(false);
        this.timeAssignationInputEdit.setOnClickListener(v -> {

            changeAssignButtonState(true);
            timeAssignationInputEdit.setFocusableInTouchMode(true);
            timeAssignationInputEdit.setFocusable(true);
            timeAssignationInputEdit.requestFocus();
        });
        this.timeAssignationInputEdit.setOnEditorActionListener((v, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_DONE || event != null
                    && event.getAction() == KeyEvent.ACTION_DOWN
                    && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                if (event == null || !event.isShiftPressed()) {

                    InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(timeAssignationInputEdit.getApplicationWindowToken(), 0);

                    final String value = String.valueOf(timeAssignationInputEdit.getText());
                    if (!value.equals("")) {
                        getProfile().setHiringTime( Integer.valueOf(value) );
                    }

                    timeAssignationInputEdit.setFocusable(false);
                    return true;
                }
            }
            return false;
        });

        this.timeAssignationInputEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                final String value = String.valueOf(s);
                if (!value.equals("")) {
                    getProfile().setHiringTime( Integer.valueOf(String.valueOf(value)) );
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeAssignButtonState(boolean state) {

        this.timeAssignationUndefinedBool = !state;
        if (state) {

            // --
            this.timeAssignationInput.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.timeAssignationInput);

            ((TextView) timeAssignationInput.findViewById(R.id.timeAssignationTitle)).setTextColor(Color.WHITE);
            ((TextView) timeAssignationInput.findViewById(R.id.timeAssignationTitleTwo)).setTextColor(Color.WHITE);
            ((ImageView) timeAssignationInput.findViewById(R.id.timeAssignationIcon)).setImageBitmap(
                    App.getImage(getContext(), "select_white_option")
            );

            // --
            this.timeAssignationUndefined.setBackground( ContextCompat.getDrawable(mContext, R.drawable.white_border_10) );
            this.timeAssignationUndefined.setTextColor(Color.parseColor("#001330"));
            this.timeAssignationUndefined.setCompoundDrawablesWithIntrinsicBounds(null, null, getContext().getResources().getDrawable( R.drawable.empty_circle ), null);

        } else {

            this.getProfile().setHiringTime(0);

            // --
            this.timeAssignationInput.setBackground( ContextCompat.getDrawable(mContext, R.drawable.white_border_10) );
            ((TextView) timeAssignationInput.findViewById(R.id.timeAssignationTitle)).setTextColor(Color.parseColor("#001330"));
            ((TextView) timeAssignationInput.findViewById(R.id.timeAssignationTitleTwo)).setTextColor(Color.parseColor("#001330"));
            ((ImageView) timeAssignationInput.findViewById(R.id.timeAssignationIcon)).setImageBitmap(
                    App.getImage(getContext(), "pencil_month")
            );

            // --
            this.timeAssignationUndefined.setBackground( ContextCompat.getDrawable(getContext(), R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.timeAssignationUndefined);

            this.timeAssignationUndefined.setTextColor(Color.WHITE);
            this.timeAssignationUndefined.setCompoundDrawablesWithIntrinsicBounds(null, null, getContext().getResources().getDrawable( R.drawable.select_white_option ), null);
        }
    }

    private void canActivateFooterButton() {

        /*((RecruitmentFlowController) this.getActivity()).disableContinueButton(false);

        // --
        if (!this.wireSelected && !this.wirelessSelected && !this.mouseSelected) {
            return;
        }

        //--
        String dateText = String.valueOf(((TextView) this.calendarBox.findViewById(R.id.calendarBoxText)).getText());
        if (dateText.equals("")) {
            return;
        }

        // --
        ((RecruitmentFlowController) this.getActivity()).disableContinueButton(true);*/
    }

    private void turnOffModalities() {

        this.wireSelected = false;
        this.wirelessSelected = false;
        this.mouseSelected = false;
    }

    // --
    @Override
    public void onClick(View v) {

        this.turnOffModalities();
        String tag = String.valueOf(v.getTag());

        switch(tag) {

            case "11":
                if(!getProfile().getModalityId().equals("1000100001") /*&& getProfile().getJobLocation() != null*/) {
                    this.setLocationView(R.drawable.component_search, "", 60, false,
                            R.color.hint_text_gray, R.font.os_regular, 16,
                            20, 19, 133, 19, "search");
                }
                this.wireSelected = true;
                getProfile().setModalityId("1000100001");
                labelLocation.setText(getString(R.string.request_vacancy_modality_location));
                inLocation.setHint(getText(R.string.request_vacancy_modality_location_searcher));
                labelLocation.setVisibility(View.VISIBLE);
                rlLocation.setVisibility(View.VISIBLE);
                getProfile().setJobLocation(null);
                break;

            case "12":
                if(!getProfile().getModalityId().equals("1000100002") /*&& !App.isEmpty(getProfile().getRemoteJobCountries())*/) {
                    this.setLocationView(R.drawable.component_search, "", 60, false,
                            R.color.hint_text_gray, R.font.os_regular, 16,
                            20, 19, 133, 19, "search");
                }
                this.wirelessSelected = true;
                getProfile().setModalityId("1000100002");
                labelLocation.setText(getString(R.string.request_vacancy_modality_location_remote));
                inLocation.setHint(getText(R.string.request_vacancy_modality_location_searcher_remote));
                labelLocation.setVisibility(View.VISIBLE);
                rlLocation.setVisibility(View.VISIBLE);
                getProfile().setJobLocation(null);
                break;

            case "13":
                if(!getProfile().getModalityId().equals("1000100003") /*&& getProfile().getJobLocation() != null*/) {
                    this.setLocationView(R.drawable.component_search, "", 60, false,
                            R.color.hint_text_gray, R.font.os_regular, 16,
                            20, 19, 133, 19, "search");
                }
                this.mouseSelected = true;
                getProfile().setModalityId("1000100003");
                labelLocation.setText(getString(R.string.request_vacancy_modality_location));
                inLocation.setHint(getText(R.string.request_vacancy_modality_location_searcher));
                labelLocation.setVisibility(View.VISIBLE);
                rlLocation.setVisibility(View.VISIBLE);
                getProfile().setJobLocation(null);
                List<LocationPlace> newPlaces = getProfile().getLocationPlaces();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    newPlaces.forEach(locationPlace -> {
                        locationPlace.setSelected(false);
                    });
                    getProfile().setLocationPlaces(newPlaces);
                }
                break;
        }
        // --
        this.checkButtonsState();
        this.canActivateFooterButton();
    }

    // -- Abstract methods
    @Override
    public void onFooterContinueButtonClick() {

        //--
        super.onFooterContinueButtonClick();
        if (this.timeAssignationUndefinedBool != null && !this.timeAssignationUndefinedBool) {
            final String value = String.valueOf(timeAssignationInputEdit.getText());
            if (!value.equals("")) {
                getProfile().setHiringTime( Integer.valueOf(value) );
            }
        }
    }
}