package com.stratis.estaffing.Fragment.TalentFlowV3.Feedback

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.TalentV3.FeedbackAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Control.Loader
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Model.TalentFlowV3.Feedback
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.Talent.TalentService
import java.util.ArrayList

class FeedbackDetailFragment : ABFragment(), View.OnClickListener {

    var talentId : String? = null
    var talentName: String? = null
    var rating: String?=null
    var feedbacks:ArrayList<Feedback>? = null

    private var loader : Loader? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mView = inflater.inflate(R.layout.fragment_talent_feedback_detail, container, false)

        setHeader("")
        loader = Loader(mContext)
        loader!!.show()
        initView()
        getFeedbacks()

        return mView

    }

    private fun initView() {
        setViewAsPrimaryColor(mView.findViewById(R.id.avgButton))
        val addFeedbackButton: Button = mView.findViewById(R.id.addFeedbackButton)
        setViewAsPrimaryColor(addFeedbackButton)
        addFeedbackButton.tag = "10"
        addFeedbackButton.setOnClickListener(this)

        if(this.rating!=null && !this.rating!!.equals("0.0")) {
            if(this.rating.toString().endsWith(".0") || this.rating.toString().endsWith(".00"))
                mView.findViewById<TextView>(R.id.rate_value).text = this.rating!!.substring(0, this.rating!!.lastIndexOf("."))
            else
                mView.findViewById<TextView>(R.id.rate_value).text = this.rating.toString()
        }
    }

    fun getFeedbacks(){
        TalentService.getFeedbacks( Session.get(mContext).dataSessionId, talentId, object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse?) {
                val res = JsonBuilder.stringToJsonArray(response!!.body)
                feedbacks = Feedback.parse(res)
                loader!!.cancel()
                setFeedbacks()
            }

            override fun onError(response: ReSTResponse?) {
                // --
                Log.d("DXGOP", "ERROR :: " + response!!.body)
                loader!!.cancel()
            }
        })
    }

    private fun setFeedbacks() {
        if(this.feedbacks?.size!! > 0) {
            mView.findViewById<TextView>(R.id.not_feedback).visibility = View.GONE
            this.mView.findViewById<RecyclerView>(R.id.multiRatedComponentReviewList).visibility = View.VISIBLE

            // -- Review List
            val multiRatedComponentReviewList: RecyclerView = mView.findViewById(R.id.multiRatedComponentReviewList)
            App.createRecyclerHorizontalList(multiRatedComponentReviewList, context)

            var feedbackTop5: List<Feedback> =
                this.feedbacks?.let { ArrayList(it).sortedWith(compareBy({it.date})).asReversed() } as List<Feedback>
            if(feedbackTop5.size > 5) {
                feedbackTop5 = feedbackTop5.subList(0, 5)
            }
            val adapter = FeedbackAdapter(mContext, feedbackTop5)
            adapter.setListener(object : FeedbackAdapter.FeedbackAdapterListener{
                override fun onFeedbackClick(feedback: Feedback) {
                    showDetail(feedback)
                }
            })
            multiRatedComponentReviewList.setAdapter(adapter)
        }
    }

    private fun showDetail(feedback: Feedback) {
        val detailFragment = FeedbackDetailItemFragment()
        detailFragment.feedback = feedback
        this.continueSegue(detailFragment)
    }

    override fun onClick(v: View?) {
        val tag = v!!.tag.toString()
        when (tag) {
            "10" -> {
                val feedbackStarRateFragment = FeedbackStarRateFragment()
                feedbackStarRateFragment.talentId = this.talentId
                feedbackStarRateFragment.talentName = this.talentName
                this.continueSegue(feedbackStarRateFragment)
            }
        }
    }
}