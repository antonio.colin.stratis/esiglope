package com.stratis.estaffing.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Modal.CertificateModal;
import com.stratis.estaffing.Model.Course;
import com.stratis.estaffing.Model.Language;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 18/06/20
 */

public class CourseAdapterDeprecieted extends ArrayAdapter<Course> {

    public CourseClickListener listener = null;
    public CourseViewHolder clickedView = null;

    public CourseAdapterDeprecieted(Context context, ArrayList<Course> courses) {
        super(context, R.layout.fragment_talent_courses_item, courses);
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final Course course = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(getContext());

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.fragment_talent_courses_item, parent, false);
            final CourseViewHolder holder = new CourseViewHolder(convertView);
            convertView.setTag(holder);

            // -- Logic for views
            //holder.getCourseInitials().setImageDrawable( course.getInitials(getContext().getColor(R.color.colorPrimary), getContext()) );
            holder.getCourseTitle().setText( course.getName() );
            holder.getCourseEndDate().setText( "Concluido el: " + App.getDateCourse(course.getDate()) );

            holder.getSchoolView().setText( course.getSchool() );
            holder.getValidityView().setText( course.getValidityDate() );
            holder.getCertificateView().setText( course.getCertificateNumber() );
            holder.getCourseDescription().setText( course.getDescription() );

            holder.getArrow().setEnabled(false);
            holder.getArrow().setClickable(false);

            holder.getCourseMoreInfo().setVisibility(View.VISIBLE);

            // -- Cert Image
            holder.getShowCertificateButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final CertificateModal modal = new CertificateModal(getContext());
                    modal.setCourse(course);
                    modal.show();
                }
            });

            // -- All View Click
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // --
                    if (clickedView != null && clickedView != holder) {

                        clickedView.getCourseMoreInfo().setVisibility(View.GONE);
                        clickedView.getArrow().setRotation(0);
                    }
                    clickedView = holder;

                    // --
                    if (listener != null) {
                        listener.onCourseClick();
                    }

                    // --
                    if (holder.getCourseMoreInfo().getVisibility() == View.GONE) {

                        holder.getCourseMoreInfo().setVisibility(View.VISIBLE);
                        holder.getArrow().setRotation(180);

                    } else {

                        holder.getCourseMoreInfo().setVisibility(View.GONE);
                        holder.getArrow().setRotation(0);
                    }
                }
            });

        } else {

            final CourseViewHolder holder = (CourseViewHolder) convertView.getTag();
            convertView.setTag(holder);
        }

        // --
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setListener(CourseClickListener listener) {
        this.listener = listener;
    }

    public class CourseViewHolder {

        private View row;
        private ImageView courseInitials = null;
        private TextView courseTitle = null;
        private AutoResizeTextView courseEndDate = null;
        private ImageButton arrow = null;

        private RelativeLayout courseMoreInfo = null;
        private AutoResizeTextView schoolView = null;
        private AutoResizeTextView validityView = null;
        private AutoResizeTextView certificateView = null;
        private TextView course_description = null;
        private Button show_certificate_button = null;

        public CourseViewHolder(View row) {
            this.row = row;
        }

        public ImageView getCourseInitials() {

            if (this.courseInitials == null) {
                this.courseInitials = row.findViewById(R.id.courseInitials);
            }
            return this.courseInitials;
        }

        public TextView getCourseTitle() {

            if (this.courseTitle == null) {
                this.courseTitle = row.findViewById(R.id.courseTitle);
            }
            return this.courseTitle;
        }

        public AutoResizeTextView getCourseEndDate() {

            if (this.courseEndDate == null) {
                this.courseEndDate = row.findViewById(R.id.courseEndDate);
            }
            return this.courseEndDate;
        }

        public ImageButton getArrow() {

            if (this.arrow == null) {
                this.arrow = row.findViewById(R.id.arrow);
            }
            return this.arrow;
        }

        public RelativeLayout getCourseMoreInfo() {

            if (this.courseMoreInfo == null) {
                this.courseMoreInfo = row.findViewById(R.id.courseMoreInfo);
            }
            return this.courseMoreInfo;
        }

        public AutoResizeTextView getSchoolView() {

            if (this.schoolView == null) {
                this.schoolView = row.findViewById(R.id.schoolView);
            }
            return this.schoolView;
        }

        public AutoResizeTextView getValidityView() {

            if (this.validityView == null) {
                this.validityView = row.findViewById(R.id.validityView);
            }
            return this.validityView;
        }

        public AutoResizeTextView getCertificateView() {

            if (this.certificateView == null) {
                this.certificateView = row.findViewById(R.id.certificateView);
            }
            return this.certificateView;
        }

        public TextView getCourseDescription() {

            if (this.course_description == null) {
                this.course_description = row.findViewById(R.id.course_description);
            }
            return this.course_description;
        }

        public Button getShowCertificateButton() {

            if (this.show_certificate_button == null) {
                this.show_certificate_button = row.findViewById(R.id.show_certificate_button);
            }
            return this.show_certificate_button;
        }
    }

    public interface CourseClickListener {
        public void onCourseClick();
    }
}
