package com.stratis.estaffing.Core;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKey;
import androidx.security.crypto.MasterKeys;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

/**
 * Created by Erick Sanchez
 * Revision 1 - 20/01/21
 */

public class Configurations {

    private SharedPreferences prefs;

    public Configurations(final Context context) {

        try {

            MasterKey masterKey = new MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
                    .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                    .build();

            this.prefs = EncryptedSharedPreferences.create(
                    context,
                    context.getPackageName() + "_secret_shared_prefs",
                    masterKey,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            );

        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    public String get(String key) {
        return this.prefs.getString(key, "");
    }

    @SuppressLint("ApplySharedPref")
    public void add(String key, String value) {

        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    @SuppressLint("ApplySharedPref")
    public void remove(String key) {

        if (exists(key)) {
            SharedPreferences.Editor editor = this.prefs.edit();
            editor.remove(key);
            editor.commit();
        }
    }

    public boolean exists(String preference) {
        return this.prefs.contains(preference);
    }

    public void print() {

        Map<String,?> keys = this.prefs.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            Log.d("DXGOP", entry.getKey() + ": " + entry.getValue().toString());
        }
    }

    public void removePreferences() {

        Map<String,?> keys = this.prefs.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            this.remove(entry.getKey());
        }
    }
}
