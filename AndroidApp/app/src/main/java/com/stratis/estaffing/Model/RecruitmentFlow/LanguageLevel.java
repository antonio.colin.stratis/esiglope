package com.stratis.estaffing.Model.RecruitmentFlow;

import android.content.Context;

import java.util.ArrayList;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class LanguageLevel {

    private String name = "";
    private LanguageCatalog.LanguageCatalogLevel level = LanguageCatalog.LanguageCatalogLevel.basic;

    public LanguageLevel() {}

    public LanguageLevel(String name) {
        this.name = name;
    }

    public static ArrayList<LanguageLevel> getBase(Context context) {

        // --
        ArrayList<LanguageLevel> levels = new ArrayList<>();

        // 1
        final LanguageLevel basic = new LanguageLevel(context.getString(R.string.modal_language_level_basic));
        levels.add(basic);

        // 2
        final LanguageLevel medium = new LanguageLevel(context.getString(R.string.modal_language_level_medium));
        medium.level = LanguageCatalog.LanguageCatalogLevel.medium;
        levels.add(medium);

        // 3
        final LanguageLevel advance = new LanguageLevel(context.getString(R.string.modal_language_level_advanced));
        advance.level  = LanguageCatalog.LanguageCatalogLevel.advance;
        levels.add(advance);

        // --
        return levels;
    }

    public String getName() {
        return name;
    }

    public LanguageCatalog.LanguageCatalogLevel getLevelType() {
        return this.level;
    }

    public String getLevel(Context context) {

        if (this.level ==  LanguageCatalog.LanguageCatalogLevel.basic) {
            return context.getString(R.string.modal_language_level_basic);
        } else if (this.level == LanguageCatalog.LanguageCatalogLevel.medium) {
            return context.getString(R.string.modal_language_level_medium);
        } else {
            return context.getString(R.string.modal_language_level_advanced);
        }
    }
}
