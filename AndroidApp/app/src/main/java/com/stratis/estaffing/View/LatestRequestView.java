package com.stratis.estaffing.View;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 21/08/20
 */

public class LatestRequestView extends RecyclerView.ViewHolder {

    private ImageView image;
    private TextView vacancyName;
    private TextView seniority;
    private TextView date;

    public LatestRequestView(View itemView) {

        super(itemView);
        this.image = itemView.findViewById(R.id.icon);
        this.vacancyName = itemView.findViewById(R.id.title);
        this.seniority = itemView.findViewById(R.id.sub);
        this.date = itemView.findViewById(R.id.date);
    }

    // --

    public ImageView getImage() {
        return image;
    }

    public TextView getVacancyName() {
        return vacancyName;
    }

    public TextView getSeniority() {
        return seniority;
    }

    public TextView getDate() {
        return date;
    }
}
