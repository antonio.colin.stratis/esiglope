package com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate;

import android.content.Context;

import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 18/12/20
 *
 */

public class CandidateMenuModel {

    private Integer id = 0;
    private String icon = "";
    private String title = "";

    public CandidateMenuModel() {}

    public static ArrayList<CandidateMenuModel> getMenu(Context context) {

        // --
        ArrayList<CandidateMenuModel> items = new ArrayList<>();

        // -- Chat
        final CandidateMenuModel chat = new CandidateMenuModel();
        chat.id = 1;
        chat.icon = "icon_chat_two";
        chat.title = context.getString(R.string.recruitment_request_candidate_single_menu_chat);
        items.add(chat);

        // -- Download
        final CandidateMenuModel download = new CandidateMenuModel();
        download.id = 2;
        download.icon = "download_cv";
        download.title = context.getString(R.string.recruitment_request_candidate_single_menu_download);
        items.add(download);

        // -- Share
        final CandidateMenuModel share = new CandidateMenuModel();
        share.id = 3;
        share.icon = "share_cv";
        share.title = context.getString(R.string.recruitment_request_candidate_single_menu_share);
        items.add(share);


        // --
        return items;
    }

    public static ArrayList<CandidateMenuModel> getMenuTalent(Context context) {

        // --
        ArrayList<CandidateMenuModel> items = new ArrayList<>();

        // -- Download
        final CandidateMenuModel download = new CandidateMenuModel();
        download.id = 1;
        download.icon = "download_cv";
        download.title = context.getString(R.string.recruitment_request_candidate_single_menu_download);
        items.add(download);

        // -- Share
        final CandidateMenuModel share = new CandidateMenuModel();
        share.id = 2;
        share.icon = "share_cv";
        share.title = context.getString(R.string.recruitment_request_candidate_single_menu_share);
        items.add(share);


        // --
        return items;
    }

    public int getId() {
        return id;
    }

    public String getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }
}