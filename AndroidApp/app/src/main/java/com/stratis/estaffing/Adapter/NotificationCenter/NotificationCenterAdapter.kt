package com.stratis.estaffing.Adapter.NotificationCenter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.NotificationCenterFlow.NotificationCenterModel
import com.stratis.estaffing.R
import com.stratis.estaffing.View.NotificationCenter.NotificationCenterView
import java.util.*

/**
 * Created by Erick Sanchez
 * Revision 1 - 24/05/21
 */
open class NotificationCenterAdapter(context: Context?, items: ArrayList<NotificationCenterModel>?) :
        RecyclerView.Adapter<NotificationCenterView>() {

    private var items: ArrayList<NotificationCenterModel>? = null
    private var original: ArrayList<NotificationCenterModel>? = null

    var listener: NotificationCenterListener? = null
    private var context: Context? = null
    private var filtered = false
    public var forceAllViewed = false

    init {

        this.items = items
        this.original = items
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationCenterView {

        val layoutView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.activity_notification_center_controller_item, parent, false)
        return NotificationCenterView(layoutView)
    }

    override fun onBindViewHolder(holder: NotificationCenterView, position: Int) {

        holder.setIsRecyclable(false)
        val notification :NotificationCenterModel = this.items!![position]

        // --
        holder.title?.text = notification.title
        holder.content?.text = notification.message
        holder.dateTime?.text = App.getTimeAgo(notification.createdDate)

        // -- Background Icon
        when(notification.priorityId) {

            NotificationCenterModel.notificationPriorityDown -> {
                holder.iconContainer?.background = ContextCompat.getDrawable(context!!, R.drawable.low_priority)
            }

            NotificationCenterModel.notificationPriorityMedium -> {
                holder.iconContainer?.background = ContextCompat.getDrawable(context!!, R.drawable.medium_priority)
            }

            NotificationCenterModel.notificationPriorityHigh -> {
                holder.iconContainer?.background = ContextCompat.getDrawable(context!!, R.drawable.red_priority)
            }
        }

        // -- Icon
        when(notification.iconName) {

            "icon_01" -> { holder.icon?.setImageBitmap(App.getImage(context, "notification_${notification.iconName}")) }
            "icon_02" -> { holder.icon?.setImageBitmap(App.getImage(context, "notification_${notification.iconName}")) }
            "icon_03" -> { holder.icon?.setImageBitmap(App.getImage(context, "notification_${notification.iconName}")) }
            "icon_04" -> { holder.icon?.setImageBitmap(App.getImage(context, "notification_${notification.iconName}")) }
            "icon_05" -> { holder.icon?.setImageBitmap(App.getImage(context, "notification_${notification.iconName}")) }
            "icon_06" -> { holder.icon?.setImageBitmap(App.getImage(context, "notification_${notification.iconName}")) }
            "icon_07" -> { holder.icon?.setImageBitmap(App.getImage(context, "notification_${notification.iconName}")) }
            "icon_08" -> { holder.icon?.setImageBitmap(App.getImage(context, "notification_${notification.iconName}")) }
            "icon_09" -> { holder.icon?.setImageBitmap(App.getImage(context, "notification_${notification.iconName}")) }
            "icon_10" -> { holder.icon?.setImageBitmap(App.getImage(context, "notification_${notification.iconName}")) }
        }

        // -- Click
        holder.shape?.setOnClickListener {

            if (this.listener != null) {

                this.items!![position].viewed = true
                this.notifyDataSetChanged()
                this.listener?.onNotificationClick(notification)
            }
        }

        // -- Opacity
        if (notification.viewed || this.forceAllViewed) {
            holder.shape?.alpha = 0.65f
        }
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    open fun reload(items: ArrayList<NotificationCenterModel>?) {

        this.items = items
        notifyItemRangeInserted(0, this.items!!.size - 1)
    }

    open fun clear() {

        val size = items!!.size
        if (size > 0) {
            for (i in 0 until size) {
                items!!.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    open fun filterByCategoryId(categoryId:String) {

        var filterName = ""
        this.removeFilters()
        this.items = ArrayList<NotificationCenterModel>()
        for (item in original!!) {
            if (item.categoryId == categoryId ) {
                items!!.add(item)
                filterName = item.categoryName
            }
        }
        this.onFilter(filterName)
    }

    open fun filterByNotSeen() {

        var filterName = ""
        this.removeFilters()
        this.items = ArrayList<NotificationCenterModel>()
        for (item in original!!) {
            if (!item.viewed) {
                items!!.add(item)
                filterName = item.categoryName
            }
        }
        this.onFilter(filterName)
    }

    private fun onFilter(filterName:String) {

        this.filtered = true
        notifyDataSetChanged()
        if (this.listener != null && this.items!!.size <= 0) {
            this.listener?.onEmptyResultsAtFilter(filterName)
        } else {
            this.listener?.onFilterResults()
        }
    }

    open fun removeFilters() {

        if (this.filtered) {
            items = original
            this.filtered = false
            notifyDataSetChanged()
        }
    }

    interface NotificationCenterListener {

        fun onNotificationClick(notification:NotificationCenterModel)
        fun onEmptyResultsAtFilter(filterName:String)
        fun onFilterResults()
    }
}