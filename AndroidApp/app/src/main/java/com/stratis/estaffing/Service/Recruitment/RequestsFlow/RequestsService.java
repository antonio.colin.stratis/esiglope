package com.stratis.estaffing.Service.Recruitment.RequestsFlow;

import android.util.Log;

import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Core.eStaffingClient;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Erick Sanchez
 * Revision 1 - 17/09/20
 */

public class RequestsService {

    public static void getRequests(final Session session, final ReSTCallback listener) {
        RequestsService.getRequests(session.getDataSessionId(), null, listener);
    }

    public static void getRequests(final String userId, final String masterAdminId, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/request/" + userId + "/resume");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        if (masterAdminId != null) {
            request.addParameter("masterAdminId", masterAdminId);
        }

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }


    public static void getRequest(String id, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/request/" + id + "/detail");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void pauseRequest(String requestId, String cause, final Session session, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/request/setStatus");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_PUT, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("idStatusStaffing", LatestVacancyRequest.statusPaused);
            body.put("idUser", session.getDataSessionId());
            body.put("vacancyId", requestId);
            body.put("cause", cause);
            body.put("email", session.getUsername());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "STATUS SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void cancelRequest(String requestId, String cause, final Session session, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/request/setStatus");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_PUT, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("idStatusStaffing", LatestVacancyRequest.statusCancel);
            body.put("idUser", session.getDataSessionId());
            body.put("vacancyId", requestId);
            body.put("cause", cause);
            body.put("email", session.getUsername());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "STATUS SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void restartRequest(String requestId, final Session session, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/request/restart");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("idUser", session.getDataSessionId());
            body.put("vacancyId", requestId);
            body.put("cause", "Reanudar solicitud");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "STATUS SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getCandidates(String requestId, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/candidate/" + requestId + "/find");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getCandidate(String requestId, String candidateId ,final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/candidate/" + requestId + "/" + candidateId + "/resume");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }
}
