package com.stratis.estaffing.Adapter.TalentV3

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView.OnEditorActionListener
import androidx.core.view.isGone
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Model.TalentFlowV3.FeedbackDetail
import com.stratis.estaffing.R
import com.stratis.estaffing.View.Talent.FeedbackSkillView

open class FeedbackSkillAdapter(context: Context, items: MutableList<FeedbackDetail>?, selectedItems: MutableList<FeedbackDetail>) : RecyclerView.Adapter<FeedbackSkillView>() {

    private var items: MutableList<FeedbackDetail>? = null
    private var selectedItems: MutableList<FeedbackDetail>? = null
    private var context: Context? = null

    private var listener:FeedbackSkillAdapterListener? = null

    init {
        this.context = context
        this.items = items
        this.selectedItems = selectedItems
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackSkillView {
        val layoutView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.fragment_feedback_selection_item, parent, false)
        return FeedbackSkillView(layoutView)
    }

    @SuppressLint("SetTextI18n", "ClickableViewAccessibility")
    override fun onBindViewHolder(holder: FeedbackSkillView, position: Int) {
        holder.setIsRecyclable(false)
        val feedbackDetail : FeedbackDetail = this.items!![position]

        holder.skillTextContainer?.setOnClickListener { listener?.onSkillSelected(feedbackDetail) }

        if (selectedItems?.contains(feedbackDetail)!!) {
            holder.skillIconImageView?.setImageResource(R.drawable.checkbox_square_selected)
            holder.skillTextContainer?.setBackgroundResource(R.drawable.talent_feedback_skill_selected)
        } else {
            holder.skillIconImageView?.setImageResource(R.drawable.checkbox_square_unselected)
            holder.skillTextContainer?.setBackgroundResource(R.drawable.talent_feedback_skill_unselected)
        }

        if (feedbackDetail.skillTagId == 0L && feedbackDetail.name == "") {
            holder.skillEditText?.isGone = false
            holder.skillTextView?.isGone = true

            holder.skillEditText?.requestFocus()
            holder.skillEditText?.showSoftKeyboard()
            holder.skillEditText?.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    holder.skillEditText?.clearFocus()
                    listener?.onAddSkill(holder.skillEditText?.text.toString())
                }
                false
            })
        } else {
            holder.skillEditText?.isGone = true
            holder.skillTextView?.isGone = false

            holder.skillTextView?.text = feedbackDetail.name
        }

        holder.newSkillIconImageView?.isGone = feedbackDetail.skillTagId != 0L
    }

    override fun getItemViewType(position: Int): Int {
        val feedbackDetail : FeedbackDetail = this.items!![position]
        return feedbackDetail.skillTagId.toInt()
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    fun setListener(listener: FeedbackSkillAdapterListener) {
        this.listener = listener
    }

    interface FeedbackSkillAdapterListener {
        fun onSkillSelected(feedback: FeedbackDetail)

        fun onAddSkill(name: String)
    }

    fun EditText.showSoftKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0)
    }

}

