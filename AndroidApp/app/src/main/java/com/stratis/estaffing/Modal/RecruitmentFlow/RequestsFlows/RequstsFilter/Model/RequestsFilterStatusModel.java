package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.Model;

import android.content.Context;
import android.graphics.Bitmap;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 09/12/20
 */

public class RequestsFilterStatusModel {

    private String id = "";
    private String name = "";
    private String icon = "";

    public static ArrayList<RequestsFilterStatusModel> getStatusFilter(Context context) {

        ArrayList<RequestsFilterStatusModel> filters = new ArrayList<>();

        //  -- All
        final RequestsFilterStatusModel allFilter = new RequestsFilterStatusModel();
        allFilter.setId("0");
        allFilter.setName(context.getString(R.string.recruitment_actions_requests_filter_all));
        allFilter.setIcon("icon_request_filter_all");
        filters.add(allFilter);

        //  -- All
        final RequestsFilterStatusModel activeFilter = new RequestsFilterStatusModel();
        activeFilter.setId(LatestVacancyRequest.statusConfirm);
        activeFilter.setName(context.getString(R.string.recruitment_actions_requests_filter_active));
        activeFilter.setIcon("icon_request_active");
        filters.add(activeFilter);

        //  -- All
        final RequestsFilterStatusModel pausedFilter = new RequestsFilterStatusModel();
        pausedFilter.setId(LatestVacancyRequest.statusPaused);
        pausedFilter.setName(context.getString(R.string.recruitment_actions_requests_filter_paused));
        pausedFilter.setIcon("icon_request_paused");
        filters.add(pausedFilter);

        //  -- All
        final RequestsFilterStatusModel cancelFilter = new RequestsFilterStatusModel();
        cancelFilter.setId(LatestVacancyRequest.statusCancel);
        cancelFilter.setName(context.getString(R.string.recruitment_actions_requests_filter_cancel));
        cancelFilter.setIcon("icon_request_cancel");
        filters.add(cancelFilter);

        //  -- All
        final RequestsFilterStatusModel doneFilter = new RequestsFilterStatusModel();
        doneFilter.setId(LatestVacancyRequest.statusDone);
        doneFilter.setName(context.getString(R.string.recruitment_actions_requests_filter_closed));
        doneFilter.setIcon("icon_request_done");
        filters.add(doneFilter);

        // --
        return filters;
    }

    public static ArrayList<RequestsFilterStatusModel> getDatesFilter(Context context) {

        ArrayList<RequestsFilterStatusModel> filters = new ArrayList<>();

        //  -- All
        final RequestsFilterStatusModel allFilter = new RequestsFilterStatusModel();
        allFilter.setId("10");
        allFilter.setName(context.getString(R.string.recruitment_actions_requests_filter_all_month));
        filters.add(allFilter);

        //  -- All
        final RequestsFilterStatusModel currentMonth = new RequestsFilterStatusModel();
        currentMonth.setId("11");
        currentMonth.setName(context.getString(R.string.recruitment_actions_requests_filter_current_month));
        filters.add(currentMonth);

        //  -- All
        final RequestsFilterStatusModel twoMonthsFilter = new RequestsFilterStatusModel();
        twoMonthsFilter.setId("12");
        twoMonthsFilter.setName(context.getString(R.string.recruitment_actions_requests_filter_2_month_ago));
        filters.add(twoMonthsFilter);

        // --
        return filters;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return this.icon;
    }

    public Bitmap getIcon(Context context) {
        return App.getImage(context, this.icon);
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}