package com.stratis.estaffing.Control.Searcher.Filter;

import android.widget.Filter;

import com.stratis.estaffing.Control.Searcher.Adapter.SearcherSkillAdapter;
import com.stratis.estaffing.Model.RecruitmentFlow.SkillCatalog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erick Sanchez - App Builders CTO
 * Revision 1 - 01/09/20
 */

public class SkillSearchFilter extends Filter {

    SearcherSkillAdapter adapter;

    public SkillSearchFilter(SearcherSkillAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

        FilterResults results = new FilterResults();
        // We implement here the filter logic
        if (constraint == null || constraint.length() == 0) {

            // No filter implemented we return all the list
            results.values = this.adapter.getOriginal();
            results.count = this.adapter.getOriginal().size();

        } else {

            // We perform filtering operation
            List<SkillCatalog> nArrayList = new ArrayList<SkillCatalog>();
            if (nArrayList.size() > 1) {
                nArrayList.set(0, new SkillCatalog((String) constraint, true));
            } else {
                nArrayList.add(0, new SkillCatalog((String) constraint, true));
            }

            // --
            for (SkillCatalog item : this.adapter.getOriginal()) {

                if (item.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    if (nArrayList.size() < 4 && !this.adapter.getSelected().contains(item)) {
                        nArrayList.add(item);
                    }
                }

                if (item.getName().toLowerCase().equals(constraint.toString().toLowerCase())) {
                    nArrayList.remove(0);
                }
            }

            results.values = nArrayList;
            results.count = nArrayList.size();
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        // Now we have to inform the adapter about the new list filtered
        this.adapter.reload((ArrayList<SkillCatalog>) results.values);
        this.adapter.notifyDataSetChanged();
    }
}
