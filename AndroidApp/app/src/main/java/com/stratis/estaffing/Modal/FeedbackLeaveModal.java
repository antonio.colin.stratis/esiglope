package com.stratis.estaffing.Modal;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.R;

/**
 * Created by MVE
 * Revision 1 - 03/09/21
 */

public class FeedbackLeaveModal extends Dialog {

    private ImageView icon = null;
    private AutoResizeTextView title = null;
    private AutoResizeTextView content = null;
    private Button closeButton = null;
    private Button continueButton = null;

    private String msgWarning = "";

    private FeedbackLeaveModalListener listener;

    public FeedbackLeaveModal(Context context) {

        super(context);
        this.init();
    }

    public FeedbackLeaveModal(Context context, int themeResId) {

        super(context, themeResId);
        this.init();
    }

    protected FeedbackLeaveModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.init();
    }

    @SuppressLint("ResourceType")
    private void init() {

        // -- R styles
        this.setContentView(R.layout.modal_feedback_leave);
        this.setCanceledOnTouchOutside(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow()
                .setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );

        // --
        this.icon = this.findViewById(R.id.icon);
        this.title = this.findViewById(R.id.title);
        this.content = this.findViewById(R.id.content);
        this.closeButton = this.findViewById(R.id.closeButton);
        this.continueButton = this.findViewById(R.id.feedbackContinueButton);
        // --
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.backPressed();
                cancel();
            }
        });
        this.continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        App.setViewAsSecondaryColor(this.closeButton, getContext());
    }

    public void setListener(FeedbackLeaveModalListener listener) {
        this.listener = listener;
    }

    public interface FeedbackLeaveModalListener {
        void backPressed();
    }

}
