package com.stratis.estaffing.Model.RecruitmentFlow;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class SkillCatalog implements Serializable {

    private Integer id = 0;
    private String name = "";
    private String description = "";
    private Boolean isRequired = null;

    public SkillCatalog() {}

    public SkillCatalog(String name, Boolean isRequired) {

        this.id = 0;
        this.name = name;
        this.description = "";
        this.isRequired = isRequired;
    }

    public SkillCatalog(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optInt("id", 0);
            this.name = obj.optString("name", "");
            this.description =  obj.optString("description", "");
            this.isRequired = obj.optBoolean("isRequired", false);
        }
    }

    public static ArrayList<SkillCatalog> parse(JSONArray array) {

        if (array != null) {

            ArrayList<SkillCatalog> skills = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                skills.add(new SkillCatalog(obj));
            }
            return skills;
        }
        return null;
    }

    public static ArrayList<SkillCatalog> getBase() {

        // --
        ArrayList<SkillCatalog> skills = new ArrayList<>();

        // 1
        final SkillCatalog skillOne = new SkillCatalog();
        skills.add(skillOne);

        // --
        return skills;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getRequired() {
        return isRequired;
    }

    public void setRequired(Boolean required) {
        isRequired = required;
    }
}
