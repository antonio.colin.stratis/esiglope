package com.stratis.estaffing.Listener;

import org.json.JSONObject;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

interface RequestDone {

    public void onFinish(JSONObject obj);
    public void onError(JSONObject obj);
}