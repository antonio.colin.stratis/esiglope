package com.stratis.estaffing.Model.RecruitmentFlow;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class SoftwareCatalog implements Serializable {

    private Integer id = 0;
    private String name = "";
    private String observations = "";
    private Integer vacancyId = 0;

    public SoftwareCatalog() {}

    public SoftwareCatalog(String name) {

        this.id = 0;
        this.name = name;
        this.observations = "";
        this.vacancyId = 0;
    }

    public SoftwareCatalog(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optInt("id", 0);
            this.name = obj.optString("name", "");
            this.observations =  obj.optString("observations", "");
            this.vacancyId =  obj.optInt("vacancyId", 0);
        }
    }

    public static ArrayList<SoftwareCatalog> parse(JSONArray array) {

        if (array != null) {

            ArrayList<SoftwareCatalog> skills = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                skills.add(new SoftwareCatalog(obj));
            }
            return skills;
        }
        return null;
    }

    public static ArrayList<SoftwareCatalog> getBase() {

        // --
        ArrayList<SoftwareCatalog> softs = new ArrayList<>();

        // 1
        final SoftwareCatalog softOne = new SoftwareCatalog();
        softs.add(softOne);

        // --
        return softs;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public Integer getVacancyId() {
        return vacancyId;
    }
}
