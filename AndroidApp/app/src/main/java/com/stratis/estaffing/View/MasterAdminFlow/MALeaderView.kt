package com.stratis.estaffing.View.MasterAdminFlow

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.R

/**
 * Created by Erick Sanchez
 * Revision 1 - 24/05/21
 */
open class MALeaderView(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var shape:ConstraintLayout? = null
    var nameIcon:TextView? = null
    var name:TextView? = null
    var area:TextView? = null

    init {

        this.shape = itemView.findViewById(R.id.shape)
        this.nameIcon = itemView.findViewById(R.id.nameIcon)
        this.name = itemView.findViewById(R.id.name)
        this.area = itemView.findViewById(R.id.area)
    }
}