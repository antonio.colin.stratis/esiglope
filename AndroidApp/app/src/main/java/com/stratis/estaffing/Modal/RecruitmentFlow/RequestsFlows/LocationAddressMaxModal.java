package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.R;

import androidx.annotation.Nullable;

public class LocationAddressMaxModal  extends Dialog {

    private Button closeButton = null;

    public LocationAddressMaxModal(Context context) {
        super(context);
        this.init();
    }

    public LocationAddressMaxModal(Context context, int themeResId) {

        super(context, themeResId);
        this.init();
    }

    protected LocationAddressMaxModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.init();
    }

    @SuppressLint("ResourceType")
    private void init() {

        // -- R styles
        this.setContentView(R.layout.modal_location_address_max);
        this.setCanceledOnTouchOutside(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow()
                .setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );

        this.closeButton = this.findViewById(R.id.closeButton);
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        App.setViewAsSecondaryColor(this.closeButton, getContext());
    }

}
