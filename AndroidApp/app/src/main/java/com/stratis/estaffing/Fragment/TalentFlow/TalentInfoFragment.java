package com.stratis.estaffing.Fragment.TalentFlow;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stratis.estaffing.Adapter.LanguageAdapter;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.RatingStar.RatingStar;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.Talent;
import com.stratis.estaffing.R;

public class TalentInfoFragment extends ABFragment {

    public Talent talent = null;

    public TalentInfoFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // --
        this.mView = inflater.inflate(R.layout.fragment_talent_info, container, false);

        // -- Init views
        this.initViews();

        // --
        return this.mView;
    }

    private void initViews() {

        if (this.talent != null) {

            // -- Antiquity View
            final RelativeLayout antiquityView = this.mView.findViewById(R.id.antiquityView);
            final TextView antiquityTitleView = antiquityView.findViewById(R.id.title);
            antiquityTitleView.setText( getContext().getResources().getString(R.string.info_fragment_antiquity_title) );
            final TextView antiquityContentView = antiquityView.findViewById(R.id.content);
            final String antiquityString = String.format("%s\n(%s)", this.talent.getOldDate(), App.getDateFormatted(this.talent.getHireDate(), "yyyy-MM-dd HH:mm:ss") );
            antiquityContentView.setText(antiquityString);

            // -- Rating
            final RatingStar ratingStar = new RatingStar((LinearLayout) this.mView.findViewById(R.id.stars), getContext());
            ratingStar.setRating(this.talent.getRating());

            // -- Last Evaluation View
            final RelativeLayout lastEvaluation = this.mView.findViewById(R.id.lastEvaluation);
            final TextView lastEvaluationTitleView = lastEvaluation.findViewById(R.id.title);
            lastEvaluationTitleView.setText( getContext().getResources().getString(R.string.info_fragment_last_evaluation) );
            final TextView lastEvaluationContentView = lastEvaluation.findViewById(R.id.content);
            lastEvaluationContentView.setText( App.getDateMonthYear(this.talent.getLastEvaluation()) );

            // -- Boss View View
            final RelativeLayout bossView = this.mView.findViewById(R.id.bossView);
            final TextView bossTitleView = bossView.findViewById(R.id.title);
            bossTitleView.setText( getContext().getResources().getString(R.string.info_fragment_boss) );
            final TextView bossContentView = bossView.findViewById(R.id.content);
            bossContentView.setText( this.talent.getCurrentLeaderShort() );

            // -- Area View
            final RelativeLayout areaView = this.mView.findViewById(R.id.areaView);
            final TextView areaTitleView = areaView.findViewById(R.id.title);
            areaTitleView.setText( getContext().getResources().getString(R.string.info_fragment_area) );
            final TextView areaContentView = areaView.findViewById(R.id.content);
            areaContentView.setText( this.talent.getPositionArea() );

            // -- Time Period View
            final RelativeLayout timePeriodView = this.mView.findViewById(R.id.timePeriodView);
            final TextView timePeriodTitleView = timePeriodView.findViewById(R.id.title);
            timePeriodTitleView.setText( getContext().getResources().getString(R.string.info_fragment_assignation_period) );
            final TextView timePeriodContentView = timePeriodView.findViewById(R.id.content);
            timePeriodContentView.setText( this.talent.getAssignmentPeriod() );

            // -- Language Table
            if (this.talent.getLanguages() != null && this.talent.getLanguages().size() > 0) {
                this.initTable();
            }
        }
    }

    private void initTable() {

        // --
        final ListView languageList = this.mView.findViewById(R.id.languageList);

        // --
        final LanguageAdapter adapter = new LanguageAdapter(getContext(), this.talent.getLanguages());

        // --
        languageList.setAdapter(adapter);

        // --
        setListViewHeightBasedOnChildren(languageList);
    }

    // -- ListView modifier -- Issue fix
    public static void setListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);if (i == 0) view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        params.height = params.height + 50;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}