package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.View;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class RequestFilterStatusView extends RecyclerView.ViewHolder {

    private ConstraintLayout box;
    private TextView title;
    private ImageView icon;

    public RequestFilterStatusView(View itemView, boolean asDate) {

        // --
        super(itemView);

        // --
        this.box = itemView.findViewById(R.id.box);
        this.title = itemView.findViewById(R.id.title);
        if (!asDate) {
            this.icon = itemView.findViewById(R.id.icon);
        }

    }

    public ConstraintLayout getBox() {
        return box;
    }

    public TextView getTitle() {
        return title;
    }

    public ImageView getIcon() {
        return icon;
    }
}
