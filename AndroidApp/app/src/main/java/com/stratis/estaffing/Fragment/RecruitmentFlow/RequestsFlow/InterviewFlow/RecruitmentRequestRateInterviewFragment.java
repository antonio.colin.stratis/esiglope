package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.InterviewFlow;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Control.RatingStar.RatingStarComplete;
import com.stratis.estaffing.Control.ScreenView;
import com.stratis.estaffing.Control.TimePicker.TimePickerDialog;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Interview.RequestCandidateInterview;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Interview.InterviewService;

import org.json.JSONObject;

import java.util.Calendar;

public class RecruitmentRequestRateInterviewFragment extends ABFragment implements View.OnClickListener {

    private TextView title;
    private Button notMakedButton;
    private Button makedButton;
    private TextInputEditText makedContainerInput;
    private LoadingButton actionButton;

    private RatingStarComplete starComplete;

    private ImageButton notMakedRadioButton;
    private RelativeLayout inotMakedCalendarBox;
    private DatePickerDialog picker = null;

    private RequestCandidateInterview interview;
    private RecruitmentRequestCreateInterviewFragment.RecruitmentRequestCreateInterviewListener interviewListener;
    private RecruitmentRequestRateInterviewListener listener;

    private boolean interviewMaked = true;
    private boolean interviewNotMakedCheckbox = false;

    private Button notMakeNoRemote;
    private Button notMakeRemote;
    private Boolean inPersonState = null;

    private String originalDate = "";

    private TextView interviewEditCommentTitle;

    public RecruitmentRequestRateInterviewFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_rate_interview, container, false);

        this.setHeader("");
        this.initViews();

        return this.mView;
    }

    private void initViews() {

        // --
        this.title = this.mView.findViewById(R.id.title);

        // --
        this.notMakedButton = this.mView.findViewById(R.id.notMakedButton);
        notMakedButton.setTag("10");
        notMakedButton.setOnClickListener(this);

        this.makedButton = this.mView.findViewById(R.id.makedButton);
        makedButton.setTag("11");
        makedButton.setOnClickListener(this);
        this.setViewAsSecondaryColor(this.makedButton);

        // --
        final String dateName = String.format( getString(R.string.recruitment_request_candidate_rate_interview_sub_title),
                App.capitalize(this.interview.getCandidate().getFirstNameShort()));
        final TextView dateTitle = this.mView.findViewById(R.id.dateTitle);
        dateTitle.setText( dateName );

        // --
        this.actionButton = new LoadingButton(this.mView.findViewById(R.id.actionButton));
        this.initMakedComponentViews();
        this.initNotMakedComponentViews();

        // --
        this.makedContainerInput = this.mView.findViewById(R.id.makedContainerInput);

        this.actionButton.getButton().setBackgroundResource(R.drawable.interview_selected_button);
        this.actionButton.getButton().setTag("15");
        this.actionButton.getButton().setOnClickListener(this);
        this.actionButton.setText( getString(R.string.recruitment_request_candidate_rate_interview_save_button) );
        this.setViewAsPrimaryColor(this.actionButton.getButton());
    }

    private void initMakedComponentViews() {

        final String name = String.format( getString(R.string.recruitment_request_candidate_rate_interview_maked_container_title),
                App.capitalize(this.interview.getCandidate().getFirstNameShort()));
        final TextView makedContainerTitle = this.mView.findViewById(R.id.makedContainerTitle);
        makedContainerTitle.setText( name );

        // -- Stars
        this.starComplete = new RatingStarComplete(this.mView.findViewById(R.id.makedContainerStars), getContext());
        this.starComplete.setListener(this);
    }

    private void initNotMakedComponentViews() {

        // --
        this.notMakedRadioButton = this.mView.findViewById(R.id.notMakedRadioButton);
        this.notMakedRadioButton.setTag("12");
        this.notMakedRadioButton.setOnClickListener(this);

        // --
        final String nameString = String.format(getString(R.string.recruitment_request_candidate_create_interview_date_title),
                App.capitalize(this.interview.getCandidate().getFirstNameShort()) );
        ((TextView) this.mView.findViewById(R.id.notMakedDateTitle)).setText( nameString );

        // --
        this.inotMakedCalendarBox = this.mView.findViewById(R.id.inotMakedCalendarBox);
        this.inotMakedCalendarBox.setOnClickListener(v -> {
            openCalendar();
        });

        // --
        this.notMakeNoRemote = this.mView.findViewById(R.id.interviewInPerson);
        this.notMakeNoRemote.setTag("5");
        this.notMakeNoRemote.setOnClickListener(this);

        this.notMakeRemote = this.mView.findViewById(R.id.interviewInRemote);
        this.notMakeRemote.setTag("6");
        this.notMakeRemote.setOnClickListener(this);

        // --
        this.canActivateFooterButton();
    }

    private void openCalendar() {

        if (picker == null) {

            final Calendar sCalendar = Calendar.getInstance();
            final ScreenView screenView = ScreenView.getInstance(getContext());
            picker = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {

                month = month + 1;
                String date = String.format("%s/%s/%s", dayOfMonth,  month, year);
                interview.setDateDate(date);

                final boolean isToday = (sCalendar.get(Calendar.YEAR) == year &&
                        sCalendar.get(Calendar.MONTH) == (month-1) &&
                        sCalendar.get(Calendar.DAY_OF_MONTH) == dayOfMonth);
                openClock(isToday);

            }, sCalendar.get(Calendar.YEAR), sCalendar.get(Calendar.MONTH), sCalendar.get(Calendar.DAY_OF_MONTH));
            picker.getWindow().setLayout( screenView.getX() - screenView.threeRuleX(5), screenView.getY()/5);
            picker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        }
        picker.show();
    }

    private void openClock(boolean isToday) {

        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog dialog = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {

            @SuppressLint("DefaultLocale")
            final String curMin = String.format("%02d:%02d", hourOfDay, minute);
            interview.setDateTime(curMin);
            setDateLabels();
        }, mHour, mMinute, true);
        dialog.setAccentColor(Color.parseColor("#001330"));

        // --
        if (isToday) { dialog.setMinTime(mHour+1, 0, 0); }
        dialog.show(getFragmentManager(), "");
    }

    private void setDateLabels() {

        String date = (!interview.getDateDate().equals("") && !interview.getDateTime().equals("")) ? interview.getDateDate() : "";
        if (!date.equals("")) {

            date = App.getDateFormatted(date, "dd MMMM yyyy", "dd/M/yyyy");
            date = date + " - " + interview.getDateTime() + "hrs";

            this.inotMakedCalendarBox.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.inotMakedCalendarBox);
            ((TextView) this.inotMakedCalendarBox.findViewById(R.id.calendarBoxText)).setTextColor(  ContextCompat.getColor(mContext, R.color.white) );
            ((TextView) this.inotMakedCalendarBox.findViewById(R.id.calendarBoxText)).setText( date );
            ((ImageView) this.inotMakedCalendarBox.findViewById(R.id.calendarBoxIcon)).setImageBitmap( App.getImage(mContext, "time_white") );

        } else {

            this.inotMakedCalendarBox.setBackgroundDrawable( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            ((TextView) this.inotMakedCalendarBox.findViewById(R.id.calendarBoxText)).setTextColor(  ContextCompat.getColor(mContext, R.color.recruitment_request_vacancy_modality_gray) );
            ((TextView) this.inotMakedCalendarBox.findViewById(R.id.calendarBoxText)).setText("");
            ((ImageView) this.inotMakedCalendarBox.findViewById(R.id.calendarBoxIcon)).setImageBitmap( App.getImage(mContext, "time_gray") );
        }
        // --
        this.canActivateFooterButton();
    }

    private void manageRatingStarsComponent(int stars) {

        this.starComplete.setRating(stars);
        ((TextView) this.mView.findViewById(R.id.makedContainerStarsStatus)).
                setText( this.starComplete.getTitles().get(stars) );
    }

    private void turnOnOffMakedComponent(boolean state) {

        this.interviewMaked = state;
        if (state) {

            this.makedButton.setBackground(mContext.getDrawable(R.drawable.button_orange_selected));
            this.setViewAsSecondaryColor(this.makedButton);
            this.makedButton.setTextColor(Color.WHITE);
            this.mView.findViewById(R.id.makedContainer).setVisibility(View.VISIBLE);

            this.notMakedButton.setBackground(mContext.getDrawable(R.drawable.new_interview_bk));
            this.notMakedButton.setTextColor(Color.parseColor("#767676"));
            this.mView.findViewById(R.id.notMakedContainer).setVisibility(View.GONE);

            interviewEditCommentTitle = findView(R.id.interviewEditCommentTitle);
            interviewEditCommentTitle.setText(R.string.recruitment_request_candidate_rate_interview_maked_container_comment_title);

            this.actionButton.setText( getString(R.string.recruitment_request_candidate_rate_interview_save_button) );

        } else {

            this.makedButton.setBackground(mContext.getDrawable(R.drawable.new_interview_bk));
            this.makedButton.setTextColor(Color.parseColor("#767676"));
            this.mView.findViewById(R.id.makedContainer).setVisibility(View.GONE);

            this.notMakedButton.setBackground(mContext.getDrawable(R.drawable.button_orange_selected));
            this.setViewAsSecondaryColor(this.notMakedButton);
            this.notMakedButton.setTextColor(Color.WHITE);
            this.mView.findViewById(R.id.notMakedContainer).setVisibility(View.VISIBLE);

            interviewEditCommentTitle = findView(R.id.interviewEditCommentTitle);
            interviewEditCommentTitle.setText(R.string.recruitment_actions_request_single_modal_cancel_other_option_title);

            this.actionButton.setText( getString(R.string.recruitment_request_candidate_rate_interview_continue_button) );
        }
        // --
        this.canActivateFooterButton();
    }

    private void reScheduleInterviewCheckboxAction() {

        this.interviewNotMakedCheckbox = !this.interviewNotMakedCheckbox;
        if (this.interviewNotMakedCheckbox) {

            this.notMakedRadioButton.setImageBitmap(App.getImage(getContext(), "orange_box_check"));
            this.mView.findViewById(R.id.notMakedDateTitle).setVisibility(View.VISIBLE);
            this.inotMakedCalendarBox.setVisibility(View.VISIBLE);

            this.actionButton.setText( getString(R.string.recruitment_request_candidate_rate_interview_reagend_iterviwe_button) );

            // --
            this.mView.findViewById(R.id.modalityTitle).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.modalitySubTitle).setVisibility(View.VISIBLE);
            this.notMakeNoRemote.setVisibility(View.VISIBLE);
            this.notMakeRemote.setVisibility(View.VISIBLE);

        } else {

            this.notMakedRadioButton.setImageBitmap(App.getImage(getContext(), "rectangle_fill"));
            this.mView.findViewById(R.id.notMakedDateTitle).setVisibility(View.GONE);
            this.inotMakedCalendarBox.setVisibility(View.GONE);

            this.actionButton.setText( getString(R.string.recruitment_request_candidate_rate_interview_continue_button) );

            // --
            this.mView.findViewById(R.id.modalityTitle).setVisibility(View.GONE);
            this.mView.findViewById(R.id.modalitySubTitle).setVisibility(View.GONE);
            this.notMakeNoRemote.setVisibility(View.GONE);
            this.notMakeRemote.setVisibility(View.GONE);
        }
        // --
        this.canActivateFooterButton();
    }

    private void actionButton() {

        final String review = String.valueOf(this.makedContainerInput.getText());
        this.actionButton.showLoading();

        if (this.interviewMaked) {

            InterviewService.evaluate(this.interview, review, this.starComplete.getRate(),
                    new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {

                    // --
                    Bundle parameters = new Bundle();
                    parameters.putString("entrevista_evaluada", String.valueOf(starComplete.getRate()));
                    final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
                    mFirebaseAnalytics.logEvent("Entrevistas_Evaluadas", parameters);

                    // --
                    actionButton.hideLoading();
                    interview.setRating(String.valueOf(starComplete.getRate()));
                    interview.setReview(review);
                    if (listener != null) {
                        listener.onRated(interview);
                        back();
                    }
                }

                @Override
                public void onError(ReSTResponse response) {

                    actionButton.hideLoading();
                    final Notification notification = new Notification(getContext(), R.layout.notification_light_error);
                    notification.setMessage("Error");
                    notification.show(4000);
                }
            });


        } else {

            if (this.interviewNotMakedCheckbox) {

                // --
                if (this.inPersonState) {

                    final String inPersonString = String.valueOf(((TextInputEditText)
                            this.mView.findViewById(R.id.interviewInPersonInput)).getText());
                    this.interview.setLocationAddress( inPersonString );

                } else {

                    final String inRemoteString = String.valueOf(((TextInputEditText)
                            this.mView.findViewById(R.id.interviewInRemoteInput)).getText());
                    this.interview.setLocationAddress( inRemoteString );
                }

                // --
                InterviewService.reschedule(this.interview, review, "", new ReSTCallback() {
                    @Override
                    public void onSuccess(ReSTResponse response) {

                        actionButton.hideLoading();
                        if (interviewListener != null) {
                            interview.setReview(review);
                            interviewListener.onReScheduledSuccess(interview);
                            back();
                        }
                    }

                    @Override
                    public void onError(ReSTResponse response) {

                        actionButton.hideLoading();
                        JSONObject res = JsonBuilder.stringToJson(response.body);

                        String msg = "Ha ocurrido un error, intenta más tarde";
                        if (res != null) {
                            msg = res.optString("msg", "Ha ocurrido un error, intenta más tarde");
                        }

                        // --
                        final Notification notification = new Notification(getContext(), R.layout.notification_light_error);
                        notification.setMessage(msg);
                        notification.show(4000);
                    }
                });

            } else {

                InterviewService.updateStatus(this.interview, review, "",
                        RequestCandidateInterview.interviewNotMade, new ReSTCallback() {
                    @Override
                    public void onSuccess(ReSTResponse response) {

                        actionButton.hideLoading();
                        if (listener != null) {
                            interview.setReview(review);
                            listener.onNotMade(interview);
                            back();
                        }
                    }

                    @Override
                    public void onError(ReSTResponse response) {

                        actionButton.hideLoading();
                        JSONObject res = JsonBuilder.stringToJson(response.body);

                        String msg = "Ha ocurrido un error, intenta más tarde";
                        if (res != null) {
                            msg = res.optString("msg", "Ha ocurrido un error, intenta más tarde");
                        }

                        // --
                        final Notification notification = new Notification(getContext(), R.layout.notification_light_error);
                        notification.setMessage(msg);
                        notification.show(4000);
                    }
                });
            }

        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeInPersonButtonState() {

        this.interview.setRemote(!this.inPersonState);
        if (this.inPersonState) {

            this.notMakeNoRemote.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.notMakeNoRemote);
            this.notMakeNoRemote.setTextColor(Color.WHITE);
            this.notMakeNoRemote.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.select_white_option ), null);

            this.notMakeRemote.setBackgroundDrawable( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            this.notMakeRemote.setTextColor(Color.parseColor("#001330"));
            this.notMakeRemote.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.empty_circle ), null);

            // -- Show elements
            this.mView.findViewById(R.id.interviewInPersonSubTitle).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.interviewInPersonInputContainer).setVisibility(View.VISIBLE);
            // -- Hide elements
            this.mView.findViewById(R.id.interviewInRemoteSubTitle).setVisibility(View.GONE);
            this.mView.findViewById(R.id.interviewInRemoteInputContainer).setVisibility(View.GONE);
            this.mView.findViewById(R.id.interviewInRemoteInputExample).setVisibility(View.GONE);

        } else {

            this.notMakeNoRemote.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            this.notMakeNoRemote.setTextColor(Color.parseColor("#001330"));
            this.notMakeNoRemote.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.empty_circle ), null);

            this.notMakeRemote.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.notMakeRemote);
            this.notMakeRemote.setTextColor(Color.WHITE);
            this.notMakeRemote.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.select_white_option ), null);

            // -- Show elements
            this.mView.findViewById(R.id.interviewInRemoteSubTitle).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.interviewInRemoteInputContainer).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.interviewInRemoteInputExample).setVisibility(View.VISIBLE);
            // -- Hide elements
            this.mView.findViewById(R.id.interviewInPersonSubTitle).setVisibility(View.GONE);
            this.mView.findViewById(R.id.interviewInPersonInputContainer).setVisibility(View.GONE);
        }
        // --
        this.canActivateFooterButton();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void canActivateFooterButton() {

        if (this.interviewMaked ) {

            this.actionButton.getButton().setBackground(mContext.getDrawable(R.drawable.button_paused_blue));
            this.setViewAsPrimaryColor(this.actionButton.getButton());
            this.actionButton.getButton().setClickable(true);
            this.actionButton.getButton().setEnabled(true);

        } else {

            if (this.interviewNotMakedCheckbox && (this.inPersonState == null || this.interview.getDateServer().equals(this.originalDate)) ) {

                this.actionButton.getButton().setBackground(mContext.getDrawable(R.drawable.button_paused_gray));
                this.actionButton.getButton().setClickable(false);
                this.actionButton.getButton().setEnabled(false);

            } else {

                this.actionButton.getButton().setBackground(mContext.getDrawable(R.drawable.button_paused_blue));
                this.setViewAsPrimaryColor(this.actionButton.getButton());
                this.actionButton.getButton().setClickable(true);
                this.actionButton.getButton().setEnabled(true);
            }
        }
    }

    @Override
    public void onClick(View v) {

        final String tag = String.valueOf(v.getTag());
        switch (tag) {

            case "5":

                this.inPersonState = true;
                this.changeInPersonButtonState();
                break;

            case "6":

                this.inPersonState = false;
                this.changeInPersonButtonState();
                break;

            case "10":
                this.turnOnOffMakedComponent(false);
                break;

            case "11":
                this.turnOnOffMakedComponent(true);
                break;

            case "12":
                this.reScheduleInterviewCheckboxAction();
                break;

            case "15":
                this.actionButton();
                break;

            case RatingStarComplete.RatingStarCompleteOne:
                this.manageRatingStarsComponent(1);
                break;

            case RatingStarComplete.RatingStarCompleteTwo:
                this.manageRatingStarsComponent(2);
                break;

            case RatingStarComplete.RatingStarCompleteThree:
                this.manageRatingStarsComponent(3);
                break;

            case RatingStarComplete.RatingStarCompleteFour:
                this.manageRatingStarsComponent(4);
                break;

            case RatingStarComplete.RatingStarCompleteFive:
                this.manageRatingStarsComponent(5);
                break;
        }
    }

    public void setInterview(RequestCandidateInterview interview) {

        this.interview = interview;
        this.originalDate = this.interview.getDateServer();
    }

    public void setListener(RecruitmentRequestRateInterviewListener listener) {
        this.listener = listener;
    }

    public void setInterviewListener(RecruitmentRequestCreateInterviewFragment.RecruitmentRequestCreateInterviewListener interviewListener) {
        this.interviewListener = interviewListener;
    }

    public interface RecruitmentRequestRateInterviewListener {

        void onRated(RequestCandidateInterview interview);
        void onNotMade(RequestCandidateInterview interview);
    }
}