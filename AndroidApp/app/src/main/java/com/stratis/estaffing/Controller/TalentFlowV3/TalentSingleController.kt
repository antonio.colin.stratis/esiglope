package com.stratis.estaffing.Controller.TalentFlowV3

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Fragment.TalentFlowV3.TalentSingleInfoFragment
import com.stratis.estaffing.R

class TalentSingleController : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_talent_single_v3_controller)

        val talentSingleInfoFragmnent = TalentSingleInfoFragment()
        if(intent != null && intent.hasExtra("talentId")) {
            talentSingleInfoFragmnent.talentId = intent.getStringExtra("talentId")
        }
        this.loadFragment(talentSingleInfoFragmnent)
    }

    private fun loadFragment(fragment: ABFragment) {

        this.supportFragmentManager.
        beginTransaction().
        replace(R.id.fragment_container, fragment).
        commit()
        return
    }
}