package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal.Model.NoCandidateInterestModel;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal.View.NoCandidateInterestView;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.Model.RequestPausedCancelFeedbackModel;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.View.RequestPausedCancelFeedbackView;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class NoCandidateInterestAdapter extends RecyclerView.Adapter<NoCandidateInterestView> {

    protected ArrayList<NoCandidateInterestModel> items;
    protected Context context;
    protected NoCandidateInterestAdapterListener listener;
    protected int previousHolder = -1;

    public NoCandidateInterestAdapter(ArrayList<NoCandidateInterestModel> items, Context context) {

        this.context = context;
        this.items = items;
    }

    @Override
    public NoCandidateInterestView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recruitment_request_single_modal_item, parent, false);
        return new NoCandidateInterestView(layoutView);
    }

    @Override
    public void onBindViewHolder(final NoCandidateInterestView holder, final int position) {

        holder.setIsRecyclable(false);
        final NoCandidateInterestModel item = this.items.get(position);

        // --
        holder.getName().setText(item.getValueItem());
        if (item.getSelected()) {
            holder.getBox().setBackground(context.getResources().getDrawable(R.drawable.request_paused_cancel_feedback_modal_item_selected));
            holder.getName().setTextColor(Color.WHITE);
            App.setViewAsPrimaryColor(holder.getBox(), context);
        }

        // --
        holder.getBox().setOnClickListener(v -> {

            // --
            if (this.previousHolder != -1) {
                this.items.get(previousHolder).setSelected(false);
            }
            this.items.get(position).setSelected(true);
            previousHolder = position;

            // --
            if (listener != null) {
                listener.onOptionClick(item.getValueItem());
            }
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void setListener(NoCandidateInterestAdapterListener listener) {
        this.listener = listener;
    }

    public interface NoCandidateInterestAdapterListener {
        public void onOptionClick(String option);
    }
}