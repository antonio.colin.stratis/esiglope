package com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.RequestView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * Created by Erick Sanchez
 * Revision 1 - 21/08/20
 */

public class RequestAdapter extends RecyclerView.Adapter<RequestView> {

    protected ArrayList<LatestVacancyRequest> items;
    protected ArrayList<LatestVacancyRequest> original;

    protected RequestAdapterListener  listener;
    protected Context context;

    // -- Date filters
    @SuppressLint("SimpleDateFormat")
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private final Date today = new Date();

    public RequestAdapter(final Context context, ArrayList<LatestVacancyRequest> items) {

        Collections.reverse(items);
        this.items = items;
        this.original = items;
        this.context = context;
    }

    @Override
    public RequestView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.fragment_recruitment_requests_item, parent, false);
        return new RequestView(layoutView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RequestView holder, int position) {

        holder.setIsRecyclable(false);
        final LatestVacancyRequest item = this.items.get(position);

        // -- Setting the views
        holder.getRequestOrder().setText( String.format(context.getString(R.string.recruitment_actions_request_title_2), item.getId()) );
        holder.getRequestTitle().setText( item.getName() + " " + item.getJobSeniorityShort() );
        holder.getRequestDate().setText( App.getDateFormatted(item.getDate(), "dd/MMM")
                .toUpperCase().replaceAll("\\.","") );
        holder.getRequestButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onDetail(item);
                }
            }
        });

        // -- Icon and Text
        this.setIcon(item, holder);
    }

    protected void setIcon(final LatestVacancyRequest item, final RequestView holder) {

        switch(item.getStatusId()) {

            case LatestVacancyRequest.statusCandidates:
            case LatestVacancyRequest.statusInterviews:
            case LatestVacancyRequest.statusHire:
            case LatestVacancyRequest.statusHireStop:
                holder.getRequestDescription().setText( context.getString(R.string.recruitment_actions_requests_candidates_item_desc) );
                holder.getRequestIcon().setImageBitmap( App.getImage(context, "icon_candidatos") );
                break;

            case LatestVacancyRequest.statusPaused:
                holder.getRequestDescription().setText( context.getString(R.string.recruitment_actions_requests_paused_item_desc) );
                holder.getRequestIcon().setImageBitmap( App.getImage(context, "icon_pause_request") );
                break;

            case LatestVacancyRequest.statusCancel:
                holder.getRequestDescription().setText( context.getString(R.string.recruitment_actions_requests_cancel_item_desc) );
                holder.getRequestIcon().setImageBitmap( App.getImage(context, "icon_red_cancel") );
                break;

            case LatestVacancyRequest.statusDone:
                holder.getRequestDescription().setText( context.getString(R.string.recruitment_actions_requests_done_item_desc) );
                holder.getRequestIcon().setImageBitmap( App.getImage(context, "icon_check_green") );
                break;

            default:
                holder.getRequestDescription().setText( context.getString(R.string.recruitment_actions_requests_active_item_desc) );
                break;
        }
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
        //int items = (this.items != null && this.items.size() > 9) ? 9 : this.items.size();
        //return this.items == null ? 0 : items;
    }

    public void reload(ArrayList<LatestVacancyRequest> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void setListener(RequestAdapterListener listener) {
        this.listener = listener;
    }

    public void filterCurrentMonth() {

        this.items = new ArrayList<>();
        for (LatestVacancyRequest item : this.original) {
            if (this.filterByNumberOfMonth(item.getDate(), 1)) {
                this.items.add(item);
            }
        }
        this.notifyDataSetChanged();
    }

    public void filterTwoCurrentMonth() {

        this.items = new ArrayList<>();
        for (LatestVacancyRequest item : this.original) {
            if (this.filterByNumberOfMonth(item.getDate(), 2)) {
                this.items.add(item);
            }
        }
        this.notifyDataSetChanged();
    }

    public void removeDateFilter() {

        this.items = this.original;
        this.notifyDataSetChanged();
    }

    private boolean filterByNumberOfMonth(String date, int months) {

        Log.d("DXGOP", "today : " + sdf.format(today));
        final Calendar thirtyDaysAgo = Calendar.getInstance();
        thirtyDaysAgo.add(Calendar.MONTH, -months);
        Log.d("DXGOP", "to : " + sdf.format(today));

        Date thirtyDaysAgoDate = thirtyDaysAgo.getTime();
        Date newDate = null;
        try {
            newDate = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (newDate.before(thirtyDaysAgoDate)) {
            Log.d("DXGOP", "is older than " + months + " month(s)");
            return false;
        }
        return true;
    }

    public interface RequestAdapterListener {
        void onDetail(LatestVacancyRequest item);
    }
}