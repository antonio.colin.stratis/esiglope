package com.stratis.estaffing.Listener

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/06/21
 */
interface NotificationCenterFragmentListener {
    fun onVacancyNotAvailable()
    //fun onVacancyPaused()
    //fun onVacancyStopped()
    fun onCandidateListNotAvailable()
    fun onCandidateNotAvailable()
}