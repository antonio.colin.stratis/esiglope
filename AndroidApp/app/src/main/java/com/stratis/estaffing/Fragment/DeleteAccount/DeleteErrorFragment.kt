package com.stratis.estaffing.Fragment.DeleteAccount

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Controller.MainFlow.SupportFlow.SupportController
import com.stratis.estaffing.R

class DeleteErrorFragment: ABFragment(), View.OnClickListener {

    private var listener: DeleteErrorFragment.DeleteErrorFragmentListener? = null
    private lateinit var buttonContinue: Button
    private lateinit var buttonSupport: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_delete_error, container, false)

        this.initViews()
        return this.mView
    }

    private fun initViews() {

        buttonContinue = mView.findViewById(R.id.done)
        buttonContinue.tag = "13"
        buttonContinue.setOnClickListener(this)

        buttonSupport = mView.findViewById(R.id.cancel)
        buttonSupport.tag = "14"
        buttonSupport.setOnClickListener(this)

    }

    @SuppressLint("ResourceAsColor")
    override fun onClick(v: View?) {
        when(v!!.tag.toString()) {
            "13" -> {
                this.backRemove(this)
            }
            "14" -> {
                activity?.let{
                    val intent = Intent (it, SupportController::class.java)
                    it.startActivity(intent)
                }
            }
        }
    }

    fun setListener(listener: DeleteErrorFragmentListener) {
        this.listener = listener
    }

    interface DeleteErrorFragmentListener {
        fun onBackDelete()
    }

}