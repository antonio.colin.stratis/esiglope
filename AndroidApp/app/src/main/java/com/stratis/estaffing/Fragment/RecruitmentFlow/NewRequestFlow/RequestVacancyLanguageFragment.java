package com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Adapter.RecruitmentFlow.SelectedLanguageAdapter;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Control.Searcher.Listener.RecyclerItemClickListener;
import com.stratis.estaffing.Control.Searcher.Searcher;
import com.stratis.estaffing.Control.eStaffingFragment;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Modal.LanguageLevel.LanguageLevelModal;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageCatalog;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageLevel;
import com.stratis.estaffing.R;

import java.util.ArrayList;

public class RequestVacancyLanguageFragment extends eStaffingFragment implements Searcher.SearcherListener {

    private Searcher searcher;
    private SelectedLanguageAdapter adapter;
    private RecyclerView languageTable;

    public RequestVacancyLanguageFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        this.mView = inflater.inflate(R.layout.fragment_request_vacancy_language, container, false);
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName());

        this.initViews();

        // --
        return this.mView;
    }

    @Override
    public void onResume() {

        super.onResume();
        this.checkProfile();
    }

    @SuppressLint("SetTextI18n")
    private void initViews() {

        this.searcher = new Searcher(getContext(),
                (RelativeLayout) this.mView.findViewById(R.id.searchComponent), Searcher.SearcherType.language);
        searcher.setDataLanguage(this.getProfile().getLanguageCatalogs());
        searcher.setInputHint(getContext().getString(R.string.request_vacancy_language_searcher_hint));
        searcher.setListener(this, "71");

        // --
        this.languageTable = this.mView.findViewById(R.id.languageTable);

        final DividerItemDecoration itemDecorator = new DividerItemDecoration(this.mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this.mContext, R.drawable.recycler_devider_5));
        LinearLayoutManager layout = new LinearLayoutManager(this.mContext, LinearLayoutManager.VERTICAL, false);

        // --
        languageTable.getRecycledViewPool().setMaxRecycledViews(0, 3);
        languageTable.setLayoutManager(layout);
        languageTable.addItemDecoration(itemDecorator);
        languageTable.setItemViewCacheSize(8);
        languageTable.setDrawingCacheEnabled(true);
        languageTable.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // --
        //this.adapter = new SelectedLanguageAdapter(LanguageCatalog.getBase(), this.mContext);
        this.adapter = new SelectedLanguageAdapter(LanguageCatalog.getBase(), this.mContext);
        this.languageTable.setAdapter(this.adapter);
        this.createLanguageAdapter();
    }

    private void createLanguageAdapter() {

        this.adapter.setListener(new SelectedLanguageAdapter.OnLanguageAction() {
            @Override
            public void onCloseButton(Integer position) {

                adapter.removeElement(position);
                adapter.notifyDataSetChanged();
                // --
                if (adapter.getCurrentAddedLanguagePointer() <= 2) {
                    searcher.getSearchComponentBoxInput().setEnabled(true);
                }
                // -- Getting the current skills
                getProfile().setCurrentLanguages( adapter.getItems() );
            }

            @Override
            public void onLevelClick(Integer position) {

                final ArrayList<LanguageLevel> levels = LanguageLevel.getBase(getContext());
                final LanguageLevelModal modal = new LanguageLevelModal(mContext);
                final LanguageCatalog languageCatalog = adapter.getItems().get(position);
                modal.setLanguageCatalog(languageCatalog);
                modal.setLevels(levels);
                modal.setListener(new RecyclerItemClickListener(getContext(), languageTable, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        final LanguageLevel level = levels.get(position);
                        languageCatalog.setLevel(level.getLevelType());
                        modal.cancel();
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {}
                }));
                modal.show();
            }
        });
    }

    private void checkProfile() {

        if (this.getProfile() != null && this.getProfile().getCurrentLanguages() != null) {

            if (this.getProfile().isLanguagesFromSaveRequest()) {
                this.getProfile().transformCurrentLanguageObjects();
            }

            this.adapter = new SelectedLanguageAdapter(LanguageCatalog.getBase(), this.mContext);
            this.languageTable.setAdapter(this.adapter);
            this.createLanguageAdapter();

            // -- Add
            if (this.getProfile().getCurrentLanguages() != null) {
                for (LanguageCatalog languageCatalog : this.getProfile().getCurrentLanguages()) {
                    this.adapter.addElement(languageCatalog);
                }
            }
            if (this.adapter.getCurrentAddedLanguagePointer() > 2) {
                searcher.getSearchComponentBoxInput().setEnabled(false);
            }
            // -- Passing prev languages
            this.searcher.getLanguageAdapter().setSelected(this.getProfile().getCurrentLanguages());
        }
    }

    @Override
    public void onItemSelected(Object item, RelativeLayout box, RecyclerView recyclerView, EditText editText, ImageView icon) {

        // --
        final LanguageCatalog languageCatalog = (LanguageCatalog) item;

        // -- Hide keyboard
        editText.setText("");
        editText.clearFocus();
        hideSoftKeyboard(getActivity(), editText);

        // --
        this.adapter.addElement(languageCatalog);
        this.adapter.notifyDataSetChanged();

        // --
        if (this.adapter.getCurrentAddedLanguagePointer() > 2) {
            editText.setEnabled(false);
        }

        // --
        this.searcher.setSelectedLanguage(this.adapter.getItems());

        // --
        final Notification notification = new Notification(getContext(), R.layout.notification_light);
        notification.setMessage(getContext().getString(R.string.request_vacancy_language_notification_text));
        notification.show(1500);

        // --
        this.getProfile().setCurrentLanguages( this.adapter.getItems() );
    }

    // -- UN-USED
    @Override
    public void onItemSelected(Object item, ConstraintLayout box, RecyclerView recyclerView, EditText editText, ImageView icon) {}

    // -- Abstract methods

    @Override
    public void onFooterContinueButtonClick() {

        //--
        super.onFooterContinueButtonClick();

        // -- Getting the current skills
        this.getProfile().setCurrentLanguages( this.adapter.getItems() );
    }
}