package com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateWorkExperienceModel;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate.CandidateWorkExperienceView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class RequestCandidateWorkExperienceAdapter extends RecyclerView.Adapter<CandidateWorkExperienceView> {

    protected ArrayList<CandidateWorkExperienceModel> items;

    public RequestCandidateWorkExperienceAdapter(ArrayList<CandidateWorkExperienceModel> items) {
        this.items = items;
}

    @Override
    public CandidateWorkExperienceView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recruitment_request_candidate_work_experience_item,
                parent, false);
        return new CandidateWorkExperienceView(layoutView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final CandidateWorkExperienceView holder, final int position) {

        holder.setIsRecyclable(false);
        final CandidateWorkExperienceModel experience = this.items.get(position);

        // --
        holder.getJobName().setText( experience.getEmployer() + " - " + experience.getPosition() );

        // --
        String initialDate = App.getDateFormatted(experience.getStartDate(), "MMMM yyyy");
        initialDate = initialDate.substring(0, 1).toUpperCase() + initialDate.substring(1).toLowerCase();

        String endDate = App.getDateFormatted(experience.getEndDate(), "MMMM yyyy");
        endDate = endDate.substring(0, 1).toUpperCase() + endDate.substring(1).toLowerCase();

        String date = String.format("%s - %s", initialDate, endDate);
//        final SpannableStringBuilder dateBuilder = App.getTextWithSpanColor(date, duration,
//                new ForegroundColorSpan(Color.parseColor("#959595")));
        holder.getJobPeriod().setText(date);

        final String duration = !App.isEmpty(experience.getDuration()) ?
                App.capitalize(experience.getDuration()) : "";
        holder.getJobDuration().setText( "(" + duration + ")");

        // --
        holder.getJobDescription().setText( experience.getDescription() );
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<CandidateWorkExperienceModel> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<CandidateWorkExperienceModel> getItems() {
        return items;
    }
}