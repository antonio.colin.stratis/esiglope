package com.stratis.estaffing.Model.MasterAdminFlow

import com.stratis.estaffing.Model.NotificationCenterFlow.NotificationCenterModel
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Erick Sanchez
 * Revision 1 - 30/06/21
 */
open class Leader:Serializable {

    var id:String = ""
    var firstName:String = ""
    var lastName:String = ""
    var area:String = ""
    var clientName:String = ""
    var areaId:Int = 0
    var clientId:Long = 0
    var vacancyCount:Int = 0

    constructor(obj: JSONObject?) {

        if (obj != null) {
            this.id = obj.optString("userId")
            this.firstName = obj.optString("firstName")
            this.lastName = obj.optString("lastName")
            this.area = obj.optString("area")
            this.clientName = obj.optString("clientName")
            this.areaId = obj.optInt("areaId")
            this.clientId = obj.optLong("clientId")
            this.vacancyCount = obj.optInt("vacancyCount")
        }
    }

    fun getNameShort():String {

        var ret = ""
        if (this.firstName != "") {
            ret = this.firstName.get(0).toString()
        }
        if (this.lastName != "") {
            ret += this.lastName.get(0).toString()
        }
        return ret
    }

    fun getFullName(): String{
        return this.firstName + " " + this.lastName
    }

    fun getPoliteName() :String {

        var name = ""
        if (this.firstName != "") {
            name = this.firstName
        }
        if (this.lastName != "") {
            name = "$name ${this.lastName.get(0)}."
        }
        return name
    }

    override fun toString(): String {
        return "Leader(id='$id', firstName='$firstName', lastName='$lastName', area='$area', clientName='$clientName', areaId=$areaId, clientId=$clientId, vacancyCount=$vacancyCount)"
    }

    companion object {

        fun parse(array: JSONArray?): ArrayList<Leader>? {

            if (array != null) {
                val leaders = ArrayList<Leader>()
                for (i in 0 until array.length()) {
                    val obj = array.optJSONObject(i)
                    leaders.add(Leader(obj))
                }
                return leaders
            }
            return null
        }
    }
}