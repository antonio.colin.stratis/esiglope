package com.stratis.estaffing.Controller.MainFlow;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.stratis.estaffing.BuildConfig;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Controller.Configuration.ConfigurationController;
import com.stratis.estaffing.Controller.LoginFlow.LoginController;
import com.stratis.estaffing.Controller.MainFlow.NotificationCenterFlow.NotificationCenterController;
import com.stratis.estaffing.Controller.MainFlow.SupportFlow.SupportController;
import com.stratis.estaffing.Controller.RecruitmentFlow.RecruitmentController;
import com.stratis.estaffing.Controller.SearchingJobFlow.UploadCvController;
import com.stratis.estaffing.Controller.TalentFlow.TalentController;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Preferences;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Controller.MasterAdminFlow.MasterAdminController;
import com.stratis.estaffing.Model.NotificationCenterFlow.NotificationCenterModel;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Notification.NotificationService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class HomeController extends AppCompatActivity implements View.OnClickListener {

    private Session session;

    private ArrayList<NotificationCenterModel> notifications;
    final String[] notificationOfMasterAdmin = new String[] {"54", "55", "57", "58", "59", "60"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // --
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        this.session = Session.get(this);

        // -- Getting and checking the ROL Session to assign the VIEW
        this.setContentView();

        setHeader();

        // -- Init Firebase
        this.initFirebase();

        // -- Init views
        this.initViews();

        // -- Saving FCM Token
        this.saveFCMToken();
    }

    private void setHeader() {
        App.setViewAsPrimaryColor(findViewById(R.id.header_bk), this);
        final String color = App.fixWrongColorATuDePiglek(this.session.getPreferences().getPrimaryColor());
        this.getWindow().setStatusBarColor(Color.parseColor(color));
    }

    private void initFirebase() {

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setUserProperty("Rol", this.session.getType().name());
        mFirebaseAnalytics.setUserProperty("Cliente", this.session.getClientName());

        Bundle parameters = new Bundle();
        parameters.putString("traffic_type", Api.getInstance().isDev() ? "internal" : "production"); // Change type for enviroment QA or Release "production"
        mFirebaseAnalytics.setDefaultEventParameters(parameters);
    }

    private void setContentView() {

        switch(this.session.getType()) {

            case client:
                setContentView(R.layout.activity_home_controller);
                break;

            case talent:
            case company:
            default:
                setContentView(R.layout.activity_home_minified_controller);
                break;
        }

        /// -- Setting the logo
        final ImageView logo = this.findViewById(R.id.logo);
        Glide.with(this)
                .load(this.session.getPreferences().getLogoUrl())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(logo);

        TextView privacySubTextView = this.findViewById(R.id.sub);
        if (privacySubTextView.getText().toString().isEmpty()) {
            privacySubTextView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        // -- Get notifications
        //NotificationService.save(Session.get(this));

        // -- Show pending notifications
        this.showPendingNotification();

        // --
        final RelativeLayout notificationsContainer = this.findViewById(R.id.notifications);
        if (notificationsContainer != null) {
            this.animateSquareContent(notificationsContainer, true);
        }
        this.getNotificationCenter();

        // -- Check pending Push Notifications
        this.validateIfHasPendingPushNotifications();
    }

    private void initViews() {

        // -- Name
        final TextView titleName = this.findViewById(R.id.title);
        //final String ln = String.valueOf((!this.session.getLastName().equals("")) ? this.session.getLastName().charAt(0) : "");
        final String nameString = String.format(this.getString(R.string.home_title), this.session.getFirstName());
        final SpannableStringBuilder name = App.getTextWithSpan(nameString, this.session.getFirstName(), new StyleSpan(Typeface.BOLD));
        titleName.setText(name);

        // -- Config Menu
        this.setConfig();

        // -- My talent and new talent
        final RelativeLayout newTalent = this.findViewById(R.id.newTalent);
        newTalent.setBackgroundResource(R.drawable.new_talent_bk);
        switch(this.session.getType()) {

            case client:

                final RelativeLayout myTalent = this.findViewById(R.id.myTalent);
                this.paintSquarerData(myTalent, 11, this.getImage("icon_search_people"),
                        getResources().getString(R.string.home_item_myTalent_title),
                        getResources().getString(R.string.home_item_myTalent_sub_title));
                this.paintSquarerData(newTalent, 12, this.getImage("icon_new_talent"),
                        getResources().getString(R.string.home_item_newTalent_title),
                        getResources().getString(R.string.home_item_newTalent_sub_title));
                App.setViewAsSecondaryColor(myTalent, this);
                break;

            case talent:

                this.paintSquarerData(newTalent, 12, this.getImage("icon_cv"),
                        getResources().getString(R.string.home_item_newProfile_title),
                        getResources().getString(R.string.home_item_newProfile_sub_title), true);
                break;

            case masterAdmin:

                newTalent.setBackground(ContextCompat.getDrawable(this, R.drawable.my_talent_bk) );
                newTalent.findViewById(R.id.arrow).setVisibility(View.INVISIBLE);
                final String clientTitle = String.format(getResources().getString(R.string.home_item_ma_myTalent_title), session.getClientName());
                @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) final String clientSub = String.format(getResources().getString(R.string.home_item_ma_myTalent_sub_title), session.getClientName());
                this.paintSquarerData(newTalent, 21, this.getImage("icon_search_people"),
                        clientTitle, clientSub);
                break;

            default:

                this.paintSquarerData(newTalent, 12, this.getImage("icon_new_talent"),
                        getResources().getString(R.string.home_item_newTalent_title),
                        getResources().getString(R.string.home_item_newTalent_sub_title));
                break;

        }
        App.setViewAsPrimaryColor(newTalent, this);
        FirebaseEvent.logEvent(this, this.getClass().getSimpleName().concat(this.session.getType().toString()));

        // -- Notifications
        final RelativeLayout notifications = this.findViewById(R.id.notifications);
        this.paintSquarerData(notifications, 14, this.getImage("notification"),
                getResources().getString(R.string.home_item_notifications_title),
                getResources().getString(R.string.home_item_notifications_sub_title));

        // -- Support
        final RelativeLayout support = this.findViewById(R.id.support);
        this.paintSquarerData(support, 15, this.getImage("icon_support"),
                getResources().getString(R.string.home_item_support_title),
                getResources().getString(R.string.home_item_support_sub_title));

        // -- Privacy
        final RelativeLayout privacy = this.findViewById(R.id.privacy);
        privacy.setTag(16);
        privacy.setOnClickListener(this);

        // -- App Version
        final TextView appVersion = this.findViewById(R.id.app_version);
        appVersion.setText(String.format(getResources().getString(R.string.home_app_version), BuildConfig.VERSION_NAME));
    }

    private void setConfig() {

        ImageView dotsMenu = this.findViewById(R.id.dotsMenu);
        dotsMenu.setTag(10);
        dotsMenu.setOnClickListener(this);
    }

    private Bitmap getImage(String name) {

        int id = this.getResources().getIdentifier(name, "drawable", this.getPackageName());
        return BitmapFactory.decodeStream(this.getResources().openRawResource(id));
    }

    private void paintSquarerData(final RelativeLayout square, int tag, Bitmap icon, String title, String sub) {
        this.paintSquarerData(square, tag, icon, title, sub, false);
    }

    private void paintSquarerData(final RelativeLayout square, int tag, Bitmap icon, String title, String sub, boolean centerIcon) {

        // --
        square.setTag(tag);
        square.setOnClickListener(this);

        // --
        final ImageView iconMyTalent = square.findViewById(R.id.icon);
        iconMyTalent.setImageBitmap(icon);
        if (centerIcon) {
            iconMyTalent.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }

        // --
        final TextView titleMyTalent = square.findViewById(R.id.title);
        titleMyTalent.setText( title );

        final TextView subMyTalent = square.findViewById(R.id.subtitle);
        subMyTalent.setText( sub );
    }

    private void animateSquareContent(final RelativeLayout square, boolean state) {

        // --
        square.setClickable(!state);

        // --
        if (state) {

            // --
            square.findViewById(R.id.icon).setVisibility(View.INVISIBLE);
            square.findViewById(R.id.title).setVisibility(View.INVISIBLE);
            square.findViewById(R.id.subtitle).setVisibility(View.INVISIBLE);
            square.findViewById(R.id.badge).setVisibility(View.INVISIBLE);

            // --
            final ProgressBar bar = square.findViewById(R.id.progress);
            bar.setVisibility(View.VISIBLE);
            //noinspection deprecation
            bar.getIndeterminateDrawable().setColorFilter(
                    getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);

        } else {

            // --
            square.findViewById(R.id.icon).setVisibility(View.VISIBLE);
            square.findViewById(R.id.title).setVisibility(View.VISIBLE);
            square.findViewById(R.id.subtitle).setVisibility(View.VISIBLE);

            // --
            square.findViewById(R.id.progress).setVisibility(View.INVISIBLE);
        }
    }

    private void validateIfHasPendingPushNotifications() {

        final Preferences preferences = new Preferences(this);
        if (preferences.existsPreference("pendingPushNotification")) {

            //Log.i("DXGOP", "AQUI ANDO MERENGUES");
            final String pendingPushNotification = preferences.getStringPreference("pendingPushNotification");
            final JSONObject pendingPushNotificationObj = JsonBuilder.stringToJson(pendingPushNotification);
            final NotificationCenterModel notificationCenterModel = new NotificationCenterModel(pendingPushNotificationObj);

            preferences.removePreference("pendingPushNotification");
            if (!Arrays.asList(notificationOfMasterAdmin).contains(notificationCenterModel.getNotificationTypeId())) {
                this.goToNotificationCenter(true, notificationCenterModel);
            }
        }
    }

    private void getNotificationCenter() {

        final Session session = Session.get(this);
        NotificationService.getAll(session, new ReSTCallback() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(ReSTResponse response) {

                final JSONObject res = JsonBuilder.stringToJson(response.body);
                final JSONArray notificationsArray = res.optJSONArray("content");
                notifications = NotificationCenterModel.Companion.parse(notificationsArray);

                // --
                final RelativeLayout notContainer = findViewById(R.id.notifications);
                animateSquareContent(notContainer, false);

                // --
                final int notificationNotSeen =
                        NotificationCenterModel.Companion.howManyNotSeen(notifications);
                if (notificationNotSeen > 0) {

                    final TextView badge = notContainer.findViewById(R.id.badge);
                    badge.setVisibility(View.VISIBLE);

                    if (notificationNotSeen >= 100) {
                        badge.setText( "99+" );
                    } else {
                        badge.setText( "" + notificationNotSeen );
                    }
                }
            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                notifications = new ArrayList<>();

                // --
                final RelativeLayout notContainer = findViewById(R.id.notifications);
                animateSquareContent(notContainer, false);
            }
        });
    }

    private void goToNotificationCenter(boolean asSpecificFragment, NotificationCenterModel notificationCenterModel) {

        final Intent intent = new Intent(this, NotificationCenterController.class);
        if (asSpecificFragment) {
            intent.putExtra("notification", notificationCenterModel);
        }
        if (this.notifications != null) { intent.putExtra("notifications", this.notifications); }
        startActivity(intent);
    }

    private void saveFCMToken() {

        final Session session = Session.get(this);
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        Log.w("DXGOP", "Fetching FCM ERROR :: ", task.getException());
                        return;
                    }

                    session.setFcmToken( task.getResult() );
                    NotificationService.saveConfiguration(Session.get(this));
                });
    }

    private void showPendingNotification() {

        final Preferences preferences = new Preferences(this);
        String message = "";
        boolean asError = false;

        if (preferences.existsPreference("pendingErrorNotification")) {

            message = preferences.getStringPreference("pendingErrorNotification");
            preferences.removePreference("pendingErrorNotification");
            asError = true;

        } else if (preferences.existsPreference("pendingNotification")) {

            message = preferences.getStringPreference("pendingNotification");
            preferences.removePreference("pendingNotification");
        }

        // --
        if (!message.equals("")) {

            final Notification notification = new Notification(this,
                    (asError) ? R.layout.notification_light_error : R.layout.notification_light);
            notification.setTitle("");
            notification.setMessage(message);
            notification.show();
        }
    }

    /** Implementations **/

    @Override
    public void onClick(View v) {

        final Integer tag = (Integer) v.getTag();
        switch (tag) {

            case 10:
                startActivity(new Intent(this, ConfigurationController.class));
                break;

            case 11:
                startActivity(new Intent(this, TalentController.class));
                break;

            case 12:
                if (this.session.getType() == Session.SessionRolType.talent) {
                    startActivity(new Intent(this, UploadCvController.class));
                } else {
                    startActivity(new Intent(this, RecruitmentController.class));
                }
                break;

            case 14:
                this.goToNotificationCenter(false, null);
                break;

            case 15:
                startActivity(new Intent(this, SupportController.class));
                break;

            case 16:
                startActivity(new Intent(this, PolicyController.class));
                break;

            case 17:
                this.session.logout();
                startActivity(new Intent(this, LoginController.class));
                finishAffinity();
                break;

            case 21: // Master Admin
                startActivity(new Intent(this, MasterAdminController.class));
                break;
        }
    }
}