package com.stratis.estaffing.Service.Recruitment.NewRequestFlow;

import android.util.Log;

import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Core.eStaffingClient;

import org.json.JSONObject;

/**
 * Created by Erick Sanchez
 * Revision 1 - 11/09/20
 */

public class ProfileService {

    public static void getLanguageCatalog(final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/catalog/language");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getSoftwareCatalog(final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/catalog/customSoftware");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void save(final Session session, int vacancies, JSONObject body, final ReSTCallback listener) {

        // -- Logs
        Log.d("DXGOP", "EMAIL ::: " + session.getUsername());
        Log.d("DXGOP", "Vacancies ::: " + vacancies);
        Log.d("DXGOP", "Body ::: " + JsonBuilder.jsonToString(body));

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/request/save");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("email", session.getUsername());
        request.addParameter("numOfVacancy", String.valueOf(vacancies));

        // --
        request.addBodyJson(JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void update(final Session session, JSONObject body, final ReSTCallback listener) {

        // -- Logs
        Log.d("DXGOP", "EMAIL ::: " + session.getUsername());
        Log.d("DXGOP", "iD ::: " + session.getDataSessionId());
        Log.d("DXGOP", "Body ::: " + JsonBuilder.jsonToString(body));

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/request/update");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_PUT, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("email", session.getUsername());
        request.addParameter("userId", session.getDataSessionId());

        // --
        request.addBodyJson(JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getSimpleProfiles(final ReSTCallback listener) {

        // -- Single Profile logic
        final String url = Api.getInstance().getUrl("/profile/findAll");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }
}