package com.stratis.estaffing.Fragment.TalentFlowV3.Feedback

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Control.RatingStar.RatingStarComplete
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Modal.FeedbackLeaveModal
import com.stratis.estaffing.Model.TalentFlowV3.FeedbackDetail
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.Talent.TalentService

@Suppress("DEPRECATION")
class FeedbackStarRateFragment : ABFragment(), View.OnClickListener {

    private var starComplete: RatingStarComplete? = null
    private var closeButton: Button? = null
    var talentId: String? = null
    var talentName: String? = null
    var skills: ArrayList<FeedbackDetail>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_feedback_star_rate, container, false)

        this.setHeader("")
        this.closeButton = mView.findViewById<Button>(R.id.closeButton)

        // --
        // --
        this.closeButton?.setOnClickListener {
            val feedbackDetailFragment = FeedbackDetailFragment()
            feedbackDetailFragment.talentName = this.talentName
            this.continueSegue(feedbackDetailFragment)
        }
        val app_screen_return_arrow = mView.findViewById<ImageButton>(R.id.app_screen_return_arrow)
        app_screen_return_arrow.setOnClickListener {
            val modal = FeedbackLeaveModal(context)
            modal.setListener( object : FeedbackLeaveModal.FeedbackLeaveModalListener {
                override fun backPressed() {
                    activity!!.onBackPressed()
                }
            } )
            modal.show()
        }
        this.initViews()
        return mView
    }
    private fun initViews() {
        initMakedComponentViews()

        // --
        val txtTalentName = String.format(getString(R.string.feedback_fragment_q_talent_star_rate),
                App.capitalize(talentName))
        val txtFeedbackRateTitle = mView.findViewById<TextView>(R.id.txtFeedbackRateTitle)
        txtFeedbackRateTitle.text = txtTalentName

        getSkills()
    }

    private fun getSkills() {
        TalentService.getFeedbackSkills( Session.get(mContext).dataSessionId, talentId, object :
            ReSTCallback {
            override fun onSuccess(response: ReSTResponse?) {
                val res = JsonBuilder.stringToJsonArray(response!!.body)
                skills = FeedbackDetail.parse(res)
            }

            override fun onError(response: ReSTResponse?) {
                // --
                Log.d("DXGOP", "ERROR :: " + response!!.body)
            }
        })
    }

    private fun initMakedComponentViews() {
        val name = String.format(getString(R.string.feedback_fragment_talent_star_rate),
                App.capitalize(talentName))
        val makedContainerTitle = mView.findViewById<TextView>(R.id.txtFeedbackRateStarsSubTitle)
        makedContainerTitle.text = name

        // -- Stars
        this.starComplete = RatingStarComplete(mView.findViewById(R.id.makedContainerStars), context)
        this.starComplete!!.setListener(this)
    }

    override fun onClick(v: View?) {
        val tag = v!!.tag.toString()
        when (tag) {
            RatingStarComplete.RatingStarCompleteOne -> this.manageRatingStarsComponent(1)
            RatingStarComplete.RatingStarCompleteTwo -> this.manageRatingStarsComponent(2)
            RatingStarComplete.RatingStarCompleteThree -> this.manageRatingStarsComponent(3)
            RatingStarComplete.RatingStarCompleteFour -> this.manageRatingStarsComponent(4)
            RatingStarComplete.RatingStarCompleteFive -> this.manageRatingStarsComponent(5)
            "6" -> {
                val feedbackDetailFragment = FeedbackDetailFragment()
                feedbackDetailFragment.talentName = this.talentName
                this.continueSegue(feedbackDetailFragment)
            }
        }
    }
    private fun manageRatingStarsComponent(stars: Int) {
        starComplete!!.setRating(stars)
        val rateDescription : String = starComplete!!.geFeedbackRateDescription(stars)
        (mView.findViewById<View>(R.id.txtFeedbackRateStarsSubTitle) as TextView).text = rateDescription
        val feedbackNewStreingthFragment = FeedbackNewFragment()
        feedbackNewStreingthFragment.type = FeedbackNewFragment.FeedbackType.STRENGTH
        feedbackNewStreingthFragment.talentId = this.talentId
        feedbackNewStreingthFragment.talentName = App.capitalize(talentName)
        feedbackNewStreingthFragment.rate = stars
        feedbackNewStreingthFragment.rateDescription = rateDescription
        feedbackNewStreingthFragment.allSkills = skills
        this.continueSegue(feedbackNewStreingthFragment)
    }
}