package com.stratis.estaffing.Control.RatingStar;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 14/10/20
 */

public class RatingStar {

    private Context  context;
    private LinearLayout baseLayout;
    private ImageView starOne;
    private ImageView starTwo;
    private ImageView starThree;
    private ImageView starFour;
    private ImageView starFive;

    public RatingStar(LinearLayout layout, Context context)  {

        this.context  = context;
        this.baseLayout = layout;
        this.setup();
    }

    private void setup() {

        // --
        this.starOne = this.baseLayout.findViewById(R.id.starOne);
        this.starTwo = this.baseLayout.findViewById(R.id.starTwo);
        this.starThree = this.baseLayout.findViewById(R.id.starThree);
        this.starFour = this.baseLayout.findViewById(R.id.starFour);
        this.starFive = this.baseLayout.findViewById(R.id.starFive);

        // --
        /*this.starOne.setVisibility(ImageView.INVISIBLE);
        this.starTwo.setVisibility(ImageView.INVISIBLE);
        this.starThree.setVisibility(ImageView.INVISIBLE);
        this.starFour.setVisibility(ImageView.INVISIBLE);
        this.starFive.setVisibility(ImageView.INVISIBLE);*/
    }

    public void setRating(String rating) {

        String[] ratingParts = rating.split("[.]");

        // --
        if (ratingParts.length > 0) {

            // --
            if (ratingParts[0] != null) {
                setFirstPartOfRating(ratingParts[0]);
            }

            // --
            if (ratingParts[1] != null && ratingParts[1].equals("5")) {
                setSecondPartOfRating(ratingParts[0]);
            }
        }
    }

    private void setFirstPartOfRating(String star) {

        int s = Integer.parseInt(star);

        // --
        if (s >= 1) {
            this.starOne.setImageBitmap(App.getImage(this.context, "full_star") );
        }

        if (s >= 2) {
            this.starTwo.setImageBitmap(App.getImage(this.context, "full_star") );
        }

        if (s >= 3) {
            this.starThree.setImageBitmap(App.getImage(this.context, "full_star") );
        }

        if (s >= 4) {
            this.starFour.setImageBitmap(App.getImage(this.context, "full_star") );
        }

        if (s >= 5) {
            this.starFive.setImageBitmap(App.getImage(this.context, "full_star") );
        }
    }

    private void setSecondPartOfRating(String star)  {

        int s = Integer.parseInt(star);

        // --
        switch (s) {

            case 0:
                this.starOne.setImageBitmap(App.getImage(this.context, "star_half") );
                break;

            case 1:
                this.starTwo.setImageBitmap(App.getImage(this.context, "star_half") );
                break;

            case 2:
                this.starThree.setImageBitmap(App.getImage(this.context, "star_half") );
                break;

            case 3:
                this.starFour.setImageBitmap(App.getImage(this.context, "star_half") );
                break;

            case 4:
                this.starFive.setImageBitmap(App.getImage(this.context, "star_half") );
                break;
        }
    }
}