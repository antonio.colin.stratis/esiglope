package com.stratis.estaffing.Core.Rester;

import android.util.ArrayMap;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ReSTResponse {

    public int statusCode;
    public int contentLength;
    public String contentType;
    public String body;
    public JSONObject json;
    public ArrayMap<String, String> headers;

    public ReSTResponse() {

        this.body = "";
        this.json = null;
        this.statusCode = 0;
        this.contentLength = 0;
        this.contentType = "";
        this.headers = new ArrayMap<>();
    }

    protected void setHeaders(Map<String, List<String>> headers) {

        for (Map.Entry<String, List<String>> entries : headers.entrySet()) {
            String values = "";
            for (String value : entries.getValue()) {
                values += value + ",";
            }
            this.headers.put(entries.getKey(), values);
        }
    }

    public String getHeader(String key) {

        String ret = "";
        if (this.headers != null && this.headers.size() > 0) {
            ret = this.headers.get(key);
        }
        return ret;
    }
}