package com.stratis.estaffing.Controller.MasterAdminFlow

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Fragment.MasterAdminFlow.MALeadersFragment
import com.stratis.estaffing.R

class MasterAdminController : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_master_admin_controller)

        // -- Loading leaders fragment
        val leadersFragment = MALeadersFragment()
        this.loadFragment(leadersFragment)
    }

    private fun loadFragment(fragment: ABFragment) {

        this.supportFragmentManager.
        beginTransaction().
        replace(R.id.fragment_container, fragment).
        commit()
        return
    }
}