package com.stratis.estaffing.Adapter.RecruitmentFlow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.ABActivity;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyLanguageFragment;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageCatalog;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.NewRequestFlow.SelectedLanguageView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class SelectedLanguageAdapter extends RecyclerView.Adapter<SelectedLanguageView> {

    protected ArrayList<LanguageCatalog> items;
    protected Context context;
    private int currentAddedLanguagePointer = 0;
    protected OnLanguageAction listener;

    public SelectedLanguageAdapter(ArrayList<LanguageCatalog> items, Context context) {

        this.context = context;
        this.items = items;
    }

    @Override
    public SelectedLanguageView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_request_vacancy_language_item, parent, false);
        return new SelectedLanguageView(layoutView);
    }

    @Override
    public void onBindViewHolder(final SelectedLanguageView holder, final int position) {

        holder.setIsRecyclable(false);
        final LanguageCatalog language = this.items.get(position);
        final Boolean isFilled = !language.getName().equals("");

        // --
        holder.getTitle().setVisibility( isFilled ? View.VISIBLE : View.INVISIBLE);
        holder.getLevelButton().setVisibility( isFilled ? View.VISIBLE : View.INVISIBLE );
        holder.getCloseButton().setVisibility( isFilled ? View.VISIBLE : View.INVISIBLE );

        if (!isFilled) {

            holder.getBox().setBackground( ContextCompat.getDrawable(context, R.drawable.rectangle_dotty) );

        } else {

            holder.getBox().setBackground( ContextCompat.getDrawable(context, R.drawable.request_vacancy_selected_button) );
            //((ABActivity) context).setViewAsSecondaryColor(holder.getBox());
            App.setViewAsSecondaryColor(holder.getBox(), context);

            holder.getTitle().setText( language.getName() );
            holder.getLevelButton().setText( language.getLevel(context) );
            // -- Clicks
            holder.getCloseButton().setOnClickListener(v -> {
                if (listener != null) {
                    listener.onCloseButton(position);
                }
            });
            // --
            holder.getLevelButton().setOnClickListener(v -> {
                if (listener != null) {
                    listener.onLevelClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void addElement(LanguageCatalog language) {

        if (!language.getName().equals("")) {
            this.items.set(this.currentAddedLanguagePointer, language);
            this.currentAddedLanguagePointer++;
        }
    }

    public void removeElement(int index) {

        this.items.remove(index);
        this.items.add(new LanguageCatalog());
        this.currentAddedLanguagePointer--;
    }

    public void reload(ArrayList<LanguageCatalog> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public int getCurrentAddedLanguagePointer() {
        return currentAddedLanguagePointer;
    }

    public ArrayList<LanguageCatalog> getItems() {
        return items;
    }

    // --

    public OnLanguageAction getListener() {
        return listener;
    }

    public void setListener(OnLanguageAction listener) {
        this.listener = listener;
    }

    // --
    public interface OnLanguageAction {
        public void onCloseButton(Integer position);
        public void onLevelClick(Integer position);
    }
}
