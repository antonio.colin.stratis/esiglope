package com.stratis.estaffing.Model.RecruitmentFlow

import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

open class LocationCountry: Serializable {

    var catalogId : Int = 0
    var catalogItemId : Int = 0
    var description : String = ""
    var descriptioni18n : String = ""
    var valueItem : String = ""
    var valueItemi18n : String = ""

    constructor(obj: JSONObject?) {
        if (obj != null) {
            this.catalogId = obj.optInt("catalogId")
            this.catalogItemId = obj.optInt("catalogItemId")
            this.description = obj.optString("description")
            this.descriptioni18n = obj.optString("descriptioni18n")
            this.valueItem = obj.optString("valueItem")
            this.valueItemi18n = obj.optString("valueItemi18n")
        }
    }

    companion object {

        fun parse(array: JSONArray?): ArrayList<LocationCountry>? {
            if (array != null) {
                val locationCountryDetail = ArrayList<LocationCountry>()
                for (i in 0 until array.length()) {
                    val obj = array.optJSONObject(i)
                    locationCountryDetail.add(LocationCountry(obj))
                }
                return locationCountryDetail
            }
            return null
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LocationPlace) return false

        if (description != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return description.hashCode()
    }

    fun getObject(): JSONObject {
        val obj = JSONObject()
        obj.put("name", description)
        obj.put("address", description)

        return obj
    }

    override fun toString(): String {
        return "$valueItem"
    }
}