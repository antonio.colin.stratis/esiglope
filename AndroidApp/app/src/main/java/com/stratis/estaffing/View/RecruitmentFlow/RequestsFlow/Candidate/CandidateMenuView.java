package com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class CandidateMenuView extends RecyclerView.ViewHolder {

    private ConstraintLayout shape;
    private ImageView icon = null;
    private TextView bubble = null;
    private TextView title = null;

    public CandidateMenuView(View itemView) {

        // --
        super(itemView);

        // --
        this.shape = itemView.findViewById(R.id.shape);
        this.icon = itemView.findViewById(R.id.icon);
        this.bubble = itemView.findViewById(R.id.bubble);
        this.title = itemView.findViewById(R.id.title);
    }

    public ConstraintLayout getShape() {
        return shape;
    }

    public ImageView getIcon() {
        return icon;
    }

    public TextView getBubble() {
        return bubble;
    }

    public TextView getTitle() {
        return title;
    }
}
