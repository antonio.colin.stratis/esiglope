package com.stratis.estaffing.Fragment.MasterAdminFlow

import android.graphics.Typeface
import android.os.Bundle
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.stratis.estaffing.Adapter.RecruitmentFlow.NewRequest.RequestProfileCheckAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Control.Loader
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidateSingleFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidatesFragment
import com.stratis.estaffing.Listener.NotificationCenterFragmentListener
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.RequestPausedCancelModal
import com.stratis.estaffing.Model.MasterAdminFlow.Leader
import com.stratis.estaffing.Model.Profile
import com.stratis.estaffing.Model.RecruitmentFlow.ProfileCheck
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.RequestsService

open class MARequestSingleFragment : ABFragment(), View.OnClickListener {

    open var profile:Profile? = null
    open var leader:Leader? = null
    open var latestVacancyRequest: LatestVacancyRequest? = null
    var notificationCenterListener: NotificationCenterFragmentListener? = null

    private var loader: Loader? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_m_a_request_single, container, false)

        // -- Set header
        setHeader("")

        // --
        getRequest()

        // -- Init views
        initViews()

        return this.mView
    }

    private fun initViews() {}

    // -- Method to get requests
    private fun getRequest() {

        // -- Show loader
        loader = Loader(mContext)
        loader?.show()

        // --
        val id = if (profile != null) profile!!.provisionalId else ""
        RequestsService.getRequest(id, object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {

                val res = JsonBuilder.stringToJson(response.body)
                profile?.loadFromJSON(res)
                paintRequestInfo()
                loader?.cancel()
            }

            override fun onError(response: ReSTResponse) {

                loader?.cancel()
                if (notificationCenterListener != null) {
                    notificationCenterListener?.onVacancyNotAvailable()
                }
            }
        })
    }

    // -- Paint profile info with the request
    private fun paintRequestInfo() {

        val request_info_date = findView<TextView>(R.id.request_info_date)
        val request_info_order = findView<TextView>(R.id.request_info_order)
        val request_info_profile = findView<TextView>(R.id.request_info_profile)
        val request_vacancy = findView<TextView>(R.id.request_vacancy)
        val request_scotia_leader = findView<TextView>(R.id.request_scotia_leader)
        val request_scotia_area = findView<TextView>(R.id.request_scotia_area)

        // -- DATE
        val dateParsedString = App.getDateFormatted(profile!!.creationDate, "dd/MM/yyyy")
        val dateString = String.format(mContext.resources
                .getString(R.string.recruitment_actions_request_single_top_card_date), dateParsedString)
        val dateBuilder = App.getTextWithSpan(dateString, dateParsedString, StyleSpan(Typeface.BOLD))
        request_info_date.text = dateBuilder

        // -- ORDER
        val orderString = String.format(mContext.resources.
        getString(R.string.recruitment_actions_request_single_top_card_order), profile!!.provisionalId)
        val orderBuilder = App.getTextWithSpan(orderString, profile!!.provisionalId, StyleSpan(Typeface.BOLD))
        request_info_order.text = orderBuilder

        // -- PROFILE
        request_info_profile.text = profile!!.currentProfile.name.toUpperCase()

        // -- Vacancy
        if (this.latestVacancyRequest?.folioClient != "0") {

            val vacancyString = String.format(
                    getString(R.string.recruitment_actions_request_single_scotia_top_card_vacancy_title),
                    latestVacancyRequest?.folioClient )
            request_vacancy.text = vacancyString

        } else {
            request_vacancy.visibility = View.GONE
        }

        // -- Leader and area
        val leaderString = String.format(
                getString(R.string.recruitment_actions_request_single_scotia_top_card_leader_title),
                leader?.getFullName())
        val leaderBuilder = App.getTextWithSpan(leaderString, leader?.getFullName(), StyleSpan(Typeface.BOLD))
        request_scotia_leader.text = leaderBuilder

        val areaString = String.format(
                getString(R.string.recruitment_actions_request_single_scotia_top_card_area_title),
                leader?.area)
        val areaBuilder = App.getTextWithSpan(areaString, leader?.area, StyleSpan(Typeface.BOLD))
        request_scotia_area.text = areaBuilder

        // -- STATUS
        this.paintRequestStatusInfo()
    }

    private fun paintRequestStatusInfo() {

        val request_info_status = mView.findViewById<TextView>(R.id.request_info_status)
        val status_date_title = mView.findViewById<TextView>(R.id.status_date_title)
        val status_date = mView.findViewById<TextView>(R.id.status_date)
        val status_comment = mView.findViewById<TextView>(R.id.status_comment)
        val buttonActions = mView.findViewById<Button>(R.id.request_button_actions)

        // -- Commons
        request_info_status.text = profile!!.statusValue
        buttonActions.visibility = View.GONE
        when (profile!!.statusId) {

            LatestVacancyRequest.statusSent,
            LatestVacancyRequest.statusConfirm,
            LatestVacancyRequest.statusAttention,
            LatestVacancyRequest.statusSearch -> this.setRequestStatusInfoAfterPaused()

            LatestVacancyRequest.statusCandidates, LatestVacancyRequest.statusInterviews -> {
                this.setRequestStatusInfoAfterPaused()
                buttonActions.visibility = View.VISIBLE
                if (profile!!.statusId == LatestVacancyRequest.statusCandidates) {
                    buttonActions.tag = "100"
                } else {
                    buttonActions.text = mContext.getString(R.string.recruitment_actions_request_single_review_request)
                    buttonActions.tag = "101"
                }
                buttonActions.setOnClickListener(this)
            }

            LatestVacancyRequest.statusHire, LatestVacancyRequest.statusHireStop,
            LatestVacancyRequest.statusDone -> {

                this.setRequestStatusInfoAfterPaused()
                if (profile!!.statusId == LatestVacancyRequest.statusDone) {
                    //mView.findViewById<View>(R.id.cancelPauseButtons).visibility = View.GONE
                }
                this.getCandidatesOnlyInHire()
            }

            LatestVacancyRequest.statusCancel, LatestVacancyRequest.statusPaused -> {

                // -- TopBars Color
                mView.findViewById<View>(R.id.request_info_header_separator).background =
                        if (LatestVacancyRequest.statusPaused == profile!!.statusId) ContextCompat.getDrawable(mContext, R.drawable.black_top_border) else ContextCompat.getDrawable(mContext, R.drawable.red_top_border)
                mView.findViewById<View>(R.id.request_info_status_header_separator).background =
                        if (LatestVacancyRequest.statusPaused == profile!!.statusId) ContextCompat.getDrawable(mContext, R.drawable.black_top_border) else ContextCompat.getDrawable(mContext, R.drawable.red_top_border)

                // -- Titles
                val statusString = if (LatestVacancyRequest.statusPaused == profile!!.statusId) getString(R.string.recruitment_actions_request_single_states_paused_title) else getString(R.string.recruitment_actions_request_single_states_cancel_title)
                val dateTitleString = String.format(
                        getString(R.string.recruitment_actions_request_single_status_date_title),
                        statusString.toLowerCase())
                status_date_title.text = dateTitleString
                status_date_title.visibility = View.VISIBLE
                status_date.text = App.getDateFormatted(profile!!.statusDate, "dd/MM/yyyy")
                status_date.visibility = View.VISIBLE
                mView.findViewById<View>(R.id.status_date_divider).visibility = View.VISIBLE

                // -- Comment
                val requestCommentString = String.format(
                        getString(R.string.recruitment_actions_request_single_status_comment_example),
                        profile!!.statusComment)
                status_comment.text = requestCommentString
                status_comment.visibility = View.VISIBLE

                // -- Buttons
                if (LatestVacancyRequest.statusPaused == profile!!.statusId) {
                    buttonActions.visibility = View.VISIBLE
                    buttonActions.text = getString(R.string.recruitment_actions_request_single_resume_button)
                    buttonActions.tag = "200"
                    buttonActions.setOnClickListener(this)

                    // -- Settign the pause button to blue - rePlay
                    val pauseButton = mView.findViewById<RelativeLayout>(R.id.pauseButton)
                    //pauseButton.setBackgroundDrawable(getContext().getDrawable(R.drawable.button_paused_gray));
                    pauseButton.tag = "200"
                    (pauseButton.findViewById<View>(R.id.pauseTitle) as TextView).text = "Reanudar"
                    (pauseButton.findViewById<View>(R.id.icon) as ImageView).setImageBitmap(App.getImage(mContext, "play_icon_filled"))
                }

                // --  Cancel && Pause buttons
                if (LatestVacancyRequest.statusCancel == profile!!.statusId) {
                    mView.findViewById<View>(R.id.cancelPauseButtons).visibility = View.GONE
                }
            }
        }

        // -- PROFILE REQUEST DATA TABLE
        paintRequestDataTable()
    }

    private fun paintSelectedCandidateComponent(candidate: RequestCandidate) {

        val candidate_component = mView.findViewById<RelativeLayout>(R.id.candidate_component)
        candidate_component.clipToOutline = true
        val candidate_image = candidate_component.findViewById<ImageView>(R.id.image)
        val candidate_seniority = candidate_component.findViewById<TextView>(R.id.seniority)
        val candidate_name = candidate_component.findViewById<TextView>(R.id.name)
        val candidate_speciality = candidate_component.findViewById<TextView>(R.id.speciality)
        candidate_component.visibility = View.VISIBLE

        candidate_component.setOnClickListener { v: View? ->
            val candidateSingleFragment = RecruitmentRequestCandidateSingleFragment()
            candidateSingleFragment.setProfile(profile)
            candidateSingleFragment.setCandidate(candidate)
            this.continueSegue(candidateSingleFragment)
        }

        // -- Profile image
        candidate_image.clipToOutline = true
        if (candidate.imgProfile != "") {
            Glide.with(this)
                    .load(candidate.imgProfile)
                    .centerCrop()
                    .thumbnail(.50f)
                    .diskCacheStrategy(DiskCacheStrategy.NONE) // .placeholder(R.drawable.loading_spinner)
                    .skipMemoryCache(true)
                    .into(candidate_image)
        }

        // -- Fields
        candidate_name.text = candidate.fullNameShort
        candidate_speciality.text = candidate.positionDesc

        // --
        val s = candidate.jobSeniority
        if (s == "" || s == "null" || s == "Null" || s == "NULL") {
            candidate_seniority.visibility = View.INVISIBLE
        } else {
            candidate_seniority.text = App.capitalize(candidate.jobSeniority)
        }
        /** Regards component  */
        if (profile!!.statusId == LatestVacancyRequest.statusDone) {
            //mView.findViewById<View>(R.id.states).visibility = View.GONE
        }
    }

    private fun paintRequestDataTable() {

        // -- PROFILE REQUEST DATA TABLE
        var checksTable = mView.findViewById<RecyclerView>(R.id.checksTable)
        App.createVerticalRecyclerList(checksTable, mContext)

        // --
        var adapter = RequestProfileCheckAdapter(
                ProfileCheck.getRequestSelectedItemsFromMasterAdmin(profile, mContext), mContext)
        checksTable.setAdapter(adapter)
    }

    open fun getCandidatesOnlyInHire() {
        RequestsService.getCandidates(profile!!.provisionalId, object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {

                // --
                val res = JsonBuilder.stringToJsonArray(response.body)
                val items = RequestCandidate.parse(res)
                for (candidate in items) {
                    if (candidate.statusId == RequestCandidate.statusCandidateChoose) {
                        paintSelectedCandidateComponent(candidate)
                        return
                    }
                }
            }

            override fun onError(response: ReSTResponse) {}
        })
    }

    private fun setRequestStatusInfoAfterPaused() {

        // -- TopBars Color
        mView.findViewById<View>(R.id.request_info_header_separator).background = ContextCompat.getDrawable(mContext, R.drawable.orange_top_border)
        mView.findViewById<View>(R.id.request_info_status_header_separator).background = ContextCompat.getDrawable(mContext, R.drawable.orange_top_border)

        // --
        mView.findViewById<View>(R.id.status_date_divider).visibility = View.GONE
        mView.findViewById<View>(R.id.status_date_title).visibility = View.GONE
        mView.findViewById<View>(R.id.status_comment).visibility = View.GONE
        mView.findViewById<View>(R.id.status_date).visibility = View.GONE
    }

    /** implementations **/

    override fun onClick(v: View?) {

        val tag = v!!.tag.toString()
        when (tag) {
            "100", "101" -> {

                val candidatesFragment = MARequestSingleCandidatesFragment()
                candidatesFragment.profile = profile
                this.continueSegue(candidatesFragment)
            }
        }
    }
}