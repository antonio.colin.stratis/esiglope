package com.stratis.estaffing.Fragment.DeleteAccount

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Model.CatalogItem
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.Account.AccountService

class DeleteFragment: ABFragment() {

    private var listener: DeleteFragmentListener? = null
    lateinit var reason: CatalogItem
    lateinit var comment: String
    lateinit var password: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_delete, container, false)
        setHeader("")

        return this.mView
    }

    override fun onResume() {
        super.onResume()
        deleteAccount()
    }

    private fun deleteAccount() {
        AccountService.deleteAccount(password, reason.catalogItemId, comment, Session.get(context),
                object : ReSTCallback {
                    override fun onSuccess(response: ReSTResponse) {
                        // --
                        Log.d("DXGOP", "DELETE ACCOUNT RESPONSE : " + response.body)
                        val res = JsonBuilder.stringToJson(response.body)

                        if (res.optInt("code", 500) == 201) {
                            this@DeleteFragment.deleteConfirm()
                        } else {
                            this@DeleteFragment.deleteError()
                        }
                    }

                    override fun onError(response: ReSTResponse) {
                        if (response.statusCode == 400) {
                            this@DeleteFragment.deleteErrorPassword()
                        } else {
                            this@DeleteFragment.deleteError()
                        }
                    }
                }
        )
    }
    private fun deleteConfirm() {
        val deleteConfirmFragment = DeleteConfirmFragment()
        this.continueSegue(deleteConfirmFragment)
    }

    private fun deleteErrorPassword() {
        listener?.onBackPassword()
        this.backRemove(this)
    }

    private fun deleteError() {
        val deleteErrorFragment = DeleteErrorFragment()
        deleteErrorFragment.setListener(object: DeleteErrorFragment.DeleteErrorFragmentListener {
            override fun onBackDelete() {
                deleteAccount()
            }
        })
        this.continueSegue(deleteErrorFragment)
    }

    fun setListener(listener: DeleteFragmentListener) {
        this.listener = listener
    }

    interface DeleteFragmentListener {
        fun onBackPassword()
    }

}
