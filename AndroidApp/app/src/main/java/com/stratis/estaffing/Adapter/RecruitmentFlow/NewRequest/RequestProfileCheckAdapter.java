package com.stratis.estaffing.Adapter.RecruitmentFlow.NewRequest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Model.RecruitmentFlow.ProfileCheck;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.RequestProfileCheckView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class RequestProfileCheckAdapter extends RecyclerView.Adapter<RequestProfileCheckView> {

    protected ArrayList<ProfileCheck> items;
    protected Context context;
    protected ProfileCheckClickListener listener;

    public RequestProfileCheckAdapter(ArrayList<ProfileCheck> items, Context context) {

        this.context = context;
        this.items = items;
    }

    @Override
    public RequestProfileCheckView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recruitment_request_single_item, parent, false);
        return new RequestProfileCheckView(layoutView);
    }

    @Override
    public void onBindViewHolder(final RequestProfileCheckView holder, final int position) {

        holder.setIsRecyclable(false);
        final ProfileCheck check = this.items.get(position);

        holder.getTitle().setText( check.getTitle() );
        holder.getContent().setText( check.getContent() );

        if (check.getEditable()) {

            // -- Button
            holder.getEditButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onCheckButtonClicked(position);
                    }
                }
            });
        } else {
            holder.getEditButton().setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<ProfileCheck> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<ProfileCheck> getItems() {
        return items;
    }

    public void setListener(ProfileCheckClickListener listener) {
        this.listener = listener;
    }

    public interface ProfileCheckClickListener {
        void onCheckButtonClicked(int position);
    }
}
