package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.Model.RequestsFilterStatusModel;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.View.RequestFilterStatusView;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class RequestFilterStatusAdapter extends RecyclerView.Adapter<RequestFilterStatusView> {

    protected ArrayList<RequestsFilterStatusModel> items;
    protected Context context;
    protected Boolean asDate = false;

    protected String currentFilter = "";
    protected RequestFilterStatusAdapterListener listener;
    protected BottomSheetDialog parent;

    public RequestFilterStatusAdapter(ArrayList<RequestsFilterStatusModel> items, Context context, final Boolean asDate) {

        this.context = context;
        this.items = items;
        this.asDate = asDate;
    }

    @Override
    public RequestFilterStatusView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(
                (asDate) ? R.layout.fragment_recruitment_requests_modal_date_item : R.layout.fragment_recruitment_requests_modal_item,
                parent, false);
        return new RequestFilterStatusView(layoutView, asDate);
    }

    @Override
    public void onBindViewHolder(final RequestFilterStatusView holder, final int position) {

        holder.setIsRecyclable(false);
        final RequestsFilterStatusModel item = this.items.get(position);

        // -- Icon
        if (!this.asDate) {
            holder.getIcon().setImageBitmap( item.getIcon(this.context) );
        }

        // --
        if (this.currentFilter != null && this.currentFilter.equals(item.getId()))  {

            holder.getBox().setBackground( ContextCompat.getDrawable(context, R.drawable.button_orange_selected) );
            App.setViewAsSecondaryColor(holder.getBox(), context);

            holder.getTitle().setTextColor(Color.WHITE);
            if (!this.asDate) {
                holder.getIcon().setImageBitmap(App.getImage(context, item.getIcon() + "_white"));
            }
        }
        holder.getTitle().setText(item.getName());

        // --
        holder.getBox().setOnClickListener(v -> {
            if (listener != null) {
                listener.onFilterClick(item.getId());
            }

            if (parent != null) {
                parent.cancel();
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    // --
    public void setCurrentFilter(String currentFilter) {
        this.currentFilter = currentFilter;
    }

    public void setListener(RequestFilterStatusAdapterListener listener) {
        this.listener = listener;
    }

    public void setParent(BottomSheetDialog parent) {
        this.parent = parent;
    }

    public interface RequestFilterStatusAdapterListener {
        public void onFilterClick(String filterId);
    }
}