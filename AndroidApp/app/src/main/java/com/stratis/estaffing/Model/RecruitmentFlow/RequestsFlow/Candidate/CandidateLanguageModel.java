package com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 18/12/20
 *
 */

public class CandidateLanguageModel {

    private String id = "";
    private String talentId = "";
    private String languageId = "";
    private String langLevelId = "";
    private String langName = "";
    private String langDescription = "";
    private String langLevelName = "";
    private String langLevelPercentage = "";
    private String videoUrl = "";

    public CandidateLanguageModel() {}

    public CandidateLanguageModel(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optString("id");
            this.talentId = obj.optString("talentId");
            this.languageId = obj.optString("languageId");
            this.langLevelId = obj.optString("langLevelId");
            this.langName = obj.optString("langName");
            this.langDescription = obj.optString("langDescription");
            this.langLevelName = obj.optString("langLevelName");
            this.langLevelPercentage = obj.optString("langLevelPercentage");
            this.videoUrl = obj.optString("videoUrl");
        }
    }

    public static ArrayList<CandidateLanguageModel> parse(JSONArray array) {

        ArrayList<CandidateLanguageModel> languages = new ArrayList<>();
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                languages.add(new CandidateLanguageModel(obj));
            }
        }
        return languages;
    }

    public String getId() {
        return id;
    }

    public String getTalentId() {
        return talentId;
    }

    public String getLanguageId() {
        return languageId;
    }

    public String getLangLevelId() {
        return langLevelId;
    }

    public String getLangName() {
        return langName;
    }

    public String getLangDescription() {
        return langDescription;
    }

    public String getLangLevelName() {
        return langLevelName;
    }

    public String getLangLevelPercentage() {
        return langLevelPercentage;
    }

    public String getVideoUrl() {
        return videoUrl;
    }
}