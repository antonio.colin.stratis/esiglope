package com.stratis.estaffing.Modal;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.Nullable;

import com.stratis.estaffing.Adapter.SkillAdapter;
import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.Skill;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 28/07/20
 */

public class SkillsModal extends Dialog {

    private ListView skillsTableView = null;
    private Button closeButton = null;

    private ArrayList<Skill> skills = null;

    public SkillsModal(Context context) {

        super(context);
        this.init();
    }

    public SkillsModal(Context context, int themeResId) {

        super(context, themeResId);
        this.init();
    }

    protected SkillsModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.init();
    }

    @SuppressLint("ResourceType")
    private void init() {

        // -- R styles
        this.setContentView(R.layout.modal_skills);
        this.setCanceledOnTouchOutside(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow()
                .setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );

        // --
        this.skillsTableView = this.findViewById(R.id.skillsTable);
        this.closeButton = this.findViewById(R.id.closeButton);

        // --
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        App.setViewAsSecondaryColor(this.closeButton, getContext());
    }

    public void setSkills(ArrayList<Skill> skills) {

        // --
        this.skills = skills;

        // --
        if (skills !=  null &&  skills.size() > 0) {
            final SkillAdapter adapter = new SkillAdapter(getContext(), this.skills);
            this.skillsTableView.setAdapter(adapter);
        }
    }
}
