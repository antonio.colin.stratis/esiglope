package com.stratis.estaffing.Modal.LanguageLevel.View;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez - App Builders CTO
 * Revision 1 - 01/09/20
 */

public class LanguageLevelView extends RecyclerView.ViewHolder {

    private ConstraintLayout box;
    private TextView title;
    private ImageView icon;

    public LanguageLevelView(View itemView) {

        // --
        super(itemView);

        // --
        this.box = itemView.findViewById(R.id.box);
        this.title = itemView.findViewById(R.id.level);
        this.icon = itemView.findViewById(R.id.icon);
    }

    public ConstraintLayout getBox() {
        return box;
    }

    public TextView getTitle() {
        return title;
    }

    public ImageView getIcon() {
        return icon;
    }
}
