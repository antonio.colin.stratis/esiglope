package com.stratis.estaffing.Control.RatingStar;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.R;

import java.util.HashMap;

/**
 * Created by Erick Sanchez
 * Revision 1 - 14/10/20
 */

public class RatingStarComplete {

    public static final String RatingStarCompleteOne = "101";
    public static final String RatingStarCompleteTwo = "102";
    public static final String RatingStarCompleteThree = "103";
    public static final String RatingStarCompleteFour = "104";
    public static final String RatingStarCompleteFive = "105";

    private Context  context;
    private LinearLayout baseLayout;
    private View.OnClickListener listener;

    private int rate = 0;
    private ImageView starOne;
    private ImageView starTwo;
    private ImageView starThree;
    private ImageView starFour;
    private ImageView starFive;

    private HashMap<Integer, String> titles;

    public RatingStarComplete(LinearLayout layout, Context context)  {

        this.context  = context;
        this.baseLayout = layout;
        this.setup();
    }

    private void setup() {

        // --
        this.starOne = this.baseLayout.findViewById(R.id.starOne);
        this.starOne.setTag(RatingStarCompleteOne);

        this.starTwo = this.baseLayout.findViewById(R.id.starTwo);
        this.starTwo.setTag(RatingStarCompleteTwo);

        this.starThree = this.baseLayout.findViewById(R.id.starThree);
        this.starThree.setTag(RatingStarCompleteThree);

        this.starFour = this.baseLayout.findViewById(R.id.starFour);
        this.starFour.setTag(RatingStarCompleteFour);

        this.starFive = this.baseLayout.findViewById(R.id.starFive);
        this.starFive.setTag(RatingStarCompleteFive);

        // --
        this.titles = new HashMap<>();
        titles.put(1, this.context.getString(R.string.recruitment_request_candidate_rate_interview_star_one));
        titles.put(2, this.context.getString(R.string.recruitment_request_candidate_rate_interview_star_two));
        titles.put(3, this.context.getString(R.string.recruitment_request_candidate_rate_interview_star_three));
        titles.put(4, this.context.getString(R.string.recruitment_request_candidate_rate_interview_star_four));
        titles.put(5, this.context.getString(R.string.recruitment_request_candidate_rate_interview_star_five));
    }

    public void setRating(int rating) {

        this.rate = rating;

        // -- Turn off all
        this.starOne.setImageBitmap(App.getImage(this.context, "gray_star") );
        this.starTwo.setImageBitmap(App.getImage(this.context, "gray_star") );
        this.starThree.setImageBitmap(App.getImage(this.context, "gray_star") );
        this.starFour.setImageBitmap(App.getImage(this.context, "gray_star") );
        this.starFive.setImageBitmap(App.getImage(this.context, "gray_star") );

        // -- Check
        switch(this.rate) {

            case 1:
                this.starOne.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                break;

            case 2:
                this.starOne.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                this.starTwo.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                break;

            case 3:
                this.starOne.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                this.starTwo.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                this.starThree.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                break;

            case 4:
                this.starOne.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                this.starTwo.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                this.starThree.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                this.starFour.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                break;

            case 5:
                this.starOne.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                this.starTwo.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                this.starThree.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                this.starFour.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                this.starFive.setImageBitmap(App.getImage(this.context, "orange_calification_star") );
                break;
        }
    }

    public void setListener(View.OnClickListener listener) {

        this.listener = listener;
        this.starOne.setOnClickListener(this.listener);
        this.starTwo.setOnClickListener(this.listener);
        this.starThree.setOnClickListener(this.listener);
        this.starFour.setOnClickListener(this.listener);
        this.starFive.setOnClickListener(this.listener);
    }

    public int getRate() {
        return rate;
    }

    public LinearLayout getBaseLayout() {
        return baseLayout;
    }

    public HashMap<Integer, String> getTitles() {
        return titles;
    }

    public String geFeedbackRateDescription(Integer rateValue) {
        HashMap<Integer, String> values = new HashMap<Integer, String>();
        values.put(1, context.getString(R.string.feedback_rater_star_one));
        values.put(2, context.getString(R.string.feedback_rater_star_two));
        values.put(3, context.getString(R.string.feedback_rater_star_three));
        values.put(4, context.getString(R.string.feedback_rater_star_four));
        values.put(5, context.getString(R.string.feedback_rater_star_five));
        return values.get(rateValue);
    }
}