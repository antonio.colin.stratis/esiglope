package com.stratis.estaffing.Fragment.TalentFlow;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Adapter.Talent.FeedbackAdapter;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Modal.FeedbackModal;
import com.stratis.estaffing.Model.Feedback;
import com.stratis.estaffing.Model.Talent;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Talent.TalentService;

import org.json.JSONException;
import org.json.JSONObject;

public class TalentFeedbackFragment extends ABFragment {

    public Talent talent = null;
    private RecyclerView feedbackRecycler = null;
    private FeedbackAdapter adapter = null;
    private LinearLayoutManager feedbackLayout = null;
    public NestedScrollView scrollView = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // --
        this.mView = inflater.inflate(R.layout.fragment_talent_feedback, container, false);

        // -- Init views
        this.initViews();

        //
        return this.mView;
    }

    private void initViews() {

        /** DividerItemDecoration instance **/
        final DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.recycler_devider));

        /** LinearLayoutManager instances **/
        this.feedbackLayout = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        // --
        this.feedbackRecycler = this.mView.findViewById(R.id.feedbacksList);
        this.feedbackRecycler.getRecycledViewPool().setMaxRecycledViews(0, 4);
        this.feedbackRecycler.setLayoutManager(this.feedbackLayout);
        this.feedbackRecycler.addItemDecoration(itemDecorator);
        this.feedbackRecycler.setItemViewCacheSize(8);
        this.feedbackRecycler.setDrawingCacheEnabled(true);
        this.feedbackRecycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // --
        if (this.talent != null && this.talent.getFeedbacks() != null && this.talent.getFeedbacks().size() > 0) {

            // -- Adapter
            this.adapter = new FeedbackAdapter(this.talent.getFeedbacks(), getContext());

        } else {
            this.feedbackRecycler.setVisibility(View.GONE);
        }

        // --
        this.feedbackRecycler.setAdapter(this.adapter);

        // -- NEW FEEDBACK
        final RelativeLayout newFeedback = this.mView.findViewById(R.id.newFeedback);
        final TextInputEditText new_strengths_text = this.mView.findViewById(R.id.new_strengths_text);
        final TextInputEditText new_opportunities_text = this.mView.findViewById(R.id.new_opportunities_text);

        final RelativeLayout rSendButton = this.mView.findViewById(R.id.sendButton);
        final LoadingButton sendButton = new LoadingButton((RelativeLayout) rSendButton);
        sendButton.setText("Enviar mi feedback");
        sendButton.getButton().setTag(100);
        sendButton.getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String strengths = String.valueOf(new_strengths_text.getText());
                if (strengths.equals("")) {

                    // -- Show
                    final FeedbackModal modal = new FeedbackModal(getContext());
                    modal.setWarningEmpty();
                    modal.show();
                    return;
                }

                final String opportunities = String.valueOf(new_opportunities_text.getText());
                if (opportunities.equals("")) {

                    // -- Show
                    final FeedbackModal modal = new FeedbackModal(getContext());
                    modal.setWarningEmpty();
                    modal.show();
                    return;
                }

                // --
                sendFeedback(sendButton, strengths, opportunities, newFeedback);
            }
        });
        this.setViewAsSecondaryColor(rSendButton);
    }

    private void sendFeedback(final LoadingButton sendButton, final String strengths, final String opportunities, final RelativeLayout newFeedback) {
        // --
        sendButton.showLoading(true);
        // --
        JSONObject params = new JSONObject();
        try {

            params.put("authorId", Session.get(getContext()).getDataSessionId());
            params.put("talentId", talent.getId());
            params.put("strengths", strengths);
            params.put("opportunities", opportunities);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("DXGOP", "JSON ENVIADO: " + JsonBuilder.jsonToString(params));


        // --
        TalentService.sendFeedback(params, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Log.d("DXGOP", "SINGLE TALENT RESPONSE = " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);

                // -- Hide loader
                sendButton.showLoading(false);

                // --
                int code = res.optInt("code", 500);
                if (code == 201) {

                    // --
                    Bundle parameters = new Bundle();
                    final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
                    mFirebaseAnalytics.logEvent("Feedbacks_Enviados", parameters);

                    // --
                    newFeedback.setVisibility(View.GONE);

                    // -- Name
                    final Session session = Session.get(getContext());
                    final String ln = (!session.getLastName().equals("")) ? session.getLastName() : "";

                    // -- Creating new item
                    Feedback feedback = new Feedback();
                    feedback.setUserFullName( String.format("%s %s", session.getFirstName(), ln) );
                    feedback.setOpportunities(opportunities);
                    feedback.setStrengths(strengths);
                    feedback.setDate("Hoy");

                    // -- Show
                    final FeedbackModal modal = new FeedbackModal(getContext());
                    modal.setSuccess();
                    modal.show();

                    // --
                    if (talent.getFeedbacks() == null || talent.getFeedbacks().size() <= 0) {

                        talent.createFeedbacks(feedback);
                        adapter = new FeedbackAdapter(talent.getFeedbacks(), getContext());
                        feedbackRecycler.setAdapter(adapter);

                    } else {

                        // -- Delete last element if have more tha 4 elements
                        if (talent.getFeedbacks().size() >= 4) {
                            talent.getFeedbacks().remove(talent.getFeedbacks().size() - 1);
                        }

                        // --
                        talent.getFeedbacks().add(0, feedback);
                        adapter = new FeedbackAdapter(talent.getFeedbacks(), getContext());
                        feedbackRecycler.setAdapter(adapter);

                        // --
                        if (scrollView != null) {
                            scrollView.scrollTo(0, 1100);
                        }
                    }

                } else {

                    // -- Show
                    final FeedbackModal modal = new FeedbackModal(getContext());
                    modal.setWarning();
                    modal.show();
                }
            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                Log.d("DXGOP", "ERROR :: " + response.body + "\n CODDE :: " + response.statusCode);
                // --
                sendButton.showLoading(false);
                // --
                // -- Show
                final FeedbackModal modal = new FeedbackModal(getContext());
                modal.setWarning();
                modal.show();
            }
        });

    }
}