package com.stratis.estaffing.Fragment.DeleteAccount

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.R

class DeleteDisclaimerFragment: ABFragment(), View.OnClickListener {

    private lateinit var disclaimerCheck: ImageView
    private lateinit var buttonContinue: RelativeLayout
    private var isCheckmark: Boolean = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_delete_disclaimer, container, false)
        setHeader("")

        this.initViews()
        return this.mView
    }

    private fun initViews() {

        val buttonCancel: Button = mView.findViewById(R.id.footerCancelButton)
        buttonCancel.tag = "11"
        buttonCancel.setOnClickListener(this)

        disclaimerCheck = mView.findViewById(R.id.disclaimerCheck)
        disclaimerCheck.tag = "12"
        disclaimerCheck.setOnClickListener(this)

        buttonContinue = mView.findViewById(R.id.footerContinueButton)
        buttonContinue.tag = "13"
        buttonContinue.setOnClickListener(this)

    }

    @SuppressLint("ResourceAsColor")
    override fun onClick(v: View?) {
        when(v!!.tag.toString()) {
            "11" -> {
                this.activity?.finish()
                return
            }
            "12" -> {
                isCheckmark = !isCheckmark
                val image: Int = if (isCheckmark) R.drawable.checkbox_square_selected else R.drawable.checkbox_square_unselected
                disclaimerCheck.setImageResource(image)

                if (isCheckmark) {
                    setViewAsPrimaryColor(buttonContinue)
                } else {
                    setViewColor(buttonContinue, ContextCompat.getColor(mContext, R.color.talent_feedback_disabled))
                }
            }
            "13" -> {
                if (!isCheckmark) { return }
                val deleteReasonsFragment = DeleteReasonsFragment()
                this.continueSegue(deleteReasonsFragment)
            }
        }
    }
}