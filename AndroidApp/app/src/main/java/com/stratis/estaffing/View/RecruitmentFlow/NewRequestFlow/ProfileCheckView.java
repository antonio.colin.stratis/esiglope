package com.stratis.estaffing.View.RecruitmentFlow.NewRequestFlow;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class ProfileCheckView extends RecyclerView.ViewHolder {

    private ConstraintLayout box;
    private ImageView icon;
    private TextView title;
    private TextView content;
    private ImageButton editButton;

    public ProfileCheckView(View itemView) {

        // --
        super(itemView);

        // --
        this.box = itemView.findViewById(R.id.box);
        this.icon = itemView.findViewById(R.id.icon);
        this.title = itemView.findViewById(R.id.tiitle);
        this.content = itemView.findViewById(R.id.content);
        this.editButton = itemView.findViewById(R.id.editButton);
    }

    public ConstraintLayout getBox() {
        return box;
    }

    public TextView getTitle() {
        return title;
    }

    public ImageView getIcon() {
        return icon;
    }

    public TextView getContent() {
        return content;
    }

    public ImageButton getEditButton() {
        return editButton;
    }
}
