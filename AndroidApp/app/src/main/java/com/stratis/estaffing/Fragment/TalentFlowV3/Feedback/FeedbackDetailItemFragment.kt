package com.stratis.estaffing.Fragment.TalentFlowV3.Feedback

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isGone
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.ListButtonAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.RatingStar.RatingStarComplete
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.TalentFlowV3.Feedback
import com.stratis.estaffing.Model.TalentFlowV3.FeedbackDetail
import com.stratis.estaffing.R
import java.util.ArrayList

class FeedbackDetailItemFragment : ABFragment() {

    open var feedback: Feedback? = null

    var strengthsHardAdapter: ListButtonAdapter? = null
    var strengthsSoftAdapter: ListButtonAdapter? = null
    var strengthsHabitAdapter: ListButtonAdapter? = null
    var strengthsToolsAdapter: ListButtonAdapter? = null
    var opportunitiesHardAdapter: ListButtonAdapter? = null
    var opportunitiesSoftAdapter: ListButtonAdapter? = null
    var opportunitiesHabitAdapter: ListButtonAdapter? = null
    var opportunitiesToolsAdapter: ListButtonAdapter? = null

    var strengthsHardSubtitle: TextView? = null
    var strengthsSoftSubtitle: TextView? = null
    var strengthsHabitSubtitle: TextView? = null
    var strengthsToolsSubtitle: TextView? = null
    var opportunitiesHardSubtitle: TextView? = null
    var opportunitiesSoftSubtitle: TextView? = null
    var opportunitiesHabitSubtitle: TextView? = null
    var opportunitiesToolsSubtitle: TextView? = null

    var strengthsHardList: RecyclerView? = null
    var strengthsSoftList: RecyclerView? = null
    var strengthsHabitList: RecyclerView? = null
    var strengthsToolsList: RecyclerView? = null
    var opportunitiesHardList: RecyclerView? = null
    var opportunitiesSoftList: RecyclerView? = null
    var opportunitiesHabitList: RecyclerView? = null
    var opportunitiesToolsList: RecyclerView? = null

    var hardStrenghtsSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var softStrenghtsSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var habitsStrenghtsSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var toolsStrenghtsSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var hardOpportunitiesSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var softOpportunitiesSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var habitsOpportunitiesSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var toolsOpportunitiesSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mView = inflater.inflate(R.layout.fragment_talent_feedback_item_detail, container, false)

        setHeader("")
        initView()

        return mView
    }

    private fun initView(){
        mView.findViewById<TextView>(R.id.title).text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_title),feedback!!.id)

        // -- Stars
        val ratedContainerStars = RatingStarComplete(mView.findViewById(R.id.ratedContainerStars), context)
        ratedContainerStars.setRating(feedback!!.rateValue)

        mView.findViewById<TextView>(R.id.ratedDescription).text = ratedContainerStars.geFeedbackRateDescription(feedback!!.rateValue)
        mView.findViewById<TextView>(R.id.rateDateValue).text = App.getDateCourse(feedback!!.dateStr)
        mView.findViewById<TextView>(R.id.rateTimeValue).text = App.getDateFormatted(feedback!!.dateStr, "HH:mm", "yyyy-MM-dd HH:mm:ss")
        mView.findViewById<TextView>(R.id.raterValue).text = feedback!!.authorName // Hacer nombre corto
        mView.findViewById<TextView>(R.id.rateValue).text = feedback!!.id.toString()

//        mView.findViewById<TextView>(R.id.strengthsSubtitle).text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_strengths), feedback!!.strengths!!.size)
//
//        val strengths: RecyclerView = mView.findViewById(R.id.strengthsList)
//        App.createVerticalRecyclerList(strengths, context)
//        val strengthsAdapter= ListButtonAdapter(parseTalents(feedback!!.strengths))
//        strengths.adapter = strengthsAdapter

        mView.findViewById<TextView>(R.id.strengthsCommentValue).text = feedback!!.strengthsComment

//        mView.findViewById<TextView>(R.id.oportunitySubtitle).text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_oportunities), feedback!!.opportunities!!.size)
//
//        val opportunities: RecyclerView = mView.findViewById(R.id.oportunityList)
//        App.createVerticalRecyclerList(opportunities, context)
//        val opportunitiesAdapter= ListButtonAdapter(parseTalents(feedback!!.opportunities))
//        opportunities.adapter = opportunitiesAdapter

        mView.findViewById<TextView>(R.id.oportunityCommentValue).text = feedback!!.opportunitiesComment

        // Adapters
        strengthsHardList = mView.findViewById(R.id.strengthsHardList)
        strengthsHardSubtitle = mView.findViewById(R.id.strengthsHardSubtitle)
        strengthsSoftList = mView.findViewById(R.id.strengthsSoftList)
        strengthsSoftSubtitle = mView.findViewById(R.id.strengthsSoftSubtitle)
        strengthsHabitList = mView.findViewById(R.id.strengthsHabitList)
        strengthsHabitSubtitle = mView.findViewById(R.id.strengthsHabitSubtitle)
        strengthsToolsList = mView.findViewById(R.id.strengthsToolsList)
        strengthsToolsSubtitle = mView.findViewById(R.id.strengthsToolsSubtitle)

        App.createVerticalRecyclerList(strengthsHardList, context)
        App.createVerticalRecyclerList(strengthsSoftList, context)
        App.createVerticalRecyclerList(strengthsHabitList, context)
        App.createVerticalRecyclerList(strengthsToolsList, context)

        opportunitiesHardList = mView.findViewById(R.id.opportunitiesHardList)
        opportunitiesHardSubtitle = mView.findViewById(R.id.opportunitiesHardSubtitle)
        opportunitiesSoftList = mView.findViewById(R.id.opportunitiesSoftList)
        opportunitiesSoftSubtitle = mView.findViewById(R.id.opportunitiesSoftSubtitle)
        opportunitiesHabitList = mView.findViewById(R.id.opportunitiesHabitList)
        opportunitiesHabitSubtitle = mView.findViewById(R.id.opportunitiesHabitSubtitle)
        opportunitiesToolsList = mView.findViewById(R.id.opportunitiesToolsList)
        opportunitiesToolsSubtitle = mView.findViewById(R.id.opportunitiesToolsSubtitle)

        App.createVerticalRecyclerList(opportunitiesHardList, context)
        App.createVerticalRecyclerList(opportunitiesSoftList, context)
        App.createVerticalRecyclerList(opportunitiesHabitList, context)
        App.createVerticalRecyclerList(opportunitiesToolsList, context)

        updateView()
    }

    private fun parseTalents(catalogs: List<FeedbackDetail>): ArrayList<String> {
        var descriptionParsed: ArrayList<String> = ArrayList()
        catalogs.forEach{
            descriptionParsed.add(it.name)
        }
        return descriptionParsed
    }

    private fun updateView() {
        hardStrenghtsSkills = feedback!!.strengths!!.filter { it.categoryId == 1001100001L }
        if (hardStrenghtsSkills.count() > 0) {
            strengthsHardSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_hard),
                    hardStrenghtsSkills.count())
            strengthsHardAdapter = ListButtonAdapter(parseTalents(hardStrenghtsSkills))
            strengthsHardList?.adapter = strengthsHardAdapter
            strengthsHardList?.isGone = false
            strengthsHardSubtitle?.isGone = false
            strengthsHardAdapter?.notifyDataSetChanged()
        } else {
            strengthsHardList?.isGone = true
            strengthsHardSubtitle?.isGone = true
        }

        softStrenghtsSkills = feedback!!.strengths!!.filter { it.categoryId == 1001100002L }
        if (softStrenghtsSkills.count() > 0) {
            strengthsSoftSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_soft),
                    softStrenghtsSkills.count())
            strengthsSoftAdapter = ListButtonAdapter(parseTalents(softStrenghtsSkills))
            strengthsSoftList?.adapter = strengthsSoftAdapter
            strengthsSoftList?.isGone = false
            strengthsSoftSubtitle?.isGone = false
            strengthsSoftAdapter?.notifyDataSetChanged()
        } else {
            strengthsSoftList?.isGone = true
            strengthsSoftSubtitle?.isGone = true
        }

        habitsStrenghtsSkills = feedback!!.strengths!!.filter { it.categoryId == 1001100003L }
        if (habitsStrenghtsSkills.count() > 0) {
            strengthsHabitSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_habits),
                    habitsStrenghtsSkills.count())
            strengthsHabitAdapter = ListButtonAdapter(parseTalents(habitsStrenghtsSkills))
            strengthsHabitList?.adapter = strengthsHabitAdapter
            strengthsHabitList?.isGone = false
            strengthsHabitSubtitle?.isGone = false
            strengthsHabitAdapter?.notifyDataSetChanged()
        } else {
            strengthsHabitList?.isGone = true
            strengthsHabitSubtitle?.isGone = true
        }

        toolsStrenghtsSkills = feedback!!.strengths!!.filter { it.categoryId == 1001100004L }
        if (toolsStrenghtsSkills.count() > 0) {
            strengthsToolsSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_tools),
                    toolsStrenghtsSkills.count())
            strengthsToolsAdapter = ListButtonAdapter(parseTalents(toolsStrenghtsSkills))
            strengthsToolsList?.adapter = strengthsToolsAdapter
            strengthsToolsList?.isGone = false
            strengthsToolsSubtitle?.isGone = false
            strengthsToolsAdapter?.notifyDataSetChanged()
        } else {
            strengthsToolsList?.isGone = true
            strengthsToolsSubtitle?.isGone = true
        }

        hardOpportunitiesSkills = feedback!!.opportunities!!.filter { it.categoryId == 1001100001L }
        if (hardOpportunitiesSkills.count() > 0) {
            opportunitiesHardSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_hard),
                    hardOpportunitiesSkills.count())
            opportunitiesHardAdapter = ListButtonAdapter(parseTalents(hardOpportunitiesSkills))
            opportunitiesHardList?.adapter = opportunitiesHardAdapter
            opportunitiesHardList?.isGone = false
            opportunitiesHardSubtitle?.isGone = false
            opportunitiesHardAdapter?.notifyDataSetChanged()
        } else {
            opportunitiesHardList?.isGone = true
            opportunitiesHardSubtitle?.isGone = true
        }

        softOpportunitiesSkills = feedback!!.opportunities!!.filter { it.categoryId == 1001100002L }
        if (softOpportunitiesSkills.count() > 0) {
            opportunitiesSoftSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_soft),
                    softOpportunitiesSkills.count())
            opportunitiesSoftAdapter = ListButtonAdapter(parseTalents(softOpportunitiesSkills))
            opportunitiesSoftList?.adapter = opportunitiesSoftAdapter
            opportunitiesSoftList?.isGone = false
            opportunitiesSoftSubtitle?.isGone = false
            opportunitiesSoftAdapter?.notifyDataSetChanged()
        } else {
            opportunitiesSoftList?.isGone = true
            opportunitiesSoftSubtitle?.isGone = true
        }

        habitsOpportunitiesSkills = feedback!!.opportunities!!.filter { it.categoryId == 1001100003L }
        if (habitsOpportunitiesSkills.count() > 0) {
            opportunitiesHabitSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_habits),
                    habitsOpportunitiesSkills.count())
            opportunitiesHabitAdapter = ListButtonAdapter(parseTalents(habitsOpportunitiesSkills))
            opportunitiesHabitList?.adapter = opportunitiesHabitAdapter
            opportunitiesHabitList?.isGone = false
            opportunitiesHabitSubtitle?.isGone = false
            opportunitiesHabitAdapter?.notifyDataSetChanged()
        } else {
            opportunitiesHabitList?.isGone = true
            opportunitiesHabitSubtitle?.isGone = true
        }

        toolsOpportunitiesSkills = feedback!!.opportunities!!.filter { it.categoryId == 1001100004L }
        if (toolsOpportunitiesSkills.count() > 0) {
            opportunitiesToolsSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_tools),
                    toolsOpportunitiesSkills.count())
            opportunitiesToolsAdapter = ListButtonAdapter(parseTalents(toolsOpportunitiesSkills))
            opportunitiesToolsList?.adapter = opportunitiesToolsAdapter
            opportunitiesToolsList?.isGone = false
            opportunitiesToolsSubtitle?.isGone = false
            opportunitiesToolsAdapter?.notifyDataSetChanged()
        } else {
            opportunitiesToolsList?.isGone = true
            opportunitiesToolsSubtitle?.isGone = true
        }
    }


}