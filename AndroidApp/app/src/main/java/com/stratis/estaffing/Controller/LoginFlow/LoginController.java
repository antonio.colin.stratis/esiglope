package com.stratis.estaffing.Controller.LoginFlow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.stratis.estaffing.BuildConfig;
import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Loader;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Controller.MainFlow.HomeController;
import com.stratis.estaffing.Controller.RegisterFlow.ChooseTypeAccountController;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Preferences;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Login.LoginService;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoginController extends AppCompatActivity implements View.OnClickListener {

    private Loader loader;

    private TextInputLayout userLayout;
    private TextInputEditText userEditText;
    private TextInputLayout passwordLayout;
    private TextInputEditText passwordEditText;

    private LoadingButton loginButton;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // --
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_controller);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName());

        // -- Validate if have pending Push Notifications
        if (getIntent() != null) {

            String pendingPushNotification = "";
            if (getIntent().hasExtra("pendingPushNotification")) {
                pendingPushNotification = getIntent().getStringExtra("pendingPushNotification");
            } else if (getIntent().hasExtra("notificationBody")) {
                pendingPushNotification = getIntent().getStringExtra("notificationBody");
            }
            Log.i("DXGOP", "STEP 1 : " + pendingPushNotification);
            if (!pendingPushNotification.equals("")) {
                new Preferences(this).savePreference("pendingPushNotification", pendingPushNotification);
            }
        }

        // -- Init views
        this.initViews();

        // -- Check version status -- ONLY IN PROD
        this.checkSystemStatus();

        // -- Showing loader
        this.loader = new Loader(this);
        this.runOnUiThread(() -> {
            if (!this.loader.isShowing()) { this.loader.show(); }
        });

        // -- Showing intent notifications
        this.showIntentNotification();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        if (this.loader != null) {
            this.loader.cancel();
        }
    }

    private void initViews() {

        // --
        this.userLayout = this.findViewById(R.id.user);
        this.userEditText = this.findViewById(R.id.userText);
        this.passwordLayout = this.findViewById(R.id.password);
        this.passwordEditText = this.findViewById(R.id.passwordText);

        // -- ONLY FOR DEV
        if (Api.getInstance().getFlavor().equals(Api.DEVFlavor)) {
            this.userEditText.setText( BuildConfig.devUser );
            this.passwordEditText.setText( BuildConfig.devPass );
        }

        // --
        this.loginButton = new LoadingButton((RelativeLayout) this.findViewById(R.id.loginButtonInit));
        this.loginButton.setText(this.getResources().getString(R.string.login_login_button));
        this.loginButton.getButton().setTag(10);
        this.loginButton.getButton().setOnClickListener(this);

        // --
        final Button registerButton = this.findViewById(R.id.registerButton);
        registerButton.setTag(12);
        registerButton.setOnClickListener(this);

        // --
        final AutoResizeTextView recoverPassword = this.findViewById(R.id.recoverPassword);
        recoverPassword.setTag(11);
        recoverPassword.setOnClickListener(this);
    }

    private void showIntentNotification() {

        if (getIntent().hasExtra("notification")) {

            final String nt = getIntent().getStringExtra("notification");
            if (nt.equals("reLogin")) {

                final Notification notification = new Notification(this);
                notification.setTitle(getResources().getString(R.string.login_re_login_title_notification));
                notification.setMessage(getResources().getString(R.string.login_re_login_sub_title_notification));
                notification.show();
            }
        }
    }

    private void login() {

        // -- User
        String user = String.valueOf(this.userEditText.getText());
        user = App.trimSpaces(user);
        if (user.equals("")) {
            this.showError(true);
            return;
        }

        // -- Password
        String password = String.valueOf(this.passwordEditText.getText());
        password = App.trimSpaces(password);
        if (password.equals("")) {
            this.showError(true);
            return;
        }

        // --
        this.showError(false);
        this.loginButton.showLoading(true);

        // --
        LoginService.login(user, password, new ReSTCallback() {

            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Log.d("DXGOP", "LOGIN RESPONSE = " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);

                // --
                if (res != null) {

                    int code = res.optInt("code", 500);
                    if (code == 100) {

                        // --
                        JSONObject entity = res.optJSONObject("entity");
                        session = Session.get(LoginController.this);
                        session.login(entity);
                        checkSession(session);
                    } else {

                        // --
                        showError(true);
                        // --
                        loginButton.showLoading(false);
                    }
                } else {

                    // --
                    showError(true);
                    // --
                    loginButton.showLoading(false);
                }
            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                Log.d("DXGOP", "ERROR :: " + response.body);
                // --
                showError(true);
                // --
                loginButton.showLoading(false);
            }
        });
    }

    private void checkSystemStatus() {

        LoginService.getSystemStatus(new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                for(int i = 0; i < res.length(); i++) {

                    JSONObject temp = res.optJSONObject(i);
                    if (temp != null) {
                        final int platform = temp.optInt("platform", 0);
                        if (platform == 1) { // -- Android
                           validateSystemStatus(temp);
                        }
                    }
                }
            }

            @Override
            public void onError(ReSTResponse response) {}
        });
    }

    private void validateSystemStatus(JSONObject system) {

        final Session session = Session.get(this);
        final String appPackageName = getPackageName();
        final String minimumVersion = system.optString("minimumVersion");
        final String currentVersion = system.optString("currentVersion");
        final String version = BuildConfig.VERSION_NAME;

        //minimumVersion = "1.8.108";
        if (!Api.getInstance().isDev()) {

            if (version.equals(currentVersion)) {

                if (session.isLogged()) { checkSession(session); }
                else { if (this.loader.isShowing()) { this.loader.cancel(); } }

            } else if (version.equals(minimumVersion)) {

                App.createDialogOfNewVersion(this,
                        "Existe una nueva versión de eStratis",
                        "Hay una nueva versión disponible ¿Deseas instalarla?",
                        (dialog, which) -> checkSession(session),
                        (dialog, which) -> {

                            if (this.loader.isShowing()) { this.loader.cancel(); }
                            App.redirectToPlayStore(LoginController.this);
                            dialog.cancel();
                        }).
                        show();
            } else {

                App.createDialogOfNecessaryNewVersion(this,
                        "eStratis requiere una nueva actualización",
                        "Para seguir utilizando eStratis, es necesario descargar la última versión.").
                        show();
            }
        } else {
            if (session.isLogged()) { checkSession(session); }
            else { if (this.loader.isShowing()) { this.loader.cancel(); } }
        }
    }

    private void checkSession(final Session session) {

        LoginService.refreshToken(session, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                Log.d("DXGOP", "REFRESH TOKEN = " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);
                if (res != null) {
                    session.setIdToken(res.optString("entity"));
                }
            }

            @Override
            public void onError(ReSTResponse response) {
                Log.d("DXGOP", "CHECK REFRESH TOKEN ERROR :: " + response.statusCode);
            }
        });

        LoginService.checkDataSession(session, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                Log.d("DXGOP", "CHECK DATA SESSION RESPONSE = " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);
                if (res != null) {

                    if (!res.has("code")) {

                        session.setDataSessionObj(res);
                        gotoHomeController();
                    }
                }
            }

            @Override
            public void onError(ReSTResponse response) {

                Log.d("DXGOP", "CHECK DATA SESSION ERROR :: " + response.statusCode);
                session.logout();
                if (response.statusCode == 404) {
                    showError(true);
                }
                loginButton.hideLoading();
                if (loader != null) {
                    loader.cancel();
                }
            }
        });
    }

    private void gotoHomeController() {

        Intent intent = new Intent(LoginController.this, HomeController.class);
        startActivity(intent);
        finishAffinity();
    }

    private void showError(boolean state) {

        // --
        final AutoResizeTextView userTitle = this.findViewById(R.id.userTitle);
        final AutoResizeTextView passwordTitle = this.findViewById(R.id.passwordTitle);
        final AutoResizeTextView passwordTitleError = this.findViewById(R.id.passwordTitleError);

        // --
        if (state) {

            // --
            userTitle.setTextColor(this.getResources().getColor(R.color.red));
            passwordTitle.setTextColor(this.getResources().getColor(R.color.red));
            this.userLayout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_error_border));
            this.passwordLayout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_error_border));

            // --
            passwordTitleError.setVisibility(View.VISIBLE);
            passwordTitleError.setText(getResources().getString(R.string.login_generic_error));

        } else {

            // --
            userTitle.setTextColor(this.getResources().getColor(R.color.hint));
            passwordTitle.setTextColor(this.getResources().getColor(R.color.hint));
            this.userLayout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_border));
            this.passwordLayout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_border));

            // --
            passwordTitleError.setVisibility(View.GONE);
            passwordTitleError.setText("");
        }
    }

    @Override
    public void onClick(View v) {

        final Integer tag = (Integer) v.getTag();
        switch (tag) {

            case 10:
                this.login();
                //throw new RuntimeException("Test Login Crash");
                break;

            case 11:
                startActivity(new Intent(this, RecoverController.class));
                break;

            case 12:
                startActivity(new Intent(this, ChooseTypeAccountController.class));
                break;
        }
    }
}