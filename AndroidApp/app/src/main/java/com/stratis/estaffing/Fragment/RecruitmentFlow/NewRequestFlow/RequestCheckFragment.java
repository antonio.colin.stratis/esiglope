package com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Adapter.RecruitmentFlow.ProfileCheckAdapter;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Control.eStaffingFragment;
import com.stratis.estaffing.Controller.RecruitmentFlow.RecruitmentFlowEditorController;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.Model.RecruitmentFlow.ProfileCheck;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 08/09/20
 */

public class RequestCheckFragment extends eStaffingFragment {

    private ProfileCheckAdapter adapter;

    public RequestCheckFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        this.mView = inflater.inflate(R.layout.fragment_request_check, container, false);
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName());

        this.initViews();

        // --
        return this.mView;
    }

    @SuppressLint("SetTextI18n")
    private void initViews() {

        // -- Checks Table
        RecyclerView checksTable = this.mView.findViewById(R.id.checksTable);
        LinearLayoutManager layout = new LinearLayoutManager(this.mContext, LinearLayoutManager.VERTICAL, false);

        // --
        checksTable.getRecycledViewPool().setMaxRecycledViews(0, 10);
        checksTable.setLayoutManager(layout);
        checksTable.setItemViewCacheSize(10);
        checksTable.setDrawingCacheEnabled(true);
        checksTable.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // --
        this.createProfileAdapter();
        checksTable.setAdapter(this.adapter);
    }

    private void createProfileAdapter() {

        this.adapter = new ProfileCheckAdapter(ProfileCheck.getRequestSelectedItems(this.getProfile(), this.mContext), this.mContext);
        this.adapter.setListener(position -> {

            RequestCheckEditFragment.RequestCheckEditType type = null;
            switch (position)  {

                case 0:
                    type = RequestCheckEditFragment.RequestCheckEditType.vacancies;
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    type = RequestCheckEditFragment.RequestCheckEditType.profile;
                    break;
                case 6:
                    type = RequestCheckEditFragment.RequestCheckEditType.language;
                    break;
                case 7:
                case 8:
                case 9:
                case 10:
                    type = RequestCheckEditFragment.RequestCheckEditType.modality;
                    break;
                case 11:
                case 12:
                case 13:
                    type = RequestCheckEditFragment.RequestCheckEditType.budget;
                    break;
            }
            final Intent intent = new Intent(mContext, RecruitmentFlowEditorController.class);
            intent.putExtra("type", type);
            intent.putExtra("profile", getProfile());
            intent.putExtra("position", position);
            activityResultLauncher.launch(intent);
        });
    }

    // You can do the assignment inside onAttach or onCreate, i.e, before the activity is displayed
    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
        new ActivityResultContracts.StartActivityForResult(),
        new ActivityResultCallback<ActivityResult>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onActivityResult(ActivityResult result) {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    assert data != null;
                    setProfile((Profile) data.getSerializableExtra("profile"));
                    adapter.reload(ProfileCheck.getRequestSelectedItems(getProfile(), mContext));
                    adapter.notifyDataSetChanged();

                    // --
                    final Notification notification = new Notification(getContext(), R.layout.notification_light);
                    notification.setMessage(mContext.getString(R.string.request_vacancy_check_notification_text));
                    notification.show(1500);
                }
            }
        }
    );

}
