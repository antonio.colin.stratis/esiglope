package com.stratis.estaffing.Control;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.stratis.estaffing.R;

import java.util.Objects;

/**
 * Created by Erick Sanchez
 * Revision 1 - 27/05/20
 */
public class Loader extends Dialog {

    public Loader(@NonNull Context context) {

        // --
        super(context);

        // --
        this.init();
    }

    public Loader(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this.init();
    }

    protected Loader(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.init();
    }

    private void init() {

        this.setContentView(R.layout.screen_loader);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        //this.getWindow().setLayout(sv.getX(), sv.getY() - sv.threeRuleY(700));
        //Objects.requireNonNull(this.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ((ProgressBar) this.findViewById(R.id.progress_bar)).getIndeterminateDrawable().setColorFilter(
                this.getContext().getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);

        // --
        Window window = this.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}