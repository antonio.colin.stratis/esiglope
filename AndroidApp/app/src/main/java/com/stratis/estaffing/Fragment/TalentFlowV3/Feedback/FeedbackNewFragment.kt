package com.stratis.estaffing.Fragment.TalentFlowV3.Feedback

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.TalentV3.FeedbackSkillsAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Control.Loader
import com.stratis.estaffing.Control.RatingStar.RatingStarComplete
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.FirebaseEvent
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Model.CatalogItem
import com.stratis.estaffing.Model.TalentFlowV3.FeedbackDetail
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.CatalogService

class FeedbackNewFragment: ABFragment(), View.OnClickListener {

    var type: FeedbackType? = null
    var talentId: String? = null
    var talentName: String? = null
    var rate: Int? = null
    var rateDescription: String? = null

    private var loader : Loader? = null

    private var title : TextView? = null
    private var subTitle : TextView? = null
    private var comment : TextView? = null
    private var commentValue : TextView? = null
    private var continueButton : Button? = null
    private var catItems: ArrayList<CatalogItem> = ArrayList()
    private var skills: LinkedHashMap<CatalogItem, ArrayList<FeedbackDetail>> = LinkedHashMap()
    var selectedSkills: ArrayList<FeedbackDetail> = ArrayList()
    var allSkills: ArrayList<FeedbackDetail>? = null
    private var count: Int = 0
    private val maxSkills: Int = 5

    var strenghtsSkills: ArrayList<FeedbackDetail> = ArrayList()
    var opportunitiesSkills: ArrayList<FeedbackDetail> = ArrayList()
    var strenghtsComment: String? = ""
    var opportunitiesComment: String? = ""
    var isEdit = false

    private var listener: FeedbackConfirmListener? = null

    private var skillListAdapter: FeedbackSkillsAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mView = inflater.inflate(R.layout.fragment_talent_feedback_new, container, false)
        FirebaseEvent.logEvent(mContext, this.javaClass.simpleName.plus(type.toString()))

        setHeader("")
        loader = Loader(mContext)
        loader!!.show()
        getSkillList()
        return mView
    }

    private fun initView() {
        title = mView.findViewById(R.id.title)
        subTitle = mView.findViewById(R.id.subtitle)
        comment = mView.findViewById(R.id.comment)
        commentValue = mView.findViewById(R.id.commentValue)
        if(type!! == FeedbackType.STRENGTH) {
            initStrengths()
        } else if (type!! == FeedbackType.OPORTUNITY) {
            initOportunities()
        }
    }

    private fun initButtons() {
        continueButton = mView.findViewById(R.id.continueButton)
        continueButton?.tag = if (this.type!! == FeedbackType.STRENGTH) "1" else ( if (this.type!! == FeedbackType.OPORTUNITY) "2" else "0")
        continueButton?.setOnClickListener(this)
        checkIfButtonEnabled()
        val cancelButton: Button = mView.findViewById(R.id.backButton)
        cancelButton.tag = "3"
        cancelButton.setOnClickListener(this)
    }

    private fun initStrengths() {
        title?.text = getString(R.string.feedback_fragment_strengths_title)
        title?.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22f)
        subTitle?.text = App.fromHtml(String.format(getString(R.string.feedback_fragment_new_strength_subtitle), App.capitalize(this.talentName)))
        mView.findViewById<ImageView>(R.id.strengthsIcon).visibility = View.VISIBLE
        comment?.text = getString(R.string.feedback_fragment_detail_item_comment)
                .plus(" ").plus(getString(R.string.feedback_fragment_detail_item_about)).plus(" ")
                .plus( getString(R.string.feedback_fragment_strengths_title))
        commentValue?.hint = String.format(
                getString(R.string.feedback_fragment_detail_item_comment_hint),
                getString(R.string.feedback_fragment_strengths_title))
        val rateTitle: TextView = mView.findViewById(R.id.txtFeedbackRateTitle)
        rateTitle.visibility = View.VISIBLE
        rateTitle.text = String.format(getString(R.string.feedback_fragment_q_talent_star_rate), App.capitalize(this.talentName))
        mView.findViewById<RelativeLayout>(R.id.makedContainer).visibility = View.VISIBLE
        val ratedContainerStars = RatingStarComplete(mView.findViewById(R.id.makedContainerStars), context)
        ratedContainerStars.setRating(this.rate!!)
        mView.findViewById<TextView>(R.id.txtFeedbackRateStarsSubTitle).text = this.rateDescription

        if (isEdit) { commentValue?.text = strenghtsComment }
    }

    private fun initOportunities() {
        title?.text = getString(R.string.feedback_fragment_opportunities_title)
        title?.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32f)
        title?.layoutParams?.width = ViewGroup.LayoutParams.MATCH_PARENT
        subTitle?.text = App.fromHtml(String.format(getString(R.string.feedback_fragment_new_oportunity_subtitle), this.talentName))
        comment?.text = getString(R.string.feedback_fragment_detail_item_comment)
                .plus(" ").plus(getString(R.string.feedback_fragment_detail_item_about)).plus(" ")
                .plus(getStr(R.string.feedback_fragment_opportunities_title))
        commentValue?.hint = String.format(
                getString(R.string.feedback_fragment_detail_item_comment_hint),
                getString(R.string.feedback_fragment_opportunities_title))
        if (isEdit) { commentValue?.text = opportunitiesComment }
    }

    private fun initSkillListAdapter() {
        catItems.forEach {
            skills[it] = ArrayList()
        }
        catItems.forEach {
            selectedSkills.forEach { it2 ->
                if (it2.categoryId == it.catalogItemId) {
                    skills[it]!!.add(it2)
                }
            }
        }
        val skillList: RecyclerView = mView.findViewById(R.id.skillList)
        App.createVerticalRecyclerList(skillList, context)
        skillListAdapter= FeedbackSkillsAdapter(context, skills)
        skillListAdapter!!.setListener(object : FeedbackSkillsAdapter.FeedbackSkillClickListener {
            override fun onClick(catalogItem: CatalogItem) {
                showCatalogSkills(catalogItem)
            }
            override fun chooseSkills(): Boolean {
                return count < maxSkills
            }
            override fun refreshButton() {
                checkIfButtonEnabled()
            }
            override fun onDelete(skill: FeedbackDetail) {
                val newSelected = selectedSkills.filter { !(it.categoryId == skill.categoryId
                        && it.name == skill.name
                        && it.id == skill.id) }
                selectedSkills = newSelected as ArrayList<FeedbackDetail>

                updateSkills()
            }
        })
        skillList.adapter = skillListAdapter
    }

    private fun showCatalogSkills(catalogItem: CatalogItem) {
        val feedbackSelectionFragment = FeedbackSelectionFragment()
        feedbackSelectionFragment.talentId = this.talentId
        feedbackSelectionFragment.category = catalogItem
        feedbackSelectionFragment.isStrenghts = type == FeedbackType.STRENGTH
        val onlySkills: ArrayList<FeedbackDetail> = ArrayList()
        allSkills?.forEach {
            if (it.categoryId == catalogItem.catalogItemId) {
                onlySkills.add(it)
            }
        }
        Log.d("DXGOP", "Gettin all skill tags with category $onlySkills")
        feedbackSelectionFragment.skills = onlySkills

        feedbackSelectionFragment.selectedSkills = selectedSkills.toMutableList() as ArrayList<FeedbackDetail>
        feedbackSelectionFragment.setListener(object: FeedbackSelectionFragment.FeedbackSelectedListener {
            override fun onSkillSelected(selectedSkills: ArrayList<FeedbackDetail>) {
                this@FeedbackNewFragment.selectedSkills = selectedSkills
                updateSkills()
            }

        })
        this.continueSegue(feedbackSelectionFragment)
    }

    private fun addKeyListenerComment() {
        val commentLetter = mView.findViewById<TextView>(R.id.commentLetter)
        mView.findViewById<EditText>(R.id.commentValue).addTextChangedListener( object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                commentLetter.text =  String.format("%d/%d", s?.length, 300)
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })
    }

    @SuppressLint("ResourceAsColor")
    private fun checkIfButtonEnabled() {
        this.count = 0
        for((_,v) in this.skills){
            if(v.isNotEmpty()) count += v.size
        }
        if(count > 0) {
            continueButton?.isEnabled = true
            setViewAsPrimaryColor(continueButton)
        } else {
            continueButton?.isEnabled = false
            val drawable = continueButton!!.background
            drawable.setTint(R.color.talent_feedback_disabled)
        }
    }

    private fun getSkillList() {
        CatalogService.findById("1000010011", object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {
                val res = JsonBuilder.stringToJsonArray(response.body)
                catItems = CatalogItem.parse(res)!!
                Log.d("DXGOP", "catItems $catItems")
                initSkillListAdapter()
                initView()
                initButtons()
                addKeyListenerComment()
                loader!!.cancel()
            }
            override fun onError(response: ReSTResponse) {
                Log.d("DXGOP", "ERROR :: " + response.body)
                loader!!.cancel()
            }
        })
    }

    private fun updateSkills() {
        catItems.forEach {
            skills[it]!!.clear()
            selectedSkills.forEach { it2 ->
                if (it2.categoryId == it.catalogItemId) {
                    skills[it]!!.add(it2)
                }
            }
        }
        skillListAdapter!!.notifyDataSetChanged()
        checkIfButtonEnabled()
    }

    enum class FeedbackType {
        STRENGTH, OPORTUNITY
    }

    override fun onClick(v: View?) {
        when (v!!.tag.toString()) {
            "1" -> {
                val commentValue = commentValue?.text.toString()
                if (isEdit) {
                    listener?.onEdit(selectedSkills, commentValue)
                    back()
                    return
                }

                val feedbackNewOportunityFragment = FeedbackNewFragment()
                feedbackNewOportunityFragment.type = FeedbackType.OPORTUNITY
                feedbackNewOportunityFragment.talentId = this.talentId
                feedbackNewOportunityFragment.talentName = this.talentName
                feedbackNewOportunityFragment.allSkills = allSkills
                feedbackNewOportunityFragment.strenghtsComment = commentValue
                feedbackNewOportunityFragment.strenghtsSkills = selectedSkills
                feedbackNewOportunityFragment.rate = rate
                this.continueSegue(feedbackNewOportunityFragment)
            }
            "2" -> {
                val commentValue = commentValue?.text.toString()
                if (isEdit) {
                    listener?.onEdit(selectedSkills, commentValue)
                    back()
                    return
                }

                val confirmFragment = FeedbackConfirmFragment()
                confirmFragment.strenghtsSkills = strenghtsSkills
                confirmFragment.opportunitiesSkills = selectedSkills
                confirmFragment.strenghtsComment = strenghtsComment
                confirmFragment.opportunitiesComment = commentValue
                confirmFragment.rate = rate
                confirmFragment.talentName = talentName
                confirmFragment.talentId = talentId
                confirmFragment.skills = allSkills
                this.continueSegue(confirmFragment)
            }
            "3" -> backRemove(this)
        }
    }

    fun setListener(listener: FeedbackConfirmListener) {
        this.listener = listener
    }

    interface FeedbackConfirmListener {
        fun onEdit(selectedSkills: ArrayList<FeedbackDetail>, comment: String)
    }

}