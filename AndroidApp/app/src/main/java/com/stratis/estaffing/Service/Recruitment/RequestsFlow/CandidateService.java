package com.stratis.estaffing.Service.Recruitment.RequestsFlow;

import android.util.Log;

import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Core.eStaffingClient;
import com.stratis.estaffing.Model.MasterAdminFlow.MARequestCandidate;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidateMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Erick Sanchez
 * Revision 1 - 28/12/20
 */

public class CandidateService {

    public static void setFavoriteStatusToCandidate(final RequestCandidate candidate,
                                                    final Session session, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/candidate/favorite");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("id", "0");
            body.put("vacancyId", candidate.getVacancyId());
            body.put("talentId", candidate.getTalentId());
            body.put("status", !candidate.getFavorite());
            body.put("userId", session.getDataSessionId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "STATUS SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getCandidateEducation(final String candidateId, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/candidate/" + candidateId + "/education");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getCandidateWorkExperience(final String candidateId,final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/candidate/" + candidateId + "/experience");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getCandidateLanguages(final String candidateId, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/candidate/" + candidateId + "/video");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void candidateWithNoInterest(final RequestCandidate candidate, String cause,
                                               final Session session, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/candidate/updateStatus");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_PUT, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("talentId", candidate.getTalentId());
            body.put("userId", session.getDataSessionId());
            body.put("vacancyId", candidate.getVacancyId());
            body.put("cause", cause);
            body.put("candidateStatusId", RequestCandidate.statusCandidateNotInterest);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "STATUS SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void updateStatus(final RequestCandidate candidate, String cause, String status,
                                    final Session session, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/candidate/updateStatus");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_PUT, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("talentId", candidate.getTalentId());
            body.put("userId", session.getDataSessionId());
            body.put("vacancyId", candidate.getVacancyId());
            body.put("cause", cause);
            body.put("candidateStatusId", status);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "STATUS SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getMessages(final RequestCandidate candidate, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/message/find/" + candidate.getVacancyCandidateId());
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void sendMessage(final RequestCandidateMessage message, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/message/save");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("vacancyCandidateId", message.getVacancyCandidateId());
            body.put("message", message.getMessage());
            body.put("authorId", message.getAuthorId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "MESSAGE SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void readMessage(final RequestCandidateMessage message, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/message/updateViewed");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_PUT, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("id", message.getId());
            body.put("viewed", true);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "MESSAGE SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    /**
     * Thwe difference between this service and Talent Share CV service is that this service share
     * all CV's of the candidates and the other one just 1 CV
     * **/
    public static void shareCV(final Session session, final String requestId, final String email, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/candidate/" + requestId + "/sendcv");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("email", email);
        request.addParameter("userId", session.getDataSessionId());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }
}