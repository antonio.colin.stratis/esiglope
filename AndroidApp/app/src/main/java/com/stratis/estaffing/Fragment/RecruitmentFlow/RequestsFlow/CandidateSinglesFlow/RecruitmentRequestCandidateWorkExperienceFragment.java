package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow;

import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate.RequestCandidateWorkExperienceAdapter;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateWorkExperienceModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.CandidateService;

import org.json.JSONArray;

import java.util.ArrayList;

public class RecruitmentRequestCandidateWorkExperienceFragment extends ABFragment {

    private RequestCandidate candidate;

    public RecruitmentRequestCandidateWorkExperienceFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_candidate_work_experience, container, false);

        this.setHeader("");
        this.showLoader();
        this.getWorkExperience();

        return this.mView;
    }

    private void getWorkExperience() {

        CandidateService.getCandidateWorkExperience(this.candidate.getId(), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                hideLoader();
                final JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                parseWorkExperience(res);
            }

            @Override
            public void onError(ReSTResponse response) {
                hideLoader();
            }
        });
    }

    private void parseWorkExperience(JSONArray res) {

        final ArrayList<CandidateWorkExperienceModel> experienceModels = CandidateWorkExperienceModel.parse(res);
        final RecyclerView workExperienceList = this.mView.findViewById(R.id.workExperienceList);
        App.createVerticalRecyclerList(workExperienceList, getContext());
        final RequestCandidateWorkExperienceAdapter adapter = new RequestCandidateWorkExperienceAdapter(experienceModels);
        workExperienceList.setAdapter(adapter);
    }

    public void setCandidate(RequestCandidate candidate) {
        this.candidate = candidate;
    }
}