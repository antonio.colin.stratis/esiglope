package com.stratis.estaffing.Controller.LoginFlow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTClient;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.R;

import org.json.JSONObject;

public class RecoverController extends AppCompatActivity implements View.OnClickListener {

    private TextInputLayout userLayout;
    private TextInputEditText userEditText;
    private LoadingButton recoverButton;
    private Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // --
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_controller);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName());

        // --
        this.initViews();
    }

    private void initViews() {

        // --
        this.userLayout = this.findViewById(R.id.user);
        this.userEditText = this.findViewById(R.id.userText);

        // --
        this.recoverButton = new LoadingButton((RelativeLayout) this.findViewById(R.id.recoverButton));
        this.recoverButton.setText(getResources().getString(R.string.recover_recover_button));
        this.recoverButton.getButton().setTag(10);
        this.recoverButton.getButton().setOnClickListener(this);

        this.cancelButton = this.findViewById(R.id.cancelButton);
        this.cancelButton.setTag(11);
        this.cancelButton.setOnClickListener(this);
    }

    private void recover() {

        // --
        this.showError(false);
        this.enableLoginButton(false);

        // -- User
        final String user = String.valueOf(this.userEditText.getText());
        if (user.equals("") || !App.isEmailValid(user)) {

            this.showError(true);
            this.enableLoginButton(true);
            return;
        }

        // -- Login logic
        final String url = Api.getInstance().getLoginUrl("/resetPassword");
        ReSTClient rest = new ReSTClient(url);
        final ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addParameter("username", user);

        // --
        request.addHeader("Accept", "application/json");
        request.addHeader("Content-Type", "application/json");

        // --
        rest.execute(request, new ReSTCallback() {

            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Log.d("DXGOP", "RECOVER RESPONSE = " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);

                // --
                if (res != null) {

                    int code = res.optInt("code", 500);
                    if (code == 100) {

                        //JSONObject data = res.getJSONObject("data");
                        Intent intent = new Intent(RecoverController.this, ResetController.class);
                        intent.putExtra("user", user);
                        intent.putExtra("notification", "reset");
                        startActivity(intent);
                        finish();

                    } else {

                        // --
                        showError(true);
                        // --
                        enableLoginButton(true);
                    }
                } else {

                    // --
                    showError(true);
                    // --
                    enableLoginButton(true);
                }
            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                Log.d("DXGOP", "ERROR :: " + response.body);
                // --
                showError(true);
                // --
                enableLoginButton(true);
            }
        });
    }

    private void enableLoginButton(boolean state) {

        // --
        this.recoverButton.showLoading(!state);

        // --
        this.cancelButton.setEnabled(state);
        this.cancelButton.setClickable(state);
    }

    private void showError(boolean state) {

        // --
        final AutoResizeTextView userTitle = this.findViewById(R.id.userTitle);
        final AutoResizeTextView userTitleError = this.findViewById(R.id.userTitleError);

        // --
        if (state) {

            // --
            userTitle.setTextColor(this.getResources().getColor(R.color.red));
            this.userLayout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_error_border));

            // --
            userTitleError.setVisibility(View.VISIBLE);
            userTitleError.setText(getResources().getString(R.string.reset_generic_error));

        } else {

            // --
            userTitle.setTextColor(this.getResources().getColor(R.color.hint));
            this.userLayout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_border));

            // --
            userTitleError.setVisibility(View.GONE);
            userTitleError.setText("");
        }
    }

    // -- Click Listener
    @Override
    public void onClick(View v) {

        // --
        v.setEnabled(false);
        v.setClickable(false);

        // --
        final Integer tag = (Integer) v.getTag();
        switch (tag) {

            case 10:
                recover();
                break;

            case 11:
                this.finish();
                break;
        }
    }
}