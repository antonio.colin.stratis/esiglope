package com.stratis.estaffing.Fragment.TalentFlowV3.Feedback

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.TalentV3.FeedbackSkillAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.Loader
import com.stratis.estaffing.Control.SwipeDelete.SwipeToDeleteCallback
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Model.CatalogItem
import com.stratis.estaffing.Model.TalentFlowV3.FeedbackDetail
import com.stratis.estaffing.R
import org.json.JSONObject

class FeedbackSelectionFragment : ABFragment(), View.OnClickListener {

    var talentId: String? = null
    var talentName: String? = null
    var category : CatalogItem ? = null
    var isStrenghts: Boolean = false
    var skills: ArrayList<FeedbackDetail> = ArrayList()
    var selectedSkills: ArrayList<FeedbackDetail> = ArrayList()

    private var listener: FeedbackSelectedListener? = null

    var buttonContainer: ConstraintLayout? = null
    var buttonTextView: TextView? = null
    var remainingTextView: TextView? = null
    var adapter: FeedbackSkillAdapter? = null
    var skillsData: MutableList<FeedbackDetail>? = emptyArray<FeedbackDetail>().toMutableList()
    var loader: Loader? = null
    val REMAINING_SKILL = 5
    var isActive = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.mView = inflater.inflate(R.layout.fragment_feedback_selection, container, false)

        setHeader("")
        initView()

        return mView
    }

    private fun initView() {
        val addNewSkill: Button = mView.findViewById(R.id.addNewSkill)
        val titleSelection: TextView = mView.findViewById(R.id.titleSelection)
        val descriptionSelection: TextView = mView.findViewById(R.id.descriptionSelection)
        remainingTextView = mView.findViewById(R.id.remainingTextView)
        buttonContainer = mView.findViewById(R.id.buttonContainer)
        buttonTextView = mView.findViewById(R.id.buttonTextView)

        setViewAsPrimaryColor(remainingTextView)
        val textDescCategory = if(isStrenghts) getString(R.string.feedback_fragment_strengths_title)
        else getString(R.string.feedback_fragment_opportunities_title)
        descriptionSelection.text = if(isStrenghts) getString(R.string.feedback_fragment_selection_strenghts)
        else getString(R.string.feedback_fragment_selection_oportinitys)
        titleSelection.text = category!!.description

        addNewSkill.tag = "10"
        addNewSkill.setOnClickListener(this)
        buttonContainer?.tag = "11"
        buttonContainer?.setOnClickListener(this)

        setSkills()
        updateSkills()
    }

    private fun setSkills() {
        // if (this.skills?.size == 0) { return }
        val customSkills = selectedSkills.filter { it.id == 0L
                && it.categoryId == category?.catalogItemId } as ArrayList<FeedbackDetail>

        skillsData?.addAll(customSkills)
        skillsData?.addAll(skills)

        val skillsRecycler =  mView.findViewById<RecyclerView>(R.id.skillsContainerList)
        App.createVerticalRecyclerList(skillsRecycler, context)
        // -- Review List
        adapter = FeedbackSkillAdapter(mContext, this.skillsData, this.selectedSkills)
        adapter?.setListener(object : FeedbackSkillAdapter.FeedbackSkillAdapterListener {
            override fun onSkillSelected(feedback: FeedbackDetail) {
                cleanEmptySkill()
                if (feedback.skillTagId == 0L) { return }
                if (!selectedSkills.contains(feedback) && selectedSkills.count() == REMAINING_SKILL) { return }
                if (selectedSkills.contains(feedback)) {
                    selectedSkills.remove(feedback)
                } else {
                    selectedSkills.add(feedback)
                }

                updateSkills()
            }

            override fun onAddSkill(name: String) {
                selectedSkills.removeAt(0)
                skillsData?.removeAt(0)
                val feedbackDetail = getNewFeedbackDetailObject(name)
                selectedSkills.add(0, feedbackDetail)
                skillsData?.add(0, feedbackDetail)
                updateSkills()
            }
        })

        val swipeHandler = object : SwipeToDeleteCallback(mContext) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val skill = skillsData?.get(viewHolder.adapterPosition)
                selectedSkills.remove(skill)
                skillsData?.remove(skill)
                view?.let { activity?.hideKeyboard(it) }
                adapter?.notifyDataSetChanged()
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(skillsRecycler)

        skillsRecycler.adapter = adapter
    }

    @SuppressLint("ResourceAsColor")
    private fun updateSkills() {
        val remainingSkills = REMAINING_SKILL - selectedSkills.count()
        val testRemainingPrefix = if(isStrenghts) getString(R.string.feedback_fragment_selection_remains_strenghts)
        else getString(R.string.feedback_fragment_selection_remains)
        val textRemaining = "$remainingSkills $testRemainingPrefix"
        remainingTextView?.text = textRemaining

        if (remainingSkills == REMAINING_SKILL) {
            isActive = false
            val drawable = buttonContainer!!.background
            drawable.setTint(R.color.talent_feedback_disabled)
            val textButton = if(isStrenghts) resources.getString(R.string.feedback_fragment_selection_disabled_strenghts)
                              else resources.getString(R.string.feedback_fragment_selection_disabled_opportunities)
            buttonTextView?.text = textButton
        } else {
            isActive = true
            setViewAsPrimaryColor(buttonContainer)
            val textButton = if(isStrenghts) resources.getString(R.string.feedback_fragment_selection_enabled_strenghts)
                    else resources.getString(R.string.feedback_fragment_selection_enabled_opportunities)
            buttonTextView?.text = textButton
        }

        adapter?.notifyDataSetChanged()
    }

    fun getNewFeedbackDetailObject(name: String): FeedbackDetail {
        val session = Session.get(mContext)
        val jsonObject = JSONObject()
        jsonObject.put("createdById", session.dataSessionId)
        jsonObject.put("name", name)
        jsonObject.put("description", "")
        jsonObject.put("isPublic", true)
        jsonObject.put("isEnable", true)
        jsonObject.put("categoryId", category!!.catalogItemId)
        jsonObject.put("categoryName", category!!.description)
        return FeedbackDetail(jsonObject)
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun cleanEmptySkill() {
        if (selectedSkills.count() == 0) { return }
        if (selectedSkills.first().name != "") { return }

        selectedSkills.removeAt(0)
        skillsData?.removeAt(0)
        updateSkills()
    }

    override fun onClick(v: View?) {
        val tag = v!!.tag.toString()
        when (tag) {
            "10" -> {
                cleanEmptySkill()
                if (selectedSkills.count() >= 5) { return }
                val feedbackDetail = getNewFeedbackDetailObject("")
                selectedSkills.add(0, feedbackDetail)
                skillsData?.add(0, feedbackDetail)
                updateSkills()
            }
            "11" -> {
                if (!isActive) { return }
                val newSelected: ArrayList<FeedbackDetail> = ArrayList()
                selectedSkills.forEach {
                    if (it.name != "") {
                        newSelected.add(it)
                    }
                }
                selectedSkills = newSelected
                Log.d("DXGOP", "SELECTED SKILLS: " + selectedSkills.toString())

                listener?.onSkillSelected(selectedSkills)
                back()
            }
        }
    }

    fun setListener(listener: FeedbackSelectedListener) {
        this.listener = listener
    }

    interface FeedbackSelectedListener {
        fun onSkillSelected(selectedSkills: ArrayList<FeedbackDetail>)
    }

}