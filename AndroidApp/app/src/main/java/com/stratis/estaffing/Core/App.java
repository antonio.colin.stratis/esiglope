package com.stratis.estaffing.Core;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brouding.simpledialog.SimpleDialog;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.stratis.estaffing.R;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Erick Sanchez
 * Revision 1 - 25/05/20
 */
public class App extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context context;

    @Override
    public void onCreate() {

        super.onCreate();
        App.context = getApplicationContext();
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
    }

    public static Context getAppContext() {
        return App.context;
    }

    public static String getDateFormatSlash(String inputDate) {
        return App.getDateFormatted(inputDate, "dd/MMM/yyyy").replaceAll("\\.", "");
    }

    public static String getDateMonthYear(String inputDate) {
        return App.getDateFormatted(inputDate, "MMMM yyyy");
    }

    public static String getDateCourse(String inputDate) {
        return App.getDateFormatted(inputDate, "dd MMMM yyyy");
    }

    public static String getDateFormatted(String inputDate, String format) {
        return App.getDateFormatted(inputDate, format, "yyyy-MM-dd");
    }

    public static String getDateFormatted(String inputDate, String format, String formatInput) {

        if (inputDate != null && !inputDate.equals("null") && !inputDate.equals("") && !inputDate.isEmpty()) {

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatInput);
            Date date = null;
            try {
                date = simpleDateFormat.parse(inputDate);
            } catch (ParseException e) {
                Log.d("DXGOP", "DATE ERROR :::: " + e.getLocalizedMessage());
                e.printStackTrace();
            }

            // --
            if (date == null) {
                return "";
            }

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat convetDateFormat = null;
            try {
                convetDateFormat = new SimpleDateFormat(format);
                return convetDateFormat.format(date);
            } catch (Exception e) {
                convetDateFormat = new SimpleDateFormat(formatInput);
                return convetDateFormat.format(date);
            }
        }
        return "SIN FECHA";
    }

    public static Date getDate(String inputDate, String formatInput) {

        if (!inputDate.equals("null") && !inputDate.equals("")) {

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatInput);
            Date date = null;
            try {
                date = simpleDateFormat.parse(inputDate);
            } catch (ParseException e) {
                Log.d("DXGOP", "DATE ERROR :::: " + e.getLocalizedMessage());
                e.printStackTrace();
            }

            // --
            return date;

        }
        return null;
    }

    public static String getCurrentDateTime() {

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
        return sdf.format(new Date());
    }

    public static String getTimeAgo(String dataDate) {

        String convTime = null;
        String prefix = "Hace";
        String suffix = "";

        try {

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date pasTime = dateFormat.parse(dataDate);
            Date nowTime = new Date();

            long dateDiff = nowTime.getTime() - pasTime.getTime();
            long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
            long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
            long hour = TimeUnit.MILLISECONDS.toHours(dateDiff);
            long day = TimeUnit.MILLISECONDS.toDays(dateDiff);

            if (second == 1) {
                convTime = context.getString(R.string.time_ago_second, second);
            } else if (second < 60) {
                convTime = context.getString(R.string.time_ago_seconds, second);
            } else if (minute == 1) {
                convTime = context.getString(R.string.time_ago_minute, minute);
            } else if (minute < 60) {
                convTime = context.getString(R.string.time_ago_minutes, minute);
            } else if (hour == 1) {
                convTime = context.getString(R.string.time_ago_hour, hour);
            } else if (hour < 24) {
                convTime = context.getString(R.string.time_ago_hours, hour);
            } else if (day >= 7) {
                if (day > 365 && day < 730) {
                    convTime = context.getString(R.string.time_ago_year, (day / 365));
                } else if (day >= 730) {
                    convTime = context.getString(R.string.time_ago_years, (day / 730));
                } else if (day > 30 && day <= 60) {
                    convTime = context.getString(R.string.time_ago_month, (day / 60));
                } else if (day > 60) {
                    convTime = context.getString(R.string.time_ago_months, (day / 60));
                } else if (day <= 14) {
                    convTime = context.getString(R.string.time_ago_week, (day / 7));
                } else {
                    convTime = context.getString(R.string.time_ago_weeks, (day / 7));
                }
            } else if (day == 1) {
                convTime = context.getString(R.string.time_ago_day, day);
            } else if (day < 7) {
                convTime = context.getString(R.string.time_ago_days, day);
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ConvTimeE", e.getMessage());
        }

        return convTime;
    }

    public static Bitmap getImage(Context context, String name) {

        int id = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
        return BitmapFactory.decodeStream(context.getResources().openRawResource(id));
    }

    public static Drawable getDrawable(Context context, int id) {
        return ContextCompat.getDrawable(context, id);
    }

    public static void createVerticalRecyclerList(RecyclerView list, final Context context) {

        final LinearLayoutManager layout = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        list.getRecycledViewPool().setMaxRecycledViews(0, 10);
        list.setLayoutManager(layout);
        list.setItemViewCacheSize(10);
        list.setDrawingCacheEnabled(true);
        list.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    public static void createRecyclerHorizontalList(RecyclerView list, final Context context) {

        final LinearLayoutManager layout = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        list.getRecycledViewPool().setMaxRecycledViews(0, 10);
        list.setLayoutManager(layout);
        list.setItemViewCacheSize(10);
        list.setDrawingCacheEnabled(true);
        list.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String capitalize(String input) {

        String[] words = input.toLowerCase().split(" ");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            String word = words[i];

            if (i > 0 && word.length() > 0) {
                builder.append(" ");
            }

            String cap = "";
            if (word.length() > 0) {
                if (word.length() == 1) {
                    cap = word.toUpperCase();
                } else {
                    cap = word.substring(0, 1).toUpperCase() + word.substring(1);
                }
            }
            builder.append(cap);
        }
        return builder.toString();
    }

    public static void enableScroll(View view) {

        if (view instanceof TextView) {
            TextView textView = (TextView) view;
            textView.setMovementMethod(new ScrollingMovementMethod());
        }

        view.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
    }

    public static SpannableStringBuilder getTextWithSpan(String text, String spanText, StyleSpan style) {

        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        int start = text.indexOf(spanText);
        int end = start + spanText.length();
        sb.setSpan(style, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return sb;
    }

    public static SpannableStringBuilder getTextWithSpanColor(String text, String spanText, ForegroundColorSpan color) {

        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        int start = text.indexOf(spanText);
        int end = start + spanText.length();
        sb.setSpan(color, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return sb;
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    public static String trimSpaces(String str) {
        return str.replaceAll(" ", "");
    }

    public static SimpleDialog.Builder createDialogOfNewVersion(final Context context, String title, String message,
                                                                SimpleDialog.BtnCallback cancelCallback,
                                                                SimpleDialog.BtnCallback confirmCallback) {

        return new SimpleDialog.Builder(context)
                .setTitle(title)
                .setContent(message, 3)
                .setCancelableOnTouchOutside(false)
                .setBtnConfirmText("Aceptar")
                .onConfirm(confirmCallback)
                .setBtnCancelText(context.getString(R.string.register_back_button))
                .onCancel(cancelCallback);
    }

    public static SimpleDialog.Builder createDialogOfNecessaryNewVersion(final Context context, String title, String message) {

        return new SimpleDialog.Builder(context)
                .setTitle(title)
                .setContent(message, 3)
                .setCancelableOnTouchOutside(false)
                .setCancelable(false)
                .setBtnConfirmText("ACTUALIZAR")
                .onConfirm((dialog, which) -> {
                    App.redirectToPlayStore(context);
                })
                .setBtnCancelText("")
                .onCancel((dialog, which) -> {
                });
    }

    public static void redirectToPlayStore(Context context) {

        final String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static void setViewAsPrimaryColor(View view, Context context) {

        final Session session = Session.get(context);
        final String primaryColor = fixWrongColorATuDePiglek(session.getPreferences().getPrimaryColor());
        if (!primaryColor.equals("")) {
            App.setViewColor(view, Color.parseColor(primaryColor));
        }
    }

    public static void setViewAsSecondaryColor(View view, Context context) {

        final Session session = Session.get(context);
        final String secondaryColor = fixWrongColorATuDePiglek(session.getPreferences().getSecondaryColor());
        if (!secondaryColor.equals("")) {
            App.setViewColor(view, Color.parseColor(secondaryColor));
        }
    }

    private static void setViewColor(View view, Integer color) {

        final Drawable drawable = view.getBackground();
        drawable.setTint(color);
    }

    public static String getTwoFirstChars(String str) {
        if (str.isEmpty()) return "";
        String[] splited = str.split(" ");
        if (splited.length == 2) {
            return splited[0].substring(0, 1).concat(splited[1].substring(0, 1)).toUpperCase();
        }
        if (splited.length > 2) {
            return splited[0].substring(0, 1).concat(splited[2].substring(0, 1)).toUpperCase();
        } else {
            return str.substring(0, 2).toUpperCase();
        }
    }

    public static String getShorName(String str) {
        if(str.isEmpty()) return "";
        String[] splited = str.split(" ");
        if(splited.length >= 3)
            return new StringBuilder(splited[0]).append(" ").append(splited[splited.length-2]).toString();
        return str;
    }

    public static String getMiniShorName(String str) {
        String miniShorName = getShorName(str);
        return miniShorName.substring(0, miniShorName.indexOf(" ") + 2).concat(".");
    }

    /**
     * Una pingeres pa resolver LES-GO-MO-NES! by Erick Sanchez
     * @param currentColor
     * @return
     */
    public static String fixWrongColorATuDePiglek(String currentColor) {
//        Log.d("DXGOP", "Fixing wrong color: " + currentColor);
        currentColor = currentColor != null && currentColor.length() == 6 && !currentColor.startsWith("#") ?
                "#" + currentColor : currentColor;
//        Log.d("DXGOP", currentColor + " handled");
        try {
            Color.parseColor(currentColor);
        } catch (Exception e) {
//            Log.e("DXGOP", "Error: " + e.getMessage());
            currentColor = "#000000";
        }
        return currentColor;
    }

    public static String shortDescription(String about) {
        final int max = 25;
        String[] nWords = about.split(" ");
        if(nWords.length <= max)
            return about;
        StringBuilder aboutShort = new StringBuilder();
        for(int i=0; i<max; i++){
            aboutShort.append(nWords[i]);
            if(i<max-1)
                aboutShort.append(" ");
        }
        return aboutShort.append("...").toString().trim();
    }

    public static String hanledStringNull(String str) {
        if(isEmpty(str))
            return "N/D";
        return str;
    }

    public static boolean isEmpty(String str) {
        return (str == null || str.equals("null") || str.equals("N/A") || str.isEmpty());
    }

    public static List<String> formatVideoAllow() {
        return Arrays.asList(new String[]{"MP4","AVI","MKV","FLV","MOV","WMV"});
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }

    public static String getDownloadPath() {
        File dir = null;
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.R ) {
            dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        } else {
            dir = Environment.getExternalStorageDirectory();
        }
        if(!dir.exists())
            dir.mkdirs();
        return dir.getPath();

    }
}