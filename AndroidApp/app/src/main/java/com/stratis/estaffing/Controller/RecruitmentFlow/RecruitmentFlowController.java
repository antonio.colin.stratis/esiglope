package com.stratis.estaffing.Controller.RecruitmentFlow;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Control.ABActivity;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Loader;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Control.eStaffingFragment;
import com.stratis.estaffing.Controller.RecruitmentFlow.RequestsFlow.RecruitmentRequestsController;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestCheckFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyBudgetFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyDetailFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyLanguageFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyModalityFragment;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageCatalog;
import com.stratis.estaffing.Model.RecruitmentFlow.SimpleProfile;
import com.stratis.estaffing.Model.RecruitmentFlow.SoftwareCatalog;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Recruitment.NewRequestFlow.ProfileService;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import org.json.JSONArray;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class RecruitmentFlowController extends ABActivity implements View.OnClickListener {

    private RelativeLayout footer;
    private Integer currentIndex = 0;
    private ArrayList<ABFragment> fragments = new ArrayList<>();
    public Profile profile;
    private Loader loader = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_recruitment_flow_controller);
        this.setStatusBarColor(Color.parseColor("#FFFFFF"));
        this.setStatusBarTextLight();
        /*findViewById(R.id.header).setBackgroundColor(Color.parseColor("#EBEDF0"));*/

        // --
        if (getIntent().hasExtra("profile")) {
            this.profile = (Profile) getIntent().getSerializableExtra("profile");
        } else {
            this.profile = new Profile();
        }

        // -- Show loader
        this.loader = new Loader(this);
        this.loader.show();

        // -- Init views
        this.initViews();

        // --
        this.getSimpleProfiles();

        // -- Creating first and next fragments
        final boolean isPreLoaded = this.profile.getCurrentProfile() != null;
        //if (isPreLoaded) {
        //    this.loadFragment(new RequestCheckFragment());
        //    this.currentIndex = 5;
        //    Log.d("DXGOP", "LOLOLOLOOLOOLO");
        //} else {
        RequestVacancyFragment requestVacancyFragment = new RequestVacancyFragment();
        requestVacancyFragment.setModeEdit(false);
        this.loadFragment(requestVacancyFragment);
        //}
        this.fragments.add(new RequestVacancyDetailFragment());
        this.fragments.add(new RequestVacancyLanguageFragment());
        this.fragments.add(new RequestVacancyModalityFragment());
        this.fragments.add(new RequestVacancyBudgetFragment());
        this.fragments.add(new RequestCheckFragment());
    }

    private void initViews() {

        // -- Footer
        this.footer = this.findViewById(R.id.footer);

        // -- Buttons
        final Button footerCancelButton = this.findViewById(R.id.footerCancelButton);
        footerCancelButton.setTag("66");
        footerCancelButton.setOnClickListener(this);

        final ImageButton app_screen_return_arrow = this.findViewById(R.id.app_screen_return_arrow);
        app_screen_return_arrow.setTag("77");
        app_screen_return_arrow.setOnClickListener(this);

        final RelativeLayout footerContinueButton = this.findViewById(R.id.footerContinueButton);
        footerContinueButton.setTag("88");
        footerContinueButton.setOnClickListener(this);
        this.setViewAsPrimaryColor(footerContinueButton);

        final TextView buttonText = this.findViewById(R.id.buttonText);
        final ImageView imageNext = this.findViewById(R.id.imageNext);
        buttonText.setTag("99");
        imageNext.setTag("99");

        // --
        KeyboardVisibilityEvent.setEventListener(
                this, isOpen -> {

                    if (isOpen) {
                        footer.setVisibility(View.GONE);
                    } else {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                footer.setVisibility(View.VISIBLE);
                            }
                        }, 150);
                    }
                });

    }

    private boolean loadFragment(Fragment fragment) {

        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    public void continueSegue(ABFragment fragment) {
        this.continueSegue(fragment,  "");
    }

    private void continueSegue(ABFragment fragment,  String tag) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.add(R.id.fragment_container, fragment, tag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    // -- Footer methods
    private void showAsFinalScreen() {

        final boolean state = this.currentIndex == 5;
        final RelativeLayout footer = this.findViewById(R.id.footerContinueButton);
        final TextView buttonText = footer.findViewById(R.id.buttonText);
        if (state) {
            this.findViewById(R.id.footerCancelButton).setVisibility(View.GONE);
            buttonText.setText(getString(R.string.app_footer_send_button));

            buttonText.setOnClickListener(this);
        } else {
            this.findViewById(R.id.footerCancelButton).setVisibility(View.VISIBLE);
            buttonText.setText(getString(R.string.app_footer_continue_button));
        }
    }

    public void disableContinueButton(boolean state) {

        final RelativeLayout footer = this.findViewById(R.id.footerContinueButton);
        final TextView textViewButton = this.findViewById(R.id.buttonText);
        final ImageView imageViewNext = this.findViewById(R.id.imageNext);

        if (state) {
            footer.setBackground( ContextCompat.getDrawable(this, R.drawable.activity_login_controller_login_button) );
            textViewButton.setTextColor(Color.rgb(255, 255, 255));
            imageViewNext.setVisibility(View.VISIBLE);
        } else {
            footer.setBackground( ContextCompat.getDrawable(this, R.drawable.new_interview_bk) );
            textViewButton.setTextColor(Color.rgb(118, 118, 118));
            imageViewNext.setVisibility(View.GONE);
        }

        footer.setEnabled(state);
        footer.setClickable(state);
    }

    // -- Factory
    private void getSimpleProfiles() {

        // -- Single Talent logic
        ProfileService.getSimpleProfiles(new ReSTCallback() {

            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Log.d("DXGOP", "SAMPLE PROFILES RESPONSE = " + response.body);
                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                profile.setSimpleProfiles(SimpleProfile.parse(res));

                // --
                getLanguageCatalog();
            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                Log.d("DXGOP", "ERROR :: " + response.body);
                // --
                loader.cancel();
            }
        });
    }

    private void getLanguageCatalog() {

        ProfileService.getLanguageCatalog(new ReSTCallback() {

            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Log.d("DXGOP", "LANGUAGE CATALOG RESPONSE = " + response.body);
                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                profile.setLanguageCatalogs(LanguageCatalog.parse(res));

                // --
                getSoftwareCatalog();
            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                Log.d("DXGOP", "ERROR :: " + response.body);
                loader.cancel();
            }
        });
    }

    private void getSoftwareCatalog() {

        ProfileService.getSoftwareCatalog(new ReSTCallback() {

            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Log.d("DXGOP", "SOFTWARE CATALOG RESPONSE = " + response.body);
                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                profile.setSoftwareCatalogs(SoftwareCatalog.parse(res));

                // --
                loader.cancel();
            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                Log.d("DXGOP", "ERROR :: " + response.body);
                // --
                loader.cancel();
            }
        });
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        this.currentIndex--;
        this.showAsFinalScreen();
        this.disableContinueButton(true);
    }

    // --
    @Override
    public void onClick(View v) {

        final String tag = (String) v.getTag();
        switch (tag) {

            case "66":
                finish();
                break;

            case "77":
                this.onBackPressed();
                break;

            case "88":

                // -- Click trigger to current fragment
                if (this.getSupportFragmentManager().getFragments() != null) {
                    ((eStaffingFragment) this.getSupportFragmentManager().getFragments().get(this.currentIndex)).onFooterContinueButtonClick();
                }

                if (this.currentIndex < 5) {

                    // -- Change view
                    this.continueSegue(this.fragments.get(this.currentIndex));
                    this.currentIndex++;
                    this.showAsFinalScreen();
                    this.disableContinueButton(true);

                } else {
                    // -- Saving
                    sendRequest();
                }

                break;
            case "99":
                sendRequest();
                break;
        }
    }

    private void sendRequest() {
        toogleButton(true);
        ProfileService.save(Session.get(getApplicationContext()),
                this.profile.getCurrentVacancies(),
                this.profile.getSaveRequestObject(getApplicationContext()),
                new ReSTCallback() {
                    @Override
                    public void onSuccess(ReSTResponse response) {
                        toogleButton(false);
                        // --
                        Log.d("DXGOP", "SAVE PROFILE RESPONSE : " + response.body);
                        // --
                        final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());;
                        mFirebaseAnalytics.logEvent("Solicitudes_Enviadas", new Bundle());

                        // -- Moving
                        RecruitmentFlowController.this.startActivity(new Intent(RecruitmentFlowController.this, RecruitmentRequestsController.class));
                        RecruitmentFlowController.this.finish();
                    }

                    @Override
                    public void onError(ReSTResponse response) {
                        final Notification notification = new Notification(RecruitmentFlowController.this, R.layout.notification_light_error);
                        notification.setMessage(response.body);
                        notification.show(4000);
                        // --
                        toogleButton(false);
                        Log.d("DXGOP", "SAVE PROFILE ERROR RESPONSE : " + response.body);
                    }
                });
    }

    @SuppressLint("ResourceAsColor")
    private void toogleButton(boolean isLoading) {
        final RelativeLayout footerContinueButton = this.findViewById(R.id.footerContinueButton);
        final TextView buttonText = this.findViewById(R.id.buttonText);
        final ImageView imageNext = this.findViewById(R.id.imageNext);
        final ConstraintLayout loadingContainer = this.findViewById(R.id.loadingContainer);
        final ProgressBar progressBar = this.findViewById(R.id.progress_bar);
        final TextView loadingText = this.findViewById(R.id.loadingTextView);

        int isGone = isLoading ? View.GONE : View.VISIBLE;
        int isVisible = isLoading ? View.VISIBLE : View.GONE;
        buttonText.setVisibility(isGone);
        imageNext.setVisibility(isGone);
        loadingContainer.setVisibility(isVisible);
        loadingText.setVisibility(isVisible);

        if (isLoading) {
            footerContinueButton.setGravity(Gravity.CENTER);
        } else {
            footerContinueButton.setGravity(Gravity.NO_GRAVITY);
        }

        footerContinueButton.setEnabled(!isLoading);
        if (isLoading) progressBar.animate();
    }
}