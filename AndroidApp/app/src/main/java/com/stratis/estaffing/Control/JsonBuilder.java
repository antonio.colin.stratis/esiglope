package com.stratis.estaffing.Control;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonBuilder {
	
	static public JSONObject stringToJson(String body) {
		
		JSONObject obj = null;
        try {
            obj = new JSONObject(body);
            return obj;
        } catch (Throwable t) {
            Log.d("DXGOP", "JsonBuilder -- Couldnt parse information to JSON -- " + t.getLocalizedMessage());
            return obj;
        }
	}
	
	static public JSONArray stringToJsonArray(String body) {
		
		JSONArray obj = null;

        try {

            obj = new JSONArray(body);
            return obj;

        } catch (Throwable t) {
            Log.d("DXGOP", "JsonBuilder -- Couldnt parse information to JSON  -- " + t.getLocalizedMessage());
            return obj;
        }
	}
	
	static public String jsonToString(JSONObject obj) {
		
		String ret = "";
		ret = obj.toString();
		return ret;
	}
	
	static public String jsonToString(JSONArray array) {
		
		String ret = "";
		ret = array.toString();
		return ret;
	}
}