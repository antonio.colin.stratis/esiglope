package com.stratis.estaffing.View

import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.R

class ButtonArrowView(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var shape: RelativeLayout? = null
    var layoutButton: Button? = null

    init {
        this.shape = itemView.findViewById(R.id.shape)
        this.layoutButton = itemView.findViewById(R.id.layoutButton)
    }

}