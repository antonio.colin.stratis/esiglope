package com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 29/07/20
 */

public class CandidateWorkExperienceView extends RecyclerView.ViewHolder {

    private RelativeLayout shape = null;
    private TextView jobName = null;
    private TextView jobPeriod = null;
    private TextView jobDuration = null;
    private TextView jobDescription = null;

    public CandidateWorkExperienceView(View row) {

        // --
        super(row);

        // --
        this.shape = row.findViewById(R.id.shape);
        this.jobName = row.findViewById(R.id.jobName);
        this.jobPeriod = row.findViewById(R.id.jobPeriod);
        this.jobDuration = row.findViewById(R.id.jobDuration);
        this.jobDescription = row.findViewById(R.id.jobDescription);
    }

    public RelativeLayout getShape() {
        return shape;
    }

    public TextView getJobName() {
        return jobName;
    }

    public TextView getJobPeriod() {
        return jobPeriod;
    }

    public TextView getJobDuration() { return jobDuration; }

    public TextView getJobDescription() {
        return jobDescription;
    }
}