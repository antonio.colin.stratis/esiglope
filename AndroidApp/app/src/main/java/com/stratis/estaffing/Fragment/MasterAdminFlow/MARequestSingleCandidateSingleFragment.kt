package com.stratis.estaffing.Fragment.MasterAdminFlow

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.firebase.analytics.FirebaseAnalytics
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.RequestCandidateSkillsAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Control.LoadingButton
import com.stratis.estaffing.Control.Notification
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.FirebaseEvent
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateDescriptionFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateEducationFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateLanguagesFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateWorkExperienceFragment
import com.stratis.estaffing.Listener.NotificationCenterFragmentListener
import com.stratis.estaffing.Modal.LanguagesModal
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal.NoCandidateInterestModal
import com.stratis.estaffing.Model.MasterAdminFlow.MARequestCandidate
import com.stratis.estaffing.Model.Profile
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidateMessage
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.MasterAdmin.MasterAdminService
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.CandidateService
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.RequestsService
import java.util.*

open class MARequestSingleCandidateSingleFragment : ABFragment(), View.OnClickListener,
        NoCandidateInterestModal.NoCandidateInterestModalListener {

    open var profile: Profile? = null
    open var candidate: MARequestCandidate? = null
    open var listener:MARequestSingleCandidateSingleListener? = null
    open var notificationCenterListener: NotificationCenterFragmentListener? = null

    private var candidateLegacy:RequestCandidate? = null
    private var favoriteButton: ImageButton? = null
    private var actionComponent: LoadingButton? = null
    private var messages: ArrayList<RequestCandidateMessage>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_m_a_request_single_candidate_single, container, false)

        // -- Set header
        setHeader(getString(R.string.recruitment_request_candidate_single_title))
        this.initViews()
        this.getCandidate()

        return this.mView
    }

    open fun getCandidate() {

        RequestsService.getCandidate(profile!!.provisionalId, candidate!!.id,
                object : ReSTCallback {
                    override fun onSuccess(response: ReSTResponse) {

                        val res = JsonBuilder.stringToJson(response.body)

                        candidate!!.loadFromJSON(res)
                        candidateLegacy = candidate?.convert()
                        candidateLegacy?.loadFromJSON(res)

                        paintInfoCandidate()
                        setActionButtonWithBaseInStatus()
                        paintAboutCandidate()
                        paintSkillsOfCandidate()
                        getMessages()
                        canReviewed()
                        Log.i("DXGOP", "CANDIDATE STATUS :::: " + candidate!!.statusCandidateId)
                    }

                    override fun onError(response: ReSTResponse) {
                        if (notificationCenterListener != null) {
                            notificationCenterListener!!.onCandidateNotAvailable()
                        }
                    }
                })
    }

    private fun initViews() {

        /** Favorite Star  */
        this.favoriteButton = mView.findViewById<ImageButton>(R.id.favoriteButton)
        favoriteButton?.setTag("10")
        favoriteButton?.setOnClickListener(this)
        if (candidate!!.favorite) {
            favoriteButton?.setImageBitmap(App.getImage(context, "fragment_recruitment_request_candidate_single_star_filled"))
        }

        /** Action button  */
        actionComponent = LoadingButton(findView(R.id.actionComponent))
        actionComponent?.button?.tag = "101"
        actionComponent?.button?.setOnClickListener(this)

        /** More info in languages  */
        val languagesContainerMoreInfoButton = mView.findViewById<Button>(R.id.languagesContainerMoreInfoButton)
        languagesContainerMoreInfoButton.tag = "15"
        languagesContainerMoreInfoButton.setOnClickListener(this)
    }

    private fun paintInfoCandidate() {

        /** Image profile  and seniority  */
        val imageProfile = mView.findViewById<ImageView>(R.id.imageProfile)
        imageProfile.clipToOutline = true
        if (candidate!!.imgProfile != "") {
            Glide.with(this)
                    .load(candidate!!.imgProfile)
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.NONE) // .placeholder(R.drawable.loading_spinner)
                    .skipMemoryCache(true)
                    .into(imageProfile)
        }
        val seniorityView = mView.findViewById<TextView>(R.id.seniorityView)
        val s = candidate!!.jobSeniority
        if (s == "" || s == "null" || s == "Null" || s == "NULL") {
            seniorityView.visibility = View.GONE
        } else {
            seniorityView.text = App.capitalize(s)
        }

        /** Date **/
        val dateView = findView<TextView>(R.id.dateView)
        val dateString = String.format( getString(R.string.fragment_m_a_request_single_candidate_date_title), candidate?.applicationDate)
        val dateSpan = App.getTextWithSpan(dateString, candidate?.applicationDate, StyleSpan(Typeface.BOLD))
        dateView.text = dateSpan

        /** Name and job  */
        val nameView = mView.findViewById<TextView>(R.id.nameView)
        nameView.text = App.capitalize(candidate!!.fullNameShort)

        val positionView = mView.findViewById<TextView>(R.id.positionView)
        positionView.text = App.capitalize(candidate!!.positionDesc)

        /** Buttons {Experience, education}  */
        val experienceButton = mView.findViewById<Button>(R.id.experienceButton)
        experienceButton.tag = "11"
        experienceButton.setOnClickListener(this)

        val educationButton = mView.findViewById<Button>(R.id.educationButton)
        educationButton.tag = "12"
        educationButton.setOnClickListener(this)

        val videoButton = mView.findViewById<Button>(R.id.videoButton)
        videoButton.tag = "13"
        videoButton.setOnClickListener(this)
    }

    private fun paintAboutCandidate() {

        val aboutContainerTitle = mView.findViewById<TextView>(R.id.aboutContainerTitle)
        val aboutContainerTitleString = String.format(
                mContext.getString(R.string.recruitment_request_candidate_single_about_container_title),
                candidate!!.firstNameShort.substring(0, 1).uppercase(Locale.getDefault()) +
                        candidate!!.firstNameShort.substring(1).lowercase(Locale.getDefault())
        )
        aboutContainerTitle.text = aboutContainerTitleString

        // --
        val aboutContainerContent = mView.findViewById<TextView>(R.id.aboutContainerContent)
        aboutContainerContent.text = candidate!!.description
        if (candidate!!.description.length > 190) {
            val aboutContainerButton = findView<Button>(R.id.aboutContainerButton)
            aboutContainerButton.visibility = View.VISIBLE
            aboutContainerButton.tag = "40"
            aboutContainerButton.setOnClickListener(this)
        }
    }

    private fun paintSkillsOfCandidate() {

        // -- SOFT
        val softSkillsContainerList: RecyclerView = mView.findViewById(R.id.softSkillsContainerList)
        App.createVerticalRecyclerList(softSkillsContainerList, context)
        val softAdapter = RequestCandidateSkillsAdapter(candidateLegacy?.softSkills)
        softSkillsContainerList.adapter = softAdapter

        // -- HARD
        val hardSkillsContainerList: RecyclerView = mView.findViewById(R.id.hardSkillsContainerList)
        App.createVerticalRecyclerList(hardSkillsContainerList, context)
        val hardAdapter = RequestCandidateSkillsAdapter(candidateLegacy?.hardSkills)
        hardSkillsContainerList.adapter = hardAdapter

        // -- LANGUAGE
        val languagesContainerList: RecyclerView = mView.findViewById(R.id.languagesContainerList)
        App.createVerticalRecyclerList(languagesContainerList, context)
        val langAdapter = RequestCandidateSkillsAdapter(candidateLegacy?.languageSkills)
        languagesContainerList.adapter = langAdapter
    }

    private fun setActionButtonWithBaseInStatus() {

        actionComponent?.button?.visibility = View.VISIBLE
        if (this.candidate?.statusCandidateId == MARequestCandidate.statusCandidateDiscarded) {

            actionComponent?.button?.background = ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button)
            actionComponent?.text?.text = mContext.getString(R.string.recruitment_request_candidate_single_reconsiderate)
            actionComponent?.text?.setTextColor(Color.parseColor("#FFFFFF"))
            FirebaseEvent.logEvent(mContext, this::class.simpleName.plus("consider"))
        } else {

            actionComponent?.button?.background = ContextCompat.getDrawable(mContext, R.drawable.fragment_recruitment_request_candidate_education_bk)
            actionComponent?.text?.text = mContext.getString(R.string.recruitment_request_candidate_single_discard)
            actionComponent?.text?.setTextColor(Color.parseColor("#767676"))
            FirebaseEvent.logEvent(mContext, this::class.simpleName.plus("discard"))
        }
    }

    /** Services **/

    private fun setFavoriteStatusToCandidate() {

        showLoader()
        CandidateService.setFavoriteStatusToCandidate(candidateLegacy, Session.get(context), object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {
                hideLoader()
                candidate?.favorite = !candidate!!.favorite
                if (candidate!!.favorite) {
                    favoriteButton!!.setImageBitmap(App.getImage(context, "fragment_recruitment_request_candidate_single_star_filled"))
                } else {
                    favoriteButton!!.setImageBitmap(App.getImage(context, "fragment_recruitment_request_candidate_single_star"))
                }
            }

            override fun onError(response: ReSTResponse) {
                hideLoader()
            }
        })
    }

    private fun setDiscardOnOff() {

        this.actionComponent?.showLoading()
        if (this.candidate?.statusCandidateId == MARequestCandidate.statusCandidateDiscarded) {

            val session = Session.get(mContext)
            MasterAdminService.reconsider(candidate!!, session, object: ReSTCallback {

                override fun onSuccess(response: ReSTResponse?) {

                    candidate?.statusCandidateId = RequestCandidate.statusCandidate
                    candidateLegacy?.statusCandidateId = RequestCandidate.statusCandidate
                    actionComponent?.hideLoading()

                    val parameters = Bundle()
                    val mFirebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
                    mFirebaseAnalytics.logEvent("CV_Compartido", parameters)

                    setActionButtonWithBaseInStatus()
                }

                override fun onError(response: ReSTResponse?) {

                    actionComponent?.hideLoading()
                    setActionButtonWithBaseInStatus()

                    val notification = Notification(mContext, R.layout.notification_light_error)
                    notification.setMessage( response?.body )
                    notification.show(3000)
                }
            })

        } else {

            val noCandidateInterestModal = NoCandidateInterestModal(mContext, candidateLegacy)
            noCandidateInterestModal.listener = this
            noCandidateInterestModal.setFromMasterAdmin(true)
            noCandidateInterestModal.continueButton.text.text = mContext.getString(R.string.recruitment_request_candidate_single_modal_discard)
            noCandidateInterestModal.show()
            this.actionComponent?.hideLoading()
        }
    }

    private fun canReviewed() {

        if (this.candidate?.isReviewed == false) {

            val session = Session.get(mContext)
            this.candidate?.isReviewed = true
            MasterAdminService.reviewed(this.candidate!!, session, object:ReSTCallback {

                override fun onSuccess(response: ReSTResponse?) {}

                override fun onError(response: ReSTResponse?) {}
            })
        }
    }

    private fun getMessages() {

        CandidateService.getMessages(candidateLegacy, object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {
                val res = JsonBuilder.stringToJsonArray(response.body)
                messages = RequestCandidateMessage.parse(res,
                        Session.get(context).dataSessionId)
                //paintMenu(false)
            }

            override fun onError(response: ReSTResponse) {
                messages = ArrayList<RequestCandidateMessage>()
                //paintMenu(false)
            }
        })
    }

    /** Implementations **/

    override fun onClick(v: View?) {

        val tag = v!!.tag.toString()
        when (tag) {
            //"2" -> //this.openChat()
            "10" -> this.setFavoriteStatusToCandidate();
            "11" -> {
                val workExperienceFragment = RecruitmentRequestCandidateWorkExperienceFragment()
                workExperienceFragment.setCandidate(candidateLegacy)
                this.continueSegue(workExperienceFragment)
            }
            "12" -> {
                val educationFragment = RecruitmentRequestCandidateEducationFragment()
                educationFragment.setCandidate(candidateLegacy)
                this.continueSegue(educationFragment)
            }
            "13" -> {
                val languagesFragment = RecruitmentRequestCandidateLanguagesFragment()
                languagesFragment.setCandidate(candidateLegacy)
                this.continueSegue(languagesFragment)
            }
            "15" -> {
                val languagesModal = LanguagesModal(context)
                languagesModal.show()
            }
            "40" -> {
                val descriptionFragment = RecruitmentRequestCandidateDescriptionFragment()
                descriptionFragment.setCandidate(candidateLegacy)
                this.continueSegue(descriptionFragment)
            }
            "101" -> {
                this.setDiscardOnOff()
            }
        }
    }

    override fun onStatusChange() {

        candidate?.statusCandidateId = MARequestCandidate.statusCandidateDiscarded
        candidateLegacy?.statusCandidateId = MARequestCandidate.statusCandidateDiscarded
        setActionButtonWithBaseInStatus()

        if (listener != null) {
            listener?.onDiscardCandidate()
            back()
        }
    }
}