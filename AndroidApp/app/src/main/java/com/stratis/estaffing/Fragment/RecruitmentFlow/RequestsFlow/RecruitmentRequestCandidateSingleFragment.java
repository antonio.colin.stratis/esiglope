package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ShareCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate.RequestCandidateInterviewReviewsAdapter;
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate.RequestCandidateMenuAdapter;
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.RequestCandidateSkillsAdapter;
import com.stratis.estaffing.BuildConfig;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Control.RatingStar.RatingStarComplete;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateDescriptionFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateEducationFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateLanguagesFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateShare;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateWorkExperienceFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.InterviewFlow.RecruitmentRequestCreateInterviewFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.InterviewFlow.RecruitmentRequestRateInterviewFragment;
import com.stratis.estaffing.Listener.NotificationCenterFragmentListener;
import com.stratis.estaffing.Modal.LanguagesModal;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.ChooseCandidate.ChooseCandidateModal;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal.NoCandidateInterestModal;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateMenuModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Interview.RequestCandidateInterview;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidateMessage;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Interview.InterviewService;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.CandidateService;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.RequestsService;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;

public class RecruitmentRequestCandidateSingleFragment extends ABFragment
        implements View.OnClickListener, RecruitmentRequestCreateInterviewFragment.RecruitmentRequestCreateInterviewListener,
        RecruitmentRequestRateInterviewFragment.RecruitmentRequestRateInterviewListener,
        ChooseCandidateModal.ChooseCandidateListener, RecruitmentRequestCandidateShare.RecruitmentRequestCandidateShareListener {

    private Profile profile;
    private RequestCandidate candidate;
    private RequestCandidateInterview interview;

    private ArrayList<RequestCandidateMessage> messages;
    private ArrayList<RequestCandidateInterview> interviews;

    private ImageButton favoriteButton;
    private Boolean dotsMenuOpened = false;

    // -- Listener for NotificationCenter
    private NotificationCenterFragmentListener notificationCenterListener;

    private enum RecruitmentRequestCandidateInterviewActionType { single, multi, forInterest }

    public RecruitmentRequestCandidateSingleFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // --
        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_candidate_single, container, false);

        // -- Set header
        this.setHeader(getString(R.string.recruitment_request_candidate_single_title), true);
        this.initViews();
        this.getCandidate();

        // --
        return this.mView;
    }

    public void getCandidate() {

        RequestsService.getCandidate(this.profile.getProvisionalId(), this.candidate.getId(),
                new ReSTCallback() {
                    @Override
                    public void onSuccess(ReSTResponse response) {

                        final JSONObject res = JsonBuilder.stringToJson(response.body);
                        candidate.loadFromJSON(res);
                        paintInfoCandidate();
                        setComponentBaseOnStatus();
                        paintAboutCandidate();
                        paintSkillsOfCandidate();
                        getMessages();
                        Log.i("DXGOP", "CANDIDATE STATUS :::: " + candidate.getStatusCandidateId());
                    }

                    @Override
                    public void onError(ReSTResponse response) {

                        if (notificationCenterListener != null) {
                            notificationCenterListener.onCandidateNotAvailable();
                        }
                    }
                });
    }

    private void initViews() {

        // --
        this.setViewAsPrimaryColor(findView(R.id.imageView));

        /** Favorite Star **/
        this.favoriteButton = this.mView.findViewById(R.id.favoriteButton);
        favoriteButton.setTag("10");
        favoriteButton.setOnClickListener(this);
        if (this.candidate.getFavorite()) {
            favoriteButton.setImageBitmap(App.getImage(mContext, "fragment_recruitment_request_candidate_single_star_filled"));
        }

        /** No interest and interest buttons **/
        final Button noInterestButton = this.mView.findViewById(R.id.noInterestButton);
        noInterestButton.setTag("8");
        noInterestButton.setOnClickListener(this);

        final Button interestButton = this.mView.findViewById(R.id.interestButton);
        interestButton.setTag("9");
        interestButton.setOnClickListener(this);
        this.setViewAsSecondaryColor(interestButton);

        /** More info in languages **/
        final Button languagesContainerMoreInfoButton = this.mView.findViewById(R.id.languagesContainerMoreInfoButton);
        languagesContainerMoreInfoButton.setTag("15");
        languagesContainerMoreInfoButton.setOnClickListener(this);
        this.setViewAsPrimaryColor(languagesContainerMoreInfoButton);
    }

    private void paintInfoCandidate() {

        /** Image profile  and seniority **/
        final ImageView imageProfile = this.mView.findViewById(R.id.imageProfile);
        imageProfile.setClipToOutline(true);
        if (!this.candidate.getImgProfile().equals("")) {
            Glide.with(this)
                    .load(candidate.getImgProfile())
                    .centerCrop()
                    .thumbnail(.50f)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    // .placeholder(R.drawable.loading_spinner)
                    .into( imageProfile );
        }

        final TextView seniorityView = this.mView.findViewById(R.id.seniorityView);
        final String s = candidate.getJobSeniority();
        if (s.equals("") || s.equals("null") || s.equals("Null") || s.equals("NULL")) {
            seniorityView.setVisibility(View.GONE);
        } else {
            seniorityView.setText( App.capitalize(s) );
        }
        this.setViewAsPrimaryColor(seniorityView);

        /** Name and job **/
        final TextView nameView = this.mView.findViewById(R.id.nameView);
        nameView.setText( App.capitalize(this.candidate.getFullNameShort()) );

        final TextView positionView = this.mView.findViewById(R.id.positionView);
        positionView.setText( App.capitalize(this.candidate.getPositionDesc()) );

        /** Buttons {Experience, education} **/
        final Button experienceButton = this.mView.findViewById(R.id.experienceButton);
        experienceButton.setTag("11");
        experienceButton.setOnClickListener(this);

        final Button educationButton = this.mView.findViewById(R.id.educationButton);
        educationButton.setTag("12");
        educationButton.setOnClickListener(this);

        final Button videoButton = this.mView.findViewById(R.id.videoButton);
        videoButton.setTag("13");
        videoButton.setOnClickListener(this);
    }

    private void setComponentBaseOnStatus() {

        final ConstraintLayout interestComponent = this.mView.findViewById(R.id.interestComponent);
        final ConstraintLayout notInterestComponent = this.mView.findViewById(R.id.notInterestComponent);

        interestComponent.setVisibility(View.GONE);
        notInterestComponent.setVisibility(View.GONE);

        this.mView.findViewById(R.id.ratedComponent).setVisibility(View.GONE);
        this.mView.findViewById(R.id.interviewComponent).setVisibility(View.GONE);
        this.mView.findViewById(R.id.interviewDoneComponent).setVisibility(View.GONE);
        this.mView.findViewById(R.id.interviewNoDoneComponent).setVisibility(View.GONE);

        switch (this.candidate.getStatusCandidateId()) {

            case RequestCandidate.statusCandidate:
            case RequestCandidate.statusCandidateFavorite:
                this.getInterviews(RecruitmentRequestCandidateInterviewActionType.forInterest);
                break;

            case RequestCandidate.statusCandidateNotInterest:

                notInterestComponent.setVisibility(View.VISIBLE);
                ((TextView) notInterestComponent.findViewById(R.id.comment)).setText( candidate.getComment() );

                if (this.interview == null) {
                    this.interview = new RequestCandidateInterview();
                }

                final Button interestButton = notInterestComponent.findViewById(R.id.interestButton);
                interestButton.setTag("9");
                interestButton.setOnClickListener(this);
                break;

            case RequestCandidate.statusCandidateInterviews:
            case RequestCandidate.statusCandidateInterviewDateAssigned:
                this.getInterviews(RecruitmentRequestCandidateInterviewActionType.single);
                break;

            case RequestCandidate.statusCandidateGraded:
            case RequestCandidate.statusCandidateChoose:
                this.getInterviews(RecruitmentRequestCandidateInterviewActionType.multi);
                break;
        }
    }

    private void setComponentsInterview() {

        if (this.interview != null) { this.interview.setCandidate(this.candidate); }

        final RelativeLayout interviewComponent = this.mView.findViewById(R.id.interviewComponent);
        final Button interviewDoneComponent = this.mView.findViewById(R.id.interviewDoneComponent);
        final ConstraintLayout interviewNoDoneComponent = this.mView.findViewById(R.id.interviewNoDoneComponent);

        this.setViewAsPrimaryColor(interviewComponent);
        this.setViewAsSecondaryColor(interviewDoneComponent);

        Log.i("DXGOP", "AYUUDAAA:: " + this.interview.getStatus());
        switch (this.interview.getStatus()) {

            case RequestCandidateInterview.interviewScheduled:
            case RequestCandidateInterview.interviewConfirmed:
            case RequestCandidateInterview.interviewReScheduled:

                if (RequestCandidateInterview.hasGradedInterviews(this.interviews, this.candidate.getVacancyId())) {

                    this.setComponentRatedFactory();
                    return;
                }

                if (new Date().after( App.getDate(this.interview.getDate(), "yyyy-MM-dd HH:mm:ss") )) {

                    this.interview.setStatus(RequestCandidateInterview.interviewDone);
                    this.setComponentsInterview();
                    return;
                }

                String date = App.getDateFormatted(interview.getDate(),
                        "dd/MMM/YY '-' HH:mm'hrs.'", "yyyy-MM-dd HH:mm:ss");
                date = date.toLowerCase().replaceAll("\\.","");
                interviewComponent.setVisibility(View.VISIBLE);

                ((TextView) interviewComponent.findViewById(R.id.date)).setText( date );
                interviewComponent.setTag("30");
                interviewComponent.setOnClickListener(this);
                break;

            case RequestCandidateInterview.interviewDone:

                if (!RequestCandidateInterview.hasGradedInterviews(this.interviews, this.candidate.getVacancyId())) {

                    Log.i("DXGOP", "NO GRADES");
                    interviewDoneComponent.setVisibility(View.VISIBLE);
                    interviewDoneComponent.setTag("31");
                    interviewDoneComponent.setOnClickListener(this);

                } else {

                    Log.i("DXGOP", "SOME INTERVIEW ITS GRADE");
                    this.setComponentRatedFactory();
                }

                break;

            case RequestCandidateInterview.interviewNotMade:
            case RequestCandidateInterview.interviewCancelled:

                if (!RequestCandidateInterview.hasGradedInterviews(this.interviews, this.candidate.getVacancyId())) {

                    interviewNoDoneComponent.setVisibility(View.VISIBLE);
                    ((TextView) interviewNoDoneComponent.findViewById(R.id.comment)).setText( this.interview.getReview() );

                    final Button selectCandidateButton = interviewNoDoneComponent.findViewById(R.id.selectCandidateButton);
                    selectCandidateButton.setTag("32");
                    selectCandidateButton.setOnClickListener(this);

                    final Button newInterviewButton = interviewNoDoneComponent.findViewById(R.id.newInterviewButton);
                    newInterviewButton.setTag("34");
                    newInterviewButton.setOnClickListener(this);

                } else {

                    Log.i("DXGOP", "SOME INTERVIEW NOT MADE -- ITS GRADE");
                    this.setComponentRatedFactory();
                }
                break;
        }
    }

    private void setComponentInterestBaseOnStatusAndInterviews() {

        if (this.interview != null &&
                this.interview.getStatus().equals(RequestCandidateInterview.interviewNotMade) &&
                this.interview.getVacancyId().equals(this.candidate.getVacancyId())) {

            this.setComponentsInterview();

        } else {

            final ConstraintLayout interestComponent = this.mView.findViewById(R.id.interestComponent);
            interestComponent.setVisibility(View.VISIBLE);
            if (this.interview == null) {
                this.interview = new RequestCandidateInterview();
            } else if (this.interview.getStatus().equals(RequestCandidateInterview.interviewCancelled)) {
                this.interview = new RequestCandidateInterview();
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private void setComponentRatedFactory() {

        final ArrayList<RequestCandidateInterview> interviewsGraded =
                RequestCandidateInterview.getGradedInterviews(this.interviews, this.candidate.getVacancyId());

        if (interviewsGraded.size() == 1) {
            this.setComponentRatedSingle(interviewsGraded);
        } else if (interviewsGraded.size() > 1) {

            this.mView.findViewById(R.id.multiRatedComponent).setVisibility(View.GONE);
            this.mView.findViewById(R.id.multiRatedComponentReviewListArrow).setVisibility(View.GONE);
            this.mView.findViewById(R.id.multiRatedComponentReviewList).setVisibility(View.GONE);
            this.mView.findViewById(R.id.multiRatedComponent).
                    findViewById(R.id.ratedComponentEvaluateButton).setVisibility(View.GONE);
            this.getInterviewsRatedFromServer();
        }
    }

    private void setComponentRatedSingle(final ArrayList<RequestCandidateInterview> interviewsGraded) {

        final RequestCandidateInterview preInterview = interviewsGraded.get(0);
        final ConstraintLayout component = this.mView.findViewById(R.id.ratedComponent);
        component.setVisibility(View.VISIBLE);

        // -- Stars
        final RatingStarComplete ratedContainerStars = new RatingStarComplete(this.mView.findViewById(R.id.ratedContainerStars), getContext());
        final String rateString = preInterview.getRatingNumber().substring(0, 1);
        final int rateInt = Integer.parseInt( App.isNumeric(rateString) ? rateString : "0");
        ratedContainerStars.setRating(rateInt);

        final TextView ratedComponentComment = this.mView.findViewById(R.id.ratedComponentComment);
        ratedComponentComment.setText( preInterview.getReview());

        final Button ratedComponentButton = this.mView.findViewById(R.id.ratedComponentButton);
        ratedComponentButton.setTag("32");
        ratedComponentButton.setOnClickListener(this);
        ratedComponentButton.setVisibility(
                (this.candidate.getStatusCandidateId().equals(RequestCandidate.statusCandidateChoose))
                        ? View.GONE : View.VISIBLE );
        this.setViewAsSecondaryColor(ratedComponentButton);

        // --
        final Button ratedComponentNewInterviewButton = this.mView.findViewById(R.id.ratedComponentNewInterviewButton);
        final RelativeLayout interviewComponentInRated = this.mView.findViewById(R.id.interviewComponentInRated);
        final Button ratedComponentEvaluateButton = this.mView.findViewById(R.id.ratedComponentEvaluateButton);
        final ConstraintLayout interviewNoDoneComponent = this.mView.findViewById(R.id.interviewNoDoneComponent);

        ratedComponentNewInterviewButton.setVisibility(View.GONE);
        interviewComponentInRated.setVisibility(View.GONE);
        ratedComponentEvaluateButton.setVisibility(View.GONE);
        interviewNoDoneComponent.setVisibility(View.GONE);

        // -- For current interview
        switch (this.interview.getStatus()) {

            case RequestCandidateInterview.interviewDone:

                ratedComponentNewInterviewButton.setTag("34");
                ratedComponentNewInterviewButton.setOnClickListener(this);
                ratedComponentNewInterviewButton.setVisibility(View.VISIBLE);
                this.setViewAsPrimaryColor(ratedComponentNewInterviewButton);
                break;

            case RequestCandidateInterview.interviewScheduled:
            case RequestCandidateInterview.interviewConfirmed:
            case RequestCandidateInterview.interviewReScheduled:

                if (new Date().after( App.getDate(this.interview.getDate(), "yyyy-MM-dd HH:mm:ss") )) {

                    ratedComponentEvaluateButton.setVisibility(View.VISIBLE);
                    final String n = "" + (interviewsGraded.size() + 1);
                    ratedComponentEvaluateButton.setText(String.format(
                            getString(R.string.recruitment_request_candidate_single_info_component_evaluate_n_interview),
                            4) );

                    ratedComponentEvaluateButton.setTag("31");
                    ratedComponentEvaluateButton.setOnClickListener(this);
                    this.setViewAsPrimaryColor(ratedComponentEvaluateButton);

                    // -- If its chosed
                    if (this.candidate.getStatusCandidateId().equals(RequestCandidate.statusCandidateChoose)) {
                        ratedComponentEvaluateButton.setVisibility(View.GONE);
                        ratedComponentNewInterviewButton.setVisibility(View.GONE);
                        interviewComponentInRated.setVisibility(View.GONE);
                    }
                    return;
                }

                interviewComponentInRated.setVisibility(View.VISIBLE);
                String date = App.getDateFormatted(this.interview.getDate(),
                        "dd/MMM/YY '-' HH:mm'hrs.'", "yyyy-MM-dd HH:mm:ss");
                date = date.toLowerCase().replaceAll("\\.","");

                ((TextView) interviewComponentInRated.findViewById(R.id.date)).setText( date );
                interviewComponentInRated.setTag("30");
                interviewComponentInRated.setOnClickListener(this);
                this.setViewAsPrimaryColor(interviewComponentInRated);
                break;

            case RequestCandidateInterview.interviewNotMade:
            case RequestCandidateInterview.interviewCancelled:

                component.setVisibility(View.GONE);
                interviewNoDoneComponent.setVisibility(View.VISIBLE);

                ((TextView) interviewNoDoneComponent.findViewById(R.id.comment)).setText( this.interview.getReview() );

                final Button selectCandidateButton = interviewNoDoneComponent.findViewById(R.id.selectCandidateButton);
                selectCandidateButton.setTag("32");
                selectCandidateButton.setOnClickListener(this);
                this.setViewAsSecondaryColor(selectCandidateButton);

                final Button newInterviewButton = interviewNoDoneComponent.findViewById(R.id.newInterviewButton);
                newInterviewButton.setTag("34");
                newInterviewButton.setOnClickListener(this);
                break;
        }

        // -- If its chosed
        if (this.candidate.getStatusCandidateId().equals(RequestCandidate.statusCandidateChoose)) {
            ratedComponentEvaluateButton.setVisibility(View.GONE);
            ratedComponentNewInterviewButton.setVisibility(View.GONE);
            interviewComponentInRated.setVisibility(View.GONE);
            interviewNoDoneComponent.setVisibility(View.GONE);
            FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat("ivwc"));
        }
    }

    private void setComponentRatedMultiple(final ArrayList<RequestCandidateInterview> interviewsGraded,
                                           final String average) {

        final ConstraintLayout multiRatedComponent = this.mView.findViewById(R.id.multiRatedComponent);
        final RecyclerView multiRatedComponentReviewList = this.mView.findViewById(R.id.multiRatedComponentReviewList);

        multiRatedComponent.setVisibility(View.VISIBLE);
        multiRatedComponentReviewList.setVisibility(View.VISIBLE);
        this.mView.findViewById(R.id.multiRatedComponentReviewListArrow).setVisibility(View.VISIBLE);

        // -- Average
        final TextView ratedAverage = multiRatedComponent.findViewById(R.id.ratedAverage);
        ratedAverage.setText( average );

        // -- Review List
        App.createRecyclerHorizontalList(multiRatedComponentReviewList, getContext());
        final RequestCandidateInterviewReviewsAdapter adapter =
                new RequestCandidateInterviewReviewsAdapter(getContext(), interviewsGraded);
        multiRatedComponentReviewList.setAdapter(adapter);

        final Button ratedComponentButton = multiRatedComponent.findViewById(R.id.ratedComponentButton);
        ratedComponentButton.setTag("32");
        ratedComponentButton.setOnClickListener(this);
        ratedComponentButton.setVisibility(
                (this.candidate.getStatusCandidateId().equals(RequestCandidate.statusCandidateChoose))
                        ? View.GONE : View.VISIBLE );
        this.setViewAsSecondaryColor(ratedComponentButton);

        // -- Buttons
        final Button ratedComponentNewInterviewButton = multiRatedComponent.findViewById(R.id.ratedComponentNewInterviewButton);
        final RelativeLayout interviewComponentInRated = multiRatedComponent.findViewById(R.id.interviewComponentInRated);
        final Button ratedComponentEvaluateButton = multiRatedComponent.findViewById(R.id.ratedComponentEvaluateButton);

        ratedComponentNewInterviewButton.setVisibility(View.GONE);
        interviewComponentInRated.setVisibility(View.GONE);
        ratedComponentEvaluateButton.setVisibility(View.GONE);

        // -- For current interview
        switch (this.interview.getStatus()) {

            case RequestCandidateInterview.interviewDone:
            case RequestCandidateInterview.interviewCancelled:
            case RequestCandidateInterview.interviewNotMade:

                ratedComponentNewInterviewButton.setTag("34");
                ratedComponentNewInterviewButton.setOnClickListener(this);
                ratedComponentNewInterviewButton.setVisibility(View.VISIBLE);
                this.setViewAsPrimaryColor(ratedComponentNewInterviewButton);
                break;

            case RequestCandidateInterview.interviewScheduled:
            case RequestCandidateInterview.interviewConfirmed:
            case RequestCandidateInterview.interviewReScheduled:

                if (new Date().after( App.getDate(this.interview.getDate(), "yyyy-MM-dd HH:mm:ss") )) {

                    ratedComponentEvaluateButton.setVisibility(View.VISIBLE);
                    final String n = "" + (interviewsGraded.size() + 1);
                    ratedComponentEvaluateButton.setText( String.format(
                            getString(R.string.recruitment_request_candidate_single_info_component_evaluate_n_interview),
                            4) );

                    ratedComponentEvaluateButton.setTag("31");
                    ratedComponentEvaluateButton.setOnClickListener(this);
                    this.setViewAsPrimaryColor(ratedComponentEvaluateButton);

                    // -- If its chosed
                    if (this.candidate.getStatusCandidateId().equals(RequestCandidate.statusCandidateChoose)) {
                        ratedComponentEvaluateButton.setVisibility(View.GONE);
                        ratedComponentNewInterviewButton.setVisibility(View.GONE);
                        interviewComponentInRated.setVisibility(View.GONE);
                    }
                    return;
                }

                interviewComponentInRated.setVisibility(View.VISIBLE);
                String date = App.getDateFormatted(this.interview.getDate(),
                        "dd/MMM/YY '-' HH:mm'hrs.'", "yyyy-MM-dd HH:mm:ss");
                date = date.toLowerCase().replaceAll("\\.","");

                ((TextView) interviewComponentInRated.findViewById(R.id.date)).setText( date );
                interviewComponentInRated.setTag("30");
                interviewComponentInRated.setOnClickListener(this);
                this.setViewAsPrimaryColor(interviewComponentInRated);
                break;
        }

        // -- If its chosed
        if (this.candidate.getStatusCandidateId().equals(RequestCandidate.statusCandidateChoose)) {
            ratedComponentEvaluateButton.setVisibility(View.GONE);
            ratedComponentNewInterviewButton.setVisibility(View.GONE);
            interviewComponentInRated.setVisibility(View.GONE);
            FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat("ivwc"));
        }
    }

    private void paintAboutCandidate() {

        final TextView aboutContainerTitle = this.mView.findViewById(R.id.aboutContainerTitle);
        final String aboutContainerTitleString = String.format(
                this.mContext.getString(R.string.recruitment_request_candidate_single_about_container_title),
                this.candidate.getFirstNameShort().substring(0,1).toUpperCase() +
                        this.candidate.getFirstNameShort().substring(1).toLowerCase());
        aboutContainerTitle.setText( aboutContainerTitleString );

        // --
        final TextView aboutContainerContent = this.mView.findViewById(R.id.aboutContainerContent);
        aboutContainerContent.setText( this.candidate.getDescription() );
        if (this.candidate.getDescription().length() > 190) {

            final Button aboutContainerButton = this.findView(R.id.aboutContainerButton);
            aboutContainerButton.setVisibility(View.VISIBLE);
            aboutContainerButton.setTag("40");
            aboutContainerButton.setOnClickListener(this);
            this.setViewAsPrimaryColor(aboutContainerButton);
        }
    }

    private void paintSkillsOfCandidate() {

        // -- SOFT
        final RecyclerView softSkillsContainerList = this.mView.findViewById(R.id.softSkillsContainerList);
        App.createVerticalRecyclerList(softSkillsContainerList, getContext());
        final RequestCandidateSkillsAdapter softAdapter = new RequestCandidateSkillsAdapter(this.candidate.getSoftSkills());
        softSkillsContainerList.setAdapter(softAdapter);

        // -- HARD
        final RecyclerView hardSkillsContainerList = this.mView.findViewById(R.id.hardSkillsContainerList);
        App.createVerticalRecyclerList(hardSkillsContainerList, getContext());
        final RequestCandidateSkillsAdapter hardAdapter = new RequestCandidateSkillsAdapter(this.candidate.getHardSkills());
        hardSkillsContainerList.setAdapter(hardAdapter);

        // -- LANGUAGE
        final RecyclerView languagesContainerList = this.mView.findViewById(R.id.languagesContainerList);
        App.createVerticalRecyclerList(languagesContainerList, getContext());
        final RequestCandidateSkillsAdapter langAdapter = new RequestCandidateSkillsAdapter(this.candidate.getLanguageSkills());
        languagesContainerList.setAdapter(langAdapter);

        final Button languagesContainerMoreInfoButton = this.mView.findViewById(R.id.languagesContainerMoreInfoButton);
    }

    private void paintMenu(boolean clean) {

        final ArrayList<RequestCandidateMessage> notReadMessages = RequestCandidateMessage.howManyWithoutRead(this.messages);
        final int messagesWithOutRead = (this.messages != null) ? notReadMessages.size() : 0;
        final int numTotalMessages = (this.messages != null) ? this.messages.size() : 0;

        // -- Init view for menu dots
        final ImageView dotsMenu = this.findView(R.id.dotsMenu);
        dotsMenu.setTag("41");
        dotsMenu.setOnClickListener(this);

        final LottieAnimationView dotsMenuAnimation = this.findView(R.id.dotsMenuAnimation);
        dotsMenuAnimation.setTag("41");
        dotsMenuAnimation.setOnClickListener(this);

        if (messagesWithOutRead > 0) {
            dotsMenu.setVisibility(View.GONE);
            dotsMenuAnimation.setVisibility(View.VISIBLE);
        }

        /** Menu Options **/
        final ConstraintLayout dotsMenuOpened = this.findView(R.id.dotsMenuOpened);
        final RecyclerView optionsMenu = dotsMenuOpened.findViewById(R.id.options);
        final RequestCandidateMenuAdapter adapter = new RequestCandidateMenuAdapter(CandidateMenuModel.getMenu(getContext()));
        adapter.setMessagesWithOutRead(messagesWithOutRead);
        adapter.setListener(option -> {

            switch (option.getId()) {

                case 1:
                    this.openChat();
                    break;

                case 2:
                    this.downloadCV();
                    break;

                case 3:
                    this.shareCV();
                    break;
            }
        });
        App.createVerticalRecyclerList(optionsMenu, this.mContext);
        optionsMenu.setAdapter(adapter);

        // --
        if (this.interview != null) {

            if (interview.getCandidate() == null) {
                interview.setCandidate(this.candidate);
            }
        }
    }

    private void setFavoriteStatusToCandidate() {

        showLoader();
        CandidateService.setFavoriteStatusToCandidate(this.candidate,
                Session.get(getContext()), new ReSTCallback() {
                    @Override
                    public void onSuccess(ReSTResponse response) {

                        hideLoader();
                        candidate.setFavorite(!candidate.getFavorite());
                        if (candidate.getFavorite()) {
                            favoriteButton.setImageBitmap(App.getImage(getContext(), "fragment_recruitment_request_candidate_single_star_filled"));
                        } else {
                            favoriteButton.setImageBitmap(App.getImage(getContext(), "fragment_recruitment_request_candidate_single_star"));
                        }
                    }

                    @Override
                    public void onError(ReSTResponse response) {
                        hideLoader();
                    }
                });
    }

    private void notInterestInCandidate() {

        final NoCandidateInterestModal noCandidateInterestModal =
                new NoCandidateInterestModal(getContext(), this.candidate);
        noCandidateInterestModal.setListener(() -> {
            getCandidate();
        });
        noCandidateInterestModal.show();
    }

    private void interestInCandidate() {

        if (interview.getStatus().equals("")) {
            this.interview.setStatus( RequestCandidateInterview.interviewScheduled );
        }

        // --
        Bundle parameters = new Bundle();
        final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
        mFirebaseAnalytics.logEvent("Candidato_Seleccionado", parameters);

        // --
        final RecruitmentRequestCreateInterviewFragment fragment = new RecruitmentRequestCreateInterviewFragment();
        fragment.setInterview(interview);
        fragment.setListener(this);
        fragment.setType( (this.interview.getId().equals("") ?
                RecruitmentRequestCreateInterviewFragment.RecruitmentRequestCreateInterviewType.create :
                RecruitmentRequestCreateInterviewFragment.RecruitmentRequestCreateInterviewType.edit) );
        this.continueSegue(fragment);
    }

    private void interestInCandidateAgain() {

        // --
        Bundle parameters = new Bundle();
        final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
        mFirebaseAnalytics.logEvent("Candidato_Reconsiderado", parameters);

        // --
        RequestCandidateInterview interviewTemp = new RequestCandidateInterview();
        interviewTemp.setCandidate(this.candidate);

        final RecruitmentRequestCreateInterviewFragment fragment = new RecruitmentRequestCreateInterviewFragment();
        fragment.setInterview(interviewTemp);
        fragment.setListener(this);
        fragment.setType(RecruitmentRequestCreateInterviewFragment.RecruitmentRequestCreateInterviewType.createAgain);
        this.continueSegue(fragment);
    }

    /** Getters -- From Server **/
    private void getInterviews(RecruitmentRequestCandidateInterviewActionType type) {

        Log.i("DXGOP", "CANDIDATE VACANCY ID :::: " + this.candidate.getVacancyId());
        if (interview != null) { Log.i("DXGOP", "INTERVIEW ID :::: " + this.interview.getId()); }
        else { Log.i("DXGOP", "INTERVIEW IS NULL "); }

        if (this.interview == null || this.interview.getId().equals("")) {

            InterviewService.get(this.candidate.getId(), new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {

                    JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                    interviews = RequestCandidateInterview.parse(res, candidate.getVacancyId(), getContext());

                    /*if (interviews.size() > 0) {
                        interview = interviews.get( interviews.size() - 1 );
                    }*/

                    interview = RequestCandidateInterview.getLastInterview(interviews, candidate.getVacancyId());
                    setInterviewRequestAction(type);
                }

                @Override
                public void onError(ReSTResponse response) {

                    //interview = null;
                    if (type == RecruitmentRequestCandidateInterviewActionType.forInterest) {
                        setInterviewRequestAction(type);
                    }
                }
            });

        } else {
            this.setInterviewRequestAction(type);
        }
    }

    private void setInterviewRequestAction(RecruitmentRequestCandidateInterviewActionType type) {

        switch(type) {

            case single:
                this.setComponentsInterview(); // -- Only have 1 interview WITHOUT grades
                break;

            case multi:
                this.setComponentRatedFactory(); // -- Have more than 1 interview whit grades
                break;

            case forInterest:
                this.setComponentInterestBaseOnStatusAndInterviews();
                break;
        }
    }

    private void getInterviewsRatedFromServer() {

        InterviewService.getRated(this.candidate, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                JSONObject res = JsonBuilder.stringToJson(response.body);
                final String average = res.optString("rating", "0");
                final JSONArray interviews = res.optJSONArray("interviewList");
                setComponentRatedMultiple( RequestCandidateInterview.parse(interviews, candidate.getVacancyId(), getContext()), average );
            }

            @Override
            public void onError(ReSTResponse response) {}
        });
    }

    private void getMessages() {

        CandidateService.getMessages(this.candidate, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                messages = RequestCandidateMessage.parse(res,
                        Session.get(getContext()).getDataSessionId());
                paintMenu(false);
            }

            @Override
            public void onError(ReSTResponse response) {
                messages = new ArrayList<>();
                paintMenu(false);
            }
        });
    }

    private void downloadCV() {

        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        Permissions.check(mContext, permissions, null, null, new PermissionHandler() {
            @Override
            public void onGranted() {

                final String currentDateTime = App.getCurrentDateTime();
                //final String destinationUrl = getApp().getExternalCacheDir().toString()+ "/" + candidate.getFirstName() + "_CV_" + currentDateTime + ".pdf";
                //final String destinationUrl = getApp().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + candidate.getFirstName() + "_CV_" + currentDateTime + ".pdf";
                final String destinationUrl = App.getDownloadPath() + "/" + candidate.getFirstName() + "_CV_" + currentDateTime + ".pdf";
                Log.d("DXGOP", "CV :: " + destinationUrl);
                final Uri uri = Uri.parse(Api.getInstance().getUrl("/talent/" + candidate.getTalentId() + "/downloadcv"));
                final Uri destinationUri = Uri.parse(destinationUrl);
                DownloadRequest downloadRequest = new DownloadRequest(uri)
                        .addCustomHeader("Authorization", "Bearer " + Session.get(App.getAppContext()).getIdToken() )
                        .setRetryPolicy(new DefaultRetryPolicy(150000, 1, 1f))
                        .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                        .setDownloadContext(this)
                        .setStatusListener(new DownloadStatusListenerV1() {
                            @Override
                            public void onDownloadComplete(DownloadRequest downloadRequest) {

                                Log.d("DXGOP", "BAJADO EL PDF en :: " + destinationUrl);
                                final Notification notification = new Notification(mContext, R.layout.notification_light);
                                notification.setMessage(String.format(
                                        getString(R.string.recruitment_request_candidate_resume_donwload_title),
                                        destinationUrl));
                                notification.show(3000);
                                //hareCVLocal(destinationUri);

                                // --
                                Bundle parameters = new Bundle();
                                final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
                                mFirebaseAnalytics.logEvent("CV_Descargado", parameters);
                            }

                            @Override
                            public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {

                                final Notification notification = new Notification(mContext, R.layout.notification_light_error);
                                notification.setMessage(errorMessage + " -- Code :: " + errorCode);
                                notification.show();
                                Log.d("DXGOP", "ERROR BAJANDO EL PDF");
                            }

                            @Override
                            public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {
                                Log.d("DXGOP", "PROGRESS ::: " + progress);
                            }
                        });

                ThinDownloadManager downloadManager = new ThinDownloadManager();
                downloadManager.add(downloadRequest);
            }
        });
    }

    private void shareCVLocal(Uri url) {

        try {

            File tmpFile = new File(url.getPath());
            Uri uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID, tmpFile.getAbsoluteFile());
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("application/pdf");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(share, "Audio teilen"));

        } catch (Exception e) {

            Log.d("DXGOP", "ERROR ::: " + e.toString());
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private void shareCV() {

        final RecruitmentRequestCandidateShare candidateShare = new RecruitmentRequestCandidateShare();
        candidateShare.setTalentId(this.candidate.getTalentId());
        candidateShare.setListener(this);
        this.continueSegueUp(candidateShare);
    }

    /** Open Actions **/
    private void openChooseModal() {

        final ChooseCandidateModal modal = new ChooseCandidateModal(getContext(), this.candidate);
        modal.setListener(this);
        modal.show();
    }

    private void openCloseDotsMenu() {

        this.findView(R.id.dotsMenuOpened).setVisibility( (dotsMenuOpened) ? View.VISIBLE : View.INVISIBLE  );
        this.dotsMenuOpened = !this.dotsMenuOpened;
    }

    private void openChat() {

        final RecruitmentRequestCandidateMessagesFragment messagesFragment = new RecruitmentRequestCandidateMessagesFragment();
        messagesFragment.setInterview(this.interview);
        messagesFragment.setMessages(this.messages);
        this.paintMenu(true);
        this.continueSegueUp(messagesFragment);
    }

    /** Setters **/
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public void setCandidate(RequestCandidate candidate) {
        this.candidate = candidate;
    }

    public void setNotificationCenterListener(NotificationCenterFragmentListener notificationCenterListener) {
        this.notificationCenterListener = notificationCenterListener;
    }

    /** Implementations **/

    @Override
    public void onClick(View v) {

        final String tag = String.valueOf(v.getTag());
        switch(tag) {

            case "2":
                this.openChat();
                break;

            case "8":
                this.notInterestInCandidate();
                break;

            case "9":
            case "30":
            case "33":
                this.interestInCandidate();
                break;

            case "10":
                this.setFavoriteStatusToCandidate();
                break;

            case "11":

                final RecruitmentRequestCandidateWorkExperienceFragment workExperienceFragment =
                        new RecruitmentRequestCandidateWorkExperienceFragment();
                workExperienceFragment.setCandidate(this.candidate);
                this.continueSegue(workExperienceFragment);
                break;

            case "12":

                final RecruitmentRequestCandidateEducationFragment educationFragment = new RecruitmentRequestCandidateEducationFragment();
                educationFragment.setCandidate(this.candidate);
                this.continueSegue(educationFragment);
                break;

            case "13":
                FirebaseEvent.logEvent(mContext, "video_candidato");
                final RecruitmentRequestCandidateLanguagesFragment languagesFragment = new RecruitmentRequestCandidateLanguagesFragment();
                languagesFragment.setCandidate(this.candidate);
                this.continueSegue(languagesFragment);
                break;

            case "15":

                //final LanguagesInfoFragment languagesInfoFragment = new LanguagesInfoFragment();
                //this.continueSegue(languagesInfoFragment);

                final LanguagesModal languagesModal = new LanguagesModal(getContext());
                languagesModal.setFromCandidate(true);
                languagesModal.show();

                break;

            case "31":

                final RecruitmentRequestRateInterviewFragment rateInterviewFragment = new RecruitmentRequestRateInterviewFragment();
                rateInterviewFragment.setInterview(this.interview);
                rateInterviewFragment.setListener(this);
                rateInterviewFragment.setInterviewListener(this);
                this.continueSegue(rateInterviewFragment);
                break;

            case "32":
                this.openChooseModal();
                break;

            case "34":
                this.interestInCandidateAgain();
                break;

            case "40":

                final RecruitmentRequestCandidateDescriptionFragment descriptionFragment =
                        new RecruitmentRequestCandidateDescriptionFragment();
                descriptionFragment.setCandidate(this.candidate);
                this.continueSegue(descriptionFragment);
                break;

            case "41":
                    this.openCloseDotsMenu();
                break;
        }
    }

    @Override
    public void onScheduledSuccess(RequestCandidateInterview interview) {

        // --
        final Notification notification = new Notification(getContext(), R.layout.notification_light);
        notification.setMessage(getString(R.string.recruitment_request_candidate_single_interview_notification_scheduled));
        notification.show(4000);

        // --
        this.candidate.setStatusCandidateId( RequestCandidate.statusCandidateInterviews );
        this.interview = interview;
        this.setComponentBaseOnStatus();
    }

    @Override
    public void onReScheduledSuccess(RequestCandidateInterview interview) {

        // --
        final Notification notification = new Notification(getContext(), R.layout.notification_light);
        notification.setMessage(getString(R.string.recruitment_request_candidate_single_interview_notification_updated));
        notification.show(4000);

        // --
        this.candidate.setStatusCandidateId( RequestCandidate.statusCandidateInterviews );
        this.interview = interview;
        this.interview.setStatus(RequestCandidateInterview.interviewReScheduled);
        this.setComponentBaseOnStatus();
    }

    @Override
    public void onCancel(RequestCandidateInterview interview) {

        // --
        final Notification notification = new Notification(getContext(), R.layout.notification_light);
        notification.setMessage(getString(R.string.recruitment_request_candidate_single_interview_notification_cancelled));
        notification.show(4000);

        this.interview.setId("");

        /*if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }*/

        this.interview = interview;
        this.interview.setStatus(RequestCandidateInterview.interviewCancelled);
        //this.candidate.setStatusCandidateId( RequestCandidate.statusCandidate );
        this.setComponentBaseOnStatus();
    }

    @Override
    public void onCreateNewAgain(RequestCandidateInterview interview) {}

    @Override
    public void onRated(RequestCandidateInterview interview) {

        // --
        final Notification notification = new Notification(getContext(), R.layout.notification_light);
        notification.setMessage(getString(R.string.recruitment_request_candidate_single_interview_notification_saved));
        notification.show(4000);

        // --
        this.candidate.setStatusCandidateId( RequestCandidate.statusCandidateGraded );
        this.interview = interview;
        this.interview.setStatus(RequestCandidateInterview.interviewDone);
        this.setComponentBaseOnStatus();
    }

    @Override
    public void onNotMade(RequestCandidateInterview interview) {

        // --
        final Notification notification = new Notification(getContext(), R.layout.notification_light);
        notification.setMessage(getString(R.string.recruitment_request_candidate_single_interview_notification_saved_2));
        notification.show(4000);

        /*this.interview = interview;
        this.interview.setStatus( RequestCandidateInterview.interviewNotMade );
        this.setComponentBaseOnStatus();*/
        back();
    }

    @Override
    public void onCandidateChoose() {

        final Notification notification = new Notification(getContext(), R.layout.notification_light);
        notification.setMessage(getString(R.string.recruitment_request_candidate_single_interview_notification_congrat));
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat("choose"));
        notification.show(4000);

        // --
        this.candidate.setStatusCandidateId( RequestCandidate.statusCandidateChoose );
        this.setComponentBaseOnStatus();
    }

    @Override
    public void onSingleCVShare() {

        final Notification notification = new Notification(mContext, R.layout.notification_light);
        notification.setMessage(getString(R.string.recruitment_request_candidate_share_success_notification));
        notification.show(1500);
    }

    @Override
    public void onMultipleCVShare() {}

    @Override
    public void onErrorSharing(@NotNull String error) {

        final Notification notification = new Notification(mContext, R.layout.notification_light_error);
        notification.setMessage(error);
        notification.show(1500);
    }
}