package com.stratis.estaffing.Control.Searcher.View;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez - App Builders CTO
 * Revision 1 - 01/09/20
 */

public class SearchLanguageView extends RecyclerView.ViewHolder {

    private TextView searchComponentBoxItemText;
    private ImageView searchComponentBoxItemIcon;

    public SearchLanguageView(View itemView) {

        // --
        super(itemView);

        // --
        this.searchComponentBoxItemText = itemView.findViewById(R.id.searchComponentBoxItemText);
        this.searchComponentBoxItemIcon = itemView.findViewById(R.id.searchComponentBoxItemIcon);
    }

    public TextView getSearchComponentBoxItemText() {
        return searchComponentBoxItemText;
    }

    public ImageView getSearchComponentBoxItemIcon() {
        return searchComponentBoxItemIcon;
    }
}
