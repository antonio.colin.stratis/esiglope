package com.stratis.estaffing.Modal.NotificationCenterFlow.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Modal.NotificationCenterFlow.Model.NotificationCenterFilterModel;
import com.stratis.estaffing.Modal.NotificationCenterFlow.NotificationCenterModal;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.View.RequestFilterStatusView;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class NotificationCenterFilterAdapter extends RecyclerView.Adapter<RequestFilterStatusView> {

    protected ArrayList<NotificationCenterFilterModel> items;
    protected Context context;

    protected String currentFilter = "";
    protected NotificationCenterModal.NotificationCenterFilterListener listener;
    protected BottomSheetDialog parent;

    public NotificationCenterFilterAdapter(ArrayList<NotificationCenterFilterModel> items, Context context) {

        this.context = context;
        this.items = items;
    }

    @Override
    public RequestFilterStatusView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.fragment_recruitment_requests_modal_item, parent, false);
        return new RequestFilterStatusView(layoutView, false);
    }

    @Override
    public void onBindViewHolder(final RequestFilterStatusView holder, final int position) {

        holder.setIsRecyclable(false);
        final NotificationCenterFilterModel item = this.items.get(position);

        // -- Icon
        holder.getIcon().setImageBitmap( item.getIcon(this.context) );

        // --
        if (this.currentFilter != null && this.currentFilter.equals(item.getId()))  {

            holder.getBox().setBackgroundDrawable( ContextCompat.getDrawable(context, R.drawable.button_orange_selected) );
            holder.getTitle().setTextColor(Color.WHITE);
            holder.getIcon().setImageBitmap(App.getImage(context, item.getIcon() + "_white"));
        }
        holder.getTitle().setText(item.getName());

        // --
        holder.getBox().setOnClickListener(v -> {
            if (listener != null) {
                listener.onFilterClick(item);
            }

            if (parent != null) {
                parent.cancel();
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    // --
    public void setCurrentFilter(String currentFilter) {
        this.currentFilter = currentFilter;
    }

    public void setListener(NotificationCenterModal.NotificationCenterFilterListener listener) {
        this.listener = listener;
    }

    public void setParent(BottomSheetDialog parent) {
        this.parent = parent;
    }

    public interface RequestCandidatesFilterAdapterListener {
        public void onFilterClick(String filterId);
    }
}