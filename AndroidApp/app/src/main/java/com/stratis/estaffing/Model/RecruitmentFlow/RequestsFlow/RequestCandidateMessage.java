package com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 08/01/21
 */

public class RequestCandidateMessage {

    public enum RequestCandidateMessageType { out, in }

    private String id = "";
    private String vacancyCandidateId = "";
    private String date = "";
    private String message = "";
    private String authorId = "";
    private boolean viewed = false;

    private RequestCandidateMessageType type = RequestCandidateMessageType.in;

    public RequestCandidateMessage() {}

    public RequestCandidateMessage(JSONObject obj, String dataSessionId) {

        if (obj != null) {

            this.id = obj.optString("id");
            this.vacancyCandidateId = obj.optString("vacancyCandidateId");
            this.date = obj.optString("date");
            this.message = obj.optString("message");
            this.authorId = obj.optString("authorId");
            this.viewed = obj.optBoolean("viewed");

            // -- Check
            if (this.authorId.equals(dataSessionId)) {
                this.type = RequestCandidateMessageType.out;
            }
        }
    }

    public static ArrayList<RequestCandidateMessage> parse(JSONArray array, String dataSessionId) {

        ArrayList<RequestCandidateMessage> messages = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {

            JSONObject obj = array.optJSONObject(i);
            messages.add(new RequestCandidateMessage(obj, dataSessionId));
        }
        return messages;
    }

    public static ArrayList<RequestCandidateMessage> howManyWithoutRead(ArrayList<RequestCandidateMessage> messages) {

        ArrayList<RequestCandidateMessage> noRead = new ArrayList<>();
        if (messages != null) {
            for (RequestCandidateMessage message : messages) {
                if (!message.isViewed() && message.type == RequestCandidateMessageType.in) {
                    noRead.add(message);
                }
            }
        }
        return noRead;
    }

    // --
    public String getId() {
        return id;
    }

    public String getVacancyCandidateId() {
        return vacancyCandidateId;
    }

    public String getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public String getAuthorId() {
        return authorId;
    }

    public boolean isViewed() {
        return viewed;
    }

    public RequestCandidateMessageType getType() {
        return type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setVacancyCandidateId(String vacancyCandidateId) {
        this.vacancyCandidateId = vacancyCandidateId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }

    public void setType(RequestCandidateMessageType type) {
        this.type = type;
    }
}