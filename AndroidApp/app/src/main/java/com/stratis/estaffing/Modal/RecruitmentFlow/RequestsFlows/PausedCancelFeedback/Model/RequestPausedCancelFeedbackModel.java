package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.Model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.RequestPausedCancelModal.otherOptionHardcode;

/**
 * Created by Erick Sanchez
 * Revision 1 - 16/12/20
 */

public class RequestPausedCancelFeedbackModel {

    private String catalogId = "";
    private String catalogItemId = "";
    private String valueItem = "";
    private String description = "";
    private Boolean selected = false;

    public RequestPausedCancelFeedbackModel() {}

    public RequestPausedCancelFeedbackModel(JSONObject object) {

        if (object != null) {

            this.catalogId = object.optString("catalogId");
            this.catalogItemId = object.optString("catalogItemId");
            this.valueItem = object.optString("valueItem");
            this.description = object.optString("description");
        }
    }

    public static ArrayList<RequestPausedCancelFeedbackModel> parse(JSONArray array) {

        if (array != null) {

            ArrayList<RequestPausedCancelFeedbackModel> pausedCancelModels = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                pausedCancelModels.add(new RequestPausedCancelFeedbackModel(obj));
            }

            // --
            final RequestPausedCancelFeedbackModel other = new RequestPausedCancelFeedbackModel();
            other.setCatalogId("");
            other.setCatalogItemId("");
            other.setValueItem(otherOptionHardcode);
            other.setDescription(otherOptionHardcode);
            pausedCancelModels.add(other);

            // --
            return pausedCancelModels;
        }
        return null;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getCatalogItemId() {
        return catalogItemId;
    }

    public void setCatalogItemId(String catalogItemId) {
        this.catalogItemId = catalogItemId;
    }

    public String getValueItem() {
        return valueItem;
    }

    public void setValueItem(String valueItem) {
        this.valueItem = valueItem;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
