package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.analytics.FirebaseAnalytics
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.LoadingButton
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.FirebaseEvent
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.CandidateService
import com.stratis.estaffing.Service.Talent.TalentService

open class RecruitmentRequestCandidateShare : ABFragment() {

    enum class RecruitmentRequestCandidateShareType { single, all }

    open var talentId:String? = null
    open var requestId:String? = null
    var shareButton:LoadingButton? = null
    var listener:RecruitmentRequestCandidateShareListener? = null
    var type:RecruitmentRequestCandidateShareType = RecruitmentRequestCandidateShareType.single

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_candidate_share, container, false)
        FirebaseEvent.logEvent(mContext, this::class.simpleName.plus(type))
        this.initViews()
        return this.mView
    }

    private fun initViews() {

        // -- Close button
        this.findView<ImageView>(R.id.closeButton).setOnClickListener { v -> back() }

        // -- Title
        val title = findView<TextView>(R.id.title)
        if (this.type == RecruitmentRequestCandidateShareType.all) {
            title.text = getString(R.string.recruitment_request_candidate_all_share_title)
        }

        // -- Inputs
        val emailInput = this.findView<EditText>(R.id.email)
        val errorEmail = this.findView<TextView>(R.id.emailError)

        val shareButtonText = if (this.type == RecruitmentRequestCandidateShareType.single)
            getString(R.string.recruitment_request_candidate_share_share_button) else
            getString(R.string.recruitment_request_candidate_share_share_all_button)

        this.shareButton = LoadingButton(this.findView(R.id.shareButton))
        shareButton?.button?.setBackgroundResource( R.drawable.primary_outline_10_button )
        shareButton?.setText(shareButtonText)
        shareButton?.text?.setTextColor(Color.parseColor("#001330"))
        shareButton?.changeBarColor("#001330")
        shareButton?.button?.setOnClickListener { v ->

            shareButton?.showLoading()
            errorEmail.visibility = View.GONE
            emailInput.background.setTint(Color.parseColor("#001330"))

            val emailString = emailInput.text.toString()
            if (!App.isEmailValid(emailString)) {

                // -- Error
                emailInput.background.setTint(Color.parseColor("#C9040F"))
                errorEmail.visibility = View.VISIBLE
                shareButton?.hideLoading()

            } else {

                if (this.type == RecruitmentRequestCandidateShareType.single) {
                    this.shareSingleCV(emailString)
                } else {
                    this.shareMultipleCV(emailString)
                }
            }
        }
    }

    private fun shareSingleCV(email:String) {

        val session = Session.get(mContext)
        TalentService.shareCV(session, talentId, email, object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {

                // --
                val parameters = Bundle()
                val mFirebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
                mFirebaseAnalytics.logEvent("CV_Compartido", parameters)

                // --
                shareButton?.hideLoading()
                if (listener != null) {
                    listener?.onSingleCVShare()
                }
                FirebaseEvent.logEvent(mContext, this::class.simpleName.plus("shared"))
                back()
            }

            override fun onError(response: ReSTResponse) {
                shareButton?.hideLoading()
                if (listener != null) {
                    listener?.onErrorSharing(response.body)
                }
                back()
            }
        })
    }

    private fun shareMultipleCV(email:String) {

        val session = Session.get(mContext)
        CandidateService.shareCV(session, this.requestId, email, object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {

                // --
                val parameters = Bundle()
                val mFirebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
                mFirebaseAnalytics.logEvent("Candidatos_Compartidos", parameters)

                // --
                shareButton?.hideLoading()
                if (listener != null) {
                    listener?.onMultipleCVShare()
                }
                FirebaseEvent.logEvent(mContext, this::class.simpleName.plus("shared"))
                back()
            }

            override fun onError(response: ReSTResponse) {
                shareButton?.hideLoading()
                if (listener != null) {
                    listener?.onErrorSharing(response.body)
                }
                back()
            }
        })
    }

    open interface RecruitmentRequestCandidateShareListener {

        fun onSingleCVShare() {}
        fun onMultipleCVShare() {}
        fun onErrorSharing(error:String) {}
    }
}