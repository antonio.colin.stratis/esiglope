package com.stratis.estaffing.Modal;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.stratis.estaffing.Adapter.SkillAdapter;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Model.Skill;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 28/07/20
 */

public class LanguagesModal extends Dialog {

    private Button closeButton = null;
    private boolean fromCandidate = false;

    public LanguagesModal(Context context) {

        super(context);
        this.init();
    }

    public LanguagesModal(Context context, int themeResId) {

        super(context, themeResId);
        this.init();
    }

    protected LanguagesModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.init();
    }

    @SuppressLint("ResourceType")
    private void init() {

        // -- R styles
        this.setContentView(R.layout.modal_languages);
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat(this.fromCandidate ? "true" : ""));
        this.setCanceledOnTouchOutside(false);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow()
                .setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );

        // --
        this.setup();

        // --
        this.closeButton = this.findViewById(R.id.closeButton);
        this.closeButton.setOnClickListener(v -> cancel());
        this.closeButton.setBackgroundResource(R.drawable.activity_login_controller_login_button);
        App.setViewAsSecondaryColor(this.closeButton, getContext());
    }

    private void setup() {

        // -- Basic
        // --
        final ConstraintLayout basic_one = this.findViewById(R.id.basic_one);
        final TextView basic_one_level = basic_one.findViewById(R.id.textView4);
        final TextView basic_one_desc = basic_one.findViewById(R.id.textView5);
        basic_one_level.setText( this.getStr(R.string.modal_languages_basic_level_one) );
        basic_one_desc.setText( this.getStr(R.string.modal_languages_basic_desc_one) );

        // --
        final ConstraintLayout basic_two = this.findViewById(R.id.basic_two);
        final TextView basic_two_level = basic_two.findViewById(R.id.textView4);
        final TextView basic_two_desc = basic_two.findViewById(R.id.textView5);
        basic_two_level.setText( this.getStr(R.string.modal_languages_basic_level_two) );
        basic_two_desc.setText( this.getStr(R.string.modal_languages_basic_desc_two) );


        // -- Medium
        // --
        final ConstraintLayout medium_one = this.findViewById(R.id.medium_one);
        final TextView medium_one_level = medium_one.findViewById(R.id.textView4);
        final TextView medium_one_desc = medium_one.findViewById(R.id.textView5);
        medium_one_level.setText( this.getStr(R.string.modal_languages_medium_level_one) );
        medium_one_level.setBackgroundColor(getContext().getColor(R.color.modal_languages_medium_color));
        medium_one_desc.setText( this.getStr(R.string.modal_languages_medium_desc_one) );
        medium_one_desc.setBackground(getContext().getResources().getDrawable(R.drawable.modal_languages_medium));

        // --
        final ConstraintLayout medium_two = this.findViewById(R.id.medium_two);
        final TextView medium_two_level = medium_two.findViewById(R.id.textView4);
        final TextView medium_two_desc = medium_two.findViewById(R.id.textView5);
        medium_two_level.setText( this.getStr(R.string.modal_languages_medium_level_two) );
        medium_two_level.setBackgroundColor(getContext().getColor(R.color.modal_languages_medium_color));
        medium_two_desc.setText( this.getStr(R.string.modal_languages_medium_desc_two) );
        medium_two_desc.setBackground(getContext().getResources().getDrawable(R.drawable.modal_languages_medium));


        // -- Advance
        // --
        final ConstraintLayout advance_one = this.findViewById(R.id.advance_one);
        final TextView advance_one_level = advance_one.findViewById(R.id.textView4);
        final TextView advance_one_desc = advance_one.findViewById(R.id.textView5);
        advance_one_level.setText( this.getStr(R.string.modal_languages_advance_level_one) );
        advance_one_level.setBackgroundColor(getContext().getColor(R.color.modal_languages_advance_color));
        advance_one_desc.setText( this.getStr(R.string.modal_languages_advance_desc_one) );
        advance_one_desc.setBackground(getContext().getResources().getDrawable(R.drawable.modal_languages_advance));

        // --
        final ConstraintLayout advance_two = this.findViewById(R.id.advance_two);
        final TextView advance_two_level = advance_two.findViewById(R.id.textView4);
        final TextView advance_two_desc = advance_two.findViewById(R.id.textView5);
        advance_two_level.setText( this.getStr(R.string.modal_languages_advance_level_two) );
        advance_two_level.setBackgroundColor(getContext().getColor(R.color.modal_languages_advance_color));
        advance_two_desc.setText( this.getStr(R.string.modal_languages_advance_desc_two) );
        advance_two_desc.setBackground(getContext().getResources().getDrawable(R.drawable.modal_languages_advance));
    }

    public String getStr(int resource) {
        return getContext().getResources().getString(resource);
    }

    public void setFromCandidate(boolean fromCandidate) {
        this.fromCandidate = fromCandidate;
    }
}
