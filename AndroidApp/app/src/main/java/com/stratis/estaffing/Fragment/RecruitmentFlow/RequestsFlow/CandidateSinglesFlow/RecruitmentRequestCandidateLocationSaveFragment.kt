package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.stratis.estaffing.Control.eStaffingFragment
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.RecruitmentFlow.LocationPlace
import com.stratis.estaffing.R

class RecruitmentRequestCandidateLocationSaveFragment : eStaffingFragment() {

    var currentLocationPlace = 0
//    var locationPlaces : MutableList<LocationPlace> = ArrayList()
    var locationPlace: LocationPlace? = null
    var name : String? = null
    private var locationName : EditText? = null
    private var wordCounter: TextView? = null
    private var listener : LocationSaveListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mView = inflater.inflate(R.layout.fragment_request_vacancy_location_save, container, false)

        init()

        return mView
    }

    private fun init() {
        locationName = mView.findViewById(R.id.location_name)
        if(!App.isEmpty(name)) {
            locationName!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 35f);
            locationName!!.setText(name)
        }
        wordCounter = mView.findViewById(R.id.word_counter)
        wordCounter!!.text = String.format(getString(R.string.request_vacancy_location_place_save_counter), locationName!!.text.length)
        locationNameTextListener()
    }

    private fun locationNameTextListener() {
        locationName!!.addTextChangedListener( object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.length == 0) {
                    locationName!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f);
                } else {
                    locationName!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 35f);
                    updateLocationList(s.toString())
                }
                wordCounter!!.text = String.format(getString(R.string.request_vacancy_location_place_save_counter), s!!.length)

            }

            override fun afterTextChanged(s: Editable?) {

            }

        } )
    }

    private fun proccessPlace(customName : String) /*: JSONObject*/ {
//        val name = if(App.isEmpty(customName)) locationPlace?.address!!.substring(0, locationPlace?.address!!.indexOf(",")).trim() else customName
//        val address = if(App.isEmpty(customName)) locationPlace?.address!!.substring(locationPlace?.address!!.indexOf(",")+1, locationPlace?.address!!.length).trim() else locationPlace?.address
//        return JSONObject("{\"name\": \"${name}\", \"address\": \"${address}\"}")
//        val name = if(App.isEmpty(customName)) locationPlace?.description!!.substring(0, locationPlace?.description!!.indexOf(",")).trim() else customName
//        val description = if(App.isEmpty(customName)) locationPlace?.description!!.substring(locationPlace?.description!!.indexOf(",")+1, locationPlace?.description!!.length).trim() else locationPlace?.description
//        return JSONObject("{\"name\": \"${name}\", \"description\": \"${description}\"}")
        locationPlace!!.name = customName
    }

    private fun updateLocationList(name : String) {
        /*val locationPlace = LocationPlace(*/proccessPlace(name)/*)*/
        if(profile.locationPlaces.contains(locationPlace)) {
            profile.locationPlaces.set(currentLocationPlace, locationPlace!!)
        } else {
            currentLocationPlace = profile.locationPlaces.size
            locationPlace!!.createdByUser = true
            locationPlace!!.selected = true
            profile.locationPlaces.add(locationPlace!!)
            profile!!.jobLocation = profile.locationPlaces[profile.locationPlaces.size - 1]
//            profile.locationPlaces.sortByDescending { it.modifiedDate }
        }
        listener!!
        listener!!.save()
    }

    fun setListener(listener : LocationSaveListener) {
        this.listener = listener
    }

    interface LocationSaveListener {
        fun save()
    }
}