package com.stratis.estaffing.Control.Searcher.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.Searcher.Filter.SkillSearchFilter;
import com.stratis.estaffing.Control.Searcher.View.SearcherOpenSkillsView;
import com.stratis.estaffing.Model.RecruitmentFlow.SkillCatalog;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class SearcherSkillAdapter extends RecyclerView.Adapter<SearcherOpenSkillsView> implements Filterable {

    protected ArrayList<SkillCatalog> items;
    protected ArrayList<SkillCatalog> original;
    protected ArrayList<SkillCatalog> selected = new ArrayList<>();
    protected Context context;

    public SearcherSkillAdapter(ArrayList<SkillCatalog> items, Context context) {

        this.context = context;
        this.items = items;
        this.original = items;
    }

    @Override
    public SearcherOpenSkillsView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.component_searcher_language_item, parent, false);
        return new SearcherOpenSkillsView(layoutView);
    }

    @Override
    public void onBindViewHolder(final SearcherOpenSkillsView holder, int position) {

        holder.setIsRecyclable(false);
        final SkillCatalog skill = this.items.get(position);

        // -- Setting the views
        holder.getSearchComponentBoxItemText().setText( skill.getName() );
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : (this.items.size() > 5) ? 5 : this.items.size();
    }

    public void reload(ArrayList<SkillCatalog> items) {

        this.items = items;
        if (this.items != null) {
            this.notifyItemRangeInserted(0, this.items.size() - 1);
        }
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<SkillCatalog> getItems() {
        return items;
    }

    public ArrayList<SkillCatalog> getOriginal() {
        return original;
    }

    public ArrayList<SkillCatalog> getSelected() {
        return selected;
    }

    public void setSelected(ArrayList<SkillCatalog> selected) {
        this.selected = selected;
    }

    // --

    @NonNull
    @Override
    public Filter getFilter() {

        SkillSearchFilter filter = new SkillSearchFilter(this);
        return filter;
    }
}
