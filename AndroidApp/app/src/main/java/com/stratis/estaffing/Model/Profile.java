package com.stratis.estaffing.Model;

import android.content.Context;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageCatalog;
import com.stratis.estaffing.Model.RecruitmentFlow.LocationPlace;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest;
import com.stratis.estaffing.Model.RecruitmentFlow.SimpleProfile;
import com.stratis.estaffing.Model.RecruitmentFlow.SkillCatalog;
import com.stratis.estaffing.Model.RecruitmentFlow.SoftwareCatalog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erick Sanchez
 *
 * Revision 1 - 01/09/20
 */

public class Profile implements Serializable {

    private String id = "";
    private String status = "Solicitud creada";
    private String statusId = LatestVacancyRequest.statusSent;
    private String statusComment = "";
    private String statusDate = "";
    private String statusValue = "";
    private String creationDate = "2020-08-11";
    private String updateDate = "2020-08-11";
    private String rhContact = "";
    private Integer hiringTime = null;

    private int currentVacancies = 1;
    private SimpleProfile currentProfile;
    private String seniorityId = "0";
    private String seniorityName = "";
    private ArrayList<SkillCatalog> currentSkills;
    private ArrayList<LanguageCatalog> currentLanguages;
    private String modalityId = "";
    private String vacancyDate = "";
    private double vacancyBudget = 0.0;
    private Boolean vacancyEquipment = false;
    private Boolean vacancyEquipmentSoftware = false;
    private ArrayList<SoftwareCatalog> currentSoftware;

    private ArrayList<SimpleProfile> simpleProfiles;
    private ArrayList<SkillCatalog> skillCatalogs;
    private ArrayList<LanguageCatalog> languageCatalogs;
    private ArrayList<SoftwareCatalog> softwareCatalogs;

    private String responsibilities = "";

    private boolean languagesFromSaveRequest = false;

//    private String location;
//    private String address;
    private String remoteJobCountries;
    private LocationPlace jobLocation;
    private ArrayList<Integer> deletedAddresses;

    // Para mantener las direcciones en memoria
    private List<LocationPlace> locationPlaces = new ArrayList<>();

    // -- NEW REQUEST METHODS
    public JSONObject getSaveRequestObject(final Context context) {

        //  -- Creating the empty JSON Object
        JSONObject request = new JSONObject();
        try {

            if (!id.equals("")) { request.put("id", id); }
            request.put("vacancyProfileId", this.getCurrentProfile().getId());
            request.put("vacancyProfileName", this.getCurrentProfile().getName());
            request.put("status", this.getStatus()); // ***
            request.put("creationDate", this.getCreationDate()); // **
            request.put("updateDate", this.getUpdateDate()); // **
            request.put("createdById", Session.get(context).getDataSessionId());
            request.put("updatedById", Session.get(context).getDataSessionId());
            request.put("clientId", "1"); // **
            request.put("seniorityId", this.getSeniorityId()); // Catalogo pasado
            request.put("seniorityName", this.getSeniorityName());

            final String date = App.getDateFormatted(getVacancyDate(),"yyyy-MM-dd","dd/M/yyyy");
            request.put("startDate", (date.equals("") || date.equals("null") || date.equals("SIN FECHA")) ? null : date );

            // -- Hiring time and jobActivities
            request.put("hiringTime", this.getHiringTime());
            request.put("jobActivities", this.getResponsibilities());

            // --
            request.put("jobModalityId", this.getModalityId()); // -- Catalogo pasado
            request.put("minBudget", this.getVacancyBudget());
            request.put("maxBudget", this.getVacancyBudget());
            request.put("requiresPc", this.getVacancyEquipment());
            request.put("group", this.getCurrentProfile().getName());
            request.put("rhContact", ""); // **

            request.put("vacancyLang", this.getLanguagesForRequest(0));
            request.put("vacancySkillTags", this.getSkillsForRequest(0));
            request.put("aditionalSoftware", this.getSoftwareForRequest(0));
            if (this.remoteJobCountries != null && !this.remoteJobCountries.equals("")) {
                request.put("remoteJobCountries", this.remoteJobCountries);
            }
            if (this.deletedAddresses != null && this.deletedAddresses.size() > 0) {
                request.put("deletedAddresses", getDeleteAddressJSON());
            }
            if (this.jobLocation != null) {
                request.put("jobLocation", this.jobLocation.getObject());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // --
        return request;
    }

    public JSONObject getUpdateRequestObject(final Context context) {

        //  -- Creating the empty JSON Object
        JSONObject request = new JSONObject();
        try {

            request.put("id", id);
            request.put("vacancyProfileId", this.getCurrentProfile().getId());
            request.put("vacancyProfileName", this.getCurrentProfile().getName());
            request.put("statusId", this.getStatusId());
            request.put("updatedById", Session.get(context).getDataSessionId());
            request.put("seniorityId", this.getSeniorityId());

            final String date = App.getDateFormatted(getVacancyDate(),"yyyy-MM-dd","dd/M/yyyy");
            request.put("startDate", (date.equals("") || date.equals("null")) ? null : date );

            // -- Hiring time and jobActivities
            request.put("hiringTime", this.getHiringTime());
            request.put("jobActivities", this.getResponsibilities());

            // --
            request.put("jobModalityId", this.getModalityId());
            request.put("minBudget", this.getVacancyBudget());
            request.put("maxBudget", this.getVacancyBudget());
            request.put("requiresPc", this.getVacancyEquipment());
            request.put("vacancyLang", this.getLanguagesForRequest(0));
            request.put("vacancySkillTags", this.getSkillsForRequest(0));
            request.put("aditionalSoftware", this.getSoftwareForRequest(0));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // --
        return request;
    }

    private JSONArray getLanguagesForRequest(int vacancyId) throws JSONException {

        JSONArray languages = new JSONArray();
        if (this.currentLanguages != null) {
            for (LanguageCatalog language : this.currentLanguages) {
                if (!language.getName().equals("")) {
                    JSONObject languageObj = new JSONObject();
                    //languageObj.put("id", 0);  // +*
                    //languageObj.put("vacancyId", vacancyId);  // **
                    languageObj.put("languageId", language.getId());
                    languageObj.put("langlevelId", language.getLevelId());
                    languages.put(languageObj);
                }
            }
        }
        return languages;
    }

    private JSONArray getSkillsForRequest(int vacancyId) throws JSONException {

        JSONArray skills = new JSONArray();
        if (this.currentSkills != null) {
            for (SkillCatalog skill : this.currentSkills) {
                if (!skill.getName().equals("")) {
                    JSONObject skillObj = new JSONObject();
                    skillObj.put("id", skill.getId());  // +*
                    //skillObj.put("vacancyId", vacancyId); // **
                    skillObj.put("tagId", skill.getId()); // **
                    skillObj.put("name", skill.getName());
                    skillObj.put("isRequired", skill.getRequired());
                    skills.put(skillObj);
                }
            }
        }
        return skills;
    }

    private JSONArray getSoftwareForRequest(int vacancyId) throws JSONException {

        JSONArray softs = new JSONArray();
        if (this.currentSoftware != null) {
            for (SoftwareCatalog soft : this.currentSoftware) {
                if (!soft.getName().equals("")) {
                    JSONObject softobj = new JSONObject();
                    softobj.put("id", soft.getId()); // **
                    softobj.put("name", soft.getName());
                    softobj.put("observations", soft.getObservations());
                    //softobj.put("vacancyId", vacancyId);
                    softs.put(softobj);
                }
            }
        }
        return softs;
    }

    // -- JSON PARSER { DECODER }

    public void loadFromJSON(JSONObject obj) {

        if (obj != null) {

            // -- Simple profile
            final SimpleProfile profile = new SimpleProfile();
            profile.setId(Integer.valueOf(obj.optString("vacancyProfileId")));
            profile.setName(obj.optString("vacancyProfileName"));
            this.setCurrentProfile(profile);

            // -- Status
            this.setStatus(obj.optString("statusValue"));
            this.setStatusId(obj.optString("statusId"));
            this.setStatusComment(obj.optString("statusComment"));
            this.setStatusDate(obj.optString("statusDate"));
            this.setStatusValue(obj.optString("statusValue"));

            // -- Dates
            this.setVacancyDate( App.getDateFormatted(obj.optString("startDate"), "dd/M/yyyy", "yyyy-MM-dd") );
            this.setCreationDate(obj.optString("creationDate"));
            this.setUpdateDate(obj.optString("updateDate"));
            //Log.d("DXGOP", "FECHAAAAAA :::::  " + getVacancyDate());

            // -- Seniority
            this.setSeniorityId(obj.optString("seniorityId"));
            this.setSeniorityName(obj.optString("seniorityName"));

            // -- Modality
            this.setModalityId(obj.optString("jobModalityId"));

            // -- Responsibilities (Job Activities)
            this.setResponsibilities(obj.optString("jobActivities"));

            // -- Budget, PC, RH
            this.setVacancyBudget(obj.optDouble("minBudget"));
            this.setVacancyEquipment(obj.optBoolean("requiresPc"));
            this.setRhContact(obj.optString("rhContact"));
            this.setHiringTime(obj.optInt("hiringTime",-1));
            if (this.getHiringTime() == -1) {
                this.setHiringTime(null);
            }

            this.setRemoteJobCountries(obj.optString("remoteJobCountries"));

            // -- Skills, Languages and Software
            try {

                this.setCurrentSkills( getSkills(obj.optJSONArray("vacancySkillTags")) );
                this.setCurrentLanguages( getLanguages(obj.optJSONArray("vacancyLang")) );
                this.setCurrentSoftware( getSoftware(obj.optJSONArray("aditionalSoftware")) );

                if (this.currentLanguages.size() > 0) {
                    this.languagesFromSaveRequest = true;
                }

                if (this.getCurrentSoftware().size() > 0) {
                    this.setVacancyEquipmentSoftware(true);
                }

                this.setJobLocation(new LocationPlace(obj.getJSONObject("jobLocationDto")));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private ArrayList<SkillCatalog> getSkills(JSONArray array) throws JSONException {

        ArrayList<SkillCatalog> skills = new ArrayList<>();
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object =  array.optJSONObject(i);
                if (object != null) {
                    SkillCatalog skill = new SkillCatalog(object);
                    skills.add(skill);
                }
            }
        }
        return skills;
    }

    private ArrayList<LanguageCatalog> getLanguages(JSONArray array) throws JSONException {

        ArrayList<LanguageCatalog> languages = new ArrayList<>();
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object =  array.optJSONObject(i);
                if (object != null) {
                    LanguageCatalog language = new LanguageCatalog();
                    language.setId(object.optInt("languageId"));
                    language.setLevelById(object.optInt("langlevelId"));
                    languages.add(language);
                }
            }
        }
        return languages;
    }

    private ArrayList<SoftwareCatalog> getSoftware(JSONArray array) throws JSONException {

        ArrayList<SoftwareCatalog> software = new ArrayList<>();
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object =  array.optJSONObject(i);
                if (object != null) {
                    SoftwareCatalog soft = new SoftwareCatalog(object);
                    software.add(soft);
                }
            }
        }
        return software;
    }

    public void transformCurrentLanguageObjects() {

        if (this.getLanguageCatalogs() != null) {
            for (LanguageCatalog currentLanguage : this.currentLanguages) {
                if (currentLanguage.getName().equals("")) {
                    for (LanguageCatalog catalog : this.getLanguageCatalogs()) {
                        if (catalog.getId().equals(currentLanguage.getId()))  {
                            currentLanguage.setName(catalog.getName());
                            break;
                        }
                    }
                }
            }
        }
    }

    // --
    public void setProvisionalId(String id) {
        this.id = id;
    }

    public String getProvisionalId() {
        return this.id;
    }

    // --
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusComment() {
        return statusComment;
    }

    public void setStatusComment(String statusComment) {
        this.statusComment = statusComment;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getRhContact() {
        return rhContact;
    }

    public void setRhContact(String rhContact) {
        this.rhContact = rhContact;
    }

    public int getCurrentVacancies() {
        return currentVacancies;
    }

    public void setCurrentVacancies(int currentVacancies) {
        this.currentVacancies = currentVacancies;
    }

    public SimpleProfile getCurrentProfile() {
        return currentProfile;
    }

    public void setCurrentProfile(SimpleProfile currentProfile) {
        this.currentProfile = currentProfile;
    }

    public String getSeniorityId() {
        return seniorityId;
    }

    public void setSeniorityId(String seniorityId) {
        this.seniorityId = seniorityId;
    }

    public String getSeniorityName() {
        return seniorityName;
    }

    public void setSeniorityName(String seniorityName) {
        this.seniorityName = seniorityName;
    }

    public ArrayList<SkillCatalog> getCurrentSkills() {
        return currentSkills;
    }

    public void setCurrentSkills(ArrayList<SkillCatalog> currentSkills) {
        this.currentSkills = currentSkills;
    }

    public ArrayList<LanguageCatalog> getCurrentLanguages() {
        return currentLanguages;
    }

    public void setCurrentLanguages(ArrayList<LanguageCatalog> currentLanguages) {
        this.currentLanguages = currentLanguages;
    }

    public String getModalityId() {
        return modalityId;
    }

    public void setModalityId(String modalityId) {
        this.modalityId = modalityId;
    }

    public String getVacancyDate() {
        return vacancyDate;
    }

    public void setVacancyDate(String vacancyDate) {
        this.vacancyDate = vacancyDate;
    }

    public double getVacancyBudget() {
        return vacancyBudget;
    }

    public void setVacancyBudget(double vacancyBudget) {
        this.vacancyBudget = vacancyBudget;
    }

    public Boolean getVacancyEquipment() {
        return vacancyEquipment;
    }

    public void setVacancyEquipment(Boolean vacancyEquipment) {
        this.vacancyEquipment = vacancyEquipment;
    }

    public Boolean getVacancyEquipmentSoftware() {
        return vacancyEquipmentSoftware;
    }

    public void setVacancyEquipmentSoftware(Boolean vacancyEquipmentSoftware) {
        this.vacancyEquipmentSoftware = vacancyEquipmentSoftware;
    }

    public ArrayList<SoftwareCatalog> getCurrentSoftware() {
        return currentSoftware;
    }

    public void setCurrentSoftware(ArrayList<SoftwareCatalog> currentSoftware) {
        this.currentSoftware = currentSoftware;
    }

    public boolean isLanguagesFromSaveRequest() {
        return languagesFromSaveRequest;
    }

    public Integer getHiringTime() {
        return hiringTime;
    }

    public void setHiringTime(Integer hiringTime) {
        this.hiringTime = hiringTime;
    }

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

    // -- CATALOGS

    public ArrayList<SimpleProfile> getSimpleProfiles() {
        return simpleProfiles;
    }

    public void setSimpleProfiles(ArrayList<SimpleProfile> simpleProfiles) {
        this.simpleProfiles = simpleProfiles;
    }

    public ArrayList<LanguageCatalog> getLanguageCatalogs() {
        return languageCatalogs;
    }

    public void setLanguageCatalogs(ArrayList<LanguageCatalog> languageCatalogs) {
        this.languageCatalogs = languageCatalogs;
    }

    public ArrayList<SoftwareCatalog> getSoftwareCatalogs() {
        return softwareCatalogs;
    }

    public void setSoftwareCatalogs(ArrayList<SoftwareCatalog> softwareCatalogs) {
        this.softwareCatalogs = softwareCatalogs;
    }

    public ArrayList<SkillCatalog> getSkillCatalogs() {
        return skillCatalogs;
    }

    public void setSkillCatalogs(ArrayList<SkillCatalog> skillCatalogs) {
        this.skillCatalogs = skillCatalogs;
    }

//    public String getLocation() {
//        return location;
//    }
//
//    public void setLocation(String location) {
//        this.location = location;
//    }
//
//    public String getAddress() {
//        return address;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }

    public String getRemoteJobCountries() {
        return remoteJobCountries;
    }

    public void setRemoteJobCountries(String remoteJobCountries) {
        this.remoteJobCountries = remoteJobCountries;
    }

    public void setJobLocation(LocationPlace jobLocation) {
        this.jobLocation = jobLocation;
    }

    public LocationPlace getJobLocation() {
        return jobLocation;
    }

    public ArrayList<Integer> getDeletedAddresses() {
        if(this.deletedAddresses == null)
            this.deletedAddresses = new ArrayList<>();
        return deletedAddresses;
    }

    public void setDeletedAddresses(ArrayList<Integer> deletedAddresses) {
        this.deletedAddresses = deletedAddresses;
    }

    public List<LocationPlace> getLocationPlaces() {
        return locationPlaces;
    }

    public void setLocationPlaces(List<LocationPlace> locationPlaces) {
        this.locationPlaces = locationPlaces;
    }

    public JSONArray getDeleteAddressJSON() {
        final JSONArray jsonArray = new JSONArray();
        for(int addressId : deletedAddresses) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", addressId);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }
}