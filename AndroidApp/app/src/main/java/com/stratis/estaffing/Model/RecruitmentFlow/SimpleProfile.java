package com.stratis.estaffing.Model.RecruitmentFlow;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class SimpleProfile implements Serializable {

    private Integer id = 0;
    private String name = "";
    private Integer skillCategoryId = 0;

    public SimpleProfile() {}

    public SimpleProfile(String name) {

        this.id = 0;
        this.name = name;
        this.skillCategoryId = 0;
    }

    public SimpleProfile(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optInt("id", 0);
            this.name = obj.optString("name", "");
            this.skillCategoryId =  obj.optInt("skillCategoryId", 0);
        }
    }

    public static ArrayList<SimpleProfile> parse(JSONArray array) {

        if (array != null) {

            ArrayList<SimpleProfile> profiles = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                profiles.add(new SimpleProfile(obj));
            }
            return profiles;
        }
        return null;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getSkillCategoryId() {
        return skillCategoryId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSkillCategoryId(Integer skillCategoryId) {
        this.skillCategoryId = skillCategoryId;
    }
}