package com.stratis.estaffing.Model;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import androidx.core.content.res.ResourcesCompat;

import com.amulyakhare.textdrawable.TextDrawable;
import com.stratis.estaffing.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 28/07/20
 */

public class Course {

    private String name = "";
    private String description = "";
    private String date = "";
    private String school = "";
    private Boolean hasCertificate = false;
    private String certificateNumber = "";
    private String certificateFileUrl = "";
    private String validityDate = "";
    private Integer categoryId = 0;
    private String talentCourseDescription;

    public Course(JSONObject obj) {

        if (obj != null) {

            this.name = obj.optString("name", "");
            this.description = obj.optString("description", "");
            this.date = obj.optString("date", "");
            this.school = obj.optString("school", "");
            this.hasCertificate = obj.optBoolean("hasCertificate", false);
            this.certificateNumber = obj.optString("certificateNumber", "");
            this.certificateFileUrl = obj.optString("certificateFileUrl", "");
            this.validityDate = obj.optString("validityDate", "");
            this.categoryId = obj.optInt("categoryId", 0);
            this.talentCourseDescription = obj.optString("talentCourseDescription", null);

            // -- Cleaning
            if (this.school.equals("null")) {
                this.school = "N/A";
            }

            if (this.validityDate.equals("null")) {
                this.validityDate = "N/A";
            }

            if (this.certificateNumber.equals("null")) {
                this.certificateNumber = "N/A";
            }
        }
    }

    public TextDrawable getInitials(int color, final Context context) {

        Log.e("DXGOP", "SDFJSDjSDJASJDS _::::: " + this.name);
        String initials = "-";

        if (!this.name.equals("")) {

            String[] nameParts = this.name.split(" ");
            if (nameParts.length > 0) {
                initials = String.valueOf(nameParts[0].charAt(0));
            }
            if (nameParts.length > 1) {
                initials = initials + nameParts[1].charAt(0);
            }
        }
        return TextDrawable.builder().beginConfig()
                .bold()
                .toUpperCase()
                .fontSize(40)
                .useFont(ResourcesCompat.getFont(context, R.font.os_bold))
                .bold()
                .endConfig()
                .buildRound(initials, color);
    }

    public static ArrayList<Course> parse(JSONArray array) {

        if (array != null) {

            ArrayList<Course> courses = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                courses.add(new Course(obj));
            }
            return courses;
        }
        return null;
    }

    //  --
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public String getSchool() {
        return school;
    }

    public Boolean getHasCertificate() {
        return hasCertificate;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public String getCertificateFileUrl() {
        return certificateFileUrl;
    }

    public String getValidityDate() {
        return validityDate;
    }

    public Integer getCategoryId() { return categoryId; }

    public String getTalentCourseDescription() { return talentCourseDescription; }
}
