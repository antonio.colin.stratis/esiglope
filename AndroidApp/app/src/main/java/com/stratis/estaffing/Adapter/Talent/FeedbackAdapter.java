package com.stratis.estaffing.Adapter.Talent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.Feedback;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.FeedbackView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 29/07/20
 */

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackView> {

    private ArrayList<Feedback> items;
    private Context context;

    public FeedbackAdapter(ArrayList<Feedback> items, Context context) {

        this.context = context;
        this.items = items;
    }

    @Override
    public FeedbackView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_talent_feedback_item, parent, false);
        /*ViewGroup.LayoutParams layoutParams = layoutView.getLayoutParams();
        layoutParams.width = (int) (parent.getWidth() * 0.85);
        layoutView.setLayoutParams(layoutParams);*/
        return new FeedbackView(layoutView);
    }

    @Override
    public void onBindViewHolder(final FeedbackView holder, int position) {

        holder.setIsRecyclable(false);
        final Feedback feedback = this.items.get(position);

        // -- Setting the views
        //holder.getFeedbackOwnerInitials().setImageDrawable( feedback.getInitials(context.getColor(R.color.colorPrimary), context) );
        App.setViewAsPrimaryColor(holder.getFeedbackOwnerInitials(), context);
        holder.getFeedbackOwnerInitials().setText(App.getTwoFirstChars(feedback.getUserFullName()));
        holder.getFeedbackOwner().setText( feedback.getUserFullName() );

        String date = (!feedback.getDate().equals("Hoy")) ? App.getDateCourse(feedback.getDate()) : feedback.getDate();
        holder.getFeedbackDate().setText( date );

        holder.getFeedbackStrengths().setText( feedback.getStrengths() );
        holder.getFeedbackOpportunities().setText( feedback.getOpportunities() );

        // -- MultiScroll Fixing
        //App.enableScroll(holder.getFeedbackStrengths());
        //App.enableScroll(holder.getFeedbackOpportunities());
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<Feedback> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }
}
