package com.stratis.estaffing.Model.NotificationCenterFlow;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 2/25/21
 */

public class NotificationModel {

    private String id = "";
    private String userId = "";
    private String message = "";
    private String parameters = "";

    public NotificationModel() {}

    public NotificationModel(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optString("id", "");
            this.userId = obj.optString("userId", "");
            this.message =  obj.optString("message", "");
            this.parameters =  obj.optString("parameters", "");
        }
    }

    public static ArrayList<NotificationModel> parse(JSONArray array) {

        if (array != null) {

            ArrayList<NotificationModel> notifications = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                notifications.add(new NotificationModel(obj));
            }
            return notifications;
        }
        return null;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getMessage() {
        return message;
    }

    public String getParameters() {
        return parameters;
    }

    public String getParameter(final String param) {

        final String[] params = this.parameters.split(",");
        for (String paramPart : params) {
            final String[] paramValues = paramPart.split(":");
            final String pV = paramValues[0].replaceAll(" ", "");
            if (paramValues.length > 0 && pV.equals(param) && paramValues[1] != null) {
                return paramValues[1].replaceAll(" ", "");
            }
        }
        return "";
    }
}
