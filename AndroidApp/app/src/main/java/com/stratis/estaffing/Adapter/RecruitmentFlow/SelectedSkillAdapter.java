package com.stratis.estaffing.Adapter.RecruitmentFlow;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.ABActivity;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyDetailFragment;
import com.stratis.estaffing.Model.RecruitmentFlow.SkillCatalog;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.NewRequestFlow.SelectedSkillView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class SelectedSkillAdapter extends RecyclerView.Adapter<SelectedSkillView> {

    protected ArrayList<SkillCatalog> items;
    protected Context context;
    private int currentAddedSkillPointer = 0;
    protected OnCloseButtonListerner listener;

    public SelectedSkillAdapter(ArrayList<SkillCatalog> items, Context context) {

        this.context = context;
        this.items = items;
    }

    @Override
    public SelectedSkillView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_request_vacancy_detail_item, parent, false);
        return new SelectedSkillView(layoutView);
    }

    @Override
    public void onBindViewHolder(final SelectedSkillView holder, final int position) {

        holder.setIsRecyclable(false);
        final SkillCatalog skill = this.items.get(position);
        final Boolean isFilled = !skill.getName().equals("");

        // --
        holder.getTitle().setVisibility( isFilled ? View.VISIBLE : View.INVISIBLE);
        holder.getCloseButton().setVisibility( isFilled ? View.VISIBLE : View.INVISIBLE );

        if (!isFilled) {

            holder.getBox().setBackground( ContextCompat.getDrawable(context, R.drawable.rectangle_dotty) );

        } else {

            holder.getBox().setBackground( ContextCompat.getDrawable(context, R.drawable.request_vacancy_selected_button) );
            //((ABActivity) context).setViewAsSecondaryColor(holder.getBox());
            App.setViewAsSecondaryColor(holder.getBox(), context);

            holder.getTitle().setText( skill.getName() );
            holder.getCloseButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onCloseButton(position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void addElement(SkillCatalog skill) {

        //Log.d("DXGOP", "TTOTAL :. " + this.items.size());
        //Log.d("DXGOP", "POINTER :. " + this.currentAddedSkillPointer);
        this.items.set(this.currentAddedSkillPointer, skill);
        //this.items.add(language);
        this.currentAddedSkillPointer++;

        if (this.items.size() >= 1 && !this.items.get(this.items.size() -1).getName().equals("")) {
            this.items.add(new SkillCatalog());
        }
    }

    public void removeElement(int index) {
        this.items.remove(index);
        this.currentAddedSkillPointer--;

        if (this.items.size() <= 2) {
            this.items.add(new SkillCatalog());
        }
    }

    public void reload(ArrayList<SkillCatalog> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<SkillCatalog> getItems() {
        return items;
    }

    public OnCloseButtonListerner getListener() {
        return listener;
    }

    public void setListener(OnCloseButtonListerner listener) {
        this.listener = listener;
    }

    // --
    public interface OnCloseButtonListerner {
        public void onCloseButton(Integer position);
    }
}
