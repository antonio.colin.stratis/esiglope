package com.stratis.estaffing.Core;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Controller.LoginFlow.LoginController;
import com.stratis.estaffing.Controller.LoginFlow.RecoverController;
import com.stratis.estaffing.Controller.LoginFlow.ResetController;
import com.stratis.estaffing.Controller.MainFlow.HomeController;
import com.stratis.estaffing.Controller.MainFlow.PolicyController;
import com.stratis.estaffing.Controller.MainFlow.SupportFlow.SupportController;
import com.stratis.estaffing.Controller.RecruitmentFlow.FirstTimeController;
import com.stratis.estaffing.Controller.RecruitmentFlow.NewRequestController;
import com.stratis.estaffing.Controller.RecruitmentFlow.RecruitmentController;
import com.stratis.estaffing.Controller.RegisterFlow.ChooseTypeAccountController;
import com.stratis.estaffing.Controller.RegisterFlow.RegisterController;
import com.stratis.estaffing.Controller.SearchingJobFlow.UploadCvController;
import com.stratis.estaffing.Controller.TalentFlow.TalentController;
import com.stratis.estaffing.Fragment.TalentFlowV3.Feedback.FeedbackConfirmFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.Feedback.FeedbackDetailFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.Feedback.FeedbackDetailItemFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.Feedback.FeedbackNewFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.Feedback.FeedbackSelectionFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.Feedback.FeedbackStarRateFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.TalentCourseCertificateFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.TalentCoursesFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.TalentExperienceFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.TalentLanguageInfoFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.TalentSingleInfoFragment;
import com.stratis.estaffing.Fragment.MasterAdminFlow.MALeadersFragment;
import com.stratis.estaffing.Fragment.MasterAdminFlow.MARequestSingleCandidateSingleFragment;
import com.stratis.estaffing.Fragment.MasterAdminFlow.MARequestSingleCandidatesFragment;
import com.stratis.estaffing.Fragment.MasterAdminFlow.MARequestSingleFragment;
import com.stratis.estaffing.Fragment.MasterAdminFlow.MARequestsFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestCheckFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyBudgetFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyDetailFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyLanguageFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyModalityFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateEducationFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateEducationSingleFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateShare;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateWorkExperienceFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.InterviewFlow.RecruitmentRequestCreateInterviewFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.InterviewFlow.RecruitmentRequestRateInterviewFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidateSingleFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidatesFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestSingleFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestsFragment;
import com.stratis.estaffing.Fragment.TalentFlow.TalentFeedbackFragment;
import com.stratis.estaffing.Fragment.TalentFlowV3.TalentSkillsFragment;
import com.stratis.estaffing.Modal.CertificateModal;
import com.stratis.estaffing.Modal.FeedbackModal;
import com.stratis.estaffing.Modal.LanguagesModal;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.ChooseCandidate.ChooseCandidateModal;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal.NoCandidateInterestModal;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.RequestPausedCancelModal;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest;

import java.util.HashMap;
import java.util.Map;

public class FirebaseEvent {

    private static Map<String, String> events = new HashMap<>();

    static {
        Log.d("DXGEV", "Initialing events");
        if(events == null) {
            Log.d("DXGEV", "Events null");
            events = new HashMap<>();
        }
        events.put(LoginController.class.getSimpleName(), "Inicio_sesión");
        events.put(ChooseTypeAccountController.class.getSimpleName(), "nueva_cuenta_perfilamiento");
        events.put(RecoverController.class.getSimpleName(), "Cambio_contraseña");
        events.put(ResetController.class.getSimpleName(), "Verificación_contraseña");
        events.put(RegisterController.class.getSimpleName().concat("true"), "registro_cuenta_empresa");
        events.put(RegisterController.class.getSimpleName().concat("false"), "registro_cuenta_aspirante");

        events.put(HomeController.class.getSimpleName().concat("client"), "client_dashboard");
        events.put(HomeController.class.getSimpleName().concat("talent"), "dashboard_aspirantes");
        events.put(HomeController.class.getSimpleName().concat("masterAdmin"), "master_admin_dashboard");
        events.put(HomeController.class.getSimpleName().concat("company"), "dashboard_nueva_empresa");
        events.put(RegisterController.class.getSimpleName().concat("true").concat("error"), "dashboard_empresa_existente");

        events.put(SupportController.class.getSimpleName(), "soporte_stratis");
        events.put(TalentController.class.getSimpleName(), "mitalento_listado");
        events.put(TalentSingleInfoFragment.class.getSimpleName(), "mitalento_perfil_resumen");
        events.put(TalentSkillsFragment.class.getSimpleName(), "Habilidades_talento");
        events.put(TalentCoursesFragment.class.getSimpleName(), "Certificaciones_cursos");
        events.put(TalentCourseCertificateFragment.class.getSimpleName(), "Certificaiones_detalle");
        events.put(TalentExperienceFragment.class.getSimpleName(), "Experiencia_laboral");
        events.put(TalentLanguageInfoFragment.class.getSimpleName(), "Tabla_idiomas");
        events.put(TalentFeedbackFragment.class.getSimpleName(), "mitalento_retro");
        events.put(LanguagesModal.class.getSimpleName(), "mitalento_idioma_info");
        events.put(CertificateModal.class.getSimpleName(), "mitalento_certificados");
        events.put("video_talento", "mitalento_perfil_video_presentación");
        events.put(FeedbackModal.class.getSimpleName().concat("success"), "mitalento_retro_confirmación");
        events.put(FeedbackModal.class.getSimpleName().concat("warn"), "mitalento_retro_error");
        events.put(PolicyController.class.getSimpleName() ,"politica_privacidad");
        events.put(UploadCvController.class.getSimpleName(), "aspirante_subircv");
        events.put(UploadCvController.class.getSimpleName().concat("success"), "aspirante-confirmación");
        events.put(UploadCvController.class.getSimpleName().concat("error"), "aspirante-error");

        events.put(RecruitmentController.class.getSimpleName(), "solicitar_talento_nuevo");
        events.put(FirstTimeController.class.getSimpleName(), "solicitar_talento_instrucciones");
        events.put(NewRequestController.class.getSimpleName(), "solicitar_talento_generar_solicitud");
        events.put(RequestVacancyFragment.class.getSimpleName().concat("false"), "solicitar_talento_num_vacantes");
        events.put(RequestVacancyFragment.class.getSimpleName().concat("true"), "solicitar_talento_editar_solicitud");
        events.put(RequestVacancyDetailFragment.class.getSimpleName(), "solicitar_talento_búsqueda_candidato");
        events.put(RequestVacancyDetailFragment.class.getSimpleName().concat("skills"), "solicitar_talento_selección_candidato");
        events.put(RequestVacancyLanguageFragment.class.getSimpleName(), "solicitar_talento_Idioma_candidato");
        events.put(RequestVacancyModalityFragment.class.getSimpleName(), "solicitar_talento_modotrabajo");
        events.put(RequestVacancyBudgetFragment.class.getSimpleName(), "solicitar_talento_presupuesto");
        events.put(RequestCheckFragment.class.getSimpleName(), "solicitar_talento_revisar_solicitud");
        events.put(RecruitmentRequestsFragment.class.getSimpleName(), "mis_solicitudes_listado");

        events.put(MALeadersFragment.class.getSimpleName(), "master_admin_listado");
        events.put(MALeadersFragment.class.getSimpleName().concat("no_items"), "master_admin_sin_solicitudes");
        events.put(MARequestsFragment.class.getSimpleName(), "master_admin_solicitudes");
        events.put(MARequestSingleFragment.class.getSimpleName(), "master_admin_detalles_solicitud");
        events.put(MARequestSingleCandidatesFragment.class.getSimpleName(), "master_admin_candidatos");
        events.put(MARequestSingleCandidateSingleFragment.class.getSimpleName().concat("discard"), "master_admin_perfil_candidato");
        events.put(MARequestSingleCandidateSingleFragment.class.getSimpleName().concat("consider"), "master_admin_volver_considerar");

        events.put(RecruitmentRequestSingleFragment.class.getSimpleName(), "Detalles_solicitud_Candidatos_disponibles");
        events.put(RecruitmentRequestSingleFragment.class.getSimpleName().concat(LatestVacancyRequest.statusConfirm), "Detalles_solicitud_confirmada");
        events.put(RecruitmentRequestSingleFragment.class.getSimpleName().concat(LatestVacancyRequest.statusPaused), "Detalles_solicitud_Pausada");
        events.put(RecruitmentRequestSingleFragment.class.getSimpleName().concat(LatestVacancyRequest.statusInterviews), "Detalles_solicitud_Entrevistas");
        events.put(RecruitmentRequestSingleFragment.class.getSimpleName().concat(LatestVacancyRequest.statusHireStop), "Detalles_solicitud_Detenida");
        events.put(RecruitmentRequestSingleFragment.class.getSimpleName().concat(LatestVacancyRequest.statusCancel), "Detalles_solicitud_Detenida");
        events.put(RecruitmentRequestSingleFragment.class.getSimpleName().concat("finish"), "Detalles_solicitud_concluida");
        events.put(RequestPausedCancelModal.class.getSimpleName(), "Detalles_solicitud_Pausar");
        events.put(RecruitmentRequestCandidatesFragment.class.getSimpleName(), "Lista_candidatos");
        events.put(RecruitmentRequestCandidateSingleFragment.class.getSimpleName(), "Perfil_candidato_elección");
        events.put(RecruitmentRequestCandidateSingleFragment.class.getSimpleName().concat("ivwp"), "Perfil_candidato_entrevista_programada");
        events.put(RecruitmentRequestCandidateSingleFragment.class.getSimpleName().concat("ivwu"), "Perfil_candidato_evaluar_entrevista");
        events.put(RecruitmentRequestCandidateSingleFragment.class.getSimpleName().concat("ivwc"), "Perfil_candidato_opcion_contratación");
        events.put(RecruitmentRequestCandidateSingleFragment.class.getSimpleName().concat("choose"), "Detalles_solicitud_candidato_elegido");
        events.put(RecruitmentRequestCandidateShare.class.getSimpleName().concat(RecruitmentRequestCandidateShare.RecruitmentRequestCandidateShareType.single.toString()), "Compartir_cv");
        events.put(RecruitmentRequestCandidateShare.class.getSimpleName().concat(RecruitmentRequestCandidateShare.RecruitmentRequestCandidateShareType.all.toString()), "Lista_candidatos_compartir_cv");
        events.put(RecruitmentRequestCandidateShare.class.getSimpleName().concat("shared"), "Lista_candidatos_CV_enviados");
        events.put(LanguagesModal.class.getSimpleName().concat("true"), "Perfil_candidato_idiomas");
        events.put(NoCandidateInterestModal.class.getSimpleName().concat("true"), "Perfil_candidato_concluir");
        events.put(RecruitmentRequestCandidateEducationFragment.class.getSimpleName(), "Perfil_candidato_eduación");
        events.put(RecruitmentRequestCandidateWorkExperienceFragment.class.getSimpleName(), "Perfil_candidato_Experiencia_Laboral");
        events.put("video_candidato", "Video_presentación");
        events.put(RecruitmentRequestCandidateEducationSingleFragment.class.getSimpleName().concat("ced"), "Perfil_candidato_eduación_Cedula");
        events.put(RecruitmentRequestCandidateEducationSingleFragment.class.getSimpleName().concat("cert"), "Perfil_candidato_educación_certificado");
        events.put(RecruitmentRequestCreateInterviewFragment.class.getSimpleName().concat(RecruitmentRequestCreateInterviewFragment.RecruitmentRequestCreateInterviewType.create.toString()), "Agendar_entrevista");
        //events.put(RecruitmentRequestCreateInterviewFragment.class.getSimpleName().concat(RecruitmentRequestCreateInterviewFragment.RecruitmentRequestCreateInterviewType.createAgain.toString()), "Agendar_nueva_entrevista");
        events.put(RecruitmentRequestCreateInterviewFragment.class.getSimpleName().concat(RecruitmentRequestCreateInterviewFragment.RecruitmentRequestCreateInterviewType.createAgain.toString()), "Reagendar_nueva_entrevista");
        events.put(RecruitmentRequestCreateInterviewFragment.class.getSimpleName().concat(RecruitmentRequestCreateInterviewFragment.RecruitmentRequestCreateInterviewType.edit.toString()), "Editar_entrevista");
        events.put(RecruitmentRequestRateInterviewFragment.class.getSimpleName(), "Evaluar_entrevista");
        events.put(ChooseCandidateModal.class.getSimpleName(), "Detalles_solicitud_concluida");

        events.put(FeedbackDetailFragment.class.getSimpleName(), "Evaluaciones");
        events.put(FeedbackDetailItemFragment.class.getSimpleName(), "evaluación_pasada");
        events.put(FeedbackStarRateFragment.class.getSimpleName(), "Calificación_desempeño");
        events.put(FeedbackSelectionFragment.class.getSimpleName(), "Categoría_c_técnicos");
        events.put(FeedbackNewFragment.class.getSimpleName().concat(FeedbackNewFragment.FeedbackType.STRENGTH.toString()), "Nueva_evaluación_información");
        events.put(FeedbackNewFragment.class.getSimpleName().concat(FeedbackNewFragment.FeedbackType.OPORTUNITY.toString()), "Areas_oportunidad");
        events.put(FeedbackConfirmFragment.class.getSimpleName(), "Revisión_evaluación");
    }

    private static String getDummyName(String key){
        Log.d("DXGEV", "Retriving value of " + key);
        if(events != null) {
            Log.d("DXGEV", "Event map: " + events);
            return events.get(key);
        }
        Log.e("DXGEV", "Events null");
        return null;
    }

    public static void logEvent(Context context, String key) {
        try {
            String dummyScreen = FirebaseEvent.getDummyName(key);
            Log.d("DXGEV", "dummyScreen: " + dummyScreen);
            if(dummyScreen!=null){
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, dummyScreen);
                bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, key);
                Log.d("DXGEV", "SCREEN ::: " + dummyScreen);
                FirebaseAnalytics.getInstance(context).logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
            }
        } catch (Exception e) {
            Log.e("DXGEV", "Error logging event, error: " + e.getMessage(), e);
        }
    }
}
