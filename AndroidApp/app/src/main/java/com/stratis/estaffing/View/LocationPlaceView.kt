package com.stratis.estaffing.View

import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Spinner
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.R

class LocationPlaceView(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var locationPlaceContainer: RelativeLayout? = null
    var locationPlaceTitle: TextView? = null
    var locationPlaceSubtitle: TextView? = null
    var locationCreatedByUser : ImageView? = null
    var locationPlaceRadio : ImageView? = null

    init {
        this.locationPlaceContainer = itemView.findViewById(R.id.locationPlaceContainer)
        this.locationPlaceTitle = itemView.findViewById(R.id.locationPlaceTitle)
        this.locationPlaceSubtitle = itemView.findViewById(R.id.locationPlaceSubtitle)
        this.locationCreatedByUser = itemView.findViewById(R.id.locationCreatedByUser)
        this.locationPlaceRadio = itemView.findViewById(R.id.locationPlaceRadio)
    }
}