package com.stratis.estaffing.Adapter.TalentV3

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.ButtonArrowAdapter
import com.stratis.estaffing.Model.TalentFlowV3.FeedbackDetail
import com.stratis.estaffing.R
import com.stratis.estaffing.View.Talent.FeedbackSkillItemView
import java.util.ArrayList

class FeedbackSkillItemAdapter(items: ArrayList<FeedbackDetail>?) : RecyclerView.Adapter<FeedbackSkillItemView>() {

    private var items: ArrayList<FeedbackDetail>? = null
    private var listener: ButtonArrowAdapter.ButtonArrowClickListener? = null

    init {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackSkillItemView {
        val layoutView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.fragment_feedback_skill_item, parent, false)
        return FeedbackSkillItemView(layoutView)
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: FeedbackSkillItemView, position: Int) {
        holder.setIsRecyclable(false)
        val description : String = this.items!![position].name

        holder.description!!.text = description
        holder.checkButton!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                listener?.onClick(position)
            }

        }
        )
    }

    fun removeItem(feedbackDetail: FeedbackDetail) {
        items!!.remove(feedbackDetail)
    }

    fun setListener(listener: ButtonArrowAdapter.ButtonArrowClickListener) {
        this.listener = listener
    }
}