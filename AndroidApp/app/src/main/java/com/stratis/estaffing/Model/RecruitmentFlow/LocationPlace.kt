package com.stratis.estaffing.Model.RecruitmentFlow

import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

open class LocationPlace: Serializable {

    var id : Int = 0
    var name : String = ""
//    var address : String = ""
    var selected : Boolean = false
    var country : String = ""
    var city : String = ""
    var description : String = ""
    var erased : Int = 0
    var neighborhood : String = ""
    var numberExt : String = ""
    var numberInt : String = ""
    var state : String = ""
    var street : String = ""
    var zipCode : Int = 0
    var createdDate = ""
    var modifiedDate = ""
    var createdByUser = false

    constructor(obj: JSONObject?) {
        if (obj != null) {
            this.id = obj.optInt("id")
            this.name = obj.optString("name")
//            this.address = obj.optString("address")
            this.selected = obj.optBoolean("selected")
            this.country = obj.optString("country")
            this.city = obj.optString("city")
            this.description = obj.optString("description")
            this.erased = obj.optInt("erased")
            this.neighborhood = obj.optString("neighborhood")
            this.numberExt = obj.optString("numberExt")
            this.numberInt = obj.optString("numberInt")
            this.state = obj.optString("state")
            this.street = obj.optString("street")
            this.zipCode = obj.optInt("zipCode")
            this.createdDate = obj.optString("createdDate")
            this.modifiedDate = obj.optString("modifiedDate")
            this.createdByUser = obj.optBoolean("createdByUser")
        }
    }

    companion object {

        fun parse(array: JSONArray?): ArrayList<LocationPlace>? {

            if (array != null) {
                val locationPlaceDetail = ArrayList<LocationPlace>()
                for (i in 0 until array.length()) {
                    val obj = array.optJSONObject(i)
                    locationPlaceDetail.add(LocationPlace(obj))
                }
                return locationPlaceDetail
            }
            return null
        }
    }



    fun getObject(): JSONObject {
        val obj = JSONObject()
        obj.put("id", id)
        obj.put("name", name)
//        obj.put("address", address)
        obj.put("description", description)
        obj.put("street", street)
        obj.put("numberExt", numberExt)
        obj.put("neighborhood", neighborhood)
        obj.put("zipCode", zipCode)
        obj.put("state", state)
        obj.put("country", country)
        obj.put("city", city)
        obj.put("createdDate", createdDate)
        obj.put("modifiedDate", modifiedDate)
        obj.put("createdByUser", createdByUser)

        return obj
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LocationPlace) return false

//        if (address != other.address) return false
        if(description != other.description) return false

        return true
    }

    override fun hashCode(): Int {
//        return address.hashCode()
        return description.hashCode()
    }

    override fun toString(): String {
        return getObject().toString();
    }
}