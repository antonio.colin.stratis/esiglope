package com.stratis.estaffing.Service.Login;

import android.util.Log;

import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTClient;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Core.eStaffingClient;
import com.stratis.estaffing.Service.CatalogService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by Erick Sanchez
 * Revision 1 - 14/10/20
 */

public class LoginService {

    public static void login(String email, String password, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getLoginUrl("/login");
        ReSTClient rest = new ReSTClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getBasicHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("username", email);
            body.put("password", password);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "LOGIN SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void register(String name, String lastName, String email, String phone, String company,
            String password, String confirm, String type, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/auth/signUp");
        ReSTClient rest = new ReSTClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getBasicHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("typeUser", type);
            body.put("name", name);
            body.put("lastName", lastName);
            body.put("email", email);
            body.put("phone", phone);
            body.put("company", company);
            body.put("password", password);
            body.put("passwordConfirm", confirm);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "REGIsTER SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void verifyCode(String email, String code, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/auth/signUp/verifyCode");
        ReSTClient rest = new ReSTClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getBasicHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("username", email);
            body.put("code", code);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "REGISTER CODE SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void resendCode(String email, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/auth/signUp/resendVerifyCode");
        ReSTClient rest = new ReSTClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getBasicHeaders());

        // -- Get Variables
        request.addParameter("username", email);

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void checkDataSession(final Session session, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/session/getDataSession");
        ReSTClient rest = new ReSTClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getBasicHeaders());
        request.addHeader("Authentication", session.getSessionToken());
        request.addHeader("token", session.getIdToken());
        request.addHeader("refreshtoken", session.getRefreshToken());

        // --
        request.addParameter("username", session.getUsername());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void refreshToken(final Session session, final ReSTCallback listener) {

        final String url = Api.getInstance().getLoginUrl("/refreshToken");
        ReSTClient rest = new ReSTClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getBasicHeaders());
        request.addHeader("Authentication", session.getSessionToken());
        request.addHeader("token", session.getIdToken());
        request.addHeader("refreshtoken", session.getRefreshToken());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void uploadCV(final Session session, final File file, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/user/uploadFile");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getSecureHeaders());
        request.addHeader("secretKey", session.getSecretKey());
        request.addHeader("sessionToken", session.getSessionToken());
        request.addHeader("accessKey", session.getAccessKey());

        // -- BODY
        request.addField("userId", session.getDataSessionId());
        request.addFile("upfile", file);

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getSystemStatus(final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/system/versions");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }
}