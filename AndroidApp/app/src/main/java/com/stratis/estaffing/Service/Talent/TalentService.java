package com.stratis.estaffing.Service.Talent;

import android.util.Log;

import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Core.eStaffingClient;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Erick Sanchez
 * Revision 1 - 24/11/20
 */

public class TalentService {

    public static void findByHierarchy(Session session, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/talent/findByHierarchy");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        //--
        request.addParameter("userId", session.getDataSessionId());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void get(String talentId, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/talent");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("id", talentId);

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getSkillCatalog(String profileId, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/skill/findByProfile");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("profileId", String.valueOf(profileId));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getDescription(String talentId ,final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/talent/" + talentId +  "/description");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void sendFeedback(JSONObject body, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/feedback/save");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addBodyJson(JsonBuilder.jsonToString(body));
        rest.execute(request, listener);
    }

    public static void getFeedbacks(String userId, String talentId, final ReSTCallback listener) {
        final String url = Api.getInstance().getUrl("/feedback/"+ userId +"/" + talentId);
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");
        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getFeedbackSkills(String userId, String talentId, final ReSTCallback listener) {
        final String url = Api.getInstance().getUrl("/feedback/findSkills");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("userId", String.valueOf(userId));
        request.addParameter("talentId", String.valueOf(talentId));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void shareCV(final Session session, final String talentId, final String email, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/talent/" + talentId + "/sendcv");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("email", email);
        request.addParameter("userId", session.getDataSessionId());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }
}