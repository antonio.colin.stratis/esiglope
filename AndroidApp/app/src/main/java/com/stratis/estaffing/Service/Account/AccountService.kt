package com.stratis.estaffing.Service.Account

import android.util.Log
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Core.Api
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTRequest
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Core.eStaffingClient
import org.json.JSONException
import org.json.JSONObject

open class AccountService {
    companion object {

        fun deleteAccount(password: String, reasonId: Long, comment: String, session: Session, listener: ReSTCallback?) {
            val url = Api.getInstance().getUrl("/account/cancel")
            val rest = eStaffingClient(url)
            val request = ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "")

            // --
            request.addHeaders(Api.getSecureHeaders())

            // -- JSON Body
            val body = JSONObject()
            try {
                body.put("password", password)
                body.put("reasonId", reasonId)
                body.put("comment", comment)
                body.put("username", session.username)
                body.put("userId", session.dataSessionId)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            request.addBodyJson(JsonBuilder.jsonToString(body))
            Log.d("DXGOP", "CANCEL ACCOUNT SEND ::: " + JsonBuilder.jsonToString(body))

            // --
            rest.setDebugMode(Api.getInstance().canLog())
            rest.execute(request, listener)
        }
    }

}