package com.stratis.estaffing.Model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 21/08/20
 */

public class LatestRequest {

    private Integer id = 0;
    private Integer vacancyProfileId = 0;
    private String vacancyProfileName = "";
    private String status = "";
    private String creationDate = "";
    private String updateDate = "";
    private Integer createdById = 0;
    private Integer updatedById = 0;
    private Integer clientId = 0;
    private Integer seniorityId = 0;
    private String seniorityName = "";
    private Integer startDateId = 0;
    private Integer jobModalityId = 0;
    private Integer minBudget = 0;
    private Integer maxBudget = 0;
    private Boolean requiresPc = true;
    private String group = "";
    private String rhContact = "";

    public LatestRequest() {}

    public LatestRequest(JSONObject object) {

        if (object != null) {

            this.id = object.optInt("id");
            this.vacancyProfileId = object.optInt("vacancyProfileId");
            this.vacancyProfileName = object.optString("vacancyProfileName");
            this.status = object.optString("status");
            this.creationDate = object.optString("creationDate");
            this.updateDate = object.optString("updateDate");
            this.createdById = object.optInt("createdById");
            this.updatedById = object.optInt("updatedById");
            this.clientId = object.optInt("clientId");
            this.seniorityId = object.optInt("seniorityId");
            this.seniorityName = object.optString("seniorityName");
            this.startDateId = object.optInt("startDateId");
            this.jobModalityId = object.optInt("jobModalityId");
            this.minBudget = object.optInt("minBudget");
            this.maxBudget = object.optInt("maxBudget");
            this.requiresPc = object.optBoolean("requiresPc");
            this.group = object.optString("group");
            this.rhContact = object.optString("rhContact");
        }
    }

    public static ArrayList<LatestRequest> parse(JSONArray array) {

        if (array != null) {

            ArrayList<LatestRequest> latestRequests = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                latestRequests.add(new LatestRequest(obj));
            }
            return latestRequests;
        }
        return null;
    }

    public static ArrayList<LatestRequest> makeDummy() {

        // --
        ArrayList<LatestRequest> requests = new ArrayList<>();

        // --
        LatestRequest fe = new LatestRequest();
        fe.vacancyProfileName = "Desarrollador Front-End";
        fe.updateDate = "Hace 3 días";
        fe.seniorityName = "Senior";
        requests.add(fe);

        // --
        LatestRequest ba = new LatestRequest();
        ba.vacancyProfileName = "Business Analyst";
        ba.updateDate = "Hace 14 días";
        ba.seniorityName = "Middle";
        requests.add(ba);

        // --
        LatestRequest ue = new LatestRequest();
        ue.vacancyProfileName = "User Experience Desiger";
        ue.updateDate = "Hace 1 mes";
        ue.seniorityName = "Middle";
        requests.add(ue);

        // --
        return requests;
    }

    // --

    public Integer getId() {
        return id;
    }

    public Integer getVacancyProfileId() {
        return vacancyProfileId;
    }

    public String getVacancyProfileName() {
        return vacancyProfileName;
    }

    public String getStatus() {
        return status;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public Integer getCreatedById() {
        return createdById;
    }

    public Integer getUpdatedById() {
        return updatedById;
    }

    public Integer getClientId() {
        return clientId;
    }

    public Integer getSeniorityId() {
        return seniorityId;
    }

    public String getSeniorityName() {
        return seniorityName;
    }

    public Integer getStartDateId() {
        return startDateId;
    }

    public Integer getJobModalityId() {
        return jobModalityId;
    }

    public Integer getMinBudget() {
        return minBudget;
    }

    public Integer getMaxBudget() {
        return maxBudget;
    }

    public Boolean getRequiresPc() {
        return requiresPc;
    }

    public String getGroup() {
        return group;
    }

    public String getRhContact() {
        return rhContact;
    }
}
