package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate.RequestCandidateLanguagesAdapter;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateLanguageModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.CandidateService;

import org.json.JSONArray;

import java.util.ArrayList;

public class RecruitmentRequestCandidateLanguagesFragment extends ABFragment {

    private RequestCandidate candidate;
    private ArrayList<CandidateLanguageModel> languageModels;
    private int currentLanguageIndex;

    private VideoView video;
    private View blur;
    private TextView noVideoTitle;
    private ImageView videoPlayButton;
    //private ProgressBar videoProgressBar;

    private ImageButton language_header_back_button;
    private TextView language_header_title;
    private ImageButton language_header_next_button;

    public RecruitmentRequestCandidateLanguagesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_candidate_languages, container, false);

        this.setHeader("");
        this.showLoader();
        this.initViews();
        this.getLanguages();

        return this.mView;
    }

    private void initViews() {

        this.video = this.mView.findViewById(R.id.video);
        this.blur = this.mView.findViewById(R.id.blur);
        this.noVideoTitle = this.mView.findViewById(R.id.noVideoTitle);
        this.videoPlayButton = this.mView.findViewById(R.id.videoPlayButton);
        //this.videoProgressBar = this.mView.findViewById(R.id.videoProgressBar);

        this.language_header_back_button = this.mView.findViewById(R.id.language_header_back_button);
        this.language_header_title = this.mView.findViewById(R.id.language_header_title);
        this.language_header_next_button = this.mView.findViewById(R.id.language_header_next_button);

        this.language_header_back_button.setOnClickListener(v -> {

           if (!languageModels.get(currentLanguageIndex-1).getVideoUrl().equals("")) {

               currentLanguageIndex--;
               setVideoComponent();
               setVideoHeaderComponent();
           }
        });

        this.language_header_next_button.setOnClickListener(v -> {

            if (!languageModels.get(currentLanguageIndex+1).getVideoUrl().equals("")) {

                currentLanguageIndex++;
                setVideoComponent();
                setVideoHeaderComponent();
            }
        });
    }

    private void getLanguages() {

        CandidateService.getCandidateLanguages(this.candidate.getId(), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                hideLoader();
                final JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                parseLanguages(res);
            }

            @Override
            public void onError(ReSTResponse response) {

                hideLoader();
                parseLanguages(null);
            }
        });
    }

    private void parseLanguages(JSONArray res) {

        this.languageModels = CandidateLanguageModel.parse(res);

        /** LANGUAGES LIST **/
        final RecyclerView languagesList = this.mView.findViewById(R.id.languagesList);
        App.createVerticalRecyclerList(languagesList, getContext());
        final RequestCandidateLanguagesAdapter adapter = new RequestCandidateLanguagesAdapter(languageModels);
        languagesList.setAdapter(adapter);

        adapter.setListener(position -> {

            currentLanguageIndex = position;
            setVideoComponent();
            setVideoHeaderComponent();
        });

        /** HEADER **/
        if (languageModels.size() == 0) {

            this.setVideoComponentEmpty();
            this.setVideoHeaderComponentEmpty(getContext().
                    getString(R.string.recruitment_request_candidate_languages_container_title_not_available));
            this.mView.findViewById(R.id.languagesContainer).setVisibility(View.GONE);

        } else {

            this.currentLanguageIndex = 0;
            this.setVideoComponent();
            this.setVideoHeaderComponent();
        }
    }

    private void setVideoComponent() {

        // -- Clean
        this.video.setVisibility(View.VISIBLE);
        if (this.video.isPlaying()) {
            this.video.pause();
        }
        this.noVideoTitle.setVisibility(View.GONE);
        this.videoPlayButton.setVisibility(View.VISIBLE);
        this.blur.setAlpha(1.0f);
        this.video.setBackgroundColor(Color.BLACK);
        //this.videoProgressBar.setVisibility(View.GONE);

        // -- Check if has video
        boolean validVideo = false;
        final CandidateLanguageModel languageModel = this.languageModels.get(this.currentLanguageIndex);
        if (languageModel != null && !languageModel.getVideoUrl().equals("")) {

            // -- Check video format
            String extension = "";
            if (languageModel.getVideoUrl().contains(".")) {
                extension = languageModel.getVideoUrl().substring(languageModel.getVideoUrl().lastIndexOf(".")+1).toUpperCase();
            }

//            if (  extension.equals(".mp4")) {
            if(App.formatVideoAllow().contains(extension)){
                // --
                Bundle parameters = new Bundle();
                parameters.putString("idioma", languageModel.getLangName());
                final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
                mFirebaseAnalytics.logEvent("Videos_Reproducidos", parameters);

                // --
                validVideo = true;
                this.blur.setAlpha(0.0f);

                // --
                MediaController mediaController = new MediaController(getContext());
                mediaController.setAnchorView(this.video);
                mediaController.setMediaPlayer(this.video);
                this.video.setVideoPath(languageModel.getVideoUrl());
                this.video.setMediaController(mediaController);
                this.video.setOnPreparedListener(mp -> {
                    //videoProgressBar.setVisibility(View.GONE);
                });
                this.video.setOnErrorListener((mp, what, extra) -> {
                    videoPlayButton.setVisibility(View.GONE);
                    return false;
                });

                // --
                this.videoPlayButton.setOnClickListener(v -> {
                    video.setBackgroundColor(Color.TRANSPARENT);
                    videoPlayButton.setVisibility(View.GONE);
                    //videoProgressBar.setVisibility(View.VISIBLE);
                    video.start();
                });
            }
        }

        if (!validVideo) {
            this.videoPlayButton.setVisibility(View.GONE);
            this.setVideoComponentEmpty();
        }
    }

    private void setVideoComponentEmpty() {

        this.video.setVisibility(View.GONE);
        this.noVideoTitle.setVisibility(View.VISIBLE);
        this.videoPlayButton.setVisibility(View.GONE);
    }

    private void setVideoHeaderComponent() {

        final CandidateLanguageModel languageModel = this.languageModels.get(this.currentLanguageIndex);

        if (this.languageModels.size() == 1) {
            this.setVideoHeaderComponentEmpty( languageModel.getLangName() );
        } else {

            // -- Back Button
            if (this.currentLanguageIndex > 0) {

                this.language_header_back_button.setImageBitmap(App.getImage(getContext(), "right_arrow"));
                this.language_header_back_button.setClickable(true);
                this.language_header_back_button.setEnabled(true);
            } else {

                this.language_header_back_button.setImageBitmap(App.getImage(getContext(), "opacity_chev_to_right"));
                this.language_header_back_button.setClickable(false);
                this.language_header_back_button.setEnabled(false);
            }

            // -- Next Button
            if (this.currentLanguageIndex < this.languageModels.size()-1) {

                this.language_header_next_button.setImageBitmap(App.getImage(getContext(), "right_arrow"));
                this.language_header_next_button.setClickable(true);
                this.language_header_next_button.setEnabled(true);
            } else {

                this.language_header_next_button.setImageBitmap(App.getImage(getContext(), "opacity_chev_to_right"));
                this.language_header_next_button.setClickable(false);
                this.language_header_next_button.setEnabled(false);
            }

            // Title
            this.language_header_title.setText( languageModel.getLangName() );
        }
    }

    private void setVideoHeaderComponentEmpty(final String title) {

        final Bitmap arrow = App.getImage(getContext(), "opacity_chev_to_right");
        this.language_header_back_button.setImageBitmap(arrow);
        this.language_header_back_button.setClickable(false);
        this.language_header_back_button.setEnabled(false);

        this.language_header_next_button.setImageBitmap(arrow);
        this.language_header_next_button.setClickable(false);
        this.language_header_next_button.setEnabled(false);

        this.language_header_title.setText(title);
    }

    public void setCandidate(RequestCandidate candidate) {
        this.candidate = candidate;
    }
}