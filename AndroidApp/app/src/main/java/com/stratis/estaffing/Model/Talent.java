package com.stratis.estaffing.Model;

import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateWorkExperienceModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 18/06/20
 */

public class Talent {

    private String id = ""; // -- Both services
    private String userFullName = ""; // -- First service only
    private String jobSeniority = ""; // -- Both services
    private String position = ""; // -- Both services
    private String userImg = ""; // -- Both services

    private String firstName = ""; // -- Second service
    private String lastName = "";
    private String phone = "";
    private String hireDate = "";
    private String currentLeader = "";
    private String assignmentPeriod = "";
    private ArrayList<Course> courses = null;
    private ArrayList<Feedback> feedbacks = null;
    private ArrayList<Language> languages = null;
    private String lastEvaluation = "";
    private String oldDate = "";
    private String positionArea = "";
    private String previousJobs = "";
    private String rating = "0.0";
    private ArrayList<Skill> skills = null;
    private String description = "";
    private ArrayList<CandidateWorkExperienceModel> experiences = null;

    public Talent() {}

    public Talent(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optString("id");
            this.userFullName = obj.optString("userFullName");
            this.jobSeniority = obj.optString("jobSeniority");
            this.position = obj.optString("position");
            this.userImg = obj.optString("userImg");

            //  --
            this.firstName = obj.optString("firstName", "");
            this.lastName = obj.optString("lastName", "");
            this.phone = obj.optString("phone", "");
            this.hireDate = obj.optString("hireDate", "");
            this.currentLeader = obj.optString("currentLeader", "");
            this.assignmentPeriod = obj.optString("assignmentPeriod", "");
            this.courses = Course.parse(obj.optJSONArray("courses"));
            this.feedbacks = Feedback.parse(obj.optJSONArray("feedbacks"));
            this.languages = Language.parse(obj.optJSONArray("languages"));
            this.lastEvaluation = obj.optString("lastEvaluation", "");
            this.oldDate = obj.optString("oldDate", "");
            this.positionArea = obj.optString("positionArea", "");
            this.previousJobs = obj.optString("previousJobs", "");
            this.skills = Skill.parse(obj.optJSONArray("skills"));
            this.description = obj.optString("description", "").replace("null", "");
            this.experiences = CandidateWorkExperienceModel.parse(obj.optJSONArray("previousJobs"));
            
            // --  Rating validation
            if (!obj.isNull("rating")) {
                this.rating = obj.optString("rating", "");
                if (this.rating.equals("")) {
                    this.rating = "0.0";
                }
            }
        }
    }

    public static ArrayList<Talent> parse(JSONArray array) {

        ArrayList<Talent> talents = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {

            JSONObject obj = array.optJSONObject(i);
            talents.add(new Talent(obj));
        }
        return talents;
    }

    public String getId() {
        return id;
    }

    public String getUserFullName() {

        String name = "";

        String[] userFullNameParts = userFullName.split(" ");

        // -- Part 0
        if (this.userFullName.equals("")) { // -- No instance o userFullName

            this.userFullName = this.firstName + " " + this.lastName;
            return this.getUserFullNameShort();
            //userFullNameParts = tempName.split(" ");
        }

        // -- Part 1
        if (userFullNameParts.length > 0) {
            name = userFullNameParts[0];
        }

        if (userFullNameParts.length >= 2) {
            name = name + " " + userFullNameParts[1];
        }

        // -- Part 2
        /*if (userFullNameParts.length >= 3) {
            name = name + " " + userFullNameParts[2];
        } else if (userFullNameParts.length >= 2) {
            name = name + " " + userFullNameParts[1];
        }*/

        // --
        return name;
    }

    public String getUserFullNameShort() {

        String name = "";
        String[] userNameParts = userFullName.split(" ");

        if (userNameParts.length > 0) {
            name = userNameParts[0] + " ";
        }

        if (userNameParts.length >= 4) {
            if (userNameParts[2].equals("")) {
                name = name + userNameParts[3];
            } else {
                name = name + userNameParts[2];
            }
        } else if (userNameParts.length == 3 || userNameParts.length == 2) {
            name = name + userNameParts[1];
        }

        // --
        return name;
    }

    public String getJobSeniority() {
        return jobSeniority;
    }

    public String getPosition() {
        return position;
    }

    public String getUserImg() {
        return userImg;
    }

    //  --

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getHireDate() {
        return hireDate;
    }

    public String getCurrentLeader() {
        return currentLeader;
    }

    public String getCurrentLeaderShort() {

        String name = "";
        final String[] leaderParts = this.currentLeader.split(" ");

        // -- Part 1
        if (leaderParts.length > 0) {
            name = leaderParts[0];
        }

        // -- Part 2
        if (leaderParts.length > 1) {
            name = String.format("%s %c.", name, leaderParts[1].charAt(0));
        }

        // --
        return name;
    }

    public String getAssignmentPeriod() {
        return assignmentPeriod;
    }

    public String getAssigmentPeriodFormatted(){
        String[] periodParsed = assignmentPeriod.split(" ");
        if(periodParsed.length == 7) {
            periodParsed[1] = periodParsed[1].substring(0,1).toUpperCase() + periodParsed[1].substring(1);
            periodParsed[5] = periodParsed[5].substring(0,1).toUpperCase() + periodParsed[5].substring(1);
            return periodParsed[0] + "/" + periodParsed[1] + "/" + periodParsed[2] + " a " +
                    periodParsed[4] + "/" + periodParsed[5] + "/" + periodParsed[6];
        }
        return getAssignmentPeriod();
    }

    public String getLastEvaluation() {
        return lastEvaluation;
    }

    public String getOldDate() {
        return oldDate;
    }

    public String getPositionArea() {
        return positionArea;
    }

    public String getPreviousJobs() {
        return previousJobs;
    }

    public String getRating() {
        return rating;
    }

    public ArrayList<Skill> getSkills() {
        return skills;
    }

    public ArrayList<Language> getLanguages() {
        return languages;
    }

    public ArrayList<Course> getCourses() {
        return courses;
    }

    public void createFeedbacks(Feedback feedback) {

        this.feedbacks = new ArrayList<>();
        this.feedbacks.add(feedback);
    }

    public ArrayList<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public String getDescription() { return description; }

    public ArrayList<CandidateWorkExperienceModel> getExperiences() { return experiences; }
}
