package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.datatransport.cct.internal.LogEvent;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal.Adapter.NoCandidateInterestAdapter;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal.Model.NoCandidateInterestModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.CatalogService;
import com.stratis.estaffing.Service.MasterAdmin.MasterAdminService;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.CandidateService;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.RequestsService;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 09/12/20
 */

public class NoCandidateInterestModal extends BottomSheetDialog implements View.OnClickListener {

    private final String otherOption = "Falta de conocimiento técnico";

    private RequestCandidate candidate;

    public LoadingButton continueButton;
    private Button cancelButton;

    public NoCandidateInterestModalListener listener;
    private ArrayList<NoCandidateInterestModel> options;
    private String optionTextSelected = "";

    private boolean fromMasterAdmin = false;

    public NoCandidateInterestModal(Context context, final RequestCandidate candidate) {

        super(context);
        this.candidate = candidate;
        this.commonInit();
    }

    public NoCandidateInterestModal(Context context, int themeResId) {

        super(context, themeResId);
        this.commonInit();
    }

    protected NoCandidateInterestModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.commonInit();
    }

    private void commonInit() {

        // -- R styles
        this.setContentView(R.layout.fragment_recruitment_request_candidate_not_interest_modal);
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName());
        this.setCanceledOnTouchOutside(false);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // -- Title
        final String name = String.format(getContext().
                getString(R.string.recruitment_request_candidate_single_modal_title), this.candidate.getFullNameShort());
        final SpannableStringBuilder nameBuilder= App.getTextWithSpan(name, this.candidate.getFullNameShort(),
                new StyleSpan(Typeface.BOLD));
        ((TextView) this.findViewById(R.id.title)).setText( nameBuilder );

        //  --
        CatalogService.getNoInterestInCandidateCatalog(new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                options = NoCandidateInterestModel.parse(res);
                handleActions();
            }

            @Override
            public void onError(ReSTResponse response) {}
        });

        // -- Buttons
        this.cancelButton = this.findViewById(R.id.cancelButton);
        cancelButton.setTag("31");
        cancelButton.setOnClickListener(this);

        this.continueButton = new LoadingButton(this.findViewById(R.id.continueButton));
        continueButton.getButton().setTag("32");
        continueButton.getButton().setOnClickListener(this);
        continueButton.setText(getContext().getString(R.string.recuritment_request_candidate_single_modal_finish));
        continueButton.getButton().setBackground(App.getDrawable(getContext(), R.drawable.button_paused_gray));
        continueButton.getButton().setClickable(false);
    }

    @Override
    public void show() {
        super.show();
    }

    @SuppressLint("ResourceType")
    private void handleActions() {

        // -- Options
        final RecyclerView optionsList = this.findViewById(R.id.options);
        App.createVerticalRecyclerList(optionsList, getContext());
        final NoCandidateInterestAdapter noCandidateInterestAdapter = new NoCandidateInterestAdapter(
                this.options, getContext()
        );
        optionsList.setAdapter(noCandidateInterestAdapter);
        noCandidateInterestAdapter.setListener(option -> {

            optionTextSelected = option;
            if (optionTextSelected.equals(otherOption)) {
                findViewById(R.id.otherAvailable).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.otherAvailable).setVisibility(View.GONE);
            }
            // --
            continueButton.getButton().setBackground(App.getDrawable(getContext(), R.drawable.activity_login_controller_login_button));
            App.setViewAsPrimaryColor(continueButton.getButton(), getContext());
            continueButton.getButton().setClickable(true);
            App.setViewAsPrimaryColor(continueButton.getButton(), getContext());
        });
    }

    public void setListener(NoCandidateInterestModalListener listener) {
        this.listener = listener;
    }

    public void setFromMasterAdmin(boolean fromMasterAdmin) {
        this.fromMasterAdmin = fromMasterAdmin;
    }

    @Override
    public void onClick(View v) {

        final String tag = String.valueOf(v.getTag());
        if (tag.equals("31")) {
            cancel();
        } else {

            final String otherCommentText = String.valueOf(((TextInputEditText) this.findViewById(R.id.otherCommentText)).getText());
            if (this.optionTextSelected.equals(otherOption) && otherCommentText.equals("")) {
                optionTextSelected = otherOption;
            } else if (this.optionTextSelected.equals(otherOption) && !otherCommentText.equals("")) {
                optionTextSelected = otherCommentText;
            }

            // --
            this.changeStatus();
            this.continueButton.showLoading();
        }
    }

    private void changeStatus() {

        final Session session = Session.get(getContext());
        this.cancelButton.setOnClickListener(null);

        if (!this.fromMasterAdmin) {

            CandidateService.candidateWithNoInterest(this.candidate, this.optionTextSelected, session,
                    new ReSTCallback() {
                        @Override
                        public void onSuccess(ReSTResponse response) {


                            // --
                            Bundle parameters = new Bundle();
                            parameters.putString("motivo_anulacion", optionTextSelected);
                            final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
                            mFirebaseAnalytics.logEvent("Candidato_Evaluado", parameters);

                            // --
                            if (listener != null) {
                                listener.onStatusChange();
                            }
                            cancel();
                        }

                        @Override
                        public void onError(ReSTResponse response) {}
                    });

        } else {

            MasterAdminService.Companion.discard(this.candidate, this.optionTextSelected, session,
                    new ReSTCallback() {
                        @Override
                        public void onSuccess(ReSTResponse response) {

                            if (listener != null) {
                                listener.onStatusChange();
                            }
                            cancel();
                        }

                        @Override
                        public void onError(ReSTResponse response) {}
                    });
        }

    }

    public interface NoCandidateInterestModalListener {
        void onStatusChange();
    }
}