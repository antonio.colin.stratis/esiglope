package com.stratis.estaffing.Controller.LoginFlow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Controller.RegisterFlow.RegisterCodeValidationController;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTClient;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ResetController extends AppCompatActivity implements View.OnClickListener {

    private TextInputLayout passwordLayout;
    private TextInputEditText passwordEditText;
    private TextInputLayout rePasswordLayout;
    private TextInputEditText rePasswordEditText;

    private ImageView character_rule_check;
    private ImageView caps_rule_check;
    private ImageView numbers_rule_check;
    private ImageView password_rule_check;

    private TextView pinResendCode;
    private LoadingButton updateButton;
    private Button cancelButton;

    private String pinCode = "";

    private boolean charactersRule = false;
    private boolean capsRule = false;
    private boolean numbersRule = false;
    private boolean passwordsRule = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // --
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_controller);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName());
        this.initViews();

        // -- Showing intent notifications
        this.showIntentNotification();
    }

    private void initViews() {

        // --
        final OtpView pinView = this.findViewById(R.id.pinView);
        pinView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                pinCode = otp;
            }
        });

        // --
        this.passwordLayout = this.findViewById(R.id.password);
        this.passwordEditText = this.findViewById(R.id.passwordText);

        this.rePasswordLayout = this.findViewById(R.id.rePassword);
        this.rePasswordEditText = this.findViewById(R.id.rePasswordText);

        // --
        this.passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {

                // --
                if (s.length() == 0) {

                    turnOffRules();
                    return;
                }

                // -- Characters rule
                validateCharactersRule(String.valueOf(s));
                // -- Numbers rule
                validateNumbersRule(String.valueOf(s));
                // -- Numbers rule
                validateCapsRule(String.valueOf(s));
                // -- Validate password
                final String rs = String.valueOf(rePasswordEditText.getText());
                if (!rs.equals("")) {
                    validatePasswordsRule(String.valueOf(s), rs);
                }
            }
        });

        this.rePasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {

                // -- Validate password
                final String os = String.valueOf(passwordEditText.getText());
                if (!os.equals("")) {
                    validatePasswordsRule(String.valueOf(s), os);
                }
            }
        });

        // --
        final RelativeLayout characters_rule = this.findViewById(R.id.characters_rule);
        this.character_rule_check = characters_rule.findViewById(R.id.state);
        final TextView characters_rule_text = characters_rule.findViewById(R.id.text);
        characters_rule_text.setText( this.getResources().getString(R.string.reset_ruler_characters_rule) );

        final RelativeLayout caps_rule = this.findViewById(R.id.caps_rule);
        this.caps_rule_check = caps_rule.findViewById(R.id.state);
        final TextView caps_rule_text = caps_rule.findViewById(R.id.text);
        caps_rule_text.setText( this.getResources().getString(R.string.reset_ruler_caps_rule) );

        final RelativeLayout numbers_rule = this.findViewById(R.id.numbers_rule);
        this.numbers_rule_check = numbers_rule.findViewById(R.id.state);
        final TextView numbers_rule_text = numbers_rule.findViewById(R.id.text);
        numbers_rule_text.setText( this.getResources().getString(R.string.reset_ruler_numbers_rule) );

        final RelativeLayout password_rule = this.findViewById(R.id.password_rule);
        this.password_rule_check = password_rule.findViewById(R.id.state);
        final TextView password_rule_text = password_rule.findViewById(R.id.text);
        password_rule_text.setText( this.getResources().getString(R.string.reset_ruler_password_rule) );

        // --
        this.updateButton = new LoadingButton((RelativeLayout) this.findViewById(R.id.resetButton));
        this.updateButton.setText(getResources().getString(R.string.reset_reset_button));
        this.updateButton.getButton().setTag(10);
        this.updateButton.getButton().setOnClickListener(this);

        this.cancelButton = this.findViewById(R.id.cancelButton);
        this.cancelButton.setTag(11);
        this.cancelButton.setOnClickListener(this);

        // --
        this.pinResendCode = this.findViewById(R.id.pinResendCode);
        this.pinResendCode.setTag(12);
        this.pinResendCode.setOnClickListener(this);
    }

    private void showIntentNotification() {

        if (getIntent().hasExtra("notification")) {

            final String nt = getIntent().getStringExtra("notification");
            if (nt.equals("reset")) {

                final Notification notification = new Notification(this);
                notification.show();
            }
        }
    }

    private void turnOffRules() {

        // --
        this.character_rule_check.setImageBitmap(this.getImage("rule_off"));
        this.caps_rule_check.setImageBitmap(this.getImage("rule_off"));
        this.numbers_rule_check.setImageBitmap(this.getImage("rule_off"));
        this.password_rule_check.setImageBitmap(this.getImage("rule_off"));
    }

    private void update() {

        // -- User by Intent
        String user = "";
        if (getIntent().hasExtra("user")) {
            user = getIntent().getStringExtra("user");
        }
        if (user.equals("")) {
            return;
        }

        // --
        this.showPinError(false);
        this.showError(false);

        // -- PinCode
        if (this.pinCode.length() < 6 || this.pinCode.equals("")) {
            this.showPinError(true);
            return;
        }

        // -- Password
        final String password = String.valueOf(this.passwordEditText.getText());
        if (password.equals("")) {
            this.showError(true);
            return;
        }

        // -- RePassword
        final String rePassword = String.valueOf(this.rePasswordEditText.getText());
        if (rePassword.equals("")) {
            this.showError(true);
            return;
        }

        // -- Check password conditions
        if (!this.charactersRule || !this.capsRule || !this.numbersRule || !this.passwordsRule) {
            this.showError(true);
            return;
        }

        // --
        this.enableLoginButton(false);

        // --
        final String url = Api.getInstance().getLoginUrl("/updatePassword");
        ReSTClient rest = new ReSTClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_PUT, "");

        // --
        request.addHeader("Accept", "application/json");
        request.addHeader("Content-Type", "application/json");

        // --
        JSONObject params = new JSONObject();
        try {

            params.put("username", user);
            params.put("password", password);
            params.put("code", this.pinCode);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // --
        request.addBodyJson(JsonBuilder.jsonToString(params));

        // --
        rest.execute(request, new ReSTCallback() {

            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Log.d("DXGOP", "STATUS RESPONSE = " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);

                // --
                if (res != null) {

                    int code = res.optInt("code", 500);
                    switch(code) {

                        case 102:
                            Intent intent = new Intent(ResetController.this, LoginController.class);
                            intent.putExtra("notification", "reLogin");
                            startActivity(intent);
                            finishAffinity();
                            break;

                        case 150:
                            showPinError(true);
                            enableLoginButton(true);
                            break;

                        default:
                            showError(true);
                            enableLoginButton(true);
                            break;
                    }
                } else {

                    showError(true);
                    enableLoginButton(true);
                }
            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                Log.d("DXGOP", "ERROR :: " + response.body);
                // --
                showError(true);
                enableLoginButton(true);
            }
        });
    }

    private void getNewCode() {

        // -- User by Intent
        String user = "";
        // --
        if (getIntent().hasExtra("user")) {
            user = getIntent().getStringExtra("user");
        }
        // --
        if (user.equals("")) {
            return;
        }

        // --
        this.pinResendCode.setEnabled(false);
        this.pinResendCode.setClickable(false);

        // --
        final String url = Api.getInstance().getLoginUrl("/resetPassword");
        ReSTClient rest = new ReSTClient(url);
        final ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeader("Accept", "application/json");
        request.addHeader("Content-Type", "application/json");

        // --
        request.addParameter("username", user);

        // --
        rest.execute(request, new ReSTCallback() {

            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Log.d("DXGOP", "RESEND CODE RESPONSE = " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);

                // --
                pinResendCode.setEnabled(true);
                pinResendCode.setClickable(true);

                // --
                if (res != null) {

                    int code = res.optInt("code", 500);
                    if (code == 100) {

                        // --
                        // Show notification
                        final Notification notification = new Notification(ResetController.this);
                        notification.setTitle(getResources().getString(R.string.reset_new_code_title_notification));
                        notification.setMessage(getResources().getString(R.string.reset_new_code_sub_title_notification));
                        notification.show();

                    } else {

                        String msg = res.optString("msg", "Ha ocurrido un error, intenta más tarde");
                        final Notification notification = new Notification(ResetController.this, R.layout.notification_light_error);
                        notification.setMessage(msg);
                        notification.show(1500);
                    }
                }
            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                Log.d("DXGOP", "ERROR :: " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);
                String msg = res.optString("msg", "Ha ocurrido un error, intenta más tarde");

                // --
                pinResendCode.setEnabled(true);
                pinResendCode.setClickable(true);

                // --
                final Notification notification = new Notification(ResetController.this, R.layout.notification_light_error);
                notification.setMessage(msg);
                notification.show(1500);
            }
        });
    }

    private void enableLoginButton(boolean state) {

        // --
        this.pinResendCode.setEnabled(state);
        this.pinResendCode.setClickable(state);

        // --
        this.updateButton.showLoading(!state);

        // --
        this.cancelButton.setEnabled(state);
        this.cancelButton.setClickable(state);
    }

    private void validateCharactersRule(String string) {

        if (string.length() >= 8) {

            this.charactersRule = true;
            this.character_rule_check.setImageBitmap(this.getImage("rule_on"));

        } else {

            this.charactersRule = false;
            this.character_rule_check.setImageBitmap(this.getImage("rule_error"));
        }
    }

    private void validateNumbersRule(String string) {

        if (string.matches(".*\\d.*")) {

            this.numbersRule = true;
            this.numbers_rule_check.setImageBitmap(this.getImage("rule_on"));

        } else {

            this.numbersRule = false;
            this.numbers_rule_check.setImageBitmap(this.getImage("rule_error"));
        }
    }

    private void validateCapsRule(String string) {

        if (string.matches("^(?=.*[a-z])(?=.*[A-Z]).+$")) {

            this.capsRule = true;
            this.caps_rule_check.setImageBitmap(this.getImage("rule_on"));

        } else {

            this.capsRule = false;
            this.caps_rule_check.setImageBitmap(this.getImage("rule_error"));
        }
    }

    private void validatePasswordsRule(String pw1, String pw2) {

        if ( (!pw1.equals("") && !pw2.equals("")) && (pw1.equals(pw2)) ) {

            this.passwordsRule = true;
            this.password_rule_check.setImageBitmap(this.getImage("rule_on"));

        } else {

            this.passwordsRule = false;
            this.password_rule_check.setImageBitmap(this.getImage("rule_error"));
        }
    }

    private void showError(boolean state) {

        // --
        final TextView passwordTitle = this.findViewById(R.id.passwordTitle);
        final TextView rePasswordTitle = this.findViewById(R.id.rePasswordTitle);
        final TextView passwordsError = this.findViewById(R.id.passwordsError);
        if (state) {

            passwordTitle.setTextColor(this.getResources().getColor(R.color.red));
            rePasswordTitle.setTextColor(this.getResources().getColor(R.color.red));
            this.passwordLayout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_error_border));
            this.rePasswordLayout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_error_border));
            passwordsError.setVisibility(View.VISIBLE);

        } else {

            passwordTitle.setTextColor(Color.parseColor("#333333"));
            rePasswordTitle.setTextColor(Color.parseColor("#333333"));
            this.passwordLayout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_border));
            this.rePasswordLayout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_border));
            passwordsError.setVisibility(View.GONE);
        }
    }

    private void showPinError(boolean state) {

        final TextView pinCodeTittle = this.findViewById(R.id.pinTitle);
        final TextView pinCodeError = this.findViewById(R.id.pinError);
        if (state) {

            pinCodeTittle.setTextColor(this.getResources().getColor(R.color.red));
            pinCodeError.setVisibility(View.VISIBLE);

        } else {

            pinCodeTittle.setTextColor(Color.parseColor("#333333"));
            pinCodeError.setVisibility(View.GONE);
        }
    }

    // -- Click Listener
    @Override
    public void onClick(View v) {

        final Integer tag = (Integer) v.getTag();
        switch (tag) {

            case 10:
                this.update();
                break;

            case 11:
                this.finish();
                break;

            case 12:
                this.getNewCode();
                break;
        }
    }

    // --
    private Bitmap getImage(String name) {

        int id = this.getResources().getIdentifier(name, "drawable", this.getPackageName());
        return BitmapFactory.decodeStream(this.getResources().openRawResource(id));
    }
}