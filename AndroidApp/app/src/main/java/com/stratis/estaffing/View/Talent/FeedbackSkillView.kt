package com.stratis.estaffing.View.Talent

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.R

class FeedbackSkillView(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var skillTextContainer: RelativeLayout? = null
    var skillEditText: EditText? = null
    var skillTextView: TextView? = null
    var skillIconImageView: ImageView? = null
    var newSkillIconImageView: ImageView? = null


    init {
        this.skillTextContainer = itemView.findViewById(R.id.skillTextContainer)
        this.skillEditText = itemView.findViewById(R.id.skillEditText)
        this.skillTextView = itemView.findViewById(R.id.skillTextView)
        this.skillIconImageView = itemView.findViewById(R.id.skillIconImageView)
        this.newSkillIconImageView = itemView.findViewById(R.id.newSkillIconImageView)
    }
}