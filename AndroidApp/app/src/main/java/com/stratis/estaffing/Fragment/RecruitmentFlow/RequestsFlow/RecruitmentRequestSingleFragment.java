package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Adapter.RecruitmentFlow.NewRequest.RequestProfileCheckAdapter;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Loader;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Controller.RecruitmentFlow.RecruitmentFlowEditorController;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestCheckEditFragment;
import com.stratis.estaffing.Listener.NotificationCenterFragmentListener;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.RequestPausedCancelModal;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageCatalog;
import com.stratis.estaffing.Model.RecruitmentFlow.ProfileCheck;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.Model.RecruitmentFlow.SoftwareCatalog;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Recruitment.NewRequestFlow.ProfileService;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.RequestsService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class RecruitmentRequestSingleFragment extends ABFragment implements View.OnClickListener, RequestPausedCancelModal.RequestPausedCancelModalListener {

    private Profile profile;

    private TextView request_info_date;
    private TextView request_info_order;
    private TextView request_info_profile;

    private RecyclerView checksTable;
    private RequestProfileCheckAdapter adapter;
    private Loader loader = null;
    private Boolean canEditTable = true;

    // -- Listener for NotificationCenter
    private NotificationCenterFragmentListener notificationCenterListener;

    public RecruitmentRequestSingleFragment() {}

    public RecruitmentRequestSingleFragment(Profile profile) {
        this.profile = profile;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_single, container, false);

        // -- Set header
        this.setHeader("");

        // --
        this.getRequest();

        // -- Init views
        this.initViews();

        // --
        return this.mView;
    }

    private void initViews() {

        this.request_info_date = this.mView.findViewById(R.id.request_info_date);
        this.request_info_order = this.mView.findViewById(R.id.request_info_order);
        this.request_info_profile = this.mView.findViewById(R.id.request_info_profile);

        // -- Paused and cancel buttons
        final RelativeLayout cancelPauseButtons =
                this.mView.findViewById(R.id.cancelPauseButtons);
        final RelativeLayout cancelButton = cancelPauseButtons.findViewById(R.id.cancelButton);
        cancelButton.setTag("10");
        cancelButton.setOnClickListener(this);

        final RelativeLayout pauseButton = cancelPauseButtons.findViewById(R.id.pauseButton);
        pauseButton.setTag("11");
        pauseButton.setOnClickListener(this);
        this.setViewAsPrimaryColor(pauseButton);
    }

    // -- Method to get requests
    private void getRequest() {

        // -- Show loader
        this.loader = new Loader(this.mContext);
        this.loader.show();

        // --
        final String id = (this.profile != null) ? this.profile.getProvisionalId() : "" ;
        RequestsService.getRequest(id, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                JSONObject res = JsonBuilder.stringToJson(response.body);
                profile.loadFromJSON(res);
                getLanguageCatalog();
            }

            @Override
            public void onError(ReSTResponse response) {
                if (notificationCenterListener != null) {
                    notificationCenterListener.onVacancyNotAvailable();
                }
            }
        });
    }

    private void getLanguageCatalog() {

        ProfileService.getLanguageCatalog(new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                Log.d("DXGOP", "LANGUAGE CATALOG RESPONSE = " + response.body);
                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                profile.setLanguageCatalogs(LanguageCatalog.parse(res));
                getSoftwareCatalog();
            }

            @Override
            public void onError(ReSTResponse response) {

                Log.d("DXGOP", "ERROR :: " + response.body);
                loader.cancel();
            }
        });
    }

    private void getSoftwareCatalog() {

        ProfileService.getSoftwareCatalog(new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                Log.d("DXGOP", "SOFTWARE CATALOG RESPONSE = " + response.body);
                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                profile.setSoftwareCatalogs(SoftwareCatalog.parse(res));
                paintRequestInfo();
                loader.cancel();
            }

            @Override
            public void onError(ReSTResponse response) {

                Log.d("DXGOP", "ERROR :: " + response.body);
                loader.cancel();
            }
        });
    }

    // -- Paint profile info with the request
    private void paintRequestInfo() {

        // -- DATE
        final String dateParsedString = App.getDateFormatted(this.profile.getCreationDate(), "dd/MM/yyyy");
        final String dateString = String.format(this.mContext.getResources()
                .getString(R.string.recruitment_actions_request_single_top_card_date), dateParsedString);
        final SpannableStringBuilder dateBuilder = App.getTextWithSpan(dateString, dateParsedString,
                new StyleSpan(Typeface.BOLD));
        this.request_info_date.setText(dateBuilder);

        // -- ORDER
        final String orderString = String.format(this.mContext.getResources().getString(R.string.recruitment_actions_request_single_top_card_order),
                this.profile.getProvisionalId());
        final SpannableStringBuilder orderBuilder = App.getTextWithSpan(orderString,
                this.profile.getProvisionalId(), new StyleSpan(Typeface.BOLD));
        this.request_info_order.setText(orderBuilder);

        // -- PROFILE
        this.request_info_profile.setText(this.profile.getCurrentProfile().getName().toUpperCase());

        // -- STATUS
        this.paintRequestStatusInfo();
    }

    private void paintRequestStatusInfo() {

        final TextView request_info_status = this.mView.findViewById(R.id.request_info_status);
        final TextView status_date_title = this.mView.findViewById(R.id.status_date_title);
        final TextView status_date = this.mView.findViewById(R.id.status_date);
        final TextView status_comment = this.mView.findViewById(R.id.status_comment);
        final Button buttonActions = this.mView.findViewById(R.id.request_button_actions);
        final ImageView imagenStatus = this.mView.findViewById(R.id.imageState);

        // --
        this.setViewAsSecondaryColor(buttonActions);

        // -- Pres
        ((TextView) this.findView(R.id.pauseTitle)).setText(getString(R.string.recruitment_actions_request_single_paused_button));
        ((ImageView) this.findView(R.id.pauseButton).findViewById(R.id.icon)).
                setImageBitmap(App.getImage(this.mContext, "icon_request_paused_white"));

        // -- Commons
        request_info_status.setText(this.profile.getStatusValue());
        buttonActions.setVisibility(View.GONE);

        setImageStatus();

        //  --
        switch (this.profile.getStatusId()) {

            case LatestVacancyRequest.statusSent:
            case LatestVacancyRequest.statusConfirm:
            case LatestVacancyRequest.statusAttention:
            case LatestVacancyRequest.statusSearch:

                this.setRequestStatusInfoAfterPaused();
                break;

            case LatestVacancyRequest.statusCandidates:
            case LatestVacancyRequest.statusInterviews:

                this.setRequestStatusInfoAfterPaused();
                buttonActions.setVisibility(View.VISIBLE);
                if (this.profile.getStatusId().equals(LatestVacancyRequest.statusCandidates)) {
                    buttonActions.setTag("100");
                } else {
                    buttonActions.setText(getString(R.string.recruitment_actions_request_single_review_request));
                    buttonActions.setTag("101");
                }
                buttonActions.setOnClickListener(this);
                break;

            case LatestVacancyRequest.statusHire:
            case LatestVacancyRequest.statusHireStop:
            case LatestVacancyRequest.statusDone:

                this.canEditTable = false;
                this.setRequestStatusInfoAfterPaused();

                if (this.profile.getStatusId().equals(LatestVacancyRequest.statusDone) ) {
                    this.mView.findViewById(R.id.cancelPauseButtons).setVisibility(View.GONE);
                }
                this.getCandidatesOnlyInHire();
                break;

            case LatestVacancyRequest.statusCancel:
            case LatestVacancyRequest.statusPaused:

                // -- TopBars Color
                this.mView.findViewById(R.id.request_info_header_separator).setBackground(
                        (LatestVacancyRequest.statusPaused.equals(this.profile.getStatusId())) ?
                        this.mContext.getResources().getDrawable(R.drawable.black_top_border) :
                        this.mContext.getResources().getDrawable(R.drawable.red_top_border)
                );
                this.mView.findViewById(R.id.request_info_status_header_separator).setBackground(
                        (LatestVacancyRequest.statusPaused.equals(this.profile.getStatusId())) ?
                                this.mContext.getResources().getDrawable(R.drawable.black_top_border) :
                                this.mContext.getResources().getDrawable(R.drawable.red_top_border)
                );

                // -- Titles
                final String statusString = (LatestVacancyRequest.statusPaused.equals(this.profile.getStatusId())) ?
                        getString(R.string.recruitment_actions_request_single_states_paused_title) :
                        getString(R.string.recruitment_actions_request_single_states_cancel_title);
                final String dateTitleString = String.format(
                        getString(R.string.recruitment_actions_request_single_status_date_title),
                        statusString.toLowerCase());

                status_date_title.setText(dateTitleString);
                status_date_title.setVisibility(View.VISIBLE);
                status_date.setText(App.getDateFormatted(this.profile.getStatusDate(), "dd/MM/yyyy"));
                status_date.setVisibility(View.VISIBLE);
                this.mView.findViewById(R.id.status_date_divider).setVisibility(View.VISIBLE);

                // -- Comment
                final String requestCommentString = String.format(
                        getString(R.string.recruitment_actions_request_single_status_comment_example),
                        this.profile.getStatusComment());
                status_comment.setText(requestCommentString);
                status_comment.setVisibility(View.VISIBLE);

                // -- Buttons
                if (LatestVacancyRequest.statusPaused.equals(this.profile.getStatusId())) {

                    buttonActions.setVisibility(View.VISIBLE);
                    buttonActions.setText(getString(R.string.recruitment_actions_request_single_resume_button));
                    buttonActions.setTag("200");
                    buttonActions.setOnClickListener(this);

                    // -- Settign the pause button to blue - rePlay
                    final RelativeLayout pauseButton = this.mView.findViewById(R.id.pauseButton);
                    //pauseButton.setBackgroundDrawable(getContext().getDrawable(R.drawable.button_paused_gray));
                    pauseButton.setTag("200");
                    ((TextView) pauseButton.findViewById(R.id.pauseTitle)).setText("Reanudar");
                    ((ImageView) pauseButton.findViewById(R.id.icon)).
                            setImageBitmap(App.getImage(this.mContext, "play_icon_filled"));
                }

                // --  Cancel && Pause buttons
                if (LatestVacancyRequest.statusCancel.equals(this.profile.getStatusId())) {
                    this.mView.findViewById(R.id.cancelPauseButtons).setVisibility(View.GONE);
                }

                // -- Table Edit
                this.canEditTable = false;
                break;
        }
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat(this.profile.getStatusId()));

        // -- PROFILE REQUEST DATA TABLE
        paintRequestDataTable();
    }

    private void setImageStatus() {
        final ImageView imagenStatus = this.mView.findViewById(R.id.imageState);
        switch (this.profile.getStatusId()) {
            case LatestVacancyRequest.statusSent:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_sent));
                break;
            case LatestVacancyRequest.statusConfirm:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_confirmed));
                break;
            case LatestVacancyRequest.statusAttention:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_attention_required));
                break;
            case LatestVacancyRequest.statusSearch:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_recruiment));
                break;
            case LatestVacancyRequest.statusCandidates:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_candidate_available));
                break;
            case LatestVacancyRequest.statusInterviews:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_interview));
                break;
            case LatestVacancyRequest.statusHire:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_hiring));
                break;
            case LatestVacancyRequest.statusHireStop:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_stoped));
                break;
            case LatestVacancyRequest.statusDone:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_concluded));
                break;
            case LatestVacancyRequest.statusCancel:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_canceled));
                break;
            case LatestVacancyRequest.statusPaused:
                imagenStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.request_paused));
                break;
        }

    }

    private void setRequestStatusInfoAfterPaused() {

        // -- TopBars Color
        this.mView.findViewById(R.id.request_info_header_separator).setBackground(
                this.mContext.getResources().getDrawable(R.drawable.orange_top_border));

        this.mView.findViewById(R.id.request_info_status_header_separator).setBackground(
                this.mContext.getResources().getDrawable(R.drawable.orange_top_border));

        // --
        this.mView.findViewById(R.id.status_date_divider).setVisibility(View.GONE);
        this.mView.findViewById(R.id.status_date_title).setVisibility(View.GONE);
        this.mView.findViewById(R.id.status_comment).setVisibility(View.GONE);
        this.mView.findViewById(R.id.status_date).setVisibility(View.GONE);

        // --
        this.mView.findViewById(R.id.pauseButton).setBackgroundDrawable(this.mContext.getDrawable(R.drawable.button_paused_blue));
        this.mView.findViewById(R.id.pauseButton).setOnClickListener(this);
        this.setViewAsPrimaryColor(this.mView.findViewById(R.id.pauseButton));
    }

    private void paintSelectedCandidateComponent(final RequestCandidate candidate) {

        final RelativeLayout candidate_component = this.mView.findViewById(R.id.candidate_component);
        candidate_component.setClipToOutline(true);

        final ImageView candidate_image = candidate_component.findViewById(R.id.image);
        final TextView candidate_seniority = candidate_component.findViewById(R.id.seniority);
        final TextView candidate_name = candidate_component.findViewById(R.id.name);
        final TextView candidate_speciality = candidate_component.findViewById(R.id.speciality);

        candidate_component.setVisibility(View.VISIBLE);
        candidate_component.setOnClickListener(v -> {

            final RecruitmentRequestCandidateSingleFragment candidateSingleFragment =
                    new RecruitmentRequestCandidateSingleFragment();
            candidateSingleFragment.setProfile(this.profile);
            candidateSingleFragment.setCandidate(candidate);
            this.continueSegue(candidateSingleFragment);
        });

        // -- Profile image
        candidate_image.setClipToOutline(true);
        if (!candidate.getImgProfile().equals("")) {
            Glide.with(this)
                    .load(candidate.getImgProfile())
                    .centerCrop()
                    .thumbnail(.50f)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    // .placeholder(R.drawable.loading_spinner)
                    .into( candidate_image );
        }

        // -- Fields
        candidate_name.setText( candidate.getFullNameShort() );
        candidate_speciality.setText( candidate.getPositionDesc() );

        // --
        final String s = candidate.getJobSeniority();
        if (s.equals("") || s.equals("null") || s.equals("Null") || s.equals("NULL")) {
            candidate_seniority.setVisibility(View.INVISIBLE);
        } else {
            candidate_seniority.setText( App.capitalize(candidate.getJobSeniority()) );
        }

        /** Regards component **/
        if (this.profile.getStatusId().equals(LatestVacancyRequest.statusDone)) {

//            this.mView.findViewById(R.id.states).setVisibility(View.GONE);
            this.mView.findViewById(R.id.regardsComponent).setVisibility(View.VISIBLE);

            final String candidateChooseRegardsString = String.
                    format(this.getString(R.string.recruitment_actions_request_single_candidate_choose_title),
                            candidate.getFullNameShort());
            ((TextView) this.mView.findViewById(R.id.regardsComponent).findViewById(R.id.title)).
                    setText(candidateChooseRegardsString);
            FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat("finish"));
        }
    }

    private void paintRequestDataTable() {

        // -- PROFILE REQUEST DATA TABLE
        this.checksTable = this.mView.findViewById(R.id.checksTable);
        App.createVerticalRecyclerList(checksTable, this.mContext);

        // --
        this.adapter = new RequestProfileCheckAdapter(
        //final ProfileCheckAdapter adapter = new ProfileCheckAdapter(
                ProfileCheck.getRequestSelectedItemsFromRequestEdit(this.profile, this.mContext, this.canEditTable), this.mContext);
        checksTable.setAdapter(this.adapter);
        this.adapter.setListener(new RequestProfileCheckAdapter.ProfileCheckClickListener() {
            @Override
            public void onCheckButtonClicked(int position) {

                final Intent intent = new Intent(getActivity(), RecruitmentFlowEditorController.class);
                RequestCheckEditFragment.RequestCheckEditType type = null;
                Log.e("DXGOP", "Poosition:::  " +  position);
                switch (position)  {

                    case 0:
                    case 1:
                    case 2:
                        type = RequestCheckEditFragment.RequestCheckEditType.profile;
                        intent.putExtra("canEditProfileName", false);
                        break;

                    case 3:
                        type = RequestCheckEditFragment.RequestCheckEditType.language;
                        break;

                    case 4:
                    case 5:
                    case 6:
                        type = RequestCheckEditFragment.RequestCheckEditType.modality;
                        break;

                    case 7:
                    case 8:
                    case 9:
                        type = RequestCheckEditFragment.RequestCheckEditType.budget;
                        break;
                }
                intent.putExtra("type", type);
                intent.putExtra("profile", profile);
                startActivityForResult(intent, 1234);
            }
        });
    }

    public void getCandidatesOnlyInHire() {

        RequestsService.getCandidates(this.profile.getProvisionalId(), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                final ArrayList<RequestCandidate> items = RequestCandidate.parse(res);
                for (RequestCandidate candidate : items) {

                    if (candidate.getStatusId().equals(RequestCandidate.statusCandidateChoose)) {
                        paintSelectedCandidateComponent(candidate);
                        return;
                    }
                }
            }

            @Override
            public void onError(ReSTResponse response) {}
        });
    }

    @Override
    public void onClick(View v) {

        String tag = String.valueOf(v.getTag());
        switch (tag) {

            case "10":

                final RequestPausedCancelModal requestCancelModal = new RequestPausedCancelModal(this.mContext,
                        RequestPausedCancelModal.RequestPausedCancelModalType.cancel);
                requestCancelModal.setRequestId(this.profile.getProvisionalId());
                requestCancelModal.setListener(this);
                requestCancelModal.show();
                break;

            case "11":

                final RequestPausedCancelModal requestPausedModal = new RequestPausedCancelModal(this.mContext,
                        RequestPausedCancelModal.RequestPausedCancelModalType.paused);
                requestPausedModal.setRequestId(this.profile.getProvisionalId());
                requestPausedModal.setListener(this);
                requestPausedModal.show();
                break;

            case "100":
            case "101":

                final RecruitmentRequestCandidatesFragment candidatesFragment = new RecruitmentRequestCandidatesFragment();
                candidatesFragment.setProfile(this.profile);
                this.continueSegue(candidatesFragment);
                break;

            case "200":
                this.restartRequest();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 12345) {

            this.profile = (Profile) data.getSerializableExtra("profile");
            this.adapter.reload(ProfileCheck.getRequestSelectedItemsFromRequestEdit(this.profile, this.mContext,this.canEditTable));
            this.adapter.notifyDataSetChanged();
            this.updateProfile();
        }
    }

    private void updateProfile() {

        // -- Updating
        final Session session = Session.get(this.mContext);
        ProfileService.update(
                session,
                profile.getUpdateRequestObject(mContext),
                new ReSTCallback() {
                    @Override
                    public void onSuccess(ReSTResponse response) {

                        if (mContext != null) {

                            final Notification notification = new Notification(mContext, R.layout.notification_light);
                            notification.setMessage(mContext.getString(R.string.request_vacancy_check_notification_text));
                            notification.show(1500);
                        }
                    }

                    @Override
                    public void onError(ReSTResponse response) {}
                });
    }

    private void restartRequest() {

        final Loader nLoader = new Loader(this.mContext);
        nLoader.show();

        final Session session = Session.get(this.mContext);
        RequestsService.restartRequest(this.profile.getProvisionalId(), session, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Bundle parameters = new Bundle();
                final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
                mFirebaseAnalytics.logEvent("Solicitudes_Reanudadas", parameters);

                // --
                nLoader.cancel();
                onStatusChange();
            }

            @Override
            public void onError(ReSTResponse response) {
                nLoader.cancel();
            }
        });
    }

    @Override
    public void onStatusChange() {

        this.initViews();
        this.getRequest();
        this.mView.findViewById(R.id.root).scrollTo(0, 0);
        //this.isUpdated = true;

        //RecruitmentRequestSingleFragment fragmentA = new RecruitmentRequestSingleFragment(this.profile);
        //this.reload(fragmentA, "RecruitmentRequestsFragmentTag");
        //final FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

        //this.getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        Log.d("DXGOP", "REFRESH FRAGMENT");

        //getActivity().getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
    }

    // --
    public void setNotificationCenterListener(NotificationCenterFragmentListener notificationCenterListener) {
        this.notificationCenterListener = notificationCenterListener;
    }
}