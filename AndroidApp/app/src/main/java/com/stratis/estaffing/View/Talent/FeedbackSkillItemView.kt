package com.stratis.estaffing.View.Talent

import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.R

class FeedbackSkillItemView(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var shape: RelativeLayout? = null
    var description: TextView? = null
    var checkButton: ImageView? = null

    init {
        this.shape = itemView.findViewById(R.id.shape)
        this.description = itemView.findViewById(R.id.description)
        this.checkButton = itemView.findViewById(R.id.checkButton)
    }
}