package com.stratis.estaffing.Adapter.MasterAdminFlow

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.MasterAdminFlow.Leader
import com.stratis.estaffing.R
import com.stratis.estaffing.View.MasterAdminFlow.MALeaderView
import java.util.*

/**
 * Created by Erick Sanchez
 * Revision 1 - 24/05/21
 */
open class MALeadersAdapter(items: ArrayList<Leader>?) :
        RecyclerView.Adapter<MALeaderView>() {

    private var items: ArrayList<Leader>? = null
    var listener: MALeadersAdapterListener? = null
    private var context: Context? = null

    init {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MALeaderView {
        context = parent.context
        val layoutView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.fragment_m_a_leaders_item, parent, false)
        return MALeaderView(layoutView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MALeaderView, position: Int) {

        holder.setIsRecyclable(false)
        val leader :Leader = this.items!![position]

        // --
        holder.nameIcon?.text = leader.getNameShort()
        holder.name?.text = App.getShorName( "${leader.firstName} ${leader.lastName}" )
        holder.area?.text = "${context!!.getText(R.string.fragment_m_a_request_single_candidate_area)} ${leader.area}"

        // -- Click
        holder.shape?.setOnClickListener {

            if (this.listener != null) {
                this.listener?.onDetail(leader)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    open fun reload(items: ArrayList<Leader>?) {

        this.items = items
        notifyItemRangeInserted(0, this.items!!.size - 1)
    }

    open fun clear() {

        val size = items!!.size
        if (size > 0) {
            for (i in 0 until size) {
                items!!.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    interface MALeadersAdapterListener {
        fun onDetail(leader:Leader)
    }
}