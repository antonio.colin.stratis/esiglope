package com.stratis.estaffing.Model

import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

open class CatalogItem:Serializable {

    var catalogId: Long = 0
    var catalogItemId: Long = 0
    var valueItem: String = ""
    var description: String = ""

    constructor(obj: JSONObject?) {
        if (obj != null) {
            this.catalogId = obj.optLong("catalogId")
            this.catalogItemId = obj.optLong("catalogItemId")
            this.valueItem = obj.optString("valueItem")
            this.description = obj.optString("description")
        }
    }

    companion object {

        fun parse(array: JSONArray?): ArrayList<CatalogItem>? {

            if (array != null) {
                val catalogItems = ArrayList<CatalogItem>()
                for (i in 0 until array.length()) {
                    val obj = array.optJSONObject(i)
                    catalogItems.add(CatalogItem(obj))
                }
                return catalogItems
            }
            return null
        }
    }

    override fun toString(): String {
        return "CatalogItem(catalogItemId=$catalogItemId, valueItem='$valueItem', description='$description, catalogId=$catalogId')"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CatalogItem) return false

        if (catalogItemId != other.catalogItemId) return false

        return true
    }

    override fun hashCode(): Int {
        return catalogItemId.hashCode()
    }


}