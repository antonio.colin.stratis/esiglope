package com.stratis.estaffing.Controller.RegisterFlow;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Controller.LoginFlow.RegisterThanksController;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Login.LoginService;

import org.json.JSONObject;

public class RegisterController extends AppCompatActivity implements View.OnClickListener {

    private TextInputEditText user;
    private TextInputEditText lastName;
    private TextInputEditText email;
    private TextInputEditText phone;
    private TextInputEditText company;
    private LoadingButton registerButton;

    private TextInputLayout passwordLayout;
    private TextInputEditText passwordEditText;
    private TextInputLayout rePasswordLayout;
    private TextInputEditText rePasswordEditText;

    private ImageView character_rule_check;
    private ImageView caps_rule_check;
    private ImageView numbers_rule_check;
    private ImageView password_rule_check;

    private boolean charactersRule = false;
    private boolean capsRule = false;
    private boolean numbersRule = false;
    private boolean passwordsRule = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_register_controller);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName()
                .concat(String.valueOf(getIntent().getBooleanExtra("isPersonal", false))));
        this.initViews();
    }

    @SuppressLint("WrongViewCast")
    private void initViews() {

        // -- Fields
        this.user = this.findViewById(R.id.userText);
        this.lastName = this.findViewById(R.id.lastNameText);
        this.email = this.findViewById(R.id.emailText);
        this.phone = this.findViewById(R.id.phoneText);
        this.company = this.findViewById(R.id.companyText);

        // -- Hide by type of user
        if (getIntent().hasExtra("isPersonal")) {

            final boolean isPersonal = getIntent().getBooleanExtra("isPersonal", false);
            this.findViewById(R.id.company).setVisibility( (isPersonal) ? View.GONE : View.VISIBLE );
            this.findViewById(R.id.companyTitle).setVisibility( (isPersonal) ? View.GONE : View.VISIBLE );

            // -- Title
            if (isPersonal) {
                ((TextView) this.findViewById(R.id.subtitle)).setText(this.getResources().getString(R.string.register_subtitle_personal));
            }
        }

        // --
        this.passwordLayout = this.findViewById(R.id.password);
        this.passwordEditText = this.findViewById(R.id.passwordText);

        this.rePasswordLayout = this.findViewById(R.id.rePassword);
        this.rePasswordEditText = this.findViewById(R.id.rePasswordText);

        // --
        this.passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {

                // --
                if (s.length() == 0) {

                    turnOffRules();
                    return;
                }

                // -- Characters rule
                validateCharactersRule(String.valueOf(s));
                // -- Numbers rule
                validateNumbersRule(String.valueOf(s));
                // -- Numbers rule
                validateCapsRule(String.valueOf(s));
                // -- Validate password
                final String rs = String.valueOf(rePasswordEditText.getText());
                if (!rs.equals("")) {
                    validatePasswordsRule(String.valueOf(s), rs);
                }
            }
        });

        this.rePasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {

                // -- Validate password
                final String os = String.valueOf(passwordEditText.getText());
                if (!os.equals("")) {
                    validatePasswordsRule(String.valueOf(s), os);
                }
            }
        });

        // --
        final RelativeLayout characters_rule = this.findViewById(R.id.characters_rule);
        this.character_rule_check = characters_rule.findViewById(R.id.state);
        final TextView characters_rule_text = characters_rule.findViewById(R.id.text);
        characters_rule_text.setText( this.getResources().getString(R.string.reset_ruler_characters_rule) );

        final RelativeLayout caps_rule = this.findViewById(R.id.caps_rule);
        this.caps_rule_check = caps_rule.findViewById(R.id.state);
        final TextView caps_rule_text = caps_rule.findViewById(R.id.text);
        caps_rule_text.setText( this.getResources().getString(R.string.reset_ruler_caps_rule) );

        final RelativeLayout numbers_rule = this.findViewById(R.id.numbers_rule);
        this.numbers_rule_check = numbers_rule.findViewById(R.id.state);
        final TextView numbers_rule_text = numbers_rule.findViewById(R.id.text);
        numbers_rule_text.setText( this.getResources().getString(R.string.reset_ruler_numbers_rule) );

        final RelativeLayout password_rule = this.findViewById(R.id.password_rule);
        this.password_rule_check = password_rule.findViewById(R.id.state);
        final TextView password_rule_text = password_rule.findViewById(R.id.text);
        password_rule_text.setText( this.getResources().getString(R.string.reset_ruler_password_rule) );

        // --
        this.registerButton = new LoadingButton((RelativeLayout) this.findViewById(R.id.registerButton));
        this.registerButton.setText(this.getResources().getString(R.string.register_form_register_button));
        this.registerButton.getButton().setTag(10);
        this.registerButton.getButton().setOnClickListener(this);

        // --
        final Button cancelButton = this.findViewById(R.id.cancelButton);
        cancelButton.setTag(11);
        cancelButton.setOnClickListener(this);
    }

    private void turnOffRules() {

        // --
        this.character_rule_check.setImageBitmap(this.getImage("rule_off"));
        this.caps_rule_check.setImageBitmap(this.getImage("rule_off"));
        this.numbers_rule_check.setImageBitmap(this.getImage("rule_off"));
        this.password_rule_check.setImageBitmap(this.getImage("rule_off"));
    }

    private void register() {

        final boolean isPersonal = getIntent().getBooleanExtra("isPersonal", false);
        this.registerButton.showLoading();

        // -- Fields
        this.showAllErrors(false);

        // User -- Name
        final String userString = String.valueOf(this.user.getText());
        if (userString.equals("")) {

            this.registerButton.hideLoading();
            this.showUserError(true);
            return;
        }

        // User -- LastName
        final String lastNameString = String.valueOf(this.lastName.getText());
        if (lastNameString.equals("")) {

            this.registerButton.hideLoading();
            this.showLastNameError(true);
            return;
        }

        // Email
        final String emailString = String.valueOf(this.email.getText());
        if (emailString.equals("") || !App.isEmailValid(emailString)) {

            this.registerButton.hideLoading();
            this.showEmailError(true);
            return;
        }

        // Phone
        String phoneString = String.valueOf(this.phone.getText());
        if (phoneString.equals("") || phoneString.length() < 10) {

            this.registerButton.hideLoading();
            this.showPhoneError(true);
            return;
        }
        phoneString = "+52" + phoneString;

        // -- Company
        final String companyString = String.valueOf(this.company.getText());

        // -- Passwords
        final String passwordEditTextString = String.valueOf(this.passwordEditText.getText());
        String rePasswordEditTextString = String.valueOf(this.rePasswordEditText.getText());
        if (!this.charactersRule || !this.capsRule || !this.numbersRule || !this.passwordsRule) {

            this.showPasswordsError(true);
            this.registerButton.hideLoading();
            return;
        }

        // -- Type
        final String typeUser = (!isPersonal) ? "1000800001" : "1000800004";

        // -- Service
        LoginService.register(userString, lastNameString, emailString, phoneString, companyString,
                passwordEditTextString, rePasswordEditTextString, typeUser,
                new ReSTCallback() {
                    @Override
                    public void onSuccess(ReSTResponse response) {

                        registerButton.hideLoading();
                        JSONObject res = JsonBuilder.stringToJson(response.body);
                        int code = res.optInt("code", 500);
                        if (code == 201) {

                            // --

                            Bundle parameters = new Bundle();
                            final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(RegisterController.this);
                            if (getIntent().hasExtra("isPersonal")) {
                                mFirebaseAnalytics.logEvent("Aspirantes_Registrados", parameters);
                            } else {
                                mFirebaseAnalytics.logEvent("Empresas_Registradas", parameters);
                            }

                            // --
                            Intent intent = new Intent(RegisterController.this, RegisterCodeValidationController.class);
                            intent.putExtra("user", emailString);
                            intent.putExtra("password", passwordEditTextString);
                            if (getIntent().hasExtra("isPersonal")) {
                                final boolean isPersonal = getIntent().getBooleanExtra("isPersonal", false);
                                intent.putExtra("isPersonal", isPersonal);
                            }
                            startActivity(intent);
                            finish();

                        } else {

                            showUserError(true);
                            showEmailError(true);
                        }
                    }

                    @Override
                    public void onError(ReSTResponse response) {

                        registerButton.hideLoading();
                        //response.body = response.body.replaceAll("\\\\", "");
                        //response.body = response.body.replaceAll("[\\\\]{1}[\"]{1}","\"");
                        //response.body = response.body.substring(response.body.indexOf("{"),  response.body.lastIndexOf("}")+1);

                        JSONObject res = JsonBuilder.stringToJson(response.body);
                        if (res != null) {
                            Log.d("DXGOP", res.toString());
                            final int errorCode = res.optInt("code", 0);
                            switch(errorCode) {

                                case 150:
                                    showEmailError(true);
                                    ((TextView)findViewById(R.id.emailTitleError)).setText(getString(R.string.register_form_user_email_used_error));
                                    FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName()
                                            .concat(String.valueOf(getIntent().getBooleanExtra("isPersonal", false)))
                                            .concat("error"));
                                    break;
                            }
                        }
                    }
                });
    }

    private void showAllErrors(boolean state) {

        this.showUserError(state);
        this.showLastNameError(state);
        this.showEmailError(state);
        this.showPhoneError(state);
        this.showPasswordsError(state);
    }

    private void showUserError(boolean state) {

        final TextView title = this.findViewById(R.id.userTitle);
        final TextInputLayout layout = this.findViewById(R.id.user);
        final TextView titleError = this.findViewById(R.id.userTitleError);
        this.showError(title, layout, titleError, state);
    }

    private void showLastNameError(boolean state) {

        final TextView title = this.findViewById(R.id.lastNameTitle);
        final TextInputLayout layout = this.findViewById(R.id.lastName);
        final TextView titleError = this.findViewById(R.id.lastNameTitleError);
        this.showError(title, layout, titleError, state);
    }

    private void showEmailError(boolean state) {

        final TextView title = this.findViewById(R.id.emailTitle);
        final TextInputLayout layout = this.findViewById(R.id.email);
        final TextView titleError = this.findViewById(R.id.emailTitleError);
        this.showError(title, layout, titleError, state);
    }

    private void showPhoneError(boolean state) {

        final TextView title = this.findViewById(R.id.phoneTitle);
        final TextInputLayout layout = this.findViewById(R.id.phone);
        final TextView titleError = this.findViewById(R.id.phoneTitleError);
        this.showError(title, layout, titleError, state);
    }

    private void showPasswordsError(boolean state) {

        final TextView title = this.findViewById(R.id.passwordTitle);
        final TextInputLayout layout = this.findViewById(R.id.password);
        final TextView titleError = this.findViewById(R.id.passwordsError);
        this.showError(title, layout, titleError, state);

        final TextView retitle = this.findViewById(R.id.rePasswordTitle);
        final TextInputLayout relayout = this.findViewById(R.id.rePassword);
        this.showError(retitle, relayout, titleError, state);
    }

    private void showError(final TextView title, final TextInputLayout layout, final TextView titleError, final boolean state) {

        if (state) {
            title.setTextColor(this.getResources().getColor(R.color.red));
            layout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_error_border));
            titleError.setVisibility(View.VISIBLE);
        } else {
            title.setTextColor(Color.parseColor("#333333"));
            layout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_border));
            titleError.setVisibility(View.GONE);
        }
    }

    private void validateCharactersRule(String string) {

        if (string.length() >= 8) {

            this.charactersRule = true;
            this.character_rule_check.setImageBitmap(this.getImage("rule_on"));

        } else {

            this.charactersRule = false;
            this.character_rule_check.setImageBitmap(this.getImage("rule_error"));
        }
    }

    private void validateNumbersRule(String string) {

        if (string.matches(".*\\d.*")) {

            this.numbersRule = true;
            this.numbers_rule_check.setImageBitmap(this.getImage("rule_on"));

        } else {

            this.numbersRule = false;
            this.numbers_rule_check.setImageBitmap(this.getImage("rule_error"));
        }
    }

    private void validateCapsRule(String string) {

        if (string.matches("^(?=.*[a-z])(?=.*[A-Z]).+$")) {

            this.capsRule = true;
            this.caps_rule_check.setImageBitmap(this.getImage("rule_on"));

        } else {

            this.capsRule = false;
            this.caps_rule_check.setImageBitmap(this.getImage("rule_error"));
        }
    }

    private void validatePasswordsRule(String pw1, String pw2) {

        if ( (!pw1.equals("") && !pw2.equals("")) && (pw1.equals(pw2)) ) {

            this.passwordsRule = true;
            this.password_rule_check.setImageBitmap(this.getImage("rule_on"));

        } else {

            this.passwordsRule = false;
            this.password_rule_check.setImageBitmap(this.getImage("rule_error"));
        }
    }

    private Bitmap getImage(String name) {

        int id = this.getResources().getIdentifier(name, "drawable", this.getPackageName());
        return BitmapFactory.decodeStream(this.getResources().openRawResource(id));
    }

    @Override
    public void onClick(View v) {

        final Integer tag = (Integer) v.getTag();
        switch (tag) {

            case 10:
                register();
                break;

            case 11:
                finish();
                break;
        }
    }
}