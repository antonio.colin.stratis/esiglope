package com.stratis.estaffing.Modal.NotificationCenterFlow.Model;

import android.content.Context;
import android.graphics.Bitmap;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.NotificationCenterFlow.NotificationCenterModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 09/12/20
 */

public class NotificationCenterFilterModel {

    private String id = "";
    private String name = "";
    private String icon = "";

    public static ArrayList<NotificationCenterFilterModel> getFilter() {

        ArrayList<NotificationCenterFilterModel> filters = new ArrayList<>();

        //  -- Talent
        /*final NotificationCenterFilterModel allFilter = new NotificationCenterFilterModel();
        allFilter.setId(NotificationCenterModel.notificationCategoryTalent);
        allFilter.setName("Talento");
        allFilter.setIcon("icon_filter_talent");
        filters.add(allFilter);*/

        //  -- Vacancies
        final NotificationCenterFilterModel activeFilter = new NotificationCenterFilterModel();
        activeFilter.setId(NotificationCenterModel.notificationCategoryVacancies);
        activeFilter.setName("Vacante");
        activeFilter.setIcon("icon_filter_vacancies");
        filters.add(activeFilter);

        //  -- Interviews
        final NotificationCenterFilterModel pausedFilter = new NotificationCenterFilterModel();
        pausedFilter.setId(NotificationCenterModel.notificationCategoryInterviews);
        pausedFilter.setName("Entrevistas");
        pausedFilter.setIcon("icon_filter_interviews");
        filters.add(pausedFilter);

        //  -- Candidates
        final NotificationCenterFilterModel cancelFilter = new NotificationCenterFilterModel();
        cancelFilter.setId(NotificationCenterModel.notificationCategoryCandidate);
        cancelFilter.setName("Candidato");
        cancelFilter.setIcon("icon_filter_candidate");
        filters.add(cancelFilter);

        //  -- All
        final NotificationCenterFilterModel doneFilter = new NotificationCenterFilterModel();
        doneFilter.setId("999");
        doneFilter.setName("No leídas");
        doneFilter.setIcon("icon_filter_not_seen");
        filters.add(doneFilter);

        // --
        return filters;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return this.icon;
    }

    public Bitmap getIcon(Context context) {
        return App.getImage(context, this.icon);
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}