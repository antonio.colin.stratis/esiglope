package com.stratis.estaffing.Control.Searcher.Filter;

import android.widget.Filter;

import com.stratis.estaffing.Control.Searcher.Adapter.SearcherAdapter;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class GlobalFilter extends Filter {

    protected SearcherAdapter adapter = null;

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        return new FilterResults();
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        // Now we have to inform the adapter about the new list filtered
        this.adapter.reload(results.values);
        this.adapter.notifyDataSetChanged();
    }
}
