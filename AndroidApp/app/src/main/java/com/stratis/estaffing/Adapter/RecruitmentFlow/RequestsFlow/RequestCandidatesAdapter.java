package com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.RequestCandidateView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class RequestCandidatesAdapter extends RecyclerView.Adapter<RequestCandidateView> {

    protected ArrayList<RequestCandidate> items;
    protected ArrayList<RequestCandidate> original;
    protected Context context;
    protected RequestCandidatesClickListener listener;
    protected boolean filtered = false;
    protected boolean notInterested = false;

    public RequestCandidatesAdapter(ArrayList<RequestCandidate> items, Context context) {

        this.context = context;
        this.items = items;
        this.original = items;
    }

    @Override
    public RequestCandidateView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recruitment_request_candidates_item,
                parent, false);
        return new RequestCandidateView(layoutView);
    }

    @Override
    public void onBindViewHolder(final RequestCandidateView holder, final int position) {

        holder.setIsRecyclable(false);
        final RequestCandidate candidate = this.items.get(position);

        // -- Shape
        if (this.notInterested) {
            holder.getShape().setAlpha(0.4f);
        }

        // -- Profile image
        holder.getImage().setClipToOutline(true);
        if (!candidate.getImgProfile().equals("")) {
            Glide
                .with(context)
                .load(candidate.getImgProfile())
                .centerCrop()
                .thumbnail(.35f)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                // .placeholder(R.drawable.loading_spinner)
                .into( holder.getImage() );
        }

        // -- Fields
        holder.getName().setText( candidate.getFullNameShort() );
        holder.getSpeciality().setText( candidate.getPositionDesc() );

        final String s = candidate.getJobSeniority();
        if (s.equals("") || s.equals("null") || s.equals("Null") || s.equals("NULL")) {
            holder.getSeniority().setVisibility(View.GONE);
        } else {
            holder.getSeniority().setText( App.capitalize(candidate.getJobSeniority()) );
        }
        App.setViewAsPrimaryColor(holder.getSeniority(), context);

        holder.getFavorite().setVisibility( (candidate.getFavorite()) ? View.VISIBLE : View.GONE );

        // -- Rating
        final String rating = candidate.getRating();
        if (!rating.equals("") && !rating.equals("null") && !rating.equals("NULL")
                && !rating.equals("0") && !rating.equals("0.0")) {

            double ratingInt = Double.parseDouble(rating);
            ratingInt = Math.round(ratingInt);

            holder.getFavorite().setVisibility(View.VISIBLE);
            holder.getRating().setVisibility(View.VISIBLE);
            holder.getRating().setText( String.format("%.0f", ratingInt) );
            App.setViewAsPrimaryColor(holder.getRating(), context);
        }

        // -- Button
        holder.getShape().setOnClickListener(v -> {
            if (listener != null) {
                listener.onDetail(candidate);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<RequestCandidate> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<RequestCandidate> getItems() {
        return items;
    }

    public boolean isEmpty() {
        return this.items.size() == 0;
    }

    public void filterByFavorite() {

        this.items = new ArrayList<>();
        for (RequestCandidate item : this.original) {
            if (item.getFavorite()) {
                this.items.add(item);
            }
        }
        this.filtered = true;
        this.notifyDataSetChanged();
    }

    public void removeFavoriteFilter() {

        if (this.filtered) {

            this.items = this.original;
            this.filtered = false;
            this.notifyDataSetChanged();
        }
    }

    public void setListener(RequestCandidatesClickListener listener) {
        this.listener = listener;
    }

    public void setNotInterested(boolean notInterested) {
        this.notInterested = notInterested;
    }

    public interface RequestCandidatesClickListener {
        void onDetail(RequestCandidate item);
    }
}