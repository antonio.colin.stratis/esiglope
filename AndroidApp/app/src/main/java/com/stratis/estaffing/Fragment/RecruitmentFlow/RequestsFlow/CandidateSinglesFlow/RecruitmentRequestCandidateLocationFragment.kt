package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.stratis.estaffing.Adapter.RecruitmentFlow.LocationPlaceAdapter
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Control.Loader
import com.stratis.estaffing.Control.SwipeDelete.SwipeDeleteLocationCallback
import com.stratis.estaffing.Control.SwipeDelete.SwipeEditLocationCallback
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.LocationAddressMaxModal
import com.stratis.estaffing.Model.RecruitmentFlow.LocationCountry
import com.stratis.estaffing.Model.RecruitmentFlow.LocationPlace
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.Recruitment.NewRequestFlow.LocationService
import android.widget.ArrayAdapter
import com.stratis.estaffing.Control.eStaffingFragment
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@Suppress("DEPRECATION")
class RecruitmentRequestCandidateLocationFragment : eStaffingFragment() {

    private var locationFragmentListener : LocationFragmentListener? = null

    private var loader : Loader? = null
    private var btnAnyUbication: Button? = null
    private var btnAnuUbicationBool: Boolean? = false

    private var remoteSubtitle: TextView? = null
    private var placeSubtitle: TextView? = null

    private var locationPlaceAdapter : LocationPlaceAdapter? = null
    //private var locationPlaces : MutableList<LocationPlace> = ArrayList()
    private var placeSearchBoxText : TextView? = null
    private val API_KEY = "AIzaSyAltHyvorOeAYSMnk2Fp_VP7UQzGpp0U94"
    private val AUTOCOMPLETE_REQUEST_CODE = 1
    private var locationIdEdit = 0
//    private val LOCATIONS_COUNTRIES : HashMap<String, MutableList<String>> = HashMap()

    private var spinnerContainer1: RelativeLayout? = null
    private var spinnerContainer2: RelativeLayout? = null
    private var spinnerContainer3: RelativeLayout? = null
    private var spinner1: Spinner? = null
    private var spinner2: Spinner? = null
    private var spinner3: Spinner? = null
    private var contry1 : CharSequence = ""
    private var contry2 : CharSequence = ""
    private var contry3 : CharSequence = ""
    private val CONTRY_SELECTED_FORMAT = "%s%s%s";
    private var contry1Str = ""
    private var contry2Str = ""
    private var contry3Str = ""
    private var spinnerCountryAdapter1 : ArrayAdapter<CharSequence>? = null
    private var spinnerCountryAdapter2 : ArrayAdapter<CharSequence>? = null
    private var spinnerCountryAdapter3 : ArrayAdapter<CharSequence>? = null
    private var listCountries: ArrayList<LocationCountry>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_request_vacancy_location, container, false)

        loader = Loader(mContext)
        loader!!.show()
        initView()
        loader!!.cancel()

        return this.mView
    }

    private fun initView() {
        this.remoteSubtitle = mView.findViewById(R.id.sub_title)
        if(this.profile?.modalityId.equals("1000100002")) {
            initLocationRemote()
        } else {
            initLocationPlace()
        }

    }

    private fun visibilityRemoteSubtitle() {
        if(locationPlaceAdapter != null)
            locationPlaceAdapter!!.notifyDataSetChanged()
        //if(locationPlaces.isEmpty()) {
        if(profile.locationPlaces.isEmpty()) {
            remoteSubtitle!!.visibility = View.GONE
            placeSubtitle!!.text = getString(R.string.request_vacancy_modality_location_searher_label_empty)
        } else {
            remoteSubtitle!!.visibility = View.VISIBLE
            placeSubtitle!!.text = getString(R.string.request_vacancy_modality_location_searher_label)
        }
    }

    private fun initLocationPlace() {
        this.placeSubtitle = mView.findViewById(R.id.placeSubtitle)
        visibilityRemoteSubtitle()

        // Get Addresses
        if(profile.locationPlaces.isEmpty())
            getAddresses()
        else
            setupPlaces()
    }

    private fun setupPlaces() {
        /*
         * Countries support list https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
         */
//        LOCATIONS_COUNTRIES.put("en_US" , mutableListOf<String>("US"))
//        LOCATIONS_COUNTRIES.put("es_ES" , mutableListOf<String>("MX","CL"))
//        LOCATIONS_COUNTRIES.put("es_MX" , mutableListOf<String>("MX","CL"))

        var locationPlaceList = mView.findViewById<RecyclerView>(R.id.locationPlaceList)
        App.createVerticalRecyclerList(locationPlaceList, mContext)
        locationPlaceAdapter = LocationPlaceAdapter(mContext, profile.locationPlaces)
        locationPlaceAdapter!!.setListener(object: LocationPlaceAdapter.LocationPlaceAdapterListener{
            override fun onClick(position: Int) {
                profile.locationPlaces[position].modifiedDate = getCurrentDate()
                profile!!.jobLocation = profile.locationPlaces[position]
//                profile!!.location = locationPlaces[position].name
//                profile!!.address = locationPlaces[position].description

            }

        })

        val swipeEditHandler = object : SwipeEditLocationCallback(mContext) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val locationPlace = profile.locationPlaces.get(viewHolder.adapterPosition)
                locationIdEdit = locationPlace.id
                // profile.locationPlaces.remove(locationPlace)
                initilizePlaces()
                intentPlaceAutocomplete()
                visibilityRemoteSubtitle()
            }

            override fun getSwipeDirs(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
                if(!profile.locationPlaces.get(viewHolder.adapterPosition).createdByUser) return 0
                return super.getSwipeDirs(recyclerView, viewHolder)
            }
        }
        val itemTouchEditHelper = ItemTouchHelper(swipeEditHandler)
        itemTouchEditHelper.attachToRecyclerView(locationPlaceList)

        val swipeDeleteHandler = object : SwipeDeleteLocationCallback(mContext) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val locationPlace = profile.locationPlaces.get(viewHolder.adapterPosition)
                profile.locationPlaces.remove(locationPlace)
                if (locationPlace.id != 0)
                    profile.deletedAddresses.add(locationPlace.id)
                visibilityRemoteSubtitle()
            }

            override fun getSwipeDirs(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
                if(!profile.locationPlaces.get(viewHolder.adapterPosition).createdByUser) return 0
                return super.getSwipeDirs(recyclerView, viewHolder)
            }
        }
        val itemTouchDeleteHelper = ItemTouchHelper(swipeDeleteHandler)
        itemTouchDeleteHelper.attachToRecyclerView(locationPlaceList)

        locationPlaceList.adapter = locationPlaceAdapter

        placeSearchBoxText = mView.findViewById(R.id.placeSearchBoxText)

        initilizePlaces()
        placeSearchBoxText!!.setOnClickListener( object : View.OnClickListener {
            override fun onClick(view: View?) {
                if(profile.locationPlaces.filter{ it.createdByUser }.size == 3) {
                    val modal = LocationAddressMaxModal(context)
                    modal.show()
                } else
                    intentPlaceAutocomplete()
            }

        })
    }

    private fun initilizePlaces() {
        if(!Places.isInitialized()) {
            Places.initialize(mContext, API_KEY)
        }
    }

    private fun intentPlaceAutocomplete(){
        val fields = listOf(/*Place.Field.ID, Place.Field.NAME, */Place.Field.ADDRESS, Place.Field.ADDRESS_COMPONENTS)
        /*.setCountries(LOCATIONS_COUNTRIES.get(Session.get(App.getAppContext()).localIdentifier)!!)*/
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .build(mContext)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    private fun initLocationRemote() {
        mView.findViewById<ConstraintLayout>(R.id.dynamicFormLocationRemote).visibility = View.VISIBLE
        mView.findViewById<ConstraintLayout>(R.id.dynamicFormLocation).visibility = View.GONE
        mView.findViewById<TextView>(R.id.title).text = getString(R.string.request_vacancy_location_title_remote)
        this.remoteSubtitle!!.text = getString(R.string.request_vacancy_location_subtitle_remote)

        btnAnyUbication = mView.findViewById(R.id.btnAnyUbication)
        btnAnyUbication!!.setOnClickListener( object : View.OnClickListener {
            override fun onClick(view: View?) {
                changeAssignButtonState()
            }

        })

        getCountries()

    }

    private fun initSpinner1() {
        spinner1!!.adapter = spinnerCountryAdapter1
        selectSpinnerValue(spinner1!!, contry1Str, spinnerCountryAdapter1!!)
        spinner1!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                actionSpinner(parent, position, "spinner1", spinner1!!, spinnerContainer1!!)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
    }

    private fun initSpinner2() {
        spinner2!!.adapter = spinnerCountryAdapter2
        selectSpinnerValue(spinner2!!, contry2Str, spinnerCountryAdapter2!!)
        spinner2!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                actionSpinner(parent, position, "spinner2", spinner2!!, spinnerContainer2!!)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
    }

    private fun initSpinner3() {
        spinner3!!.adapter = spinnerCountryAdapter3
        selectSpinnerValue(spinner3!!, contry3Str, spinnerCountryAdapter3!!)
        spinner3!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                actionSpinner(parent, position, "spinner3", spinner3!!, spinnerContainer3!!)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
    }

    private fun selectSpinnerValue(spinner : Spinner, value: String, spinnerCountryAdapter : ArrayAdapter<CharSequence>) {
        if(!App.isEmpty(value)) {
            for (position in 0 until spinnerCountryAdapter.count) {
                if (spinnerCountryAdapter.getItem(position)?.equals(value)!!) {
                    spinner.setSelection(position)
                    return
                }
            }
        }
        spinner.setSelection(0)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun changeAssignButtonState() {
        this.btnAnuUbicationBool = !this.btnAnuUbicationBool!!
        if (this.btnAnuUbicationBool!!) {
            cleanSpinners("spinner1", spinner1!!, null)
            btnAnyUbication!!.setBackground(ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button))
            setViewAsSecondaryColor(btnAnyUbication!!)
            btnAnyUbication!!.setTextColor(Color.WHITE)
            btnAnyUbication!!.setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.checkbox_white), null)
            profile!!.remoteJobCountries = btnAnyUbication!!.text.toString()
            if(App.isEmpty(profile!!.remoteJobCountries)) profile!!.remoteJobCountries = getStr(R.string.request_vacancy_location_remote_any)
            profile!!.jobLocation = null
            spinner1?.setSelection(0)
            spinner2?.setSelection(0)
            spinner3?.setSelection(0)
        } else {
            cleanBtnAnyUbitacion()
        }
    }

    private fun cleanBtnAnyUbitacion() {
        btnAnyUbication!!.setBackground(ContextCompat.getDrawable(mContext, R.drawable.white_border_10))
        btnAnyUbication!!.setTextColor(Color.parseColor("#001330"))
        btnAnyUbication!!.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.check_figure), null)
//        setValues()
    }

    private fun actionSpinner(parent: AdapterView<*>?, position: Int,
                              spinnerName: String, spinner: Spinner, spinnerContainer: RelativeLayout) {
        if(parent!!.childCount > 0) {
            val text = parent.getChildAt(0) as TextView
            if(position != 0) {
                spinner.setBackground(ContextCompat.getDrawable(mContext, R.drawable.custom_spinner_icon))
                setViewAsSecondaryColor(spinner)
                text.setTextColor(ContextCompat.getColor(mContext, R.color.white)) // TODO Esto no esta funcionando
                spinnerContainer.visibility = View.VISIBLE
                when(spinnerName) {
                    "spinner1" -> {
                        contry1 = text.text
                        contry1Str = text.text as String
                        spinnerContainer2!!.visibility = View.VISIBLE
                    }
                    "spinner2" -> {
                        contry2 = "," + text.text
                        contry2Str = text.text as String
                        spinnerContainer3!!.visibility = View.VISIBLE
                    }
                    "spinner3" -> {
                        contry3 = "," + text.text
                        contry3Str = text.text as String
                    }
                }
                profile!!.remoteJobCountries = String.format(CONTRY_SELECTED_FORMAT, contry1, contry2, contry3)
                cleanBtnAnyUbitacion()
            } else {
                cleanSpinners(spinnerName, spinner, text)
            }
            setValues(true)
            when(spinnerName) {
                "spinner1" -> { selectSpinnerValue(spinner1!!, contry1Str, spinnerCountryAdapter1!!) }
                "spinner2" -> { selectSpinnerValue(spinner2!!, contry2Str, spinnerCountryAdapter2!!) }
                "spinner3" -> { selectSpinnerValue(spinner3!!, contry3Str, spinnerCountryAdapter3!!) }
            }
        } else {
            Log.d("DXGOP", "Parent of spinner $spinnerName not have childers")
        }

    }

    private fun cleanSpinners(spinnerName: String, spinner: Spinner, text: TextView?) {
        spinner!!.setBackground(ContextCompat.getDrawable(mContext, R.drawable.custom_spinner_icon))
        if(text != null)  text.setTextColor(ContextCompat.getColor(mContext, R.color.black))
        when(spinnerName) {
            "spinner1" -> {
                spinnerContainer2!!.visibility = View.GONE
                spinnerContainer3!!.visibility = View.GONE
                contry1 = ""
                contry2 = ""
                contry3 = ""
                contry1Str = ""
                contry2Str = ""
                contry3Str = ""
            }
            "spinner2" -> {
                spinnerContainer3!!.visibility = View.GONE
                contry2 = ""
                contry3 = ""
                contry2Str = ""
                contry3Str = ""
            }
            "spinner3" -> {
                contry3 = ""
                contry3Str = ""
            }
        }
        profile!!.remoteJobCountries = String.format(CONTRY_SELECTED_FORMAT, contry1, contry2, contry3)
        setValues(true)
        selectSpinnerValue(spinner1!!, contry1Str, spinnerCountryAdapter1!!)
        selectSpinnerValue(spinner2!!, contry2Str, spinnerCountryAdapter2!!)
        selectSpinnerValue(spinner3!!, contry3Str, spinnerCountryAdapter3!!)
    }

    private fun getAddresses() {
        // -- Updating
        val session = Session.get(mContext)

        LocationService.getFindAddressById(session, object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {
                val res = JsonBuilder.stringToJson(response.body)
                val clientAddresses: ArrayList<LocationPlace>? = LocationPlace.parse(res.optJSONArray("clientAddresses"))
                clientAddresses?.sortByDescending { it.modifiedDate }
                val userAddresses: ArrayList<LocationPlace>? = LocationPlace.parse(res.optJSONArray("userAddresses"))
                userAddresses?.sortByDescending { it.modifiedDate }

                var counter = 0
                if (clientAddresses != null) {
                    for (address in clientAddresses) {
                        if( !profile.deletedAddresses.contains(address.id) ) {
                            if( counter == 3) break
                            address.createdByUser = false
                            profile.locationPlaces.add(address)
                            counter = counter.inc()
                        }
                    }
                }
                counter = 0
                if (userAddresses != null) {
                    for (address in userAddresses) {
                        if( !profile.deletedAddresses.contains(address.id) ) {
                            if( counter == 3 ) break
                            address.createdByUser = true
                            profile.locationPlaces.add(address)
                            counter = counter.inc()
                        }
                    }
                }
//                profile.locationPlaces.sortByDescending { it.modifiedDate }
                if( !profile.locationPlaces.isEmpty() && profile.locationPlaces.size >= 6)
                    profile.locationPlaces = getSubList(profile.locationPlaces, 0, 5) as MutableList<LocationPlace>

                setupPlaces()
            }

            override fun onError(response: ReSTResponse) {
                Log.d("DXGOP", "ERROR :: " + response.body)
                loader!!.cancel()
            }
        })
    }

    private fun getCountries() {
        // -- Updating
        val session = Session.get(mContext)

        LocationService.getCountries(object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {
                val res = JsonBuilder.stringToJsonArray(response.body)

                listCountries = LocationCountry.parse(res)
                spinnerContainer1 = mView.findViewById(R.id.cmbCountry1)
                spinner1 = spinnerContainer1!!.findViewById(R.id.spinner)
                spinnerContainer2 = mView.findViewById(R.id.cmbCountry2)
                spinner2 = spinnerContainer2!!.findViewById(R.id.spinner)
                spinnerContainer3 = mView.findViewById(R.id.cmbCountry3)
                spinner3 = spinnerContainer3!!.findViewById(R.id.spinner)

                setValues(false)

                initSpinner1()
                initSpinner2()
                initSpinner3()
            }

            override fun onError(response: ReSTResponse) {
                Log.d("DXGOP", "ERROR :: " + response.body)
                loader!!.cancel()
            }
        })
    }

    private fun setValues(update : Boolean) {
        val countriesString = if (profile!!.remoteJobCountries != null) profile!!.remoteJobCountries else ""
        val countries = countriesString.split(",")

        if(countriesString.equals(getString(R.string.request_vacancy_location_remote_any))){
            this.btnAnuUbicationBool = true
            btnAnyUbication!!.setBackground(ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button))
            setViewAsSecondaryColor(btnAnyUbication!!)
            btnAnyUbication!!.setTextColor(Color.WHITE)
            btnAnyUbication!!.setCompoundDrawablesWithIntrinsicBounds(null, null, resources.getDrawable(R.drawable.checkbox_white), null)
        } else if(this.btnAnuUbicationBool!! && App.isEmpty(profile!!.remoteJobCountries)) {
            profile!!.remoteJobCountries = getString(R.string.request_vacancy_location_remote_any)
        } else if(!(listCountries?.map {it.description})?.contains(profile!!.remoteJobCountries)!!) {
            // Skip total
        } else {
            when(countries.size) {
                1 -> { contry1Str = countries[0] }
                2 -> {
                    contry1Str = countries[0]
                    contry2Str = countries[1]
                }
                3 -> {
                    contry1Str = countries[0]
                    contry2Str = countries[1]
                    contry3Str = countries[2]
                }
            }
        }

        val filterCountries = listCountries?.filter { !countries.contains(it.description) }
        val arrayCountries: List<String>? = filterCountries?.map { it.description }

        if(!update) {
            spinnerCountryAdapter1 = ArrayAdapter(mContext, android.R.layout.simple_spinner_item)
            spinnerCountryAdapter1!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            spinnerCountryAdapter2 = ArrayAdapter(mContext, android.R.layout.simple_spinner_item)
            spinnerCountryAdapter2!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            spinnerCountryAdapter3 = ArrayAdapter(mContext, android.R.layout.simple_spinner_item)
            spinnerCountryAdapter3!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        } else {
            spinnerCountryAdapter1!!.clear()
            spinnerCountryAdapter2!!.clear()
            spinnerCountryAdapter3!!.clear()
        }
        spinnerCountryAdapter1!!.add(getString(R.string.request_vacancy_location_country_default))
        if( !App.isEmpty(contry1Str) ) { spinnerCountryAdapter1!!.add(contry1Str) }
        if (arrayCountries != null) { spinnerCountryAdapter1!!.addAll(arrayCountries) }
        spinnerCountryAdapter1!!.notifyDataSetChanged()

        spinnerCountryAdapter2!!.add(getString(R.string.request_vacancy_location_country_default))
        if( !App.isEmpty(contry2Str) ) { spinnerCountryAdapter2!!.add(contry2Str) }
        if (arrayCountries != null) { spinnerCountryAdapter2!!.addAll(arrayCountries) }
        spinnerCountryAdapter2!!.notifyDataSetChanged()

        spinnerCountryAdapter3!!.add(getString(R.string.request_vacancy_location_country_default))
        if( !App.isEmpty(contry3Str) ) { spinnerCountryAdapter3!!.add(contry3Str) }
        if (arrayCountries != null) { spinnerCountryAdapter3!!.addAll(arrayCountries) }
        spinnerCountryAdapter3!!.notifyDataSetChanged()
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        locationFragmentListener!!.changeButtonStyle()
                        val fragmentLocationSave = RecruitmentRequestCandidateLocationSaveFragment()
//                        fragmentLocationSave.address = place.address

//                        locationPlace = place.addressComponents
//                        locationPlace.country = place.addressComponents
//                        fragmentLocationSave.locationPlace = place.addressComponents

                        fragmentLocationSave.locationPlace = LocationPlace(createLocationPlace(place))
                        //fragmentLocationSave.locationPlaces = locationPlaces
                        fragmentLocationSave.setProfilePassed(profile)
                        fragmentLocationSave.setListener( setLocationSaveListener() )
                        continueSegue(fragmentLocationSave)
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        status.statusMessage?.let { it1 -> Log.i("DXGOP", it1) }
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setLocationSaveListener() : RecruitmentRequestCandidateLocationSaveFragment.LocationSaveListener {
        return object : RecruitmentRequestCandidateLocationSaveFragment.LocationSaveListener {
            override fun save() {
                visibilityRemoteSubtitle()
            }
        }
    }

    private fun createLocationPlace(place : Place) : JSONObject {
        val obj = JSONObject()
        if( locationIdEdit != 0 ) {
            val location = profile.locationPlaces.find { it.id == locationIdEdit }
            profile.locationPlaces.remove(location)
            obj.put("id", locationIdEdit)
        }
        obj.put("description", place.address)
        val addressComponents = place.addressComponents!!.asList()
        addressComponents.forEach {
            if( it.types.contains("administrative_area_level_1") /*|| it.types.contains("political")*/ )
                obj.put("city", it.name)
            else if( it.types.contains("country") /*|| it.types.contains("political")*/ )
                obj.put("country", it.name)
            else if( it.types.contains("sublocality_level_1") /*|| it.types.contains("sublocality")*/ )
                obj.put("neighborhood", it.name)
            else if( it.types.contains("street_number") )
                obj.put("numberExt", it.name)
            else if( it.types.contains("locality") /*|| it.types.contains("political")*/ )
                obj.put("state", it.name)
            else if( it.types.contains("route") )
                obj.put("street", it.name)
            else if( it.types.contains("postal_code"))
                obj.put("zipCode", it.name)
        }
        obj.put("createdDate", getCurrentDate())
        return obj
    }

    private fun getCurrentDate() : String {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return sdf.format(Date())
    }

    private fun <T> getSubList(list: List<T>, start: Int, end: Int): List<T>? {
        val subList: MutableList<T> = ArrayList()
        for (i in start..end) {
            subList.add(list[i])
        }
        return subList
    }

    fun setLocationFragmentListener(locationFragmentListener : LocationFragmentListener) {
        this.locationFragmentListener = locationFragmentListener
    }

    fun interface LocationFragmentListener {
        fun changeButtonStyle()
    }

}