package com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 22/12/20
 */

public class CandidateWorkExperienceModel {

    private String id = "";
    private String talentId = "";
    private String position = "";
    private String years = "";
    private String duration = "";
    private String employer = "";
    private String startDate = "";
    private String endDate = "";
    private String description = "";

    public CandidateWorkExperienceModel() {}

    public CandidateWorkExperienceModel(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optString("id");
            this.talentId = obj.optString("talentId", "");
            this.position = obj.optString("position", "");
            this.years = obj.optString("years", "");
            this.duration = obj.optString("duration", "");
            this.employer = obj.optString("employer", "");
            this.startDate = obj.optString("startDate", "");
            this.endDate = obj.optString("endDate", "");
            this.description = obj.optString("description", "");

            // -- Cleaning
            /*if (this.school.equals("null")) {
                this.school = "N/A";
            }

            if (this.validityDate.equals("null")) {
                this.validityDate = "N/A";
            }

            if (this.certificateNumber.equals("null")) {
                this.certificateNumber = "N/A";
            }*/
        }
    }

    public static ArrayList<CandidateWorkExperienceModel> parse(JSONArray array) {

        if (array != null) {

            ArrayList<CandidateWorkExperienceModel> experiences = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                experiences.add(new CandidateWorkExperienceModel(obj));
            }
            return experiences;
        }
        return null;
    }

    // --
    public String getId() {
        return id;
    }

    public String getTalentId() {
        return talentId;
    }

    public String getPosition() {
        return position;
    }

    public String getYears() {
        return years;
    }

    public String getDuration() { return duration; }

    public String getEmployer() {
        return employer;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getDescription() {
        return description;
    }
}