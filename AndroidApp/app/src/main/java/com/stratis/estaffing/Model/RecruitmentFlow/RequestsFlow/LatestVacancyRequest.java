package com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 17/09/20
 */

public class LatestVacancyRequest {

    public final static String statusSent = "1000200001";
    public final static String statusConfirm = "1000200002";
    public final static String statusAttention = "1000200003";
    public final static String statusSearch = "1000200004";
    public final static String statusCandidates = "1000200005";
    public final static String statusInterviews = "1000200006";
    public final static String statusHire = "1000200007";
    public final static String statusDone = "1000200008";
    public final static String statusHireStop = "1000200009";
    public final static String statusCancel = "1000200010";
    public final static String statusPaused = "1000200011";

    private String id = "";
    private String name = "";
    private String date = "";
    private String status = "";
    private String statusId = "";
    private String jobSeniority = "";
    private String hiringTime = "";
    private String folioClient = "";

    public LatestVacancyRequest() {}

    public LatestVacancyRequest(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optString("id", "");
            this.name = obj.optString("name", "");
            this.date =  obj.optString("creationDate", "");
            this.statusId =  obj.optString("statusId", "");
            this.status =  obj.optString("statusValue", "");
            this.jobSeniority =  obj.optString("jobSeniority", "");
            this.hiringTime =  obj.optString("hiringTime", "");
            this.folioClient =  obj.optString("folioClient", "");

            if (folioClient.equals("") || folioClient.equals("null")) {
                this.folioClient = "0";
            }
        }
    }

    public static ArrayList<LatestVacancyRequest> parse(JSONArray array) {

        if (array != null) {

            ArrayList<LatestVacancyRequest> requests = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                requests.add(new LatestVacancyRequest(obj));
            }
            return requests;
        }
        return null;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getStatusId() {
        return statusId;
    }

    public String getStatus() {
        return status;
    }

    public String getJobSeniority() {
        return jobSeniority;
    }

    public String getJobSeniorityShort() {

        String shorted = jobSeniority;
        switch (jobSeniority) {

            case "Senior":
                shorted = "SR";
                break;

            case "Middle":
                shorted = "MID";
                break;

            case "Junior":
                shorted = "JR";
                break;
        }
        return shorted;
    }

    public String getHiringTime() {
        return hiringTime;
    }

    public String getFolioClient() {
        return folioClient;
    }
}
