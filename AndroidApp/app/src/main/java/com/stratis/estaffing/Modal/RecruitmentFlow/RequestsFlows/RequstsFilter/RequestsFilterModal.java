package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.Adapter.RequestFilterStatusAdapter;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.Model.RequestsFilterStatusModel;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 09/12/20
 */

public class RequestsFilterModal extends BottomSheetDialog {

    private String currentFilterId = "";
    private String currentDateFilterId = "";
    private RequestFilterStatusAdapter.RequestFilterStatusAdapterListener listener;

    public RequestsFilterModal(Context context) {

        super(context);
        this.commonInit();
    }

    public RequestsFilterModal(Context context, int themeResId) {

        super(context, themeResId);
        this.commonInit();
    }

    protected RequestsFilterModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.commonInit();
    }

    private void commonInit() {

        // -- R styles
        this.setContentView(R.layout.fragment_recruitment_requests_modal);
        this.setCanceledOnTouchOutside(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void show() {

        super.show();

        // --
        this.handleActions();
    }

    @SuppressLint("ResourceType")
    private void handleActions() {

        final DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.recycler_devider_20));

        // -- Status Filter
        final RecyclerView stateFilters = this.findViewById(R.id.stateFilters);
        App.createRecyclerHorizontalList(stateFilters, getContext());
        stateFilters.addItemDecoration(itemDecorator);
        final RequestFilterStatusAdapter requestFilterStatusAdapter = new RequestFilterStatusAdapter(
                RequestsFilterStatusModel.getStatusFilter(getContext()),
                getContext(),
                false
        );
        stateFilters.setAdapter(requestFilterStatusAdapter);
        requestFilterStatusAdapter.setListener(this.listener);
        requestFilterStatusAdapter.setParent(this);
        requestFilterStatusAdapter.setCurrentFilter(this.currentFilterId);

        // -- Date Filter
        final RecyclerView dateFilters = this.findViewById(R.id.dateFilters);
        App.createRecyclerHorizontalList(dateFilters, getContext());
        dateFilters.addItemDecoration(itemDecorator);
        final RequestFilterStatusAdapter requestFilterDateAdapter = new RequestFilterStatusAdapter(
                RequestsFilterStatusModel.getDatesFilter(getContext()),
                getContext(),
                true
        );
        dateFilters.setAdapter(requestFilterDateAdapter);
        requestFilterDateAdapter.setListener(this.listener);
        requestFilterDateAdapter.setParent(this);
        requestFilterDateAdapter.setCurrentFilter(this.currentDateFilterId);
    }

    public void setCurrentFilterId(String currentFilterId) {
        this.currentFilterId = currentFilterId;
    }

    public void setCurrentDateFilterId(String currentDateFilterId) {
        this.currentDateFilterId = currentDateFilterId;
    }

    public void setListener(RequestFilterStatusAdapter.RequestFilterStatusAdapterListener listener) {
        this.listener = listener;
    }
}