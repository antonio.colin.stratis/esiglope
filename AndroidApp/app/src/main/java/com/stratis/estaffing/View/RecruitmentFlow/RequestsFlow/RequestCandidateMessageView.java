package com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class RequestCandidateMessageView extends RecyclerView.ViewHolder {

    private RelativeLayout shape;
    private TextView message = null;

    public RequestCandidateMessageView(View itemView) {

        // --
        super(itemView);

        // --
        //this.shape = itemView.findViewById(R.id.shape);
        this.message = itemView.findViewById(R.id.message);
    }

    public RelativeLayout getShape() {
        return shape;
    }

    public TextView getMessage() {
        return message;
    }
}