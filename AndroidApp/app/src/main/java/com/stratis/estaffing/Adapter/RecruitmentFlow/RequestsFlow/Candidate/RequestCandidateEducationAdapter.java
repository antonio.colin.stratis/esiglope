package com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandiateEducationModel;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate.CandidateEducationView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class RequestCandidateEducationAdapter extends RecyclerView.Adapter<CandidateEducationView> {

    public enum RequestCandidateEducationAdapterType { normal, course }

    protected ArrayList<CandiateEducationModel> items;
    protected Context context;
    protected RequestCandidateEducationAdapterListener listener;
    protected RequestCandidateEducationAdapterType type = RequestCandidateEducationAdapterType.normal;

    private Session session;
    private String primaryColor = "";
    private String secondaryColor = "";

    public RequestCandidateEducationAdapter(ArrayList<CandiateEducationModel> items, Context context) {

        this(items, context, RequestCandidateEducationAdapterType.normal);
        this.session = Session.get(context);
        this.primaryColor = this.session.getPreferences().getPrimaryColor();
        this.secondaryColor = this.session.getPreferences().getSecondaryColor();
    }

    public RequestCandidateEducationAdapter(ArrayList<CandiateEducationModel> items, Context context, RequestCandidateEducationAdapterType type) {

        this.items = items;
        this.context = context;
        this.type = type;
        this.session = Session.get(context);
        this.primaryColor = this.session.getPreferences().getPrimaryColor();
        this.secondaryColor = this.session.getPreferences().getSecondaryColor();
    }

    @Override
    public CandidateEducationView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recruitment_request_candidate_education_item,
                parent, false);
        return new CandidateEducationView(layoutView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final CandidateEducationView holder, final int position) {

        holder.setIsRecyclable(false);
        final CandiateEducationModel education = this.items.get(position);

        // --
        //final String color = (this.type == RequestCandidateEducationAdapterType.normal) ? "#ff7c23" : "#001330";
        final String color = App.fixWrongColorATuDePiglek(this.type == RequestCandidateEducationAdapterType.normal ? secondaryColor : primaryColor);
        holder.getInitialsView().setImageDrawable( education.getInitials(Color.parseColor(color), context) );
        holder.getName().setText( education.getCourseName() );

        // --
        holder.getShape().setOnClickListener(v -> {
            if (this.listener != null) {
                listener.onDetail(education);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<CandiateEducationModel> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<CandiateEducationModel> getItems() {
        return items;
    }

    public void setListener(RequestCandidateEducationAdapterListener listener) {
        this.listener = listener;
    }

    public interface RequestCandidateEducationAdapterListener {
        void onDetail(CandiateEducationModel item);
    }
}