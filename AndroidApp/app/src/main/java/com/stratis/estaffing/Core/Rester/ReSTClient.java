package com.stratis.estaffing.Core.Rester;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

public class ReSTClient {

    protected String mServiceUrl;
    private boolean debugMode = false;
    private int successCode = 200;
    protected ReSTWorker mWorker;

    protected final String TAG = "DXGOP";
    protected boolean customWorker = false;

    public ReSTClient() {
        this.mServiceUrl = "";
    }

    public ReSTClient(String serviceUrl) {
        this.mServiceUrl = serviceUrl;
    }

    protected class ReSTWorker extends AsyncTask<Void, Void, Integer> {

        protected ReSTRequest mRequest;
        protected ReSTResponse mResponse;
        protected ReSTCallback mCallback;

        public ReSTWorker(ReSTRequest request, ReSTCallback callback) {

            mRequest = request;
            mCallback = callback;
            mResponse = new ReSTResponse();
        }

        @Override
        protected Integer doInBackground(Void... voids) {

            HttpURLConnection connection = null;
            int ret = 0;
            try {

                String parameters = mRequest.buildQuery(ReSTRequest.REST_REQUEST_QUERY_PARAMETERS);
                //String fields = mRequest.buildQuery(ReSTRequest.REST_REQUEST_QUERY_FIELDS);
                String endpoint = mServiceUrl + mRequest.mEndpoint + (parameters.length() > 0 ? "?" + parameters : "");
                URL url = new URL(endpoint);

                if (debugMode) { Log.d(TAG, "RESTER ::: [ URL: " + endpoint + " ]"); }
                if ( url.getProtocol().compareTo("https") == 0 ) {

                    sslException();
                    connection = (HttpsURLConnection) url.openConnection();

                } else {
                    connection = (HttpURLConnection) url.openConnection();
                }

                connection.setReadTimeout(100000);
                connection.setConnectTimeout(150000);
                connection.setRequestMethod(mRequest.mMethod);
                connection.setDoInput(true);

                connection.setRequestProperty("Cache-Control", "no-cache");
                connection.setDefaultUseCaches(false);
                connection.setUseCaches(false);

                /** Adding headers **/
                for (Map.Entry<String, String> entry : mRequest.mHeaders.entrySet()) {
                    connection.setRequestProperty(entry.getKey(), entry.getValue());
                }

                /** Setting the parameters **/
                if ( mRequest.mMethod.compareTo("POST") == 0 || mRequest.mMethod.compareTo("PUT") == 0 ) {
                    connection.setDoOutput(true);
                    OutputStream os = connection.getOutputStream();
                    DataOutputStream writer = new DataOutputStream(os);
                    if (mRequest.bodyJson != null) {
                        byte[] jsonBytes = mRequest.bodyJson.getBytes(StandardCharsets.UTF_8);
                        writer.write(jsonBytes);
                    }
                    mRequest.buildPost(writer, ReSTRequest.REST_REQUEST_QUERY_FIELDS);
                    writer.close();
                }
                connection.connect();

                mResponse.statusCode = connection.getResponseCode();

                // -- Encoding
                // TODO - RestClient: Set encoding as configurition of instance
                String encoding = connection.getContentType();
                encoding = encoding.substring(encoding.lastIndexOf("=") + 1);

                // --
                BufferedReader br = (mResponse.statusCode == HttpsURLConnection.HTTP_OK || mResponse.statusCode == successCode) ?
                        new BufferedReader(new InputStreamReader(connection.getInputStream(), encoding))  :
                        new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                String line;
                while ((line = br.readLine()) != null) { mResponse.body += line; }
                if (mResponse.body.length() > 0) {
                    mResponse.contentType = connection.getContentType();
                    mResponse.contentLength = connection.getContentLength();
                    if( mResponse.contentType.compareTo("application/json") == 0 ) {
                        mResponse.json = new JSONObject(mResponse.body);
                    }

                    // -- Response headers
                    mResponse.setHeaders(connection.getHeaderFields());
                }
                ret = 1;

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
            return ret;
        }

        protected void onPostExecute(Integer result) {

            if (customWorker) {
                checkStatusCode(result);
            } else {
                this.onValidate(result);
            }
        }

        public ReSTRequest getRequest() {
            return this.mRequest;
        }

        public ReSTResponse getResponse() {
            return this.mResponse;
        }

        public ReSTCallback getCallback() {
            return mCallback;
        }

        public void onValidate(Integer result) {

            if (mResponse.statusCode == successCode && result == 1) {
                if (debugMode) { Log.d(TAG, "RESTER ::: [ SUCCESS: " + mResponse.statusCode + " :: " + mResponse.body + " ]"); }
                mCallback.onSuccess(mResponse);
            } else {
                if (debugMode) { Log.d(TAG, "RESTER ::: [ ERROR: " + mResponse.statusCode + " :: " + mResponse.body + " ]"); }
                mCallback.onError(mResponse);
            }
        }
    }

    private void sslException() {

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void execute(ReSTRequest request, ReSTCallback callback) {

        this.mWorker = new ReSTWorker(request, callback);
        this.mWorker.execute();
    }

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }

    public void setSuccessCode(int successCode) {
        this.successCode = successCode;
    }

    public void checkStatusCode(Integer result) {}
}