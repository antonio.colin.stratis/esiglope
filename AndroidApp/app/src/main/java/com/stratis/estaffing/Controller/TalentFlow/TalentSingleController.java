package com.stratis.estaffing.Controller.TalentFlow;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.tabs.TabLayout;
import com.stratis.estaffing.Adapter.HomePageAdapter;
import com.stratis.estaffing.Control.ABActivity;
import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Loader;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Fragment.TalentFlow.TalentCoursesFragment;
import com.stratis.estaffing.Fragment.TalentFlow.TalentFeedbackFragment;
import com.stratis.estaffing.Fragment.TalentFlow.TalentInfoFragment;
import com.stratis.estaffing.Modal.SkillsModal;
import com.stratis.estaffing.Model.Skill;
import com.stratis.estaffing.Model.Talent;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Talent.TalentService;

import org.json.JSONObject;

import java.util.ArrayList;

public class TalentSingleController extends ABActivity {

    private Loader loader = null;
    private String talentId = "";

    private Talent talent = null;
    private ViewPager tabContainer =  null;
    private HomePageAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_talent_single_controller);

        // -- Set header
        this.setHeader(this.getResources().getString(R.string.talent_single_header_title), true);

        // -- Check previous talentID
        if (getIntent().hasExtra("talentId")) {

            // -- Show loader
            this.loader = new Loader(this);
            this.loader.show();

            // --
            this.talentId = getIntent().getStringExtra("talentId");
            this.getTalent();

        } else {
            finish();
        }
    }

    // --
    private void getTalent() {

        TalentService.get(this.talentId, new ReSTCallback() {

            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Log.d("DXGOP", "SINGLE TALENT RESPONSE = " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);

                // -- Hide loader
                loader.cancel();

                // --
                checkResponse(res);
            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                Log.d("DXGOP", "ERROR :: " + response.body);
                // --
                loader.cancel();
            }
        });
    }

    // -- Here we gonna init tabs
    private void checkResponse(final JSONObject res) {

        // --
        if (res != null) {

            // --
            this.talent = new Talent(res);

            // --
            this.paintUserInfo();
        }
    }

    private void paintUserInfo() {

        // -- Image Profile
        final ImageView imageProfile = this.findViewById(R.id.imageProfile);
        imageProfile.setClipToOutline(true);
        if (!this.talent.getUserImg().equals("")) {
            Glide.with(this)
                .load(this.talent.getUserImg())
                .centerCrop()
                .thumbnail(.50f)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                // .placeholder(R.drawable.loading_spinner)
                .into( imageProfile );
        }

        // -- Seniority
        final TextView seniorityView = this.findViewById(R.id.seniorityView);
        if (talent.getJobSeniority().equals("") || talent.getJobSeniority().equals("null") ||
                talent.getJobSeniority().equals("Null") || talent.getJobSeniority().equals("NULL")) {
            seniorityView.setVisibility(View.GONE);
        } else {
            seniorityView.setText( this.talent.getJobSeniority().toUpperCase() );
        }
        App.setViewAsPrimaryColor(seniorityView,this);

        // --  Name View
        final TextView nameView = this.findViewById(R.id.nameeView);
        nameView.setText( this.talent.getUserFullName());

        // -- Position View
        final AutoResizeTextView positionView = this.findViewById(R.id.positionView);
        positionView.setText( this.talent.getPosition() );

        // -- Skills
        final TextView skillOne = this.findViewById(R.id.skillOne);
        final TextView skillTwo = this.findViewById(R.id.skillTwo);
        final ArrayList<Skill> skills = this.talent.getSkills();
        if (skills != null) {

            // -- Skill One
            if (skills.size() > 0) {
                skillOne.setText( skills.get(0).getName() );
            } else {
                skillOne.setVisibility(View.GONE);
            }

            // -- Skill Two
            if (skills.size() > 1) {
                skillTwo.setText( skills.get(1).getName() );
            } else {
                skillTwo.setVisibility(View.GONE);
            }
        }
        App.setViewAsSecondaryColor(skillOne,this);
        App.setViewAsSecondaryColor(skillTwo,this);

        // -- All Skills button
        final ImageView showMoreSkills =  this.findViewById(R.id.showMoreSkills);
        showMoreSkills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final SkillsModal modal = new SkillsModal(TalentSingleController.this);
                modal.setSkills(skills);
                modal.show();
            }
        });

        // -- Init Tabs
        this.initTabs();
    }

    private void initTabs() {

        // -- Tabs Logic
        // --- Creating Containers and adapter
        this. tabContainer = this.findViewById(R.id.tabContainer);
        this.adapter = new HomePageAdapter(getSupportFragmentManager());

        // --- Init Fragments
        final TalentInfoFragment infoFragment = new TalentInfoFragment();
        infoFragment.talent = this.talent;

        final TalentCoursesFragment coursesFragment = new TalentCoursesFragment();
        coursesFragment.talent = this.talent;

        final TalentFeedbackFragment feedbackFragment = new TalentFeedbackFragment();
        feedbackFragment.talent = this.talent;
        feedbackFragment.scrollView = this.findViewById(R.id.singleControllerScroll);

        // --- Adding to adapter
        adapter.addFragment(infoFragment, "Información");
        adapter.addFragment(coursesFragment, "Cursos");
        adapter.addFragment(feedbackFragment, "Feedback");

        // --- Adding adapter to ViewPage
        tabContainer.setAdapter(adapter);

        // --- Tabs interaction
        final TabLayout tabs = this.findViewById(R.id.tabs);
        tabs.setupWithViewPager(tabContainer);
    }
}