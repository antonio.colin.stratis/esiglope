package com.stratis.estaffing.Fragment.TalentFlowV3

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.Course
import com.stratis.estaffing.R

class TalentCourseCertificateFragment : ABFragment() {

    open var course : Course? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_talent_single_courses_certificate, container, false)
        setHeader("")
        this.initViews()

        return this.mView
    }

    private fun initViews() {
        mView.findViewById<TextView>(R.id.title).text = this.course!!.name

        var vSchoolValue = mView.findViewById<TextView>(R.id.schoolContainerContent);
        var scholeValue = this.course!!.school
        if(App.isEmpty(scholeValue)) {
            mView.findViewById<RelativeLayout>(R.id.schoolContainer).visibility = View.GONE
        } else
            vSchoolValue.text = this.course!!.school

        var vValidateDate = mView.findViewById<TextView>(R.id.dateContainerContent)
        var validateDate = this.course!!.validityDate
        Log.d("DXGOP", "validateDate: " + validateDate)
        if(App.isEmpty(validateDate)) {
            mView.findViewById<RelativeLayout>(R.id.dateContainer).visibility = View.GONE
        } else
            vValidateDate.text = validateDate

        var vCertificateNumber = mView.findViewById<TextView>(R.id.certificateNumberContent)
        var certificateNumber = this.course!!.certificateNumber
        if(App.isEmpty(certificateNumber)) {
            mView.findViewById<RelativeLayout>(R.id.certificateNumberContainer).visibility = View.GONE
        } else
            vCertificateNumber.text = certificateNumber

        var vCertificateDescription = mView.findViewById<TextView>(R.id.educationDescription)
        var description = this.course!!.talentCourseDescription
        if(App.isEmpty(description))
            vCertificateDescription.visibility = View.GONE
        else
            vCertificateDescription.text = description


        var image = mView.findViewById<ImageView>(R.id.educationImage)
        if (course!!.hasCertificate && !App.isEmpty(course!!.certificateFileUrl)) {
            image.setImageBitmap(null)
            image.setVisibility(View.VISIBLE)
            Glide.with(mContext!!)
                    .load(course!!.certificateFileUrl)
                    .centerCrop()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(image)
        } else {
            mView.findViewById<RelativeLayout>(R.id.imageContainer).visibility = View.GONE
        }
    }
}