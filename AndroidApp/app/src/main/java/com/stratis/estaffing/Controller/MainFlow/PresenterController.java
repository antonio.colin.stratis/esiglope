package com.stratis.estaffing.Controller.MainFlow;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.eStaffingFragment;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestCheckEditFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyBudgetFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyDetailFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyLanguageFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyModalityFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestSingleFragment;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.R;

public class PresenterController extends AppCompatActivity {

    public enum PresenterControllerType { vacancy }

    private Profile profile = null;
    private PresenterControllerType type = null;
    private ABFragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_presenter_controller);

        // --
        if (getIntent().hasExtra("type") && getIntent().hasExtra("profile")) {

            this.profile = (Profile) getIntent().getSerializableExtra("profile");
            this.type = (PresenterControllerType) getIntent().getSerializableExtra("type");
            switch (this.type) {

                case vacancy:
                    fragment = new RecruitmentRequestSingleFragment(profile);
                    break;
            }
            this.loadFragment(fragment);
        }
    }

    private boolean loadFragment(Fragment fragment) {

        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

}