package com.stratis.estaffing.Controller.RecruitmentFlow;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;

import com.stratis.estaffing.Control.eStaffingFragment;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestCheckEditFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyBudgetFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyDetailFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyLanguageFragment;
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyModalityFragment;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.R;

public class RecruitmentFlowEditorController extends AppCompatActivity {

    RequestCheckEditFragment.RequestCheckEditType type = null;
    public Profile profile = null;
    public Integer position = 0;
    private eStaffingFragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_recruitment_flow_editor_controller);
        this.initViews();

        // --
        if (getIntent().hasExtra("type") && getIntent().hasExtra("profile")) {

            boolean canEditProfileName = true;
            if (getIntent().hasExtra("canEditProfileName")) {
                canEditProfileName = getIntent().getBooleanExtra("canEditProfileName", true);
            }
            this.profile = (Profile) getIntent().getSerializableExtra("profile");
            this.type = (RequestCheckEditFragment.RequestCheckEditType) getIntent().getSerializableExtra("type");
            Log.d("TAG", this.type.toString());
            switch (this.type) {

                case vacancies:
                    RequestVacancyFragment requestVacancyFragment = new RequestVacancyFragment();
                    requestVacancyFragment.setModeEdit(true);
                    fragment = requestVacancyFragment;
                    break;

                case profile:
                    fragment = new RequestVacancyDetailFragment(canEditProfileName, position == 1);
                    break;

                case language:
                    fragment = new RequestVacancyLanguageFragment();
                    break;

                case modality:
                    fragment = new RequestVacancyModalityFragment();
                    break;

                case budget:
                    fragment = new RequestVacancyBudgetFragment();
                    break;
            }
            fragment.setProfilePassed(this.profile);
            this.loadFragment(fragment);
        }

    }

    private void initViews() {
        final Button footerSaveButton = this.findViewById(R.id.footerSaveButton);
        footerSaveButton.setOnClickListener(v -> {

            if (this.fragment != null) {
                this.fragment.onFooterContinueButtonClick();
            }

            Intent returnIntent = new Intent();
            returnIntent.putExtra("profile", profile);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });

        final ImageButton app_screen_return_arrow = this.findViewById(R.id.app_screen_return_arrow);
        app_screen_return_arrow.setOnClickListener(v -> finish());
    }

    private void loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
        }
    }
}
