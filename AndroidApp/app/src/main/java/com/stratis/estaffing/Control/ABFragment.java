package com.stratis.estaffing.Control;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 2020-03-10
 */
public class ABFragment extends Fragment {

    protected View mView = null;
    protected Context mContext = null;

    private Loader loader;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        // --
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {

        this.mContext = context;
        super.onAttach(context);

        Log.d("DXGOP", "ATTACHING FRAGMENT ::: WITH CONTEXT: " + this.mContext);
        // -- Register event in Firebase
        if (this.mContext != null) {
            Log.d("DXGOP", "CREATING EVENT");
            FirebaseEvent.logEvent(this.mContext, this.getClass().getSimpleName());
        }
    }

    @Override
    public void onDetach() {

        this.mContext = null;
        super.onDetach();
    }

    protected <T extends View> T findView(@IdRes int id) {
        return (T) this.mView.findViewById(id);
    }

    protected void performSegue(Fragment fragment) {
        this.performSegue(fragment, R.id.fragment_container);
    }

    protected void performSegue(Fragment fragment, String tag) {
        this.performSegue(fragment, R.id.fragment_container, tag);
    }

    protected void performSegueUp(Fragment fragment) {
        this.performSegueUp(fragment, R.id.fragment_container);
    }

    protected void continueSegue(Fragment fragment) {
        this.continueSegue(fragment, R.id.fragment_container);
    }

    protected void continueSegueName(Fragment fragment, String name) {
        this.continueSegueName(fragment, R.id.fragment_container, name);
    }

    protected void continueSegueUp(Fragment fragment) {
        this.continueSegueUp(fragment, R.id.fragment_container);
    }

    protected void reload(Fragment fragment, String tag) {
        this.reload(fragment, R.id.fragment_container, tag);
    }

    protected void performSegue(Fragment fragment, int container) {

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    protected void performSegue(Fragment fragment, int container, String tag) {

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    protected void reload(Fragment fragment, int container, String tag) {

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(container, fragment, tag);
        //transaction.addToBackStack(tag);
        transaction.detach(this);
        transaction.commit();
    }

    protected void performSegueUp(Fragment fragment, int container) {

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_up, R.anim.no_anim, R.anim.no_anim, R.anim.slide_down);
        transaction.replace(container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    protected void continueSegue(Fragment fragment, int container) {

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.add(container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    protected void continueSegueName(Fragment fragment, int container, String name) {

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.add(container, fragment);
        transaction.addToBackStack(name);
        transaction.commit();
    }

    protected void continueSegueUp(Fragment fragment, int container) {

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_up, R.anim.no_anim, R.anim.no_anim, R.anim.slide_down);
        transaction.add(container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    protected void presentController(Intent intent) {
        getActivity().startActivity(intent);
    }

    protected void presentController(Class clazz) {
        getActivity().startActivity(new Intent(getActivity(), clazz));
    }

    protected void back() {

        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        }
    }

    protected void backStack(String name) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    protected void backRemove(Fragment fragment) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove(fragment);
        trans.commit();
        manager.popBackStack();
    }

    protected void showLoader() {

        if (this.loader == null) {
            this.loader = new Loader(getContext());
        }
        this.loader.show();
    }

    protected void hideLoader() {
        if (this.loader != null && this.loader.isShowing()) {
            this.loader.cancel();
        }
    }

    // -- Helpers
    protected String getStr(int resource) {
        return this.mContext.getResources().getString(resource);
    }

    public App getApp() {
        return ((App) getActivity().getApplication());
    }

    @Override
    public void onDestroyView() {

        // --
        super.onDestroyView();

        // -- Destroy the view
        mView = null;
    }

    protected void setHeader(String title) {
        this.setHeader(title, false);
    }

    protected void setHeader(String title, Boolean custom) {

        // -- Shape and status bar
        final Session session = Session.get(mContext);
        final String primaryColor = App.fixWrongColorATuDePiglek(session.getPreferences().getPrimaryColor());

        if (!primaryColor.equals("") && custom) {
            findView(R.id.header).setBackgroundColor(Color.parseColor(primaryColor));
            this.setStatusBarColor(Color.parseColor(primaryColor));
        }

        final ImageButton app_screen_return_arrow = this.mView.findViewById(R.id.app_screen_return_arrow);
        app_screen_return_arrow.setOnClickListener(v -> getActivity().onBackPressed());

        // --
        final TextView titleHeader = this.mView.findViewById(R.id.app_screen_title_header);
        titleHeader.setText(title);
    }

    protected void setStatusBarColor(int color) {
        getActivity().getWindow().setStatusBarColor(color);
    }

    protected void setStatusBarTextLight() {
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }

    protected void setViewAsPrimaryColor(View view) {

        final Session session = Session.get(mContext);
        final String primaryColor = App.fixWrongColorATuDePiglek(session.getPreferences().getPrimaryColor());
        if (!primaryColor.equals("")) {
            this.setViewColor(view, Color.parseColor(primaryColor));
        }
    }

    protected void setViewAsSecondaryColor(View view) {

        final Session session = Session.get(mContext);
        final String secondaryColor = App.fixWrongColorATuDePiglek(session.getPreferences().getSecondaryColor());
        if (!secondaryColor.equals("")) {
            this.setViewColor(view, Color.parseColor(secondaryColor));
        }
    }

    protected void setViewColor(View view, Integer color) {

        final Drawable drawable = view.getBackground();
        drawable.setTint(color);
    }
}
