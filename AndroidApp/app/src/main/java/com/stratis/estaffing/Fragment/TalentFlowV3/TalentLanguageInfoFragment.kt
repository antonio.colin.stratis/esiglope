package com.stratis.estaffing.Fragment.TalentFlowV3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.R

class TalentLanguageInfoFragment: ABFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mView = inflater.inflate(R.layout.fragment_talent_language_info, container, false)
        setHeader("")
        setup()
        return this.mView
    }

    private fun setup() {

        // -- Basic
        // --
        val basic_one: ConstraintLayout = mView.findViewById(R.id.basic_one)
        val basic_one_level = basic_one.findViewById<TextView>(R.id.textView4)
        basic_one_level.background.setTint(ContextCompat.getColor(mContext, R.color.modal_languages_basic_color))
        val basic_one_desc = basic_one.findViewById<TextView>(R.id.textView5)
        basic_one_level.text = getStr(R.string.modal_languages_basic_level_one)
        basic_one_desc.text = getStr(R.string.modal_languages_basic_desc_one)
        // --
        val basic_two: ConstraintLayout = mView.findViewById(R.id.basic_two)
        val basic_two_level = basic_two.findViewById<TextView>(R.id.textView4)
        basic_two_level.background.setTint(ContextCompat.getColor(mContext, R.color.modal_languages_basic_color))
        val basic_two_desc = basic_two.findViewById<TextView>(R.id.textView5)
        basic_two_level.text = getStr(R.string.modal_languages_basic_level_two)
        basic_two_desc.text = getStr(R.string.modal_languages_basic_desc_two)

        // -- Medium
        // --
        val medium_one: ConstraintLayout = mView.findViewById(R.id.medium_one)
        val medium_one_level = medium_one.findViewById<TextView>(R.id.textView4)
        medium_one_level.background.setTint(ContextCompat.getColor(mContext, R.color.modal_languages_medium_color))
        val medium_one_desc = medium_one.findViewById<TextView>(R.id.textView5)
        medium_one_level.text = getStr(R.string.modal_languages_medium_level_one)
        medium_one_desc.text = getStr(R.string.modal_languages_medium_desc_one)
        // --
        val medium_two: ConstraintLayout = mView.findViewById(R.id.medium_two)
        val medium_two_level = medium_two.findViewById<TextView>(R.id.textView4)
        medium_two_level.background.setTint(ContextCompat.getColor(mContext, R.color.modal_languages_medium_color))
        val medium_two_desc = medium_two.findViewById<TextView>(R.id.textView5)
        medium_two_level.text = getStr(R.string.modal_languages_medium_level_two)
        medium_two_desc.text = getStr(R.string.modal_languages_medium_desc_two)

        // -- Advance
        // --
        val advance_one: ConstraintLayout = mView.findViewById(R.id.advance_one)
        val advance_one_level = advance_one.findViewById<TextView>(R.id.textView4)
        advance_one_level.background.setTint(ContextCompat.getColor(mContext, R.color.modal_languages_advance_color))
        val advance_one_desc = advance_one.findViewById<TextView>(R.id.textView5)
        advance_one_level.text = getStr(R.string.modal_languages_advance_level_one)
        advance_one_desc.text = getStr(R.string.modal_languages_advance_desc_one)
        // --
        val advance_two: ConstraintLayout = mView.findViewById(R.id.advance_two)
        val advance_two_level = advance_two.findViewById<TextView>(R.id.textView4)
        advance_two_level.background.setTint(ContextCompat.getColor(mContext, R.color.modal_languages_advance_color))
        val advance_two_desc = advance_two.findViewById<TextView>(R.id.textView5)
        advance_two_level.text = getStr(R.string.modal_languages_advance_level_two)
        advance_two_desc.text = getStr(R.string.modal_languages_advance_desc_two)
    }
}