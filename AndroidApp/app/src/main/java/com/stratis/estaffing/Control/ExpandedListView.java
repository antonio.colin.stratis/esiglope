package com.stratis.estaffing.Control;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ListView;

/**
 * Created by Erick Sanchez
 * Revision 1 - 28/07/20
 */

public class ExpandedListView extends ListView {

    public ExpandedListView  (Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpandedListView  (Context context) {
        super(context);
    }

    public ExpandedListView  (Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        //Log.d("DXGOP", "EVENT ::: " + ev.getAction());
        if (ev.getAction() == MotionEvent.ACTION_MOVE)
            return true;

        return super.dispatchTouchEvent(ev);
    }
}