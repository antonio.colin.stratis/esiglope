package com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 29/07/20
 */

public class CandidateEducationView extends RecyclerView.ViewHolder {

    private RelativeLayout shape = null;
    private ImageView initialsView = null;
    private TextView name = null;

    public CandidateEducationView(View row) {

        // --
        super(row);

        // --
        this.shape = row.findViewById(R.id.shape);
        this.initialsView = row.findViewById(R.id.initialsView);
        this.name = row.findViewById(R.id.name);
    }

    public RelativeLayout getShape() {
        return shape;
    }

    public ImageView getInitialsView() {
        return initialsView;
    }

    public TextView getName() {
        return name;
    }
}