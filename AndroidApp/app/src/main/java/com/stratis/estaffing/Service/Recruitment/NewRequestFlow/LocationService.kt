package com.stratis.estaffing.Service.Recruitment.NewRequestFlow

import com.stratis.estaffing.Core.Api
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTRequest
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Core.eStaffingClient

object LocationService {

    fun getFindAddressById(session: Session, listener: ReSTCallback?) {

        // -- Single Talent logic
        val url = Api.getInstance().getUrl("/catalog/address/find")
        val rest = eStaffingClient(url)
        val request = ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "")

        // --
        request.addHeaders(Api.getSecureHeaders())

        // --
        request.addParameter("userId", session.dataSessionId)

        // --
        rest.setDebugMode(Api.getInstance().canLog())
        rest.execute(request, listener)
    }

    fun getCountries(listener: ReSTCallback?) {

        // -- Single Talent logic
        val url = Api.getInstance().getUrl("/catalog/findById")
        val rest = eStaffingClient(url)
        val request = ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "")

        // --
        request.addHeaders(Api.getSecureHeaders())

        // --
        request.addParameter("catalogId", "1000020034")

        // --
        rest.setDebugMode(Api.getInstance().canLog())
        rest.execute(request, listener)
    }

}