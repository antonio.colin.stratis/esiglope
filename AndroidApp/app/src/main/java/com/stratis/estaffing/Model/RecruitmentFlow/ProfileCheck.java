package com.stratis.estaffing.Model.RecruitmentFlow;

import android.content.Context;
import android.graphics.Bitmap;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 09/09/20
 */

public class ProfileCheck {

    private int id = 0;
    private String title = "";
    private String content = "";
    private Bitmap icon = null;
    private Boolean editable = false;

    public static ArrayList<ProfileCheck> getRequestSelectedItems(final Profile profile, Context context) {

        // -- Creating the parent obj
        ArrayList<ProfileCheck> checks =  new ArrayList<>();

        final ProfileCheck numberVacancies = new ProfileCheck();
        numberVacancies.setId(0);
        numberVacancies.setTitle(context.getString(R.string.request_vacancy_check_num_request));
        final int stringVacancies = profile.getCurrentVacancies() > 1
            ? R.string.request_vacancy_check_num_vacancies
            : R.string.request_vacancy_check_num_vacancy;
        numberVacancies.setContent(profile.getCurrentVacancies() + " " + context.getString(stringVacancies));
        numberVacancies.setIcon(App.getImage(context, "office_chair"));
        numberVacancies.setEditable(true);
        checks.add(numberVacancies);

        // -- Profile vacancy name
        final ProfileCheck vacancyName = new ProfileCheck();
        vacancyName.setId(1);
        vacancyName.setTitle(context.getString(R.string.request_vacancy_check_talent_profile));
        vacancyName.setContent(profile.getCurrentProfile().getName());
        vacancyName.setIcon(App.getImage(context, "astronaut"));
        vacancyName.setEditable(true);
        checks.add(vacancyName);

        // -- Seniority vacancy
        final ProfileCheck seniority = new ProfileCheck();
        seniority.setId(2);
        seniority.setTitle(context.getString(R.string.request_vacancy_check_time_experience));
        seniority.setContent(getExperienceTime(profile.getSeniorityId(), context));
        seniority.setIcon(App.getImage(context, "glases"));
        seniority.setEditable(true);
        checks.add(seniority);

        // -- Skills vacancy
        String noSkillsContent = context.getString(R.string.request_vacancy_check_talent_skills_no);
        final ProfileCheck skills = new ProfileCheck();
        skills.setId(3);
        skills.setTitle(context.getString(R.string.request_vacancy_check_talent_skills));
        if (profile.getCurrentSkills() != null) {
            String skillContent = ProfileCheck.getSkills( profile.getCurrentSkills(), true );
            skills.setContent(skillContent.isEmpty() ? noSkillsContent : skillContent);
        } else {
            skills.setContent(noSkillsContent);
        }
        skills.setIcon(App.getImage(context, "camping_knife"));
        skills.setEditable(true);
        checks.add(skills);

        final ProfileCheck skills2 = new ProfileCheck();
        skills2.setId(4);
        skills2.setTitle(context.getString(R.string.request_vacancy_check_talent_skills_desirable));
        if (profile.getCurrentSkills() != null) {
            String skillContent = ProfileCheck.getSkills( profile.getCurrentSkills(), false );
            skills2.setContent(skillContent.isEmpty() ? noSkillsContent : skillContent);
        } else {
            skills.setContent(noSkillsContent);
        }
        skills2.setIcon(App.getImage(context, "camping_knife"));
        skills2.setEditable(true);
        checks.add(skills2);

        // -- Responsibilities vacancy
        final ProfileCheck responsibilities = new ProfileCheck();
        responsibilities.setId(5);
        responsibilities.setTitle(context.getString(R.string.request_vacancy_check_job_description));
        responsibilities.setContent( profile.getResponsibilities() );
        responsibilities.setIcon(App.getImage(context, "responsabilidad_puesto"));
        responsibilities.setEditable(true);
        checks.add(responsibilities);

        // -- Languages vacancy
        final ProfileCheck languages = new ProfileCheck();
        languages.setId(6);
        languages.setTitle(context.getString(R.string.request_vacancy_check_req_language));
        if (profile.getCurrentLanguages() != null) {
            languages.setContent(ProfileCheck.getLanguages(profile.getCurrentLanguages(), context));
        }
        languages.setIcon(App.getImage(context, "chat"));
        languages.setEditable(true);
        checks.add(languages);

        // -- Modality of vacancy
        final ProfileCheck modalityOfVacancy = new ProfileCheck();
        modalityOfVacancy.setId(7);
        modalityOfVacancy.setTitle(context.getString(R.string.request_vacancy_check_work_modality));
        modalityOfVacancy.setContent( getModality(profile.getModalityId()) );
        modalityOfVacancy.setIcon(App.getImage(context, "wire_mouse_orange"));
        modalityOfVacancy.setEditable(true);
        checks.add(modalityOfVacancy);

        // -- Location vacancy
        final ProfileCheck locationVacancy = new ProfileCheck();
        locationVacancy.setId(8);
        locationVacancy.setTitle(context.getString(R.string.request_vacancy_check_work_location));
        locationVacancy.setContent( getLocation(profile) );
        locationVacancy.setIcon(App.getImage(context, "ubicacion"));
        locationVacancy.setEditable(true);
        checks.add(locationVacancy);

        // -- Date of vacancy
        final ProfileCheck modalityDate = new ProfileCheck();
        modalityDate.setId(9);
        modalityDate.setTitle(context.getString(R.string.request_vacancy_check_start_date));
        modalityDate.setContent( App.getDateFormatted(profile.getVacancyDate(),
                "dd 'de' MMMM yyyy", "dd/M/yyyy") );
        modalityDate.setIcon(App.getImage(context, "calendar_clock"));
        modalityDate.setEditable(true);
        checks.add(modalityDate);

        // -- Time of vacancy
        final ProfileCheck timeAssignationDate = new ProfileCheck();
        timeAssignationDate.setId(10);
        timeAssignationDate.setTitle(context.getString(R.string.request_vacancy_check_assig_period));

        String timeAssignation = profile.getHiringTime() == null ? "" : String.valueOf(profile.getHiringTime());
        timeAssignation = (timeAssignation.equals("")) ? "" :
                (timeAssignation.equals("0")) ? "Periodo indefinido" : profile.getHiringTime() + " meses";

        timeAssignationDate.setContent( timeAssignation );
        timeAssignationDate.setIcon(App.getImage(context, "update_time"));
        timeAssignationDate.setEditable(true);
        checks.add(timeAssignationDate);

        // -- Vacancy Budget
        final NumberFormat formatter = new DecimalFormat("#,###");
        final ProfileCheck vacancyBudget = new ProfileCheck();
        vacancyBudget.setId(11);
        vacancyBudget.setTitle(context.getString(R.string.request_vacancy_check_max_budget));
        vacancyBudget.setContent( String.format(
                context.getString(R.string.request_vacancy_check_max_budget_value),
                formatter.format(profile.getVacancyBudget())) );
        vacancyBudget.setIcon(App.getImage(context, "financial_care"));
        vacancyBudget.setEditable(true);
        checks.add(vacancyBudget);

        // -- Vacancy Equipment
        final ProfileCheck vacancyEquipment = new ProfileCheck();
        vacancyEquipment.setId(12);
        vacancyEquipment.setTitle(context.getString(R.string.request_vacancy_check_comp_equipment));
        vacancyEquipment.setContent( (profile.getVacancyEquipment()) ?
                context.getString(R.string.request_vacancy_check_comp_eq_by_stratis) :
                context.getString(R.string.request_vacancy_check_comp_eq_not_by_stratis) );
        vacancyEquipment.setIcon(App.getImage(context, "laptop"));
        vacancyEquipment.setEditable(true);
        checks.add(vacancyEquipment);

        // -- Vacancy Software
        final ProfileCheck vacancySoftware = new ProfileCheck();
        vacancySoftware.setId(13);
        vacancySoftware.setTitle(context.getString(R.string.request_vacancy_check_espec_software));
        vacancySoftware.setContent( (profile.getVacancyEquipment() &&
                profile.getVacancyEquipmentSoftware()) ?
                ProfileCheck.getSoftware(profile.getCurrentSoftware()) : "");
        vacancySoftware.setIcon(App.getImage(context, "cad_compasses"));
        vacancySoftware.setEditable(true);
        checks.add(vacancySoftware);

        // --
        return checks;
    }

    public static ArrayList<ProfileCheck> getRequestSelectedItemsFromRequestEdit(final Profile profile, Context context, boolean editable) {

        // -- Creating the parent obj
        ArrayList<ProfileCheck> checks =  new ArrayList<>();

        // -- Seniority vacancy
        final ProfileCheck seniority = new ProfileCheck();
        seniority.setId(0);
        seniority.setTitle(context.getString(R.string.request_vacancy_check_time_experencie_2));
        seniority.setContent(getExperienceTime(profile.getSeniorityId(), context) + " - " + profile.getSeniorityName());
        seniority.setIcon(App.getImage(context, "glases"));
        seniority.setEditable(editable);
        checks.add(seniority);

        // -- Skills vacancy
        String noSkillsContent = context.getString(R.string.request_vacancy_check_talent_skills_no);
        final ProfileCheck skills = new ProfileCheck();
        skills.setId(1);
        skills.setTitle(context.getString(R.string.request_vacancy_check_talent_skills));
        if (profile.getCurrentSkills() != null) {
            String skillContent = ProfileCheck.getSkills( profile.getCurrentSkills(), true );
            skills.setContent(skillContent.isEmpty() ? noSkillsContent : skillContent);
        } else {
            skills.setContent(noSkillsContent);
        }
        skills.setIcon(App.getImage(context, "camping_knife"));
        skills.setEditable(editable);
        checks.add(skills);

        final ProfileCheck skills2 = new ProfileCheck();
        skills2.setId(2);
        skills2.setTitle(context.getString(R.string.request_vacancy_check_talent_skills_desirable));
        if (profile.getCurrentSkills() != null) {
            String skillContent = ProfileCheck.getSkills( profile.getCurrentSkills(), false );
            skills2.setContent(skillContent.isEmpty() ? noSkillsContent : skillContent);
        } else {
            skills.setContent(noSkillsContent);
        }
        skills2.setIcon(App.getImage(context, "camping_knife"));
        skills2.setEditable(editable);
        checks.add(skills2);

        // -- Responsibilities vacancy
        final ProfileCheck responsibilities = new ProfileCheck();
        responsibilities.setId(3);
        responsibilities.setTitle(context.getString(R.string.request_vacancy_check_job_description));
        responsibilities.setContent( profile.getResponsibilities() );
        responsibilities.setIcon(App.getImage(context, "responsabilidad_puesto"));
        responsibilities.setEditable(editable);
        checks.add(responsibilities);

        // -- Languages vacancy
        profile.transformCurrentLanguageObjects();
        final ProfileCheck languages = new ProfileCheck();
        languages.setId(4);
        languages.setTitle(context.getString(R.string.request_vacancy_check_req_language_2));
        if (profile.getCurrentLanguages() != null) {
            languages.setContent(ProfileCheck.getLanguages(profile.getCurrentLanguages(), context));
        }
        languages.setIcon(App.getImage(context, "chat"));
        languages.setEditable(editable);
        checks.add(languages);

        // -- Modality of vacancy
        final ProfileCheck modalityOfVacancy = new ProfileCheck();
        modalityOfVacancy.setId(5);
        modalityOfVacancy.setTitle(context.getString(R.string.request_vacancy_check_work_modality));
        modalityOfVacancy.setContent( getModality(profile.getModalityId()) );
        modalityOfVacancy.setIcon(App.getImage(context, "wire_mouse_orange"));
        modalityOfVacancy.setEditable(editable);
        checks.add(modalityOfVacancy);

        // -- Location of vacancy
        final ProfileCheck locationOfVacancy = new ProfileCheck();
        locationOfVacancy.setId(6);
        locationOfVacancy.setTitle(context.getString(R.string.request_vacancy_check_work_location));
        locationOfVacancy.setContent( getLocation(profile) );
        locationOfVacancy.setIcon(App.getImage(context, "ubicacion"));
        locationOfVacancy.setEditable(editable);
        checks.add(locationOfVacancy);

        // -- Date of vacancy
        final ProfileCheck modalityDate = new ProfileCheck();
        modalityDate.setId(7);
        modalityDate.setTitle(context.getString(R.string.request_vacancy_check_start_date));
        modalityDate.setContent( App.getDateFormatted(profile.getVacancyDate(),
                "dd 'de' MMMM yyyy", "dd/M/yyyy") );
        modalityDate.setIcon(App.getImage(context, "calendar_clock"));
        modalityDate.setEditable(editable);
        checks.add(modalityDate);

        // -- Time of vacancy
        final ProfileCheck timeAssignationDate = new ProfileCheck();
        timeAssignationDate.setId(8);
        timeAssignationDate.setTitle(context.getString(R.string.request_vacancy_check_assig_period));

        String timeAssignation = profile.getHiringTime() == null ? "" : String.valueOf(profile.getHiringTime());
        timeAssignation = (timeAssignation.equals("")) ? "" :
                (timeAssignation.equals("0")) ? "Periodo indefinido" : profile.getHiringTime() + " meses";

        timeAssignationDate.setContent( timeAssignation );
        timeAssignationDate.setIcon(App.getImage(context, "update_time"));
        timeAssignationDate.setEditable(editable);
        checks.add(timeAssignationDate);

        // -- Vacancy Budget
        final NumberFormat formatter = new DecimalFormat("#,###");
        final ProfileCheck vacancyBudget = new ProfileCheck();
        vacancyBudget.setId(9);
        vacancyBudget.setTitle(context.getString(R.string.request_vacancy_check_max_budget));
        vacancyBudget.setContent( String.format(
                context.getString(R.string.request_vacancy_check_max_budget_value),
                formatter.format(profile.getVacancyBudget()))  );
        vacancyBudget.setIcon(App.getImage(context, "financial_care"));
        vacancyBudget.setEditable(editable);
        checks.add(vacancyBudget);

        // -- Vacancy Equipment
        final ProfileCheck vacancyEquipment = new ProfileCheck();
        vacancyEquipment.setId(10);
        vacancyEquipment.setTitle(context.getString(R.string.request_vacancy_check_comp_equipment));
        vacancyEquipment.setContent( (profile.getVacancyEquipment()) ?
                context.getString(R.string.request_vacancy_check_comp_eq_by_stratis) :
                context.getString(R.string.request_vacancy_check_comp_eq_not_by_stratis) );
        vacancyEquipment.setIcon(App.getImage(context, "laptop"));
        vacancyEquipment.setEditable(editable);
        checks.add(vacancyEquipment);

        // -- Vacancy Software
        final ProfileCheck vacancySoftware = new ProfileCheck();
        vacancySoftware.setId(11);
        vacancySoftware.setTitle(context.getString(R.string.request_vacancy_check_espec_software));
        vacancySoftware.setContent( (profile.getVacancyEquipment() &&
                profile.getVacancyEquipmentSoftware()) ?
                ProfileCheck.getSoftware(profile.getCurrentSoftware()) : "");
        vacancySoftware.setIcon(App.getImage(context, "cad_compasses"));
        vacancySoftware.setEditable(editable);
        checks.add(vacancySoftware);


        // --
        return checks;
    }

    public static ArrayList<ProfileCheck> getRequestSelectedItemsFromMasterAdmin(final Profile profile, Context context) {

        // -- Creating the parent obj
        ArrayList<ProfileCheck> checks =  new ArrayList<>();

        // -- Vacancies
        final ProfileCheck vacancies = new ProfileCheck();
        vacancies.setId(0);
        vacancies.setTitle(context.getString(R.string.request_vacancy_check_num_request));
        vacancies.setContent(profile.getCurrentVacancies() + " " + context.getString(R.string.recruitment_actions_requests_active_title).toLowerCase());
        vacancies.setEditable(false);
        checks.add(vacancies);

        // -- Profile Name
        final ProfileCheck profileName = new ProfileCheck();
        profileName.setId(1);
        profileName.setTitle(context.getString(R.string.request_vacancy_check_talent_profile));
        profileName.setContent(profile.getCurrentProfile().getName());
        profileName.setEditable(false);
        checks.add(profileName);

        // -- Seniority vacancy
        final ProfileCheck seniority = new ProfileCheck();
        seniority.setId(2);
        seniority.setTitle(context.getString(R.string.request_vacancy_check_time_experience));
        seniority.setContent(getExperienceTime(profile.getSeniorityId(), context) + " - " + profile.getSeniorityName());
        seniority.setEditable(false);
        checks.add(seniority);

        // -- Skills vacancy
        final ProfileCheck skills = new ProfileCheck();
        skills.setId(3);
        skills.setTitle(context.getString(R.string.request_vacancy_check_talent_skills));
        if (profile.getCurrentSkills() != null) {
            skills.setContent(ProfileCheck.getSkills( profile.getCurrentSkills(), null));
        }
        skills.setEditable(false);
        checks.add(skills);

        // -- Responsibilities vacancy
        final ProfileCheck responsibilities = new ProfileCheck();
        responsibilities.setId(4);
        responsibilities.setTitle(context.getString(R.string.request_vacancy_check_job_description));
        responsibilities.setContent( profile.getResponsibilities() );
        responsibilities.setEditable(false);
        checks.add(responsibilities);

        // -- Languages vacancy
        profile.transformCurrentLanguageObjects();
        final ProfileCheck languages = new ProfileCheck();
        languages.setId(5);
        languages.setTitle(context.getString(R.string.request_vacancy_check_req_language_2));
        if (profile.getCurrentLanguages() != null) {
            languages.setContent(ProfileCheck.getLanguages(profile.getCurrentLanguages(), context));
        }
        languages.setEditable(false);
        checks.add(languages);

        // -- Modality of vacancy
        final ProfileCheck modalityOfVacancy = new ProfileCheck();
        modalityOfVacancy.setId(6);
        modalityOfVacancy.setTitle(context.getString(R.string.request_vacancy_check_work_modality));
        modalityOfVacancy.setContent( getModality(profile.getModalityId()) );
        modalityOfVacancy.setEditable(false);
        checks.add(modalityOfVacancy);

        // -- Date of vacancy
        final ProfileCheck modalityDate = new ProfileCheck();
        modalityDate.setId(7);
        modalityDate.setTitle(context.getString(R.string.request_vacancy_check_start_date));
        modalityDate.setContent( App.getDateFormatted(profile.getVacancyDate(),
                "dd 'de' MMMM yyyy", "dd/M/yyyy") );
        modalityDate.setEditable(false);
        checks.add(modalityDate);

        // -- Time of vacancy
        final ProfileCheck timeAssignationDate = new ProfileCheck();
        timeAssignationDate.setId(8);
        timeAssignationDate.setTitle(context.getString(R.string.request_vacancy_check_assig_period));

        String timeAssignation = profile.getHiringTime() == null ? "" : String.valueOf(profile.getHiringTime());
        timeAssignation = (timeAssignation.equals("")) ? "" :
                (timeAssignation.equals("0")) ? "Periodo indefinido" : profile.getHiringTime() + " meses";

        timeAssignationDate.setContent( timeAssignation );
        timeAssignationDate.setEditable(false);
        checks.add(timeAssignationDate);

        // -- Vacancy Budget
        final NumberFormat formatter = new DecimalFormat("#,###");
        final ProfileCheck vacancyBudget = new ProfileCheck();
        vacancyBudget.setId(9);
        vacancyBudget.setTitle(context.getString(R.string.request_vacancy_check_max_budget));
        vacancyBudget.setContent( String.format(
                context.getString(R.string.request_vacancy_check_max_budget_value),
                formatter.format(profile.getVacancyBudget())) );
        vacancyBudget.setEditable(false);
        checks.add(vacancyBudget);

        // -- Vacancy Equipment
        final ProfileCheck vacancyEquipment = new ProfileCheck();
        vacancyEquipment.setId(10);
        vacancyEquipment.setTitle(context.getString(R.string.request_vacancy_check_comp_equipment));
        vacancyEquipment.setContent( (profile.getVacancyEquipment()) ?
                context.getString(R.string.request_vacancy_check_comp_eq_by_stratis) :
                context.getString(R.string.request_vacancy_check_comp_eq_not_by_stratis) );
        vacancyEquipment.setEditable(false);
        checks.add(vacancyEquipment);

        // -- Vacancy Software
        final ProfileCheck vacancySoftware = new ProfileCheck();
        vacancySoftware.setId(11);
        vacancySoftware.setTitle(context.getString(R.string.request_vacancy_check_espec_software));
        vacancySoftware.setContent( (profile.getVacancyEquipment() &&
                profile.getVacancyEquipmentSoftware()) ?
                ProfileCheck.getSoftware(profile.getCurrentSoftware()) : "");
        vacancySoftware.setEditable(false);
        checks.add(vacancySoftware);


        // --
        return checks;
    }

    private static String getExperienceTime(String id, Context context) {

        String seniorityTime = "";
        switch(id) {

            case "1000300001":
                seniorityTime = context.getString(R.string.request_vacancy_detail_level_basic);
                break;

            case "1000300002":
                seniorityTime = context.getString(R.string.request_vacancy_detail_level_medium);
                break;

            case "1000300003":
                seniorityTime = context.getString(R.string.request_vacancy_detail_level_avanced);
                break;
        }
        return seniorityTime;
    }

    private static String getModality(String id) {

        String modality = "";
        switch(id) {

            case "1000100001":
                modality = "Presencial";
                break;

            case "1000100002":
                modality = "Remoto";
                break;

            case "1000100003":
                modality = "Hibrido";
                break;
        }
        return modality;
    }

    private static String getLocation(Profile profile) {
        //if (profile.getRemoteJobCountries() != null && !profile.getRemoteJobCountries().equals("")) {
        if (profile.getModalityId().equals("1000100002") && !App.isEmpty(profile.getRemoteJobCountries())) {
            return profile.getRemoteJobCountries();
        }

//        if (profile.getJobLocation() != null) {
        else if (!profile.getModalityId().equals("1000100002") && profile.getJobLocation() != null) {
            return profile.getJobLocation().getName() + "\n" + profile.getJobLocation().getDescription();
        }

        return "";
    }

    // -- Helper method to get skill in 1 line of text
    private static String getSkills(final ArrayList<SkillCatalog> skills, Boolean isRequeried) {

        int i = 0;
        String skillText = "";
        for (SkillCatalog skill : skills) {
            if(isRequeried == null || ( skill.getRequired() != null && skill.getRequired().equals(isRequeried))) {
                skillText += skill.getName();
                if (i < skills.size()-1) {
                    skillText += "\n";
                }
            }
            i++;
        }
        return skillText;
    }

    // -- Helper method to get language in 1 line of text
    private static String getLanguages(final ArrayList<LanguageCatalog> languages, Context context) {

        int i = 0;
        String skillText = "";
        for (LanguageCatalog language : languages) {
            if (!language.getName().equals("")) {
                skillText += language.getName() + " - " + language.getLevel(context);
                if (i < languages.size()-1) {
                    skillText += "\n";
                }
            }
            i++;
        }
        return skillText;
    }

    // -- Helper method to get software in 1 line of text
    private static String getSoftware(final ArrayList<SoftwareCatalog> softwares) {

        int i = 0;
        String softwareText = "";
        for (SoftwareCatalog software : softwares) {
            softwareText += software.getName();
            if (i < softwares.size()-1) {
                softwareText += "\n";
            }
            i++;
        }
        return softwareText;
    }

    // --
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }


}