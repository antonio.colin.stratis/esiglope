package com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class RequestCandidateSkillView extends RecyclerView.ViewHolder {

    private RelativeLayout shape;
    private TextView name = null;
    private TextView languageLevel = null;
    private CircularProgressBar percentBar = null;
    private TextView percent = null;

    public RequestCandidateSkillView(View itemView) {

        // --
        super(itemView);

        // --
        this.shape = itemView.findViewById(R.id.shape);
        this.name = itemView.findViewById(R.id.name);
        this.languageLevel = itemView.findViewById(R.id.languageLevel);
        this.percentBar = itemView.findViewById(R.id.percentBar);
        this.percent = itemView.findViewById(R.id.percent);
    }

    public RelativeLayout getShape() {
        return shape;
    }

    public TextView getName() {
        return name;
    }

    public TextView getLanguageLevel() {
        return languageLevel;
    }

    public CircularProgressBar getPercentBar() {
        return percentBar;
    }

    public TextView getPercent() {
        return percent;
    }
}
