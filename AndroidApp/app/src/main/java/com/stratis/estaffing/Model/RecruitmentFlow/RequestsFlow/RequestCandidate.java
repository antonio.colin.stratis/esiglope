package com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 18/12/20
 * Catalog 09
 *
 * 1000900001	Candidato
 * 1000900002	Candidato Favorito
 * 1000900003	Candidato No Seleccionado
 * 1000900004	Candidato Seleccionado
 * 1000900005	Talento
 * 1000900006	Ex-Talento
 * 1000900007	Candidato en entrevistas
 * 1000900008	Calificado
 * 1000900009	Agendado para entrevista
 * 1000900010	No me interesa
 *
 */

public class RequestCandidate {

    public final static String statusCandidate = "1000900001";
    public final static String statusCandidateFavorite = "1000900002";
    public final static String statusCandidateNotChoose = "1000900003";
    public final static String statusCandidateChoose = "1000900004";
    public final static String statusTalent = "1000900005";
    public final static String statusExTalent = "1000900006";
    public final static String statusCandidateInterviews = "1000900007";
    public final static String statusCandidateGraded = "1000900008";
    public final static String statusCandidateInterviewDateAssigned = "1000900009";
    public final static String statusCandidateNotInterest = "1000900010";

    private String id = "";
    private String imgProfile = "";
    private String firstName = "";
    private String lastName = "";
    private String positionDesc = "";
    private String jobSeniority = "";
    private Boolean isFavorite = false;
    private String statusId = "";
    private String statusValue = "";
    private String description = "";
    private String rating = "";

    private String accountManagerName = "";
    private String vacancyCandidateId = "";
    private String talentId = "";
    private String vacancyId = "";
    private String statusCandidateId = "";
    private String statusCandidateDesc = "";
    private String comment = "";

    ArrayList<RequestCandidateSkill> hardSkills = new ArrayList<>();
    ArrayList<RequestCandidateSkill> softSkills = new ArrayList<>();
    ArrayList<RequestCandidateSkill> languageSkills = new ArrayList<>();

    public RequestCandidate() {}

    public RequestCandidate(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optString("id");
            this.imgProfile = obj.optString("imgProfile");
            this.firstName = obj.optString("firstName");
            this.lastName = obj.optString("lastName");
            this.positionDesc = obj.optString("positionDesc");
            this.jobSeniority = obj.optString("jobSeniority");
            this.isFavorite = obj.optBoolean("isFavorite", false);
            this.statusId = obj.optString("statusId");
            this.statusValue = obj.optString("statusValue");
            this.description = obj.optString("description");
            this.rating = obj.optString("rating");

            // --  Rating validation
            /*if (!obj.isNull("rating")) {
                this.rating = obj.optString("rating", "");
                if (this.rating.equals("")) {
                    this.rating = "0.0";
                }
            }*/
        }
    }

    public static ArrayList<RequestCandidate> parse(JSONArray array) {

        ArrayList<RequestCandidate> candidates = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {

            JSONObject obj = array.optJSONObject(i);
            candidates.add(new RequestCandidate(obj));
        }
        return candidates;
    }

    public void loadFromJSON(JSONObject obj) {

        if (obj != null) {

            this.imgProfile = obj.optString("imgProfile");
            this.firstName = obj.optString("firstName");
            this.lastName = obj.optString("lastName");
            this.jobSeniority = obj.optString("jobSeniority");
            this.description = obj.optString("description");
            this.positionDesc = obj.optString("positionDesc");
            this.statusId = obj.optString("statusId");
            this.statusValue = obj.optString("statusValue");
            this.isFavorite = obj.optBoolean("favorite");

            this.accountManagerName = obj.optString("accountManagerName");
            this.vacancyCandidateId = obj.optString("vacancyCandidateId");
            this.talentId = obj.optString("talentId");
            this.vacancyId = obj.optString("vacancyId");
            this.statusCandidateId = obj.optString("statusCandidateId");
            this.statusCandidateDesc = obj.optString("statusCandidateDesc");
            this.comment = obj.optString("comment");

            // --
            this.hardSkills = new ArrayList<>();
            this.softSkills = new ArrayList<>();
            this.languageSkills = new ArrayList<>();

            // -- SOFT && HARD Skills
            final JSONArray hardSoftSkillsArray = obj.optJSONArray("talentSkills");
            if (hardSoftSkillsArray != null && hardSoftSkillsArray.length() > 0) {

                final ArrayList<RequestCandidateSkill> hardSoftSkills =
                        RequestCandidateSkill.parse(hardSoftSkillsArray, false);
                for (RequestCandidateSkill skill : hardSoftSkills) {
                    if (skill.getType() == RequestCandidateSkill.RequestCandidateSkillType.hard) {
                        this.hardSkills.add(skill);
                    } else {
                        this.softSkills.add(skill);
                    }
                }
            }

            // -- LANGUAGES
            final JSONArray languagesArray = obj.optJSONArray("talentLang");
            if (languagesArray != null && languagesArray.length() > 0) {
                this.languageSkills = RequestCandidateSkill.parse(languagesArray, true);
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgProfile() {
        return imgProfile;
    }

    public void setImgProfile(String imgProfile) {
        this.imgProfile = imgProfile;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getAccountManagerShort() {

        final String[] nameParts = this.accountManagerName.split(" ");
        if (nameParts != null && nameParts.length > 1) {
            return nameParts[0];
        }
        return accountManagerName;
    }

    public String getFirstNameShort() {

        final String[] nameParts = this.firstName.split(" ");
        if (nameParts != null && nameParts.length > 1) {
            return nameParts[0];
        }
        return firstName;
    }

    public String getFullNameShort() {

        String fullName = "";

        // -- First name
        String[] firstNameParts = this.firstName.split(" ");
        if (firstNameParts.length >= 1) {

            String p1 = firstNameParts[0];
            p1 = p1.substring(0, 1).toUpperCase() + p1.substring(1).toLowerCase();
            fullName = p1;
        }

        // -- Last name
        String[] lastNameParts = this.lastName.split(" ");
        if (firstNameParts.length >= 1) {

            String p2 = lastNameParts[0];
            p2 = p2.substring(0, 1).toUpperCase() + p2.substring(1).toLowerCase();
            fullName = fullName + " " + p2;
        }

        return fullName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    //public String getFullName() {}

    public String getPositionDesc() {
        return positionDesc;
    }

    public void setPositionDesc(String positionDesc) {
        this.positionDesc = positionDesc;
    }

    public String getJobSeniority() {
        return jobSeniority;
    }

    public void setJobSeniority(String jobSeniority) {
        this.jobSeniority = jobSeniority;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getVacancyCandidateId() {
        return vacancyCandidateId;
    }

    public void setVacancyCandidateId(String vacancyCandidateId) {
        this.vacancyCandidateId = vacancyCandidateId;
    }

    public String getTalentId() {
        return talentId;
    }

    public void setTalentId(String talentId) {
        this.talentId = talentId;
    }

    public String getVacancyId() {
        return vacancyId;
    }

    public void setVacancyId(String vacancyId) {
        this.vacancyId = vacancyId;
    }

    public String getStatusCandidateId() {
        return statusCandidateId;
    }

    public void setStatusCandidateId(String statusCandidateId) {
        this.statusCandidateId = statusCandidateId;
    }

    public String getStatusCandidateDesc() {
        return statusCandidateDesc;
    }

    public void setStatusCandidateDesc(String statusCandidateDesc) {
        this.statusCandidateDesc = statusCandidateDesc;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ArrayList<RequestCandidateSkill> getHardSkills() {
        return hardSkills;
    }

    public ArrayList<RequestCandidateSkill> getSoftSkills() {
        return softSkills;
    }

    public ArrayList<RequestCandidateSkill> getLanguageSkills() {
        return languageSkills;
    }
}
