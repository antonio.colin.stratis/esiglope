package com.stratis.estaffing.Service.MasterAdmin

import android.util.Log
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Core.Api
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTRequest
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Core.eStaffingClient
import com.stratis.estaffing.Model.MasterAdminFlow.MARequestCandidate
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by Erick Sanchez
 * Revision 1 - 29/06/21
 */
open class MasterAdminService {

    companion object {

        fun getLeaders(session: Session, listener:ReSTCallback) {

            val url = Api.getInstance().getUrl("/m_admin/findMyLeaders")
            val rest = eStaffingClient(url)
            val request = ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "")

            // --
            request.addHeaders(Api.getSecureHeaders())

            // --
            request.addParameter("userId", session.dataSessionId)


            // --
            rest.setDebugMode(Api.getInstance().canLog())
            rest.execute(request, listener)
        }

        fun getCandidates(requestId: String, listener: ReSTCallback) {

            val url = Api.getInstance().getUrl("/m_admin/$requestId/candidates")
            val rest = eStaffingClient(url)
            val request = ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "")

            // --
            request.addHeaders(Api.getSecureHeaders())

            // --
            rest.setDebugMode(Api.getInstance().canLog())
            rest.execute(request, listener)
        }

        fun discard(candidate: RequestCandidate, cause:String, session: Session, listener: ReSTCallback?) {

            val url = Api.getInstance().getUrl("/m_admin/candidate/discard")
            val rest = eStaffingClient(url)
            val request = ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "")

            // --
            request.addHeaders(Api.getSecureHeaders())

            // -- JSON Body
            val body = JSONObject()
            try {
                body.put("vacancyId", candidate.vacancyId)
                body.put("talentId", candidate.talentId)
                body.put("cause", cause)
                body.put("userId", session.dataSessionId)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            request.addBodyJson(JsonBuilder.jsonToString(body))
            Log.d("DXGOP", "STATUS SEND ::: " + JsonBuilder.jsonToString(body))

            // --
            rest.setDebugMode(Api.getInstance().canLog())
            rest.execute(request, listener)
        }

        fun reconsider(candidate: MARequestCandidate, session: Session, listener: ReSTCallback?) {

            val url = Api.getInstance().getUrl("/m_admin/candidate/reconsider")
            val rest = eStaffingClient(url)
            val request = ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "")

            // --
            request.addHeaders(Api.getSecureHeaders())

            // -- JSON Body
            val body = JSONObject()
            try {
                body.put("vacancyId", candidate.vacancyId)
                body.put("talentId", candidate.talentId)
                body.put("cause", "Reconsiderado")
                body.put("userId", session.dataSessionId)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            request.addBodyJson(JsonBuilder.jsonToString(body))
            Log.d("DXGOP", "STATUS SEND ::: " + JsonBuilder.jsonToString(body))

            // --
            rest.setDebugMode(Api.getInstance().canLog())
            rest.execute(request, listener)
        }

        fun reviewed(candidate: MARequestCandidate, session: Session, listener: ReSTCallback?) {

            val url = Api.getInstance().getUrl("/m_admin/${candidate.vacancyCandidateId}/${candidate.talentId}/reviewed")
            val rest = eStaffingClient(url)
            val request = ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "")

            // --
            request.addHeaders(Api.getSecureHeaders())

            // --
            rest.setDebugMode(Api.getInstance().canLog())
            rest.execute(request, listener)
        }
    }
}