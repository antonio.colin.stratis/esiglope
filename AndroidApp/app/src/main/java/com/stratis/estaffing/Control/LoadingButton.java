package com.stratis.estaffing.Control;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 27/05/20
 */
public class LoadingButton {

    public RelativeLayout base;
    public TextView text;
    public ProgressBar bar;

    public LoadingButton(RelativeLayout layout) {

        // --
        this.base = layout;

        // --
        this.text = this.base.findViewById(R.id.text);

        // --
        this.bar = this.base.findViewById(R.id.progress_bar);
        this.bar.setVisibility(View.GONE);
        this.changeBarColor("#FFFFFF");
    }

    public void setText(String text) {
        this.text.setText(text);
    }

    public void showLoading() {
        this.showLoading(true);
    }

    public void hideLoading() {
        this.showLoading(false);
    }

    public void changeBarColor(String color) {
        color = App.fixWrongColorATuDePiglek(color);
        this.bar.getIndeterminateDrawable().setColorFilter(
                Color.parseColor(color), PorterDuff.Mode.SRC_IN);
    }

    public void showLoading(boolean state) {

        this.bar.setVisibility(state ? View.VISIBLE : View.GONE);
        this.text.setVisibility(state ? View.GONE : View.VISIBLE);
        this.base.setEnabled(!state);
        this.base.setClickable(!state);
        this.bar.animate();
        this.bar.bringToFront();
    }

    public RelativeLayout getButton() {
        return this.base;
    }
}
