package com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.stratis.estaffing.Adapter.RecruitmentFlow.SelectedSkillAdapter;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Control.Searcher.Searcher;
import com.stratis.estaffing.Control.Searcher.SearcherOpen;
import com.stratis.estaffing.Control.eStaffingFragment;
import com.stratis.estaffing.Controller.RecruitmentFlow.RecruitmentFlowController;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Model.RecruitmentFlow.SimpleProfile;
import com.stratis.estaffing.Model.RecruitmentFlow.SkillCatalog;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Talent.TalentService;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Objects;

public class RequestVacancyDetailFragment extends eStaffingFragment implements View.OnClickListener, Searcher.SearcherListener {

    private NestedScrollView scroll;

    private SelectedSkillAdapter adapter;
    private SelectedSkillAdapter adapter2;

    private ConstraintLayout jrBox;
    private ConstraintLayout mdBox;
    private ConstraintLayout srBox;

    private Searcher searcher;
    private SearcherOpen skillSearcher;
    private SearcherOpen skillSearcher2;

    private RecyclerView skillsTable;
    private RecyclerView skillsTable2;

    private Boolean jrSelected = false;
    private Boolean mdSelected = false;
    private Boolean srSelected = false;

    private boolean canEditProfileName = true;
    private boolean isEditProfile = false;

    // -- Responsibilities vars
    private final static int maxLengthCharacters = 1500;
    private SpeechRecognizer mSpeechRecognizer;
    private SpeechRecognitionListener listener;
    private Intent mSpeechRecognizerIntent;
    private boolean mIsListening;
    private TextView responsibilitiesLimit;
    private EditText responsibilitiesContainerContent;
    private ImageButton responsibilitiesContainerButton;
    private LottieAnimationView responsibilitiesContainerAnimation;
    private String originalText = "";

    public RequestVacancyDetailFragment() {}

    public RequestVacancyDetailFragment(Boolean canEditProfileName, Boolean isEditProfile) {
        this.canEditProfileName = canEditProfileName;
        this.isEditProfile = isEditProfile;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        this.mView = inflater.inflate(R.layout.fragment_request_vacancy_detail, container, false);
        this.initViews();
        this.initResponsibilitiesViews();
        return this.mView;
    }

    @Override
    public void onResume() {

        super.onResume();
        this.checkProfile();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        if (this.mSpeechRecognizer != null) {
            this.mSpeechRecognizer.destroy();
        }
    }

    @SuppressLint({"SetTextI18n", "NotifyDataSetChanged"})
    private void initViews() {

        //  --
        this.searcher = new Searcher(getContext(), this.mView.findViewById(R.id.searchComponent), Searcher.SearcherType.normal);
        final ArrayList<SimpleProfile> list =
            (this.getProfile().getSimpleProfiles() == null) ?
                new ArrayList<>() :
                this.getProfile().getSimpleProfiles();
        searcher.setData(list);
        searcher.setListener(this, "91");
        searcher.getSearchComponentBoxInput().setEnabled(this.canEditProfileName);

        this.showSoftKeyboard(getActivity(), searcher.getSearchComponentBoxInput());

        // --
        this.canActivateFooterButton();

        // --
        this.jrBox = this.mView.findViewById(R.id.jrButton);
        ((TextView) this.jrBox.findViewById(R.id.title)).setText("JUNIOR");
        ((TextView) this.jrBox.findViewById(R.id.req)).setText(mContext.getText(R.string.request_vacancy_detail_level_basic));
        this.jrBox.setTag("11");
        this.jrBox.setOnClickListener(this);

        this.mdBox = this.mView.findViewById(R.id.mdButton);
        ((TextView) this.mdBox.findViewById(R.id.title)).setText("MID-LEVEL");
        ((TextView) this.mdBox.findViewById(R.id.req)).setText(mContext.getText(R.string.request_vacancy_detail_level_medium));
        this.mdBox.setTag("12");
        this.mdBox.setOnClickListener(this);

        this.srBox = this.mView.findViewById(R.id.srButton);
        ((TextView) this.srBox.findViewById(R.id.title)).setText("SENIOR");
        ((TextView) this.srBox.findViewById(R.id.req)).setText(mContext.getText(R.string.request_vacancy_detail_level_avanced));
        this.srBox.setTag("13");
        this.srBox.setOnClickListener(this);

        // --
        this.checkButtonsState();

        // --
        this.skillSearcher = new SearcherOpen(getContext(), this.mView.findViewById(R.id.searchComponentSkills));
        this.skillSearcher.setListener(this, "92");
        this.skillSearcher.setInitOpened(false);
        this.skillSearcher2 = new SearcherOpen(getContext(), this.mView.findViewById(R.id.searchComponentSkills2));
        this.skillSearcher2.setListener(new Searcher.SearcherListener() {
            @Override
            public void onItemSelected(Object item, RelativeLayout box, RecyclerView recyclerView, EditText editText, ImageView icon) {

            }

            @Override
            public void onItemSelected(Object item, ConstraintLayout box, RecyclerView recyclerView, EditText editText, ImageView icon) {
                onSkillItemSelected(item, editText, false);
            }
        }, "92");
        this.skillSearcher2.setInitOpened(false);

        // --
        this.skillsTable = this.mView.findViewById(R.id.skillsTable);
        // --
        final DividerItemDecoration itemDecorator = new DividerItemDecoration(this.mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(this.mContext, R.drawable.recycler_devider_5)));
        LinearLayoutManager layout = new LinearLayoutManager(this.mContext, LinearLayoutManager.VERTICAL, false);

        // --
        skillsTable.getRecycledViewPool().setMaxRecycledViews(0, 3);
        skillsTable.setLayoutManager(layout);
        skillsTable.addItemDecoration(itemDecorator);
        skillsTable.setItemViewCacheSize(8);
        skillsTable.setDrawingCacheEnabled(true);
        skillsTable.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // --
        this.adapter = new SelectedSkillAdapter(SkillCatalog.getBase(), this.mContext);
        skillsTable.setAdapter(this.adapter);
        this.adapter.setListener(position -> {

            adapter.removeElement(position);
            adapter.notifyDataSetChanged();
        });

        this.skillsTable2 = this.mView.findViewById(R.id.skillsTable2);
        // --
        final DividerItemDecoration itemDecorator2 = new DividerItemDecoration(this.mContext, DividerItemDecoration.VERTICAL);
        itemDecorator2.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(this.mContext, R.drawable.recycler_devider_5)));
        LinearLayoutManager layout2 = new LinearLayoutManager(this.mContext, LinearLayoutManager.VERTICAL, false);

        // --
        skillsTable2.getRecycledViewPool().setMaxRecycledViews(0, 3);
        skillsTable2.setLayoutManager(layout2);
        skillsTable2.addItemDecoration(itemDecorator2);
        skillsTable2.setItemViewCacheSize(8);
        skillsTable2.setDrawingCacheEnabled(true);
        skillsTable2.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // --
        this.adapter2 = new SelectedSkillAdapter(SkillCatalog.getBase(), this.mContext);
        skillsTable2.setAdapter(this.adapter2);
        this.adapter2.setListener(position -> {

            adapter2.removeElement(position);
            adapter2.notifyDataSetChanged();
        });

        scroll = this.findView(R.id.parentScroll);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initResponsibilitiesViews() {

        // -- Button
        this.responsibilitiesContainerButton =
                this.mView.findViewById(R.id.responsibilitiesContainerButton);

        // -- EditText
        this.responsibilitiesContainerContent =
                this.mView.findViewById(R.id.responsibilitiesContainerContent);
        this.responsibilitiesContainerContent.setFocusable(false);
        this.responsibilitiesContainerContent.setOnTouchListener((v, event) -> {

            // --
            if (mIsListening) {
                return true;
            }

            // --
            if (responsibilitiesContainerContent.hasFocus()) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_SCROLL) {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    return true;
                }
            }
            return false;
        });
        this.responsibilitiesContainerContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // -- Size
                final String size = String.valueOf(s.length());
                final String responsibilitiesLimitString = String.format(getString(R.string.request_vacancy_detail_responsibilities_hint_counter), size);
                responsibilitiesLimit.setText(responsibilitiesLimitString);

                // --
                if (!mIsListening) {
                    originalText = String.valueOf(s);
                    reAbleToSpeech();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
        this.responsibilitiesContainerContent.setOnFocusChangeListener((v, hasFocus) ->
            responsibilitiesContainerContent.setSelection(responsibilitiesContainerContent.getText().length())
        );

        // -- Text Limit
        this.responsibilitiesLimit = this.mView.findViewById(R.id.responsibilitiesLimit);
        final String responsibilitiesLimitString =
                String.format(getString(R.string.request_vacancy_detail_responsibilities_hint_counter), "0");
        this.responsibilitiesLimit.setText(responsibilitiesLimitString);

        // -- Animation
        this.responsibilitiesContainerAnimation = this.mView.findViewById(R.id.responsibilitiesContainerAnimation);
        this.responsibilitiesContainerAnimation.setOnClickListener(v -> {

            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.cancel();
            originalText = String.valueOf(this.responsibilitiesContainerContent.getText());
            destroySpeech();
        });

        // --
        String[] perms = {Manifest.permission.RECORD_AUDIO};
        Permissions.check(getContext(), perms, getString(R.string.permission_call_text),
            new Permissions.Options().setRationaleDialogTitle("Info"),
            new PermissionHandler() {
                @Override
                public void onGranted() {

                    final double mil = 1000000;
                    mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(getApp().getApplicationContext());
                    mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getApp().getPackageName());
                    mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
                    mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1000);
                    mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, mil);
                    mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS, mil);
                    mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, mil);

                    // -- Button
                    responsibilitiesContainerButton.setOnClickListener(v -> {
                        if (!mIsListening) {

                            mIsListening = true;
                            responsibilitiesContainerButton.setVisibility(View.INVISIBLE);

                            responsibilitiesContainerAnimation.playAnimation();
                            responsibilitiesContainerAnimation.setVisibility(View.VISIBLE);

                            // --
                            listener = new SpeechRecognitionListener();
                            mSpeechRecognizer.setRecognitionListener(listener);
                            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);

                        }
                    });
                }

                @Override
                public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                    Toast.makeText(context, getString(R.string.permission_call_denied), Toast.LENGTH_LONG).show();
                }

                @Override
                public boolean onBlocked(Context context, ArrayList<String> blockedList) {
                    Toast.makeText(context, getString(R.string.permission_call_denied), Toast.LENGTH_LONG).show();
                    return false;
                }

                @Override
                public void onJustBlocked(Context context, ArrayList<String> justBlockedList, ArrayList<String> deniedPermissions) {
                    Toast.makeText(context, getString(R.string.permission_call_denied), Toast.LENGTH_LONG).show();
                }
            });
    }

    @SuppressLint("NotifyDataSetChanged")
    private void checkProfile() {

        if (this.getProfile() != null) {

            // -- Profile name
            if (this.getProfile().getCurrentProfile() != null) {

                this.searcher.getSearchComponentBoxInput().clearFocus();
                this.onItemSelected(this.getProfile().getCurrentProfile(), this.searcher.getSearchComponentBox(), this.searcher.getSearchComponentList(),
                        this.searcher.getSearchComponentBoxInput(), this.searcher.getSearchComponentBoxIcon());
            }

            if (this.isEditProfile) {
                this.searcherProfileFocus();
            }

            // -- Seniority
            final String seniorityId = this.getProfile().getSeniorityId();
            if (!seniorityId.equals("0")) {

                this.turnOffSeniority();
                if (seniorityId.equals("1000300001")) {
                    this.jrBox.performClick();
                } else if (seniorityId.equals("1000300002")) {
                    this.mdBox.performClick();
                } else {
                    this.srBox.performClick();
                }
            }

            // -- Skills
            skillSearcher.setData(this.getProfile().getSkillCatalogs());
            this.adapter = new SelectedSkillAdapter(SkillCatalog.getBase(), this.mContext);
            if (this.getProfile().getCurrentSkills() != null && this.getProfile().getCurrentSkills().size() > 0) {
                for (SkillCatalog skillCatalog : this.getProfile().getCurrentSkills()) {
                    if (!skillCatalog.getName().equals("") && skillCatalog.getRequired()) {
                        this.adapter.addElement(skillCatalog);
                    }
                }
            }
            this.adapter.setListener(position -> {

                adapter.removeElement(position);
                adapter.notifyDataSetChanged();
            });
            this.skillsTable.setAdapter(this.adapter);

            skillSearcher2.setData(this.getProfile().getSkillCatalogs());
            this.adapter2 = new SelectedSkillAdapter(SkillCatalog.getBase(), this.mContext);
            if (this.getProfile().getCurrentSkills() != null && this.getProfile().getCurrentSkills().size() > 0) {
                for (SkillCatalog skillCatalog : this.getProfile().getCurrentSkills()) {
                    if (!skillCatalog.getName().equals("") && !skillCatalog.getRequired()) {
                        this.adapter2.addElement(skillCatalog);
                    }
                }
            }
            this.adapter2.setListener(position -> {

                adapter2.removeElement(position);
                adapter2.notifyDataSetChanged();
            });
            this.skillsTable2.setAdapter(this.adapter2);

            // -- Responsibilities (Job Activities)
            if (!this.getProfile().getResponsibilities().equals("")) {

                this.responsibilitiesContainerContent.setText( this.getProfile().getResponsibilities() );
                this.originalText = this.getProfile().getResponsibilities();
                this.responsibilitiesContainerContent.setFocusableInTouchMode(true);
                this.responsibilitiesContainerContent.setFocusable(true);
                //this.responsibilitiesContainerContent.requestFocus();
                this.responsibilitiesContainerContent.setSelection(
                        responsibilitiesContainerContent.getText().length());
                this.reAbleToSpeech();

                // -- Limit Text
                final String size = String.valueOf(this.originalText.length());
                final String responsibilitiesLimitString = String.format(
                                getString(R.string.request_vacancy_detail_responsibilities_hint_counter), size);
                this.responsibilitiesLimit.setText(responsibilitiesLimitString);
            }
        }
    }

    private void showAllElements() {
        // --
        final TextView content = this.mView.findViewById(R.id.contentTitle);
        content.animate().alpha(1.0f).setDuration(500);

        jrBox.animate().alpha(1.0f).setDuration(500);
        mdBox.animate().alpha(1.0f).setDuration(500);
        srBox.animate().alpha(1.0f).setDuration(500);
        new Handler().postDelayed(() -> {
            skillSearcher.getRoot().setVisibility(View.VISIBLE);
            mView.findViewById(R.id.searchComponentSkillsLimit).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.responsibilitiesContainer).setVisibility(View.VISIBLE);
            skillSearcher2.getRoot().setVisibility(View.VISIBLE);
            mView.findViewById(R.id.searchComponentSkillsLimit2).setVisibility(View.VISIBLE);
        }, 500);

        // --
        final TextView contentSubTitle = this.mView.findViewById(R.id.contentSubTitle);
        contentSubTitle.animate().alpha(1.0f).setDuration(500);

        // --
        final TextView contentSub = this.mView.findViewById(R.id.contentSub);
        contentSub.animate().alpha(1.0f).setDuration(500);

        final TextView skillsEssentialsTitle = this.mView.findViewById(R.id.skillsEssentialsTitle);
        skillsEssentialsTitle.animate().alpha(1.0f).setDuration(500);

        // --
        this.mView.findViewById(R.id.skillsTable).animate().alpha(1.0f).setDuration(500);

        // Desirable
        final TextView contentSubTitle2 = this.mView.findViewById(R.id.contentSubTitle2);
        contentSubTitle2.animate().alpha(1.0f).setDuration(500);
        final TextView skillsEssentialsTitle2 = this.mView.findViewById(R.id.skillsEssentialsTitle2);
        skillsEssentialsTitle2.animate().alpha(1.0f).setDuration(500);
        this.mView.findViewById(R.id.skillsTable2).animate().alpha(1.0f).setDuration(500);


        // -- Job Responsibilities
        this.mView.findViewById(R.id.responsibilitiesTitle).animate().alpha(1.0f).setDuration(500);
        this.mView.findViewById(R.id.responsibilitiesLimit).animate().alpha(1.0f).setDuration(500);
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat("skills"));
    }

    private void turnOffSeniority() {

        this.jrSelected = false;
        this.mdSelected = false;
        this.srSelected = false;
    }

    private void checkButtonsState() {

        this.changeStateButton(this.jrBox, this.jrSelected, "student");
        this.changeStateButton(this.mdBox, this.mdSelected, "bald_male");
        this.changeStateButton(this.srBox, this.srSelected, "einstein");
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeStateButton(ConstraintLayout box, Boolean state, String iconName) {

        if (state) {

            box.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(box);

            ((ImageView) box.findViewById(R.id.icon)).setImageBitmap( App.getImage(mContext, "white_" + iconName) );
            ((TextView) box.findViewById(R.id.title)).setTextColor(  ContextCompat.getColor(mContext, R.color.white) );
            ((TextView) box.findViewById(R.id.req)).setTextColor(  ContextCompat.getColor(mContext, R.color.white) );

        } else {

            box.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            ((ImageView) box.findViewById(R.id.icon)).setImageBitmap( App.getImage(mContext, iconName) );
            ((TextView) box.findViewById(R.id.title)).setTextColor(  ContextCompat.getColor(mContext, R.color.recruitment_request_vacancy_detail_unselected_text_button) );
            ((TextView) box.findViewById(R.id.req)).setTextColor(  ContextCompat.getColor(mContext, R.color.recruitment_request_vacancy_detail_unselected_text_button) );
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private void getSkillCatalog() {

        TalentService.getSkillCatalog(String.valueOf(this.getProfile().getCurrentProfile().getId()), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                Log.d("DXGI", "SKILL CATALOG RESPONSE = " + response.body);
                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                final ArrayList<SkillCatalog> skills = SkillCatalog.parse(res);
                getProfile().setSkillCatalogs(skills);
                skillSearcher.setData(skills);
                skillSearcher.setInputHint(mContext.getString(R.string.request_vacancy_detail_skills_hint));
                skillSearcher2.setData(skills);
                skillSearcher2.setInputHint(mContext.getString(R.string.request_vacancy_detail_skills_hint));

            }

            @Override
            public void onError(ReSTResponse response) {

                // --
                Log.d("DXGI", "ERROR :: " + response.body);
                skillSearcher.setData(new ArrayList<>());
                skillSearcher.setInputHint(mContext.getString(R.string.request_vacancy_detail_skills_hint));
                skillSearcher2.setData(new ArrayList<>());
                skillSearcher2.setInputHint(mContext.getString(R.string.request_vacancy_detail_skills_hint));
            }
        });

        // --
        if (this.getProfile().getCurrentSkills() != null) {
            this.adapter.reload(this.getProfile().getCurrentSkills());
            this.adapter.notifyDataSetChanged();
            this.adapter2.reload(this.getProfile().getCurrentSkills());
            this.adapter2.notifyDataSetChanged();
        }

        // if (!this.isEditProfile) { this.searcherSkillFocus(); }

    }

    private void canActivateFooterButton() {
        if (this.getActivity() != null && this.getActivity() instanceof RecruitmentFlowController) {
            ((RecruitmentFlowController) this.getActivity()).disableContinueButton(false);

            //--
            String searcherText = String.valueOf(this.searcher.getSearchComponentBoxInput().getText());
            if (searcherText.equals("")) { return; }

            // --
            if (!this.jrSelected && !this.mdSelected && !this.srSelected) { return; }

            // --
            ((RecruitmentFlowController) this.getActivity()).disableContinueButton(true);
        }
    }

    // -- Method to set SpeechRecognizer text into de EditText
    protected void setSpeechText(ArrayList<String> matches, boolean end) {

        // --
        if (matches.size() > 0) {

            final String match = matches.get( matches.size() - 1 );

            final String prev = this.originalText;
            final String preMatchContinues = (prev.equals("")) ? "" : " ";
            final String txt =  prev + preMatchContinues + match;
            this.responsibilitiesContainerContent.setText( txt );
            this.responsibilitiesContainerContent.setSelection(
                    responsibilitiesContainerContent.getText().length());

            // -- Check length
            this.checkLengthSpeech(txt);

            // -- End
            if (end) {

                this.originalText = String.valueOf(this.responsibilitiesContainerContent.getText());
                this.reAbleToSpeech();
            }

            // -- Responsibilities [Job activities]
            this.getProfile().setResponsibilities(
                    String.valueOf(this.responsibilitiesContainerContent.getText()));
        }
    }

    protected void reAbleToSpeech() {

        if (this.originalText.length() < maxLengthCharacters) {

            this.responsibilitiesContainerButton.setVisibility(View.VISIBLE);
            this.responsibilitiesContainerButton.setEnabled(true);
            this.responsibilitiesContainerButton.setClickable(true);
            this.responsibilitiesContainerButton.setImageBitmap(App.getImage(mContext, "speech_microphone"));

        } else {

            this.responsibilitiesContainerButton.setVisibility(View.VISIBLE);
            this.responsibilitiesContainerButton.setEnabled(false);
            this.responsibilitiesContainerButton.setClickable(false);
            this.responsibilitiesContainerButton.setImageBitmap(App.getImage(mContext, "speech_microphone_off"));
        }
        this.responsibilitiesContainerAnimation.setVisibility(View.INVISIBLE);
    }

    protected void checkLengthSpeech(final String txt) {

        if (txt.length() > maxLengthCharacters && this.mIsListening) {

            this.mSpeechRecognizer.stopListening();
            this.mSpeechRecognizer.cancel();
            this.originalText = String.valueOf(this.responsibilitiesContainerContent.getText());
            this.destroySpeech();
        }
    }

    protected void initSpeech() {

        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(responsibilitiesContainerContent.getApplicationWindowToken(), 0);
        }
        this.responsibilitiesContainerContent.setFocusableInTouchMode(true);
        this.responsibilitiesContainerContent.setFocusable(true);
        this.responsibilitiesContainerContent.requestFocus();
        scroll.post(() -> scroll.scrollTo(0, scroll.getHeight()));
    }

    protected void destroySpeech() {

        Log.d("DXGI", "onEndOfSpeech Manually");
        this.mIsListening = false;
        this.mSpeechRecognizer.destroy();
        this.originalText = String.valueOf(this.responsibilitiesContainerContent.getText());
        this.responsibilitiesContainerContent.setFocusableInTouchMode(true);
        this.responsibilitiesContainerContent.setFocusable(true);
        this.responsibilitiesContainerContent.requestFocus();
        this.responsibilitiesContainerContent.setSelection(
                responsibilitiesContainerContent.getText().length());
        this.reAbleToSpeech();
    }

    @Override
    public void onClick(View v) {

        // -- Turn of everything
        this.turnOffSeniority();
        String tag = String.valueOf(v.getTag());
        switch(tag) {

            case "11":
                this.jrSelected = true;
                this.getProfile().setSeniorityId("1000300001");
                this.getProfile().setSeniorityName("Junior");
                searcherSkillFocus();
                break;

            case "12":
                this.mdSelected = true;
                this.getProfile().setSeniorityId("1000300002");
                this.getProfile().setSeniorityName("Middle");
                searcherSkillFocus();
                break;

            case "13":
                this.srSelected = true;
                this.getProfile().setSeniorityId("1000300003");
                this.getProfile().setSeniorityName("Senior");
                searcherSkillFocus();
                break;
        }
        // --
        this.checkButtonsState();
        this.canActivateFooterButton();
    }

    private void searcherSkillFocus() {
        if(this.skillSearcher != null && this.skillSearcher.getSearchComponentBoxInput() != null) {
            this.skillSearcher.getSearchComponentBoxInput().requestFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(this.skillSearcher.getSearchComponentBoxInput(), InputMethodManager.SHOW_IMPLICIT);
            // scroll.post(() -> scroll.scrollTo(0,1250));

            ObjectAnimator anim = ObjectAnimator.ofInt(scroll, "scrollY",
                    scroll.getScrollY(), 1250);
            anim.setDuration(500);
            anim.start();
        }
    }

    private void searcherProfileFocus() {
        if(this.searcher != null && this.searcher.getSearchComponentBoxInput() != null) {
            this.searcher.getSearchComponentBoxInput().requestFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(this.searcher.getSearchComponentBoxInput(), InputMethodManager.SHOW_IMPLICIT);
        }
    }

    // -- Item selected from Profile Searcher
    @Override
    public void onItemSelected(Object item, RelativeLayout box, RecyclerView recyclerView, EditText editText, ImageView icon) {
        // --
        final SimpleProfile profile = (SimpleProfile) item;

        // -- Hide keyboard
        editText.clearFocus();
        hideSoftKeyboard(requireActivity(), editText);

        // --
        editText.setText(profile.getName());
        editText.setTextColor(  ContextCompat.getColor(requireActivity(), R.color.white) );

        // --  Hide
        recyclerView.setVisibility(View.GONE);

        // -- Box
        box.setBackground( ContextCompat.getDrawable(requireActivity(), R.drawable.request_vacancy_selected_button) );
        this.setViewAsSecondaryColor(box);

        // -- Icon
        icon.setImageBitmap(App.getImage(mContext, "select_white_option" ));

        // -- Set current profile and refresh views
        RequestVacancyDetailFragment.this.getProfile().setCurrentProfile(profile);
        this.getSkillCatalog();
        this.skillSearcher.getRoot().setVisibility(View.VISIBLE);
        this.skillSearcher2.getRoot().setVisibility(View.VISIBLE);

        // -- Show
        this.showAllElements();

        // --
        this.canActivateFooterButton();
    }

    // -- Item selected from Skill Searcher
    @Override
    public void onItemSelected(Object item, ConstraintLayout box, RecyclerView recyclerView, EditText editText, ImageView icon) {
        onSkillItemSelected(item, editText, true);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void onSkillItemSelected(Object item, EditText editText, boolean main){
        // --
        final SkillCatalog skillCatalog = (SkillCatalog) item;

        // -- Hide keyboard
        editText.setText("");
        editText.clearFocus();
        hideSoftKeyboard(requireActivity(), editText);

        if(main){
            skillCatalog.setRequired(true);
            // --
            this.adapter.addElement(skillCatalog);
            this.adapter.notifyDataSetChanged();
            // --
            this.skillSearcher.setSelected(this.adapter.getItems());
        } else {
            skillCatalog.setRequired(false);
            // --
            this.adapter2.addElement(skillCatalog);
            this.adapter2.notifyDataSetChanged();
            // --
            this.skillSearcher2.setSelected(this.adapter2.getItems());
        }

        // --
        final Notification notification = new Notification(getContext(), R.layout.notification_light);
        notification.setMessage(mContext.getString(R.string.request_vacancy_detail_skills_notification_text));
        notification.show(1500);

        // -- Getting the current skills
        this.getProfile().setCurrentSkills( this.adapter.getItems() );
    }

    // -- Abstract methods
    @Override
    public void onFooterContinueButtonClick() {

        //--
        super.onFooterContinueButtonClick();

        // -- Getting the current skills
        ArrayList<SkillCatalog> currentSkills = new ArrayList<>();
        currentSkills.addAll(this.adapter.getItems());
        currentSkills.addAll(this.adapter2.getItems());
        this.getProfile().setCurrentSkills( currentSkills );

        // -- Responsibilities [Job activities]
        this.getProfile().setResponsibilities(
                String.valueOf(this.responsibilitiesContainerContent.getText()));
    }

    // --
    protected class SpeechRecognitionListener implements RecognitionListener {

        @Override
        public void onBeginningOfSpeech() {

            Log.d("DXGI", "onBeginningOfSpeech");
            initSpeech();
        }

        @Override
        public void onBufferReceived(byte[] buffer) {}

        @Override
        public void onEndOfSpeech() {

            Log.d("DXGI", "onEndOfSpeech");
            destroySpeech();
        }

        @Override
        public void onError(int error) {
            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
        }

        @Override
        public void onEvent(int eventType, Bundle params) {}

        @Override
        public void onPartialResults(Bundle partialResults) {

            ArrayList<String> matches = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            setSpeechText(matches, false);
        }

        @Override
        public void onReadyForSpeech(Bundle params) {

            Log.d("DXGI", "onReadyForSpeech");
            //responsibilitiesContainerContent.setFocusable(false);
        }

        @Override
        public void onResults(Bundle results) {

            ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            Log.d("DXGI", "Speech Results: " + matches.get(matches.size() - 1));

            //mIsListening = !mIsListening;
            setSpeechText(matches, true);
        }

        @Override
        public void onRmsChanged(float msdB) {}
    }
}