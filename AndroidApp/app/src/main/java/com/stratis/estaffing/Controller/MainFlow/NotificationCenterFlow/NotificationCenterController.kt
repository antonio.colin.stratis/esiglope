package com.stratis.estaffing.Controller.MainFlow.NotificationCenterFlow

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Core.Preferences
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Fragment.MasterAdminFlow.MARequestSingleCandidatesFragment
import com.stratis.estaffing.Fragment.NotificationCenterFlow.NotificationCenterFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidateMessagesFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidateSingleFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidatesFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestSingleFragment
import com.stratis.estaffing.Listener.NotificationCenterFragmentListener
import com.stratis.estaffing.Model.NotificationCenterFlow.NotificationCenterModel
import com.stratis.estaffing.Model.Profile
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Interview.RequestCandidateInterview
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate
import com.stratis.estaffing.R

class NotificationCenterController : AppCompatActivity(), NotificationCenterFragmentListener {

    private var currentNotification:NotificationCenterModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_center_controller)

        // --
        var notifications:ArrayList<NotificationCenterModel>? = null
        val fragment = NotificationCenterFragment()

        // -- Check dta by intent
        if (intent != null && intent.hasExtra("notifications")) {

            notifications = intent.getSerializableExtra("notifications") as ArrayList<NotificationCenterModel>?
            fragment.notifications = notifications
            this.loadFragment(fragment)

        } else if (intent != null && intent.hasExtra("notification")) {

            val notification = intent.getSerializableExtra("notification") as NotificationCenterModel
            this.showSpecificFragment(notification)
        }
    }

    private fun showSpecificFragment(notification:NotificationCenterModel) {

        // --
        this.currentNotification = notification

        // --
        val profile = Profile()
        profile.provisionalId = notification.getParameter("vacancyId")

        // --
        when(notification.clickActionName) {

            NotificationCenterModel.toVacancyDetail -> {

                val recruitmentRequestSingleFragment = RecruitmentRequestSingleFragment(profile)
                recruitmentRequestSingleFragment.setNotificationCenterListener(this)
                this.loadFragment(recruitmentRequestSingleFragment)
            }

            NotificationCenterModel.toCandidateList -> {

                val session = Session.get(this)
                if (session.type == Session.SessionRolType.masterAdmin) {

                    val candidatesMAFragment = MARequestSingleCandidatesFragment()
                    candidatesMAFragment.profile = profile
                    candidatesMAFragment.notificationCenterListener = this
                    this.loadFragment(candidatesMAFragment)

                } else {

                    val candidatesFragment = RecruitmentRequestCandidatesFragment()
                    candidatesFragment.setProfile(profile)
                    candidatesFragment.setNotificationCenterListener(this)
                    this.loadFragment(candidatesFragment)
                }
            }

            NotificationCenterModel.toCandidateProfile -> {

                val candiate = RequestCandidate()
                candiate.id = notification.getParameter("talentId")

                val candidateSingleFragment = RecruitmentRequestCandidateSingleFragment()
                candidateSingleFragment.setProfile(profile)
                candidateSingleFragment.setCandidate(candiate)
                candidateSingleFragment.setNotificationCenterListener(this)
                this.loadFragment(candidateSingleFragment)
            }

            NotificationCenterModel.toBoard -> {

                val candiate = RequestCandidate()
                val interview = RequestCandidateInterview()

                candiate.id = notification.getParameter("talentId")
                candiate.vacancyCandidateId = notification.getParameter("vacancyCandidateId")
                interview.candidate = candiate

                val messagesFragment = RecruitmentRequestCandidateMessagesFragment()
                messagesFragment.setInterview(interview)
                this.loadFragment(messagesFragment)
            }
        }
    }

    private fun loadFragment(fragment: ABFragment) {

        this.supportFragmentManager.
                beginTransaction().
                replace(R.id.fragment_container, fragment).
                commit()
        return
    }

    // --
    // --
    override fun onVacancyNotAvailable() {

        val preferences = Preferences(this)
        val message = String.format(this.getString(R.string.notification_center_vacancy_not_available),
                currentNotification?.getParameter("vacancyId"))
        preferences.savePreference("pendingErrorNotification", message)
        this.finish()
    }

    /** Delete TODO
    override fun onVacancyPaused() {

        val preferences = Preferences(this)
        val message = String.format(this.getString(R.string.notification_center_vacancy_paused),
                currentNotification?.getParameter("vacancyId"))
        preferences.savePreference("pendingNotification", message)
        this.finish()
    }

    override fun onVacancyStopped() {

        val preferences = Preferences(this)
        val message = String.format(this.getString(R.string.notification_center_vacancy_cancel),
                currentNotification?.getParameter("vacancyId"))
        preferences.savePreference("pendingErrorNotification", message)
        this.finish()
    }
     **/

    override fun onCandidateListNotAvailable() {

        val preferences = Preferences(this)
        val message = this.getString(R.string.notification_center_candidates_not_available)
        preferences.savePreference("pendingErrorNotification", message)
        this.finish()
    }


    override fun onCandidateNotAvailable() {

        val preferences = Preferences(this)
        val message = this.getString(R.string.notification_center_candidate_not_available)
        preferences.savePreference("pendingErrorNotification", message)
        this.finish()
    }
}