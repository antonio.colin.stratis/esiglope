package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.View;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class RequestPausedCancelFeedbackView extends RecyclerView.ViewHolder {

    private RelativeLayout box;
    private TextView name;

    public RequestPausedCancelFeedbackView(View itemView) {

        // --
        super(itemView);

        // --
        this.box = itemView.findViewById(R.id.box);
        this.name = itemView.findViewById(R.id.name);
    }

    public RelativeLayout getBox() {
        return box;
    }

    public TextView getName() {
        return name;
    }
}
