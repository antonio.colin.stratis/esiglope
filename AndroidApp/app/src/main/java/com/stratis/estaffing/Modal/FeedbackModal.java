package com.stratis.estaffing.Modal;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 28/07/20
 */

public class FeedbackModal extends Dialog {

    private ImageView icon = null;
    private AutoResizeTextView title = null;
    private AutoResizeTextView content = null;
    private Button closeButton = null;

    private String msgWarning = "";

    public FeedbackModal(Context context) {

        super(context);
        this.init();
    }

    public FeedbackModal(Context context, int themeResId) {

        super(context, themeResId);
        this.init();
    }

    protected FeedbackModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.init();
    }

    @SuppressLint("ResourceType")
    private void init() {

        // -- R styles
        this.setContentView(R.layout.modal_feedback);
        this.setCanceledOnTouchOutside(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow()
                .setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );

        // --
        this.icon = this.findViewById(R.id.icon);
        this.title = this.findViewById(R.id.title);
        this.content = this.findViewById(R.id.content);
        this.closeButton = this.findViewById(R.id.closeButton);

        // --
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        App.setViewAsSecondaryColor(this.closeButton, getContext());
    }

    public void setData(Bitmap icon, String title, String msg) {

        this.icon.setImageBitmap(icon);
        this.title.setText(title);
        this.content.setText(msg);
    }

    public void setSuccess() {
        this.setData(
                getImage("green_check_box"),
                getContext().getResources().getString(R.string.feedback_modal_title_success),
                getContext().getResources().getString(R.string.feedback_modal_content_success)
        );
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat("success"));
    }

    public void setWarning() {
        this.setData(
                getImage("icon_warning"),
                getContext().getResources().getString(R.string.feedback_modal_title_warning),
                getContext().getResources().getString(R.string.feedback_modal_content_warning)
        );
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat("warn"));
    }

    public void setWarningEmpty() {
        this.setData(
                getImage("icon_warning"),
                getContext().getResources().getString(R.string.feedback_modal_title_warning),
                getContext().getResources().getString(R.string.feedback_modal_content_warning_empty)
        );
    }

    // -- Get image
    private Bitmap getImage(String name) {

        int id = getContext().getResources().getIdentifier(name, "drawable", getContext().getPackageName());
        return BitmapFactory.decodeStream(getContext().getResources().openRawResource(id));
    }
}
