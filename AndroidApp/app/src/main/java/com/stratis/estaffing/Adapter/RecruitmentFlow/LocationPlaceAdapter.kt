package com.stratis.estaffing.Adapter.RecruitmentFlow

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.RecruitmentFlow.LocationPlace
import com.stratis.estaffing.R
import com.stratis.estaffing.View.LocationPlaceView

open  class LocationPlaceAdapter (context: Context, items: MutableList<LocationPlace>?) : RecyclerView.Adapter<LocationPlaceView>() {

    private var context: Context? = null
    private var items: MutableList<LocationPlace>? = null

    private var listener: LocationPlaceAdapterListener? = null

    init {
        this.context = context
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationPlaceView {
        val layoutView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.component_location_place, parent, false)
        return LocationPlaceView(layoutView)
    }

    @SuppressLint("SetTextI18n", "ClickableViewAccessibility")
    override fun onBindViewHolder(holder: LocationPlaceView, position: Int) {
        holder.setIsRecyclable(false)
        val locationPlace : LocationPlace = this.items!![position]

        holder.locationPlaceTitle!!.text = locationPlace.name
        holder.locationPlaceSubtitle!!.text = locationPlace.description //locationPlace.description

        holder.locationPlaceContainer!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                locationPlace.selected = !locationPlace.selected
                updateItemsBackground(locationPlace.name)
                listener?.onClick(position)
            }
        })

        holder.locationCreatedByUser!!.isGone = !locationPlace.createdByUser

        if(locationPlace.selected) {
            holder.locationPlaceContainer!!.setBackground(ContextCompat.getDrawable(context!!, R.drawable.request_vacancy_selected_button))
            App.setViewAsSecondaryColor(holder.locationPlaceContainer!!, context)
            holder.locationPlaceRadio!!.setBackground(ContextCompat.getDrawable(context!!, R.drawable.select_white_option))
            holder.locationPlaceTitle!!.setTextColor(ContextCompat.getColor(context!!, R.color.white))
            holder.locationPlaceSubtitle!!.setTextColor(ContextCompat.getColor(context!!, R.color.white))
        } else {
            holder.locationPlaceContainer!!.setBackground(ContextCompat.getDrawable(context!!, R.drawable.white_border_10))
            holder.locationPlaceRadio!!.setBackground(ContextCompat.getDrawable(context!!, R.drawable.empty_circle))
            holder.locationPlaceTitle!!.setTextColor(ContextCompat.getColor(context!!, R.color.black))
            holder.locationPlaceSubtitle!!.setTextColor(ContextCompat.getColor(context!!, R.color.black))
        }

    }

    private fun updateItemsBackground(name : String) {
        items!!.forEach {
            if(it.name.equals(name)) return@forEach
            it.selected = it.name.equals(name)
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    fun setListener(listener: LocationPlaceAdapter.LocationPlaceAdapterListener) {
        this.listener = listener
    }

    interface LocationPlaceAdapterListener {
        fun onClick(position: Int)
    }
}