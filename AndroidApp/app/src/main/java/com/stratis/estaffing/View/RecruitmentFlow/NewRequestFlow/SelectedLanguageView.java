package com.stratis.estaffing.View.RecruitmentFlow.NewRequestFlow;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez - App Builders CTO
 * Revision 1 - 01/09/20
 */

public class SelectedLanguageView extends RecyclerView.ViewHolder {

    private RelativeLayout box;
    private TextView title;
    private Button levelButton;
    private ImageButton closeButton;

    public SelectedLanguageView(View itemView) {

        // --
        super(itemView);

        // --
        this.box = itemView.findViewById(R.id.box);
        this.title = itemView.findViewById(R.id.title);
        this.levelButton = itemView.findViewById(R.id.levelButton);
        this.closeButton = itemView.findViewById(R.id.close);
    }

    public RelativeLayout getBox() {
        return box;
    }

    public TextView getTitle() {
        return title;
    }

    public Button getLevelButton() {
        return levelButton;
    }

    public ImageButton getCloseButton() {
        return closeButton;
    }
}
