package com.stratis.estaffing.Model.NotificationCenterFlow

import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Erick Sanchez
 * Revision 1 - 24/05/21
 */
open class NotificationCenterModel : Serializable {

    var id:String = ""
    var notificationType:String = ""
    var notificationTypeId:String = ""
    var message:String = ""
    var createdDate:String = ""
    var viewed:Boolean = false
    var parameters:String = ""
    var categoryId:String = ""
    var categoryName:String = ""
    var priorityId:String = ""
    var priorityName:String = ""
    var title:String = ""
    var clickActionId:String = ""
    var clickActionName:String = ""
    var iconId:String = ""
    var iconName:String = ""

    constructor(obj:JSONObject?) {

        if (obj != null) {

            this.id = obj.optString("notificationId")
            this.notificationTypeId = obj.optString("notificationTypeId")
            this.notificationType = obj.optString("notificationType")
            this.message = obj.optString("message")
            this.createdDate = obj.optString("createdDate")
            this.viewed = obj.optBoolean("viewed")
            this.parameters = obj.optString("parameters")
            this.categoryId = obj.optString("categoryId")
            this.categoryName = obj.optString("categoryName")
            this.priorityId = obj.optString("priorityId")
            this.priorityName = obj.optString("priorityName")
            this.title = obj.optString("title")
            this.clickActionId = obj.optString("clickActionId")
            this.clickActionName = obj.optString("clickActionName")
            this.iconId = obj.optString("iconId")
            this.iconName = obj.optString("iconName")
        }
    }

    open fun getParameter(param: String): String? {
        val params = parameters.split(",").toTypedArray()
        for (paramPart in params) {
            val paramValues: Array<String?> = paramPart.split(":").toTypedArray()
            val pV = paramValues[0]!!.replace(" ".toRegex(), "")
            if (paramValues.size > 0 && pV == param && paramValues[1] != null) {
                return paramValues[1]!!.replace(" ".toRegex(), "")
            }
        }
        return ""
    }

    companion object {

        const val notificationCategoryTalent = "1002200001"
        const val notificationCategoryVacancies = "1002200002"
        const val notificationCategoryInterviews = "1002200003"
        const val notificationCategoryCandidate = "1002200004"

        const val notificationPriorityHigh = "1002500001"
        const val notificationPriorityMedium = "1002500002"
        const val notificationPriorityDown = "1002500003"

        const val toVacancyDetail = "to_vacancy_detail"
        const val toCandidateList = "to_candidate_list"
        const val toBoard = "to_board"
        const val toCandidateProfile = "to_candidate_profile"

        fun parse(array: JSONArray?): ArrayList<NotificationCenterModel>? {

            if (array != null) {
                val notifications = ArrayList<NotificationCenterModel>()
                for (i in 0 until array.length()) {
                    val obj = array.optJSONObject(i)
                    notifications.add(NotificationCenterModel(obj))
                }
                return notifications
            }
            return null
        }

        fun howManyNotSeen(array: ArrayList<NotificationCenterModel>?): Int {

            if (array != null) {
                var ret: Int = 0
                for (i in 0 until array.size) {

                    val not = array[i]
                    if (!not.viewed) {
                        ret++
                    }
                }
                return ret
            }
            return 0
        }

        fun sort(array: ArrayList<NotificationCenterModel>?): ArrayList<NotificationCenterModel> {

            var notSeenNotifications:ArrayList<NotificationCenterModel> = ArrayList()
            var seenNotifications:ArrayList<NotificationCenterModel> = ArrayList()

            // --
            if (array != null) {
                for (i in 0 until array.size) {
                    val notification = array[i]
                    if (notification.viewed) {
                        seenNotifications.add(notification)
                    } else {
                        notSeenNotifications.add(notification)
                    }
                }
            }

            // --
            notSeenNotifications.sortByDescending { it.createdDate }
            seenNotifications.sortByDescending { it.createdDate }

            // --
            notSeenNotifications.addAll(seenNotifications)
            return notSeenNotifications
        }
    }
}