package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.InterviewFlow;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Control.ScreenView;
import com.stratis.estaffing.Control.TimePicker.TimePickerDialog;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidateSingleFragment;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Interview.RequestCandidateInterview;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Interview.InterviewService;

import org.json.JSONObject;

import java.util.Calendar;

public class RecruitmentRequestCreateInterviewFragment extends ABFragment implements View.OnClickListener {

    public enum RecruitmentRequestCreateInterviewType { create, edit, createAgain }

    private RequestCandidateInterview interview;
    private RecruitmentRequestCreateInterviewListener listener;
    private RecruitmentRequestCreateInterviewType type = RecruitmentRequestCreateInterviewType.create;

    private RelativeLayout interviewCalendarBox;
    private DatePickerDialog picker = null;

    private Button interviewInPerson;
    private Button interviewInRemote;
    private Button interviewByMe;
    private Button interviewByOther;
    private boolean inPersonState;
    private Boolean interviewByMeState;

    private Button cancelInterviewCheck;
    private boolean cancelInterviewState = false;
    private TextInputEditText commentInput;

    private LoadingButton actionFragmentButton;

    public RecruitmentRequestCreateInterviewFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_create_interview, container, false);
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName().concat(type.toString()));

        this.setHeader("");
        this.initViews();
        this.initCalendarButton();
        this.checkInterview();
        this.canActivateFooterButton();

        return this.mView;
    }

    private void initViews() {

        // -- Title
        final String titleString = (this.type == RecruitmentRequestCreateInterviewType.create ||
                this.type == RecruitmentRequestCreateInterviewType.createAgain) ?
                getString(R.string.recruitment_request_candidate_create_interview) :
                getString(R.string.recruitment_request_candidate_create_edit_interview);
        final TextView title = this.mView.findViewById(R.id.title);
        title.setText( titleString );

        final TextView candidateContainerTitle = this.mView.findViewById(R.id.candidateContainerTitle);
        final String candidateContainerTitleString =
                String.format( getString(R.string.recruitment_request_candidate_create_interview_candidate_container_title),
                        this.interview.getCandidate().getFullNameShort());
        candidateContainerTitle.setText( candidateContainerTitleString );

        // -- Date Title
        final TextView dateTitle = this.mView.findViewById(R.id.dateTitle);
        final String dateTitleString =
                String.format( getString(R.string.recruitment_request_candidate_create_interview_date_title),
                App.capitalize(this.interview.getCandidate().getFirstNameShort()) );
        dateTitle.setText( dateTitleString );

        // -- Buttons
        this.interviewInPerson = this.mView.findViewById(R.id.interviewInPerson);
        this.interviewInPerson.setTag("10");
        this.interviewInPerson.setOnClickListener(this);

        this.interviewInRemote = this.mView.findViewById(R.id.interviewInRemote);
        this.interviewInRemote.setTag("11");
        this.interviewInRemote.setOnClickListener(this);

        this.interviewByMe = this.mView.findViewById(R.id.interviewByMe);
        this.interviewByMe.setTag("12");
        this.interviewByMe.setOnClickListener(this);

        this.interviewByOther = this.mView.findViewById(R.id.interviewByOther);
        this.interviewByOther.setTag("13");
        this.interviewByOther.setOnClickListener(this);

        this.actionFragmentButton = new LoadingButton(this.mView.findViewById(R.id.actionFragmentButton));
        this.actionFragmentButton.getButton().setTag("15");
        this.actionFragmentButton.getButton().setOnClickListener(this);
        this.actionFragmentButton.setText(getString(R.string.recruitment_request_candidate_create_interview_agend_button));
        this.setViewAsPrimaryColor(this.actionFragmentButton.getButton());
    }

    private void initCalendarButton() {

        this.interviewCalendarBox = this.mView.findViewById(R.id.interviewCalendarBox);
        interviewCalendarBox.setOnClickListener(v -> {

            if (picker == null) {

                final Calendar sCalendar = Calendar.getInstance();
                final ScreenView screenView = ScreenView.getInstance(getContext());
                picker = new DatePickerDialog(getContext(), (view, year, month, dayOfMonth) -> {

                    month = month + 1;
                    String date = String.format("%s/%s/%s", dayOfMonth,  month, year);
                    interview.setDateDate(date);

                    final boolean isToday = (sCalendar.get(Calendar.YEAR) == year &&
                            sCalendar.get(Calendar.MONTH) == (month-1) &&
                            sCalendar.get(Calendar.DAY_OF_MONTH) == dayOfMonth);
                    openClock(isToday);

                }, sCalendar.get(Calendar.YEAR), sCalendar.get(Calendar.MONTH), sCalendar.get(Calendar.DAY_OF_MONTH));
                picker.getWindow().setLayout( screenView.getX() - screenView.threeRuleX(5), screenView.getY()/5);
                picker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            }
            picker.show();
        });
    }

    private void openClock(boolean isToday) {

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog dialog = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {

            @SuppressLint("DefaultLocale")
            final String curMin = String.format("%02d:%02d", hourOfDay, minute);
            interview.setDateTime(curMin);
            setDateLabels();
        }, mHour, mMinute, true);
        dialog.setAccentColor(Color.parseColor("#001330"));

        // --
        if (isToday) { dialog.setMinTime(mHour+1, 0, 0); }
        dialog.show(getFragmentManager(), "");
    }

    private void setDateLabels() {

        String date = (!interview.getDateDate().equals("") && !interview.getDateTime().equals("")) ? interview.getDateDate() : "";
        if (!date.equals("")) {

            date = App.getDateFormatted(date, "dd MMMM yyyy", "dd/M/yyyy");
            date = date + " - " + interview.getDateTime() + "hrs";

            this.interviewCalendarBox.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.interviewCalendarBox);
            ((TextView) this.interviewCalendarBox.findViewById(R.id.calendarBoxText)).setTextColor(  ContextCompat.getColor(mContext, R.color.white) );
            ((TextView) this.interviewCalendarBox.findViewById(R.id.calendarBoxText)).setText( date );
            ((ImageView) this.interviewCalendarBox.findViewById(R.id.calendarBoxIcon)).setImageBitmap( App.getImage(mContext, "time_white") );

        } else {

            this.interviewCalendarBox.setBackgroundDrawable( ContextCompat.getDrawable(getActivity(), R.drawable.request_vacancy_unselected_button) );
            ((TextView) this.interviewCalendarBox.findViewById(R.id.calendarBoxText)).setTextColor(  ContextCompat.getColor(getActivity(), R.color.recruitment_request_vacancy_modality_gray) );
            ((TextView) this.interviewCalendarBox.findViewById(R.id.calendarBoxText)).setText("");
            ((ImageView) this.interviewCalendarBox.findViewById(R.id.calendarBoxIcon)).setImageBitmap( App.getImage(getContext(), "time_gray") );
        }
        // --
        this.canActivateFooterButton();
    }

    private void checkInterview() {

        if (interview.getRemote() != null && this.type == RecruitmentRequestCreateInterviewType.edit) {

            this.actionFragmentButton.setText( getString(R.string.recruitment_request_candidate_create_interview_re_agend_button) );

            this.setDateLabels();
            this.inPersonState = !interview.getRemote();
            this.changeInPersonButtonState();

            final TextInputEditText input = (this.inPersonState) ?
                    this.mView.findViewById(R.id.interviewInPersonInput) :
                    this.mView.findViewById(R.id.interviewInRemoteInput);
            input.setText( this.interview.getLocationAddress() );

            // -- WHO Interview
            final Session session = Session.get(getContext());
            if (this.interview.getInterviewerEmail().equals(session.getUsername())) {

                this.interviewByMeState = true;
                this.changeWhoInterviewButtonState();

            } else {

                this.interviewByMeState = false;
                this.changeWhoInterviewButtonState();

                final TextInputEditText name = this.mView.findViewById(R.id.interviewByOtherNameInput);
                name.setText( this.interview.getInterviewerName() );

                final TextInputEditText email = this.mView.findViewById(R.id.interviewByOtherEmailInput);
                email.setText( this.interview.getInterviewerEmail() );
            }

            // -- Cancel Interview
            this.mView.findViewById(R.id.cancelInterviewTitle).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.cancelInterviewContent).setVisibility(View.VISIBLE);

            this.cancelInterviewCheck = this.mView.findViewById(R.id.cancelInterviewCheck);
            this.cancelInterviewCheck.setVisibility(View.VISIBLE);
            this.cancelInterviewCheck.setTag("20");
            this.cancelInterviewCheck.setOnClickListener(this);

            // -- Comment Interview
            this.mView.findViewById(R.id.commentInterviewTitle).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.commentInterviewContent).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.commentInterviewTitleInput).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.commentInterviewInputContainer).setVisibility(View.VISIBLE);

            this.commentInput = this.mView.findViewById(R.id.commentInterviewInput);
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeInPersonButtonState() {

        this.interview.setRemote(!this.inPersonState);
        if (this.inPersonState) {

            this.interviewInPerson.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.interviewInPerson);
            this.interviewInPerson.setTextColor(Color.WHITE);
            this.interviewInPerson.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.select_white_option ), null);

            this.interviewInRemote.setBackgroundDrawable( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            this.interviewInRemote.setTextColor(Color.parseColor("#001330"));
            this.interviewInRemote.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.empty_circle ), null);

            // -- Show elements
            this.mView.findViewById(R.id.interviewInPersonSubTitle).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.interviewInPersonInputContainer).setVisibility(View.VISIBLE);
            // -- Hide elements
            this.mView.findViewById(R.id.interviewInRemoteSubTitle).setVisibility(View.GONE);
            this.mView.findViewById(R.id.interviewInRemoteInputContainer).setVisibility(View.GONE);
            this.mView.findViewById(R.id.interviewInRemoteInputExample).setVisibility(View.GONE);

        } else {

            this.interviewInPerson.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            this.interviewInPerson.setTextColor(Color.parseColor("#001330"));
            this.interviewInPerson.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.empty_circle ), null);

            this.interviewInRemote.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.interviewInRemote);
            this.interviewInRemote.setTextColor(Color.WHITE);
            this.interviewInRemote.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.select_white_option ), null);

            // -- Show elements
            this.mView.findViewById(R.id.interviewInRemoteSubTitle).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.interviewInRemoteInputContainer).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.interviewInRemoteInputExample).setVisibility(View.VISIBLE);
            // -- Hide elements
            this.mView.findViewById(R.id.interviewInPersonSubTitle).setVisibility(View.GONE);
            this.mView.findViewById(R.id.interviewInPersonInputContainer).setVisibility(View.GONE);
        }
        this.canActivateFooterButton();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeWhoInterviewButtonState() {

        if (this.interviewByMeState) {

            this.interviewByMe.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.interviewByMe);
            this.interviewByMe.setTextColor(Color.WHITE);
            this.interviewByMe.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.select_white_option ), null);

            this.interviewByOther.setBackgroundDrawable( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            this.interviewByOther.setTextColor(Color.parseColor("#001330"));
            this.interviewByOther.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.empty_circle ), null);

            // -- Hide elements
            this.mView.findViewById(R.id.interviewByOtherNameTitle).setVisibility(View.GONE);
            this.mView.findViewById(R.id.interviewByOtherNameContainer).setVisibility(View.GONE);
            this.mView.findViewById(R.id.interviewByOtherEmailTitle).setVisibility(View.GONE);
            this.mView.findViewById(R.id.interviewByOtherEmailContainer).setVisibility(View.GONE);

        } else {

            this.interviewByMe.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            this.interviewByMe.setTextColor(Color.parseColor("#001330"));
            this.interviewByMe.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.empty_circle ), null);

            this.interviewByOther.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.interviewByOther);
            this.interviewByOther.setTextColor(Color.WHITE);
            this.interviewByOther.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.select_white_option ), null);

            // -- Show elements
            this.mView.findViewById(R.id.interviewByOtherNameTitle).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.interviewByOtherNameContainer).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.interviewByOtherEmailTitle).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.interviewByOtherEmailContainer).setVisibility(View.VISIBLE);
        }
        this.canActivateFooterButton();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeCancelButtonStateInEdit() {

        if (this.cancelInterviewState) {

            this.cancelInterviewCheck.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.cancelInterviewCheck);
            this.cancelInterviewCheck.setTextColor(Color.WHITE);
            this.cancelInterviewCheck.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable( R.drawable.checkbox_white ), null);

            // --
            this.actionFragmentButton.setText( getString(R.string.recruitment_request_candidate_create_interview_re_agend_button) );

        } else {

            this.cancelInterviewCheck.setBackgroundDrawable( ContextCompat.getDrawable(getContext(), R.drawable.request_vacancy_unselected_button) );
            this.cancelInterviewCheck.setTextColor(Color.parseColor("#001330"));
            this.cancelInterviewCheck.setCompoundDrawablesWithIntrinsicBounds(null, null, getContext().getResources().getDrawable( R.drawable.check_figure ), null);

            // --
            this.actionFragmentButton.setText( getString(R.string.recruitment_request_candidate_create_interview_re_agend_button) );
        }
        this.canActivateFooterButton();
    }

    @Override
    public void onClick(View v) {

        final String tag = String.valueOf(v.getTag());
        switch (tag) {

            case "10":

                this.inPersonState = true;
                this.changeInPersonButtonState();
                break;

            case "11":

                this.inPersonState = false;
                this.changeInPersonButtonState();
                break;

            case "12":

                this.interviewByMeState = true;
                this.changeWhoInterviewButtonState();
                break;

            case "13":

                this.interviewByMeState = false;
                this.changeWhoInterviewButtonState();
                break;

            case "15":
                if (this.cancelInterviewState) {
                    this.cancelInterview();
                } else {
                    this.createInterview();
                }
                break;

            case "20":
                this.cancelInterviewState = !this.cancelInterviewState;
                this.changeCancelButtonStateInEdit();
                break;
        }
    }

    private void createInterview() {

        // --
        final Session session = Session.get(getContext());

        // --
        if (this.inPersonState) {

            final String inPersonString = String.valueOf(((TextInputEditText)
                    this.mView.findViewById(R.id.interviewInPersonInput)).getText());
            this.interview.setLocationAddress( inPersonString );

        } else {

            final String inRemoteString = String.valueOf(((TextInputEditText)
                    this.mView.findViewById(R.id.interviewInRemoteInput)).getText());
            this.interview.setLocationAddress( inRemoteString );
        }

        // --
        if (this.interviewByMeState) {

            this.interview.setInterviewerName( session.getFirstName() + " " + session.getLastName() );
            this.interview.setInterviewerEmail( session.getUsername() );

        } else {

            final String nameString = String.valueOf(((TextInputEditText)
                    this.mView.findViewById(R.id.interviewByOtherNameInput)).getText());
            final String emailString = String.valueOf(((TextInputEditText)
                    this.mView.findViewById(R.id.interviewByOtherEmailInput)).getText());

            // -- Error if its bad email
            if (!App.isEmailValid(emailString)) {

                this.showError(this.mView.findViewById(R.id.interviewByOtherEmailTitle),
                        this.mView.findViewById(R.id.interviewByOtherEmailContainer),
                        this.mView.findViewById(R.id.interviewByOtherEmailError),
                        true);

                final Notification notification = new Notification(getContext(), R.layout.notification_light_error);
                notification.setMessage(getString(R.string.recruitment_request_candidate_single_interview_notification_field_error));
                notification.show(4000);
                return;
            }

            // --
            this.interview.setInterviewerName( nameString );
            this.interview.setInterviewerEmail( emailString );
        }

        // --
        this.actionFragmentButton.showLoading();
        if (this.interview.getId().equals("")) {

            InterviewService.create(this.interview, session, new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {

                    // --
                    Bundle parameters = new Bundle();
                    final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
                    mFirebaseAnalytics.logEvent("Entrevistas_Agendadas", parameters);

                    // --
                    actionFragmentButton.hideLoading();
                    if (listener != null) {
                        listener.onScheduledSuccess(interview);
                        FirebaseEvent.logEvent(getContext(), RecruitmentRequestCandidateSingleFragment.class.getSimpleName().concat("ivwp"));
                        back();
                    }
                }

                @Override
                public void onError(ReSTResponse response) {

                    actionFragmentButton.hideLoading();
                    JSONObject res = JsonBuilder.stringToJson(response.body);
                    String msg = "Ha ocurrido un error, intenta más tarde";
                    if (res != null) {
                        msg = res.optString("msg", "Ha ocurrido un error, intenta más tarde");
                    }

                    // --
                    final Notification notification = new Notification(getContext(), R.layout.notification_light_error);
                    notification.setMessage(msg);
                    notification.show(4000);
                }
            });

        } else {
            this.rescheduleInterview();
        }
    }

    private void cancelInterview() {

        final String comment = String.valueOf(this.commentInput.getText());
        this.actionFragmentButton.showLoading();
        InterviewService.updateStatus(this.interview,
                "", comment, RequestCandidateInterview.interviewCancelled, new ReSTCallback() {
                    @Override
                    public void onSuccess(ReSTResponse response) {

                        actionFragmentButton.hideLoading();
                        if (listener != null) {
                            listener.onCancel(interview);
                            back();
                        }
                    }

                    @Override
                    public void onError(ReSTResponse response) {

                        actionFragmentButton.hideLoading();
                        JSONObject res = JsonBuilder.stringToJson(response.body);
                        String msg = "Ha ocurrido un error, intenta más tarde";
                        if (res != null) {
                            msg = res.optString("msg", "Ha ocurrido un error, intenta más tarde");
                        }

                        // --
                        final Notification notification = new Notification(getContext(), R.layout.notification_light_error);
                        notification.setMessage(msg);
                        notification.show(4000);
                    }
                });
    }

    private void rescheduleInterview() {

        final String comment = String.valueOf(this.commentInput.getText());
        InterviewService.reschedule(this.interview, "", comment, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                actionFragmentButton.hideLoading();
                if (listener != null) {
                    listener.onReScheduledSuccess(interview);
                    FirebaseEvent.logEvent(getContext(), RecruitmentRequestCandidateSingleFragment.class.getSimpleName().concat("ivwu"));
                    back();
                }
            }

            @Override
            public void onError(ReSTResponse response) {

                actionFragmentButton.hideLoading();
                JSONObject res = JsonBuilder.stringToJson(response.body);

                String msg = "Ha ocurrido un error, intenta más tarde";
                if (res != null) {
                    msg = res.optString("msg", "Ha ocurrido un error, intenta más tarde");
                }

                // --
                final Notification notification = new Notification(getContext(), R.layout.notification_light_error);
                notification.setMessage(msg);
                notification.show(4000);
            }
        });
    }

    private void canActivateFooterButton() {

        if (!interview.getDateDate().equals("") && !interview.getDateTime().equals("") && interview.getRemote() != null && interviewByMeState != null) {

            this.actionFragmentButton.getButton().setBackground(mContext.getDrawable(R.drawable.button_paused_blue));
            this.setViewAsPrimaryColor(this.actionFragmentButton.getButton());
            this.actionFragmentButton.getButton().setClickable(true);
            this.actionFragmentButton.getButton().setEnabled(true);

        } else {

            this.actionFragmentButton.getButton().setBackground(getContext().getDrawable(R.drawable.button_paused_gray));
            this.actionFragmentButton.getButton().setClickable(false);
            this.actionFragmentButton.getButton().setEnabled(false);
        }
    }

    private void showError(final TextView title, final TextInputLayout layout, final TextView titleError, final boolean state) {

        if (state) {
            title.setTextColor(this.getResources().getColor(R.color.red));
            layout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_error_border));
            titleError.setVisibility(View.VISIBLE);
        } else {
            title.setTextColor(Color.parseColor("#333333"));
            layout.setBackground(this.getResources().getDrawable(R.drawable.input_layout_border));
            titleError.setVisibility(View.GONE);
        }
    }

    public void setInterview(RequestCandidateInterview interview) {
        this.interview = interview;
    }

    public void setListener(RecruitmentRequestCreateInterviewListener listener) {
        this.listener = listener;
    }

    public void setType(RecruitmentRequestCreateInterviewType type) {
        this.type = type;
    }

    public interface RecruitmentRequestCreateInterviewListener {
        void onScheduledSuccess(RequestCandidateInterview interview);
        void onReScheduledSuccess(RequestCandidateInterview interview);
        void onCancel(RequestCandidateInterview interview);
        void onCreateNewAgain(RequestCandidateInterview interview);
    }
}