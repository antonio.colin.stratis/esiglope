package com.stratis.estaffing.Controller.RecruitmentFlow

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.*
import com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow.RequestVacancyModalityFragment
import com.stratis.estaffing.Model.Profile
import com.stratis.estaffing.R
import androidx.core.content.ContextCompat
import com.stratis.estaffing.Control.ABActivity

import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateLocationFragment

class RequestVacancyLocationController : ABActivity() {

    private var profile : Profile? = null
    private var currentFragment = 1
    private var saveLocationButton: Button? = null
    private var recruitmentRequestCandidateLocationFragment : RecruitmentRequestCandidateLocationFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recruitment_location_controller)

        init()

        recruitmentRequestCandidateLocationFragment = RecruitmentRequestCandidateLocationFragment()
        if(intent != null && intent.hasExtra("profile")) {
            profile = intent.getSerializableExtra("profile") as Profile?
            recruitmentRequestCandidateLocationFragment!!.setProfilePassed(profile)
        }
        recruitmentRequestCandidateLocationFragment!!.setLocationFragmentListener(changeButtonStyle())
        this.loadFragment(recruitmentRequestCandidateLocationFragment!!)

    }

    private fun init() {
        saveLocationButton = findViewById(R.id.saveLocationButton)
        setViewAsSecondaryColor(saveLocationButton!!)
        saveLocationButton!!.setOnClickListener(buildOnClickEvent())
    }

    private fun loadFragment(fragment: ABFragment) {
        this.supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()
        return
    }

    private fun buildOnClickEvent() : View.OnClickListener {
        return object : View.OnClickListener {
            override fun onClick(v: View?) {
                when (currentFragment) {
                    1 -> back()
                    2 -> backSave()
                }
            }

        }
    }

    private fun changeButtonStyle() : RecruitmentRequestCandidateLocationFragment.LocationFragmentListener {
        return object : RecruitmentRequestCandidateLocationFragment.LocationFragmentListener {
            override fun changeButtonStyle() {
                saveLocationButton!!.background = ContextCompat.getDrawable(baseContext, R.drawable.white_border_10_line)
                saveLocationButton!!.setTextColor(Color.parseColor("#767676"))
                saveLocationButton!!.setText(R.string.request_vacancy_location_place_save_button)
                currentFragment = 2
            }
        }
    }

    private fun back() {
        val intent = Intent(this, RequestVacancyModalityFragment::class.java)
        intent.putExtra("profile", recruitmentRequestCandidateLocationFragment!!.profile)
        setResult(4152, intent)
        finish()
    }

    private fun backSave() {
        saveLocationButton!!.background = ContextCompat.getDrawable(baseContext, R.drawable.activity_login_controller_login_button)
        setViewAsSecondaryColor(saveLocationButton!!)
        saveLocationButton!!.setTextColor(ContextCompat.getColor(baseContext, R.color.white))
        saveLocationButton!!.setText(R.string.request_vacancy_location_save_button)
        currentFragment = 1
        onBackPressed()
    }


}