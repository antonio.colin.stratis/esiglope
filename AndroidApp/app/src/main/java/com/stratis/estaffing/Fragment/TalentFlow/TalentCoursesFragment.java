package com.stratis.estaffing.Fragment.TalentFlow;

import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.stratis.estaffing.Adapter.Talent.CourseAdapter;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Model.Talent;
import com.stratis.estaffing.R;

public class TalentCoursesFragment extends ABFragment {

    public Talent talent = null;

    public TalentCoursesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // --
        this.mView = inflater.inflate(R.layout.fragment_talent_courses, container, false);

        // --
        this.initViews();

        // --
        return this.mView;
    }

    private void initViews() {

        // --
        final RelativeLayout noCourses = this.mView.findViewById(R.id.noCourses);
        final RecyclerView coursesList = this.mView.findViewById(R.id.recyclerView);
        // -- Checking if the talent has courses
        if (this.talent != null && this.talent.getCourses() != null && this.talent.getCourses().size() > 0) {

            // --
            final CourseAdapter adapter = new CourseAdapter(this.talent.getCourses());
            adapter.setContext(getContext());

            // --
            final DividerItemDecoration itemDecorator = new DividerItemDecoration(this.mContext, DividerItemDecoration.VERTICAL);
            itemDecorator.setDrawable(ContextCompat.getDrawable(this.mContext, R.drawable.recycler_devider_20));
            LinearLayoutManager layout = new LinearLayoutManager(this.mContext, LinearLayoutManager.VERTICAL, false);
            // --
            coursesList.getRecycledViewPool().setMaxRecycledViews(0, 2);
            coursesList.setLayoutManager(layout);
            coursesList.addItemDecoration(itemDecorator);
            coursesList.setItemViewCacheSize(8);
            coursesList.setDrawingCacheEnabled(true);
            coursesList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            // --
            coursesList.setAdapter(adapter);

        } else {

            coursesList.setVisibility(View.GONE);
            noCourses.setVisibility(View.VISIBLE);
        }
    }

    // -- ListView modifier -- Issue fix
    public static void setListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {

            view = listAdapter.getView(i, view, listView);if (i == 0) view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight() + 205;
        }
        // --
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        params.height = params.height + 50;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}