package com.stratis.estaffing.Model;

import android.content.Context;

import androidx.core.content.res.ResourcesCompat;

import com.amulyakhare.textdrawable.TextDrawable;
import com.stratis.estaffing.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 28/07/20
 */

public class Feedback {

    private String userFullName = "";
    private String date = "";
    private String strengths = "";
    private String opportunities = "";



    private int starRate = 0;

    public Feedback() {}

    public Feedback(JSONObject obj) {

        if (obj != null) {

            this.userFullName = obj.optString("userFullName", "");
            this.date = obj.optString("date", "");
            this.strengths = obj.optString("strengths", "");
            this.opportunities = obj.optString("opportunities", "");
        }
    }

    public static ArrayList<Feedback> parse(JSONArray array) {

        if (array != null) {

            // --
            JSONArray newArray = new JSONArray();

            // --
            try {
                for (int i = array.length()-1; i>=0; i--) { newArray.put(array.get(i)); }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // --
            ArrayList<Feedback> feedbacks = new ArrayList<>();
            final int max = (newArray.length() <= 4) ? newArray.length() : 4;
            //for (int i = array.length()-1; i>=0; i--) {
            for (int i = 0; i < max; i++) {

                JSONObject obj = newArray.optJSONObject(i);
                feedbacks.add(new Feedback(obj));
            }
            return feedbacks;
        }
        return null;
    }

    // --

    public TextDrawable getInitials(int color, final Context context) {

        String initials = "";
        String [] nameParts = this.userFullName.split(" ");

        //  --
        if (nameParts.length > 0) {
            initials = String.valueOf(nameParts[0].charAt(0));
        }

        // --
        if (nameParts.length > 1) {
            initials = initials + nameParts[1].charAt(0);
        }

        return TextDrawable.builder().beginConfig()
                .bold()
                .toUpperCase()
                .fontSize(40)
                .useFont(ResourcesCompat.getFont(context, R.font.os_bold))
                .bold()
                .endConfig()
                .buildRound(initials, color);
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setStrengths(String strengths) {
        this.strengths = strengths;
    }

    public void setOpportunities(String opportunities) {
        this.opportunities = opportunities;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public String getDate() {
        return date;
    }

    public String getStrengths() {
        return strengths;
    }

    public String getOpportunities() {
        return opportunities;
    }
    public int getStarRate() {       return starRate;    }
    public void setStarRate(int starRate) {        this.starRate = starRate;    }
}