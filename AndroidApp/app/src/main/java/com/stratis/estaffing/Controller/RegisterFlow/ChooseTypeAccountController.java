package com.stratis.estaffing.Controller.RegisterFlow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.R;

public class ChooseTypeAccountController extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_choose_type_account_controller);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName());
        this.initViews();
    }

    private void initViews() {

        final Button personalButton = this.findViewById(R.id.personalButton);
        personalButton.setTag(10);
        personalButton.setOnClickListener(this);

        final Button companyButton = this.findViewById(R.id.companyButton);
        companyButton.setTag(11);
        companyButton.setOnClickListener(this);

        final Button backButton = this.findViewById(R.id.backButton);
        backButton.setTag(12);
        backButton.setOnClickListener(this);
    }

    private void goToRegister(final boolean isPersonal) {

        Intent intent = new Intent(this, RegisterController.class);
        intent.putExtra("isPersonal", isPersonal);
        this.startActivity(intent);
    }

    // --

    @Override
    public void onClick(View v) {

        final int tag = (int) v.getTag();
        switch (tag) {

            case 10:
                this.goToRegister(true);
                break;

            case 11:
                this.goToRegister(false);
                break;

            case  12:
                finish();
                break;
        }
    }
}