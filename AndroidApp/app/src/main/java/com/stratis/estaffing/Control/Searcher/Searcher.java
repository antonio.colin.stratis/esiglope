package com.stratis.estaffing.Control.Searcher;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.Searcher.Adapter.SearcherAdapter;
import com.stratis.estaffing.Control.Searcher.Adapter.SearcherLanguageAdapter;
import com.stratis.estaffing.Control.Searcher.Listener.RecyclerItemClickListener;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageCatalog;
import com.stratis.estaffing.Model.RecruitmentFlow.SimpleProfile;
import com.stratis.estaffing.R;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class Searcher {

    private final RelativeLayout root;
    private final Context mContext;
    private final SearcherType type;
    private SearcherListener listener;
    private Object tag;

    // -- -Search profile
    private ArrayList<SimpleProfile> data = new ArrayList<>();
    private SearcherAdapter adapter;

    // -- Language
    private ArrayList<LanguageCatalog> dataLanguage =  new ArrayList<>();
    private SearcherLanguageAdapter languageAdapter;

    // --
    private RelativeLayout searchComponentBox;
    private EditText searchComponentBoxInput;
    private ImageView searchComponentBoxIcon;
    private RecyclerView searchComponentList;

    public Searcher(Context context, RelativeLayout root, SearcherType type) {

        this.mContext = context;
        this.root = root;
        this.type = type;
    }

    private void initViews() {

        this.searchComponentBox = this.root.findViewById(R.id.searchComponentBox);
        this.searchComponentBoxInput = this.root.findViewById(R.id.searchComponentBoxInput);
        this.searchComponentBoxIcon = this.root.findViewById(R.id.searchComponentBoxIcon);
        this.searchComponentList = this.root.findViewById(R.id.searchComponentList);

        // --
        final DividerItemDecoration itemDecorator = new DividerItemDecoration(this.mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(this.mContext, R.drawable.recycler_devider_5)));

        LinearLayoutManager layout = new LinearLayoutManager(this.mContext, LinearLayoutManager.VERTICAL, false);

        // --
        this.searchComponentList.getRecycledViewPool().setMaxRecycledViews(0, 4);
        this.searchComponentList.setLayoutManager(layout);
        this.searchComponentList.addItemDecoration(itemDecorator);
        this.searchComponentList.setItemViewCacheSize(4);
        this.searchComponentList.setDrawingCacheEnabled(true);
        this.searchComponentList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // --  Adapter magics
        if (this.type == SearcherType.normal) {

            this.adapter = new SearcherAdapter(this.data, this.mContext);
            this.searchComponentList.setAdapter(adapter);

        } else if (this.type == SearcherType.language) {

            this.languageAdapter = new SearcherLanguageAdapter( (this.dataLanguage) , this.mContext);
            this.searchComponentList.setAdapter(languageAdapter);
        }

        // --
        if (this.searchComponentBoxInput.getText().toString().equals("")) {
            this.searchComponentList.setVisibility(View.GONE);
        }

        // --
        this.setItemClick();

        // --
        this.setInputListener();

        if (this.adapter != null) {
            this.adapter.setListener(this::cleanSearcher);
        }
    }

    public void cleanSearcher() {
        // -- TextEdit
        searchComponentBoxInput.setTextColor(  ContextCompat.getColor(mContext, R.color.black) );
        // -- Box
        searchComponentBox.setBackground( ContextCompat.getDrawable(mContext, R.drawable.component_search) );
        // -- Icon
        searchComponentBoxIcon.setImageBitmap(App.getImage(mContext, "search" ));
    }

    private void setItemClick() {

        this.searchComponentList.addOnItemTouchListener(
                new RecyclerItemClickListener(this.mContext, this.searchComponentList, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                // --
                Object object = null;
                if (type ==  SearcherType.normal) {
                    object = adapter.getItems().get(position);
                } else if (type == SearcherType.language) {
                    object = languageAdapter.getItems().get(position);
                }

                // --
                if (listener != null) {
                    listener.onItemSelected(object, searchComponentBox, searchComponentList,
                            searchComponentBoxInput, searchComponentBoxIcon);
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {}
        }));
    }

    private void setInputListener() {

        this.searchComponentBoxInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                searchComponentList.setVisibility( (s.length() > 0) ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchComponentList.setVisibility( (s.length() > 0) ? View.VISIBLE : View.GONE);

                // -- Check adapter
                if (type == SearcherType.normal) {
                    adapter.filter(String.valueOf(s));
                } else if (type == SearcherType.language) {
                    languageAdapter.getFilter().filter(String.valueOf(s));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchComponentList.setVisibility( (s.length() > 0) ? View.VISIBLE : View.GONE);
            }
        });
    }

    public void setData(ArrayList<SimpleProfile> data) {

        this.data = data;
        this.initViews();
    }

    public void setDataLanguage(ArrayList<LanguageCatalog> dataLanguage) {

        this.dataLanguage = dataLanguage;
        this.initViews();
    }

    // --
    public void setInputHint(String string) {
        if (this.searchComponentBoxInput != null) {
            this.searchComponentBoxInput.setHint(string);
        }
    }

    public void setListener(SearcherListener listener, Object tag) {
        this.listener = listener;
        this.setTag(tag);
        if (this.searchComponentBox != null) {
            this.searchComponentBox.setTag(tag);
        }
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public Object getTag() {
        return tag;
    }

    public RelativeLayout getRoot() {
        return root;
    }

    public void setSelectedLanguage(ArrayList<LanguageCatalog> array) {
         this.languageAdapter.setSelected(array);
    }

    public RelativeLayout getSearchComponentBox() {
        return searchComponentBox;
    }

    public EditText getSearchComponentBoxInput() {
        return searchComponentBoxInput;
    }

    public ImageView getSearchComponentBoxIcon() {
        return searchComponentBoxIcon;
    }

    public RecyclerView getSearchComponentList() {
        return searchComponentList;
    }

    public SearcherLanguageAdapter getLanguageAdapter() {
        return languageAdapter;
    }

    public enum SearcherType {
         normal, skills, language, soft
    }

    public interface SearcherListener {
        void onItemSelected(Object item, RelativeLayout box, RecyclerView recyclerView, EditText editText, ImageView icon);
        void onItemSelected(Object item, ConstraintLayout box, RecyclerView recyclerView, EditText editText, ImageView icon);
    }
}
