package com.stratis.estaffing.Controller.RegisterFlow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Controller.LoginFlow.LoginController;
import com.stratis.estaffing.Controller.MainFlow.HomeController;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Login.LoginService;

import org.json.JSONObject;

public class RegisterCodeValidationController extends AppCompatActivity implements View.OnClickListener {

    private TextView pinResendCode;
    private LoadingButton verifyButton;

    private String pinCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_register_code_validation_controller);
        this.initViews();
    }

    private void initViews() {

        // --
        final OtpView pinView = this.findViewById(R.id.pinView);
        pinView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                pinCode = otp;
            }
        });

        // --
        this.verifyButton = new LoadingButton((RelativeLayout) this.findViewById(R.id.verifyButton));
        this.verifyButton.setText(this.getResources().getString(R.string.register_code_verification_success_button));
        this.verifyButton.getButton().setTag(10);
        this.verifyButton.getButton().setOnClickListener(this);

        // --
        this.pinResendCode = this.findViewById(R.id.pinResendCode);
        this.pinResendCode.setTag(12);
        this.pinResendCode.setOnClickListener(this);

        // --
        final Button cancelButton = this.findViewById(R.id.cancelButton);
        cancelButton.setTag(11);
        cancelButton.setOnClickListener(this);
    }

    private void verifyCode() {

        this.verifyButton.showLoading();
        this.showPinError(false);

        String email = "";
        if (getIntent().hasExtra("user")) {
            email = getIntent().getStringExtra("user");
        }/* else {
            email = "erick.sanchez@mystratis.com";
        }*/

        if (!email.equals("")) {

            // -- PinCode
            if (this.pinCode.length() < 6 || this.pinCode.equals("")) {

                this.verifyButton.hideLoading();
                this.showPinError(true);
                return;
            }

            LoginService.verifyCode(email, this.pinCode, new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {
                    login();
                }

                @Override
                public void onError(ReSTResponse response) {

                    JSONObject res = JsonBuilder.stringToJson(response.body);
                    String msg = res.optString("msg", "Ha ocurrido un error, intenta más tarde");
                    verifyButton.hideLoading();

                    // --
                    final Notification notification = new Notification(RegisterCodeValidationController.this, R.layout.notification_light_error);
                    notification.setMessage(msg);
                    notification.show(4000);
                }
            });
        } else {
            this.verifyButton.hideLoading();
        }
    }

    private void resendCode() {

        String email = "";
        if (getIntent().hasExtra("user")) {
            email = getIntent().getStringExtra("user");
        }/* else {
            email = "erick.sanchez.p05@gmail.com";
        }*/

        if (!email.equals("")) {

            this.pinResendCode.setEnabled(false);
            this.pinResendCode.setClickable(false);
            LoginService.resendCode(email, new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {

                    Log.d("DXGOP", "RESEND CODE RESPONSE: " + response.body);
                    // --
                    pinResendCode.setEnabled(true);
                    pinResendCode.setClickable(true);

                    // --
                    final Notification notification = new Notification(RegisterCodeValidationController.this, R.layout.notification_light);
                    notification.setMessage(RegisterCodeValidationController.this.getString(R.string.request_vacancy_check_notification_text));
                    notification.show(2500);
                }

                @Override
                public void onError(ReSTResponse response) {

                    Log.d("DXGOP", "RESEND CODE ERROR RESPONSE: " + response.body);
                    JSONObject res = JsonBuilder.stringToJson(response.body);
                    String msg = res.optString("msg", "Ha ocurrido un error, intenta más tarde");

                    // --
                    pinResendCode.setEnabled(true);
                    pinResendCode.setClickable(true);

                    // --
                    final Notification notification = new Notification(RegisterCodeValidationController.this, R.layout.notification_light_error);
                    notification.setMessage(msg);
                    notification.show(4000);
                }
            });
        }
    }

    private void login() {

        if (getIntent().hasExtra("user") && getIntent().hasExtra("password")) {

            final String email = getIntent().getStringExtra("user");
            final String password = getIntent().getStringExtra("password");
            LoginService.login(email, password, new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {

                    Log.d("DXGOP", "LOGIN RESPONSE = " + response.body);
                    JSONObject res = JsonBuilder.stringToJson(response.body);

                    // --
                    if (res != null) {

                        int code = res.optInt("code", 500);
                        if (code == 100) {

                            // --
                            JSONObject entity = res.optJSONObject("entity");
                            Session session = Session.get(RegisterCodeValidationController.this);
                            session.login(entity);
                            checkSession(session);

                        } else {
                            verifyButton.hideLoading();
                        }
                    } else {
                        verifyButton.hideLoading();
                    }
                }

                @Override
                public void onError(ReSTResponse response) {

                    Log.d("DXGOP", "LOGIN ERROR RESPONSE = " + response.body);
                    verifyButton.hideLoading();
                }
            });
        }
    }

    private void checkSession(final Session session) {

        LoginService.refreshToken(session, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {
                Log.d("DXGOP", "REFRESH TOKEN = " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);
                if (res != null) {

                    session.setIdToken(res.optString("entity"));


                }

            }

            @Override
            public void onError(ReSTResponse response) {
                Log.d("DXGOP", "CHECK REFRESH TOKEN ERROR :: " + response.statusCode);
            }
        });

        LoginService.checkDataSession(session, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                Log.d("DXGOP", "CHECK DATA SESSION RESPONSE = " + response.body);
                JSONObject res = JsonBuilder.stringToJson(response.body);
                if (res != null) {

                    if (!res.has("code")) {

                        session.setDataSessionObj(res);
                        // --
                        Intent intent = new Intent(RegisterCodeValidationController.this, HomeController.class);
                        startActivity(intent);
                        finishAffinity();
                    }
                }
            }

            @Override
            public void onError(ReSTResponse response) {

                Log.d("DXGOP", "CHECK DATA SESSION ERROR :: " + response.statusCode);
                verifyButton.hideLoading();
            }
        });
    }

    private void showPinError(boolean state) {

        final TextView pinCodeTittle = this.findViewById(R.id.pinTitle);
        final TextView pinCodeError = this.findViewById(R.id.pinError);
        if (state) {

            pinCodeTittle.setTextColor(this.getResources().getColor(R.color.red));
            pinCodeError.setVisibility(View.VISIBLE);

        } else {

            pinCodeTittle.setTextColor(Color.parseColor("#333333"));
            pinCodeError.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {

        final int tag = (int) v.getTag();
        switch (tag) {

            case 10:
                this.verifyCode();
                break;

            case 12:
                this.resendCode();
                break;

            default:
                finish();
                break;
        }

        /*if (tag == 10) {


        }*/
    }
}