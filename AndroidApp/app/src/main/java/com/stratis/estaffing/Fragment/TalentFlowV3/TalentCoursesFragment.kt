package com.stratis.estaffing.Fragment.TalentFlowV3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.ButtonArrowAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.Course
import com.stratis.estaffing.R
import java.util.*

class TalentCoursesFragment : ABFragment(){

    var courses : ArrayList<Course>? = null
    private var dip : ArrayList<Course>? = null
    private var edu : ArrayList<Course>? = null

    private val lstCourses: ArrayList<Int> = ArrayList()
    private val lstEducations: ArrayList<Int> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mView = inflater.inflate(R.layout.fragment_talent_single_courses, container, false)

        setHeader("")
        initCatalogsCourses()
        initCatalogsEducation()
        this.clasificate()
        this.initViews()

        return this.mView
    }

    private fun initViews() {

        val courses: RecyclerView = mView.findViewById(R.id.course1ContainerList)
        App.createVerticalRecyclerList(courses, context)
        val coursesAdapter= ButtonArrowAdapter(parseCourses(lstCourses))
        coursesAdapter.setListener(object : ButtonArrowAdapter.ButtonArrowClickListener{
            override fun onClick(position: Int) {
                showDetail(position, true)
            }

        })
        courses.adapter = coursesAdapter

        val education: RecyclerView = mView.findViewById(R.id.course2ContainerList)
        App.createVerticalRecyclerList(education, context)
        val educationAdapter= ButtonArrowAdapter(parseCourses(lstEducations))
        educationAdapter.setListener(object : ButtonArrowAdapter.ButtonArrowClickListener{
            override fun onClick(position: Int) {
                showDetail(position, false)
            }

        })
        education.adapter = educationAdapter

    }

    private fun clasificate(){
        dip = splitCourse(lstCourses)
        edu = splitCourse(lstEducations)
    }

    private fun showDetail(position: Int, dip: Boolean){
        val courseFragment = TalentCourseCertificateFragment()
        when(dip){
            true -> courseFragment.course = this.dip!!.get(position)
            else -> courseFragment.course = this.edu!!.get(position)
        }
        this.continueSegue(courseFragment)
    }

    private fun splitCourse(catalogs: ArrayList<Int>): ArrayList<Course> {
        val split: ArrayList<Course> = ArrayList()
        courses?.forEach{
            when( it.categoryId ) {
                in catalogs -> split.add(it)
            }
        }
        return split
    }

    private fun parseCourses(catalogs: ArrayList<Int>): ArrayList<String> {
        val coursesParsed: ArrayList<String> = ArrayList()
        courses?.forEach{
            when( it.categoryId ) {
                in catalogs -> coursesParsed.add(it.name)
            }
        }
        return coursesParsed
    }

    private fun initCatalogsCourses() {
        lstCourses.add(1001900001)
        lstCourses.add(1001900002)
        lstCourses.add(1001900006)
    }

    private fun initCatalogsEducation() {
        lstEducations.add(1001900003)
        lstEducations.add(1001900004)
        lstEducations.add(1001900005)
        lstEducations.add(1001900007)    }
}