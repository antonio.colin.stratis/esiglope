package com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class RequestCandidateView extends RecyclerView.ViewHolder {

    private RelativeLayout shape;
    private ImageView image;
    private TextView seniority;
    private TextView name;
    private TextView speciality;
    private ImageView favorite;
    private TextView rating;

    public RequestCandidateView(View itemView) {

        // --
        super(itemView);

        // --
        this.shape = itemView.findViewById(R.id.shape);
        this.image = itemView.findViewById(R.id.image);
        this.name = itemView.findViewById(R.id.name);
        this.speciality = itemView.findViewById(R.id.speciality);
        this.seniority = itemView.findViewById(R.id.seniority);
        this.favorite = itemView.findViewById(R.id.favorite);
        this.rating = itemView.findViewById(R.id.rating);
    }

    public RelativeLayout getShape() {
        return shape;
    }

    public ImageView getImage() {
        return image;
    }

    public TextView getName() {
        return name;
    }

    public TextView getSpeciality() {
        return speciality;
    }

    public TextView getSeniority() {
        return seniority;
    }

    public ImageView getFavorite() {
        return favorite;
    }

    public TextView getRating() {
        return rating;
    }
}
