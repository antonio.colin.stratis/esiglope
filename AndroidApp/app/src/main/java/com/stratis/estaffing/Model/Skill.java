package com.stratis.estaffing.Model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 27/07/20
 */

public class Skill {

    private String description = "";
    private String name = "";
    private Integer expertiseYears = 0;
    private Integer skillLevelPercentage = 0;
    private Integer categoryId = 0;
    private String categoryName = "";

    public Skill(JSONObject obj) {

        if (obj != null) {

            this.description = obj.optString("description", "");
            this.name = obj.optString("name", "");
            this.expertiseYears = obj.optInt("expertiseYears", 0);
            this.skillLevelPercentage = obj.optInt("skillLevelPercentage", 0);
            this.categoryId = obj.optInt("categoryId", 0);
            this.categoryName = obj.optString("categoryName", "");
        }
    }

    public static ArrayList<Skill> parse(JSONArray array) {

        if (array != null) {

            ArrayList<Skill> skills = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                skills.add(new Skill(obj));
            }
            return skills;
        }
        return null;
    }

    // --

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public Integer getExpertiseYears() {
        return expertiseYears;
    }

    public Integer getSkillLevelPercentage() { return skillLevelPercentage; }

    public Integer getCategoryId() { return categoryId; }

    public String getCategoryName() { return categoryName; }
}