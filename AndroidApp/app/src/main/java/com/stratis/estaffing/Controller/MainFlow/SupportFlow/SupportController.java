package com.stratis.estaffing.Controller.MainFlow.SupportFlow;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Loader;
import com.stratis.estaffing.Control.Social.Mail;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.MainFlow.SupportFlow.SupportModel;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.SupportService;

import org.json.JSONObject;

import java.util.ArrayList;

public class SupportController extends AppCompatActivity {

    private Loader loader;
    private SupportModel supportModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // --
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_support_controller);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName());

        // -- Set header
        this.setHeader(this.getResources().getString(R.string.upload_cv_controller_bar_title));

        // -- Set loader
        this.loader = new Loader(this);
        this.loader.show();

        // -- Get Support info
        this.getSupportInfo();
    }

    protected void getSupportInfo() {

        final Session session = Session.get(this);
        SupportService.getSupportInfo(session, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                loader.cancel();

                JSONObject res = JsonBuilder.stringToJson(response.body);
                supportModel = new SupportModel(res);
                initViews();
            }

            @Override
            public void onError(ReSTResponse response) {

                loader.cancel();
            }
        });
    }

    protected void initViews() {

        if (this.supportModel != null) {

            // -- Email button
            final Button emailButton = this.findViewById(R.id.emailButton);
            emailButton.setOnClickListener(view -> {

                Intent mailIntent = Mail.from(SupportController.this)
                        .to(supportModel.getEmail())
                        .subject("Ayuda con la aplicación")
                        .build();
                startActivity(mailIntent);
            });

            // -- Phone button
            final Button callButton = this.findViewById(R.id.callButton);
            callButton.setOnClickListener(view -> {

                String[] perms = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE};
                Permissions.check(this, perms, getString(R.string.permission_call_text),
                        new Permissions.Options().setRationaleDialogTitle("Info"),
                        new PermissionHandler() {
                            @Override
                            public void onGranted() {

                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:" + supportModel.getPhone()));
                                startActivity(callIntent);
                            }

                            @Override
                            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                                Toast.makeText(context, getString(R.string.permission_call_denied), Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public boolean onBlocked(Context context, ArrayList<String> blockedList) {
                                Toast.makeText(context, getString(R.string.permission_call_denied), Toast.LENGTH_LONG).show();
                                return false;
                            }

                            @Override
                            public void onJustBlocked(Context context, ArrayList<String> justBlockedList, ArrayList<String> deniedPermissions) {
                                Toast.makeText(context, getString(R.string.permission_call_denied), Toast.LENGTH_LONG).show();
                            }
                        });
            });
        }
    }

    private void setHeader(String title) {

        // --
        final ImageButton app_screen_return_arrow = this.findViewById(R.id.app_screen_return_arrow);
        app_screen_return_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // --
        final TextView titleHeader = this.findViewById(R.id.app_screen_title_header);
        titleHeader.setText(title);
    }
}
