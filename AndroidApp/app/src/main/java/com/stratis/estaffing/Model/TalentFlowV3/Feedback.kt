package com.stratis.estaffing.Model.TalentFlowV3

import com.stratis.estaffing.Core.App
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable
import java.util.*

open class Feedback(obj: JSONObject?) :Serializable {

    var id:Long = 0
    var rateValue:Int = 0
    var date: Date? = null
    var dateStr: String = ""
    var authorId: Long = 0
    var authorName: String = ""
    var talentId: Long = 0
    var strengthsComment: String = ""
    var opportunitiesComment: String = ""
    var strengths: ArrayList<FeedbackDetail>? = null
    var opportunities: ArrayList<FeedbackDetail>? = null

    init {
        if (obj != null) {

            this.id = obj.optLong("id")
            this.rateValue = obj.optInt("rateValue")
            this.dateStr = obj.optString("date")
            this.date = App.getDate(dateStr, "yyyy-MM-dd HH:mm:ss")
            this.authorId = obj.optLong("authorId")
            this.authorName = obj.optString("authorName")
            this.talentId = obj.optLong("talentId")
            this.strengthsComment = obj.optString("strengthsComment")
            this.opportunitiesComment = obj.optString("opportunitiesComment")

            val jStrengths: JSONArray = obj.optJSONArray("strengths") as JSONArray
            strengths = FeedbackDetail.parse(jStrengths)!!
            val jOpportunities: JSONArray = obj.optJSONArray("opportunities") as JSONArray
            opportunities = FeedbackDetail.parse(jOpportunities)!!
        }
    }

    companion object {

        fun parse(array: JSONArray?): ArrayList<Feedback>? {

            if (array != null) {
                val feedback = ArrayList<Feedback>()
                for (i in 0 until array.length()) {
                    val obj = array.optJSONObject(i)
                    feedback.add(Feedback(obj))
                }
                return feedback
            }
            return null
        }

    }
}