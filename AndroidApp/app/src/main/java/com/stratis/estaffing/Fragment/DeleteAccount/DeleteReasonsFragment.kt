package com.stratis.estaffing.Fragment.DeleteAccount

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.Configuration.DeleteReasonsAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Model.CatalogItem
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.CatalogService

class DeleteReasonsFragment: ABFragment(), View.OnClickListener {

    private lateinit var buttonContinue: RelativeLayout
    private lateinit var commentValue: EditText
    private lateinit var commentLetter: TextView
    private lateinit var reasonsList: RecyclerView
    private var reasonsItems: ArrayList<CatalogItem> = ArrayList()
    private var reasonsListAdapter: DeleteReasonsAdapter? = null
    private var CATALOG_ID: String = "1000020036"
    private var selectedItem: CatalogItem? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_delete_reasons, container, false)
        setHeader("")

        initViews()
        getReasons()
        return this.mView
    }

    private fun initViews() {

        val buttonCancel: Button = mView.findViewById(R.id.footerCancelButton)
        buttonCancel.tag = "11"
        buttonCancel.setOnClickListener(this)

        buttonContinue = mView.findViewById(R.id.footerContinueButton)
        buttonContinue.tag = "13"
        buttonContinue.setOnClickListener(this)
        setViewColor(buttonContinue, ContextCompat.getColor(mContext, R.color.talent_feedback_disabled))

        reasonsList = mView.findViewById(R.id.reasonsContainerList)

        commentValue = mView.findViewById(R.id.commentValue)
        commentLetter = mView.findViewById(R.id.commentLetter)

        commentValue.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                // -- Size
                val size = s.length.toString()
                val limitString = String.format(getString(R.string.delete_controller_reason_comment), size)
                commentLetter.text = limitString
            }

            override fun afterTextChanged(s: Editable) {}
        })

        // -- Text Limit
        val responsibilitiesLimitString = String.format(getString(R.string.delete_controller_reason_comment), "0")
        this.commentLetter.text = responsibilitiesLimitString
    }

    private fun getReasons() {
        CatalogService.findById(CATALOG_ID, object :
                ReSTCallback {
            @SuppressLint("NotifyDataSetChanged")
            override fun onSuccess(response: ReSTResponse?) {
                val res = JsonBuilder.stringToJsonArray(response!!.body)
                reasonsItems = CatalogItem.parse(res)!!
                setReasons()
                reasonsListAdapter?.notifyDataSetChanged()
            }

            override fun onError(response: ReSTResponse?) {
                Log.d("DXGOP", "ERROR :: " + response!!.body)
            }
        })
    }

    private fun setReasons() {
        App.createVerticalRecyclerList(reasonsList, context)
        reasonsListAdapter = DeleteReasonsAdapter(mContext, reasonsItems, selectedItem)
        reasonsListAdapter!!.setListener(object : DeleteReasonsAdapter.DeleteReasonsAdapterListener {
            @SuppressLint("NotifyDataSetChanged")
            override fun onClick(catalogItem: CatalogItem) {
                selectedItem = catalogItem
                setViewAsPrimaryColor(buttonContinue)
                setReasons()
                reasonsListAdapter!!.notifyDataSetChanged()
            }
        })
        reasonsList.adapter = reasonsListAdapter
    }

    @SuppressLint("ResourceAsColor")
    override fun onClick(v: View?) {
        when(v!!.tag.toString()) {
            "11" -> {
                this.activity?.finish()
                return
            }
            "13" -> {
                if (selectedItem == null) { return }
                val deletePasswordFragment = DeletePasswordFragment()
                deletePasswordFragment.reason = selectedItem!!
                deletePasswordFragment.comment = commentValue.text.toString()
                this.continueSegue(deletePasswordFragment)
            }
        }
    }

}