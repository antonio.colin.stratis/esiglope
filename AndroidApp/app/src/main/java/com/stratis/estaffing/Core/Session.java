package com.stratis.estaffing.Core;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.stratis.estaffing.Control.JsonBuilder;

import org.json.JSONObject;

/**
 * Created by Erick Sanchez
 * Revision 1 - 26/05/20
 */
public class Session {

    public enum SessionRolType { client, talent, company, masterAdmin }

    @SuppressLint({"StaticFieldLeak"})
    private static Session mInstance;

    private Configurations configurations;
    private Context context;

    private String idToken = "";
    private String refreshToken = "";

    private String username = "";
    private String firstName = "";
    private String lastName = "";

    private String secretKey = "";
    private String sessionToken = "";
    private String accessKey = "";

    private String dataSessionId = "-1";

    private JSONObject dataSessionObj = null;
    private JSONObject object = null;
    private SessionRolType type = SessionRolType.client;
    private String clientName = "";

    private String fcmToken = "";

    private SessionPreferences preferences = new SessionPreferences();

    private String localIdentifier = "";

    public static Session get(Context context) {

        if (mInstance == null) {
            Class clazz = Session.class;
            synchronized(clazz) {
                mInstance = new Session(context);
            }
        }
        return mInstance;
    }

    private Session(Context context) {

        this.context = context;
        this.configurations = new Configurations(this.context);
        if (this.isLogged()) {
            this.recoverSession();
            this.parse();
        }
    }

    public void login(JSONObject object) {

        // -- Saving the object
        this.configurations.add("userLogin", JsonBuilder.jsonToString(object));

        Log.d("DXGOP", "================");
        this.configurations.print();
        Log.d("DXGOP", "================");

        // -- Parsing object
        this.object = object;
        this.parse();
    }

    public void logout() {

        if (this.configurations.exists("userLogin")) {
            this.configurations.remove("userLogin");
        }
        this.object = null;
        this.type = SessionRolType.client;
    }

    public boolean isLogged() {
        return this.configurations.exists("userLogin");
    }

    private void recoverSession() {

        JSONObject ret = JsonBuilder.stringToJson( this.configurations.get("userLogin") );
        this.object = ret;
    }

    private void parse() {

        if (this.object != null) {

            // --
            final JSONObject token = this.object.optJSONObject("token");
            if (token != null) {

                this.idToken = token.optString("id_token");
                this.refreshToken = token.optString("refresh_token");
            }

            // --
            final JSONObject user = this.object.optJSONObject("user");
            if (user != null) {

                //Log.d("DXGOP", "USER OBJ :: " + JsonBuilder.jsonToString(user));
                this.username = user.optString("Username");
                this.firstName = user.optString("FirstName");
                this.lastName = user.optString("LastName");
            }

            // -- Files tokens
            final JSONObject credentials = this.object.optJSONObject("credentials");
            if (credentials != null) {
                this.secretKey = credentials.optString("secretKey");
                this.sessionToken = credentials.optString("sessionToken");
                this.accessKey = credentials.optString("accessKeyId");
            }

            //this.name = this.dict.optString("nombre");
            //this.city = this.dict.optString("ciudad");
        }

        // -- FCM TOKEN
        if (this.configurations != null && this.configurations.exists("fcmToken")) {
            this.fcmToken = this.configurations.get("fcmToken");
        }
    }

    public void setDataSessionObj(JSONObject obj) {

        this.dataSessionObj = obj;
        this.dataSessionId = this.dataSessionObj.optString("id", "-1");
        this.configurations.add("lastDataSessionId", this.dataSessionId);

        // -- Has Preferences
        final JSONObject client = this.dataSessionObj.optJSONObject("client");
        if (client != null) {

            // -- Client name
            this.clientName = client.optString("name");

            // -- Preferences
            final JSONObject prefObj = client.optJSONObject("preferences");
            if (prefObj != null) {
                this.preferences = new SessionPreferences(prefObj);
            }
        }

        // -- Extra Data
        this.getRolData();

        // Language Local Identifier
//        final JSONObject language = this.dataSessionObj.optJSONObject("language");
//        if(language!= null) {
//            this.localIdentifier = language.optString("localeIdentifier");
//        }
        /*
         * Local support: https://developer.android.com/guide/topics/resources/multilingual-support
         */
        this.localIdentifier = context.getResources().getConfiguration().locale.toString();
    }

    private void getRolData() {

        final JSONObject rol = this.dataSessionObj.optJSONObject("rol");
        if (rol != null) {
            final String rolId = rol.optString("id", "");
            if (!rolId.equals("")) {
                Log.d("DXGOP", "ROL ID :: " + rolId);
                switch(rolId) {
                    case "1000000001":
                        type = SessionRolType.talent;
                        break;

                    case "1000000002":
                        type = SessionRolType.company;
                        break;

                    case "1000000003":
                        type = SessionRolType.client;
                        break;

                    case "1000000006":
                        type = SessionRolType.masterAdmin;
                        break;
                }
            }
        }
    }

    public void setIdToken(String token){
        this.idToken = token;
    }

    // -- Login class variables {get}

    public String getFcmToken() {
        return fcmToken;
    }

    public String getIdToken() {
        return idToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {

        String[] firstNameParts = this.firstName.split(" ");
        if (firstNameParts.length > 1) {
            return firstNameParts[0];
        } else {
            return firstName;
        }
    }

    public String getFirstNameComplete() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDataSessionId() {

        if (this.dataSessionId.equals("-1")) {
            this.dataSessionId = this.configurations.get("lastDataSessionId");
        }
        return dataSessionId;
    }

    public SessionRolType getType() {
        return type;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setFcmToken(String fcmToken) {

        this.fcmToken = fcmToken;
        if (this.configurations != null) {
            this.configurations.add("fcmToken", this.fcmToken);
        }
    }

    public SessionPreferences getPreferences() {
        return preferences;
    }

    public String getClientName() {
        return clientName;
    }

    public class SessionPreferences {

        private JSONObject preferences;
        private String primaryColor = "";
        private String secondaryColor = "";
        private String logoUrl = "";

        public SessionPreferences() {}

        public SessionPreferences(JSONObject preferences) {

            this.preferences = preferences;
            this.primaryColor = preferences.optString("primaryColor");
            this.secondaryColor = preferences.optString("secondaryColor");
            this.logoUrl = preferences.optString("logoUrl");
        }

        public JSONObject getPreferences() {
            return preferences;
        }

        public String getPrimaryColor() {
            return primaryColor;
        }

        public String getSecondaryColor() {
            return secondaryColor;
        }

        public String getLogoUrl() {
            return logoUrl;
        }

    }

    public String getLocalIdentifier() {
        return localIdentifier;
    }

    public void setLocalIdentifier(String localIdentifier) {
        this.localIdentifier = localIdentifier;
    }
}
