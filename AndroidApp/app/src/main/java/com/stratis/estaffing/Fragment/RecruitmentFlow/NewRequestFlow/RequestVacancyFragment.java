package com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.stratis.estaffing.Control.NumberSelector.NumberSelector;
import com.stratis.estaffing.Control.NumberSelector.NumberSelectorListener;
import com.stratis.estaffing.Control.eStaffingFragment;
import com.stratis.estaffing.Controller.RecruitmentFlow.RecruitmentFlowController;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.R;

public class RequestVacancyFragment extends eStaffingFragment implements View.OnClickListener, NumberSelectorListener {

    private Button oneVacancyButton;
    private Button twoVacancyButton;
    private Button threeVacancyButton;
    private NumberSelector numberVacancySelector;

    private int previousChairs = 1;
    private int chairs = 1;

    private int currentCharis = 1;

    private Boolean oneVacancySelected = true;
    private Boolean twoVacancySelected = false;
    private Boolean threeVacancySelected = false;
    private Boolean moreThanVacancySelected = false;

    private Boolean modeEdit = false;

    public RequestVacancyFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // --
        this.mView = inflater.inflate(R.layout.fragment_request_vacancy, container, false);
        FirebaseEvent.logEvent(mContext, this.getClass().getSimpleName().concat(this.modeEdit.toString()));

        // --
        this.initViews();

        // --
        return this.mView;
    }

    private void initViews() {

        this.oneVacancyButton = this.mView.findViewById(R.id.vacancyOne);
        this.oneVacancyButton.setOnClickListener(this);
        this.oneVacancyButton.setTag(11);

        this.twoVacancyButton = this.mView.findViewById(R.id.vacancyTwo);
        this.twoVacancyButton.setOnClickListener(this);
        this.twoVacancyButton.setTag(22);

        this.threeVacancyButton = this.mView.findViewById(R.id.vacancyThree);
        this.threeVacancyButton.setOnClickListener(this);
        this.threeVacancyButton.setTag(33);

        this.numberVacancySelector = new NumberSelector((RelativeLayout) this.mView.findViewById(R.id.numberSelector), getActivity());
        this.numberVacancySelector.listener = this;

        // --
        checkButtonsState();
    }

    private void checkProfile() {

        if (this.getProfile() != null && this.getProfile().getCurrentVacancies() > 0) {

            this.turnOffVacancies();
            int v = this.getProfile().getCurrentVacancies();
            this.chairs = v;
            if (v == 1) {
                this.oneVacancySelected = true;
            } else if(v == 2) {
                this.twoVacancySelected = true;
            } else if (v == 3) {
                this.threeVacancySelected = true;
            } else {
                this.chairs = 4;
                this.moreThanVacancySelected = true;
                this.numberVacancySelector.setVacancies(v);
                this.mView.findViewById(R.id.moreThanFour).setVisibility(View.VISIBLE);
            }
            this.checkButtonsState();
            this.animateChairs();
        }
    }

    private void checkButtonsState() {

        // -- Turn off all
        this.changeStateButton(this.oneVacancyButton, this.oneVacancySelected);
        this.changeStateButton(this.twoVacancyButton, this.twoVacancySelected);
        this.changeStateButton(this.threeVacancyButton, this.threeVacancySelected);
        this.numberVacancySelector.activeState(this.moreThanVacancySelected);
        this.mView.findViewById(R.id.moreThanFour).setVisibility(View.INVISIBLE);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeStateButton(Button button, Boolean state) {

        if (state) {

            button.setBackground(ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button));
            this.setViewAsSecondaryColor(button);
            button.setTextColor( ContextCompat.getColor(mContext, R.color.white));

        } else {

            button.setBackgroundDrawable( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            button.setTextColor(  ContextCompat.getColor(mContext, R.color.recruitment_request_vacancy_unselected_text_button) );
        }
    }

    private void turnOffVacancies() {

        this.oneVacancySelected = false;
        this.twoVacancySelected = false;
        this.threeVacancySelected = false;
        this.moreThanVacancySelected = false;
        this.mView.findViewById(R.id.moreThanFour).setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResume() {

        // ---
        super.onResume();
        this.checkProfile();

        // --
        //((RecruitmentFlowController) this.getActivity()).disableContinueButton(true);
    }

    @Override
    public void onClick(View v) {

        // -- Hide keyboard
        this.numberVacancySelector.numberField.clearFocus();
        hideSoftKeyboard(getActivity(), this.numberVacancySelector.numberField);

        // -- Turn of everything
        this.turnOffVacancies();

        Integer tag = (Integer) v.getTag();
        switch(tag) {

            case 11:

                this.oneVacancySelected = true;
                this.getProfile().setCurrentVacancies(1);
                this.currentCharis = 1;
                this.previousChairs = this.chairs;
                this.chairs = 1;

                // -- Animate
                this.animateChairs();

                break;

            case 22:

                this.twoVacancySelected = true;
                this.getProfile().setCurrentVacancies(2);
                this.currentCharis = 2;
                this.previousChairs = this.chairs;
                this.chairs = 2;

                // -- Animate
                this.animateChairs();

                break;

            case 33:

                this.threeVacancySelected = true;
                this.getProfile().setCurrentVacancies(3);
                this.currentCharis = 3;
                this.previousChairs = this.chairs;
                this.chairs = 3;

                // -- Animate
                this.animateChairs();

                break;

            default: break;
        }

        // --
        this.checkButtonsState();
    }

    private void animateChairs() {

        final ImageView chairLeft = this.mView.findViewById(R.id.single_chair_two);
        final ImageView chairRight = this.mView.findViewById(R.id.single_chair_three);

        if (this.chairs == 1) {

            if (this.previousChairs ==  2) {
                this.animate(chairLeft, Techniques.SlideOutLeft);
            }

            if (this.previousChairs >= 3) {
                this.animate(chairLeft, Techniques.SlideOutLeft);
                this.animate(chairRight, Techniques.SlideOutRight);
            }

        } else if (this.chairs == 2) {

            if (this.previousChairs == 1) {
                this.animate(chairLeft, Techniques.BounceInRight);
            }

            if (this.previousChairs >= 3) {
                this.animate(chairRight, Techniques.SlideOutRight);
            }

        }  else if (this.chairs >= 3) {

            if (this.previousChairs == 1) {

                this.animate(chairLeft, Techniques.BounceInRight);
                this.animate(chairRight, Techniques.BounceInLeft);
            }

            if (this.previousChairs == 2) {
                this.animate(chairRight, Techniques.BounceInLeft);
            }

            if (this.chairs == 4) {

            }
        }
    }

    private void animate(final View view, Techniques techniques) {

        final ImageView c = this.mView.findViewById(R.id.single_chair);
        YoYo.with(techniques).duration(350)
        //.pivotX(c.getMeasuredWidth() / 2.0f)
        //.pivotX(4000)
        .onStart(new YoYo.AnimatorCallback() {
            @Override
            public void call(Animator animator) {
                view.setVisibility(View.VISIBLE);
            }
        }).playOn(view);
    }

    @Override
    public void onSelectedNumberSelector() {

        // -- Turn of everything
        this.oneVacancySelected = false;
        this.twoVacancySelected = false;
        this.threeVacancySelected = false;
        this.moreThanVacancySelected = true;
        this.previousChairs = this.chairs;
        this.chairs = 4;
        this.currentCharis = this.chairs;

        // --
        this.checkButtonsState();
        this.numberVacancySelector.activeState(this.moreThanVacancySelected);
        this.animateChairs();
        this.mView.findViewById(R.id.moreThanFour).setVisibility(View.VISIBLE);
    }

    @Override
    public void vacanciesChange(int number) {

        Log.i("DXGOP", "NUMBER ::::: " + number);
        if (this.moreThanVacancySelected) {
            this.getProfile().setCurrentVacancies( this.numberVacancySelector.getVacancies() );
            this.currentCharis = number;
        }
    }

    // -- Abstract methods
    @Override
    public void onFooterContinueButtonClick() {

        this.getProfile().setCurrentVacancies(this.currentCharis);
        super.onFooterContinueButtonClick();
    }

    public Boolean getModeEdit() {
        return modeEdit;
    }

    public void setModeEdit(Boolean modeEdit) {
        this.modeEdit = modeEdit;
    }
}