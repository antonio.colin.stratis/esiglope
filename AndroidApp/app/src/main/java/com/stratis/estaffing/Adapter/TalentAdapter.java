package com.stratis.estaffing.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
//import com.stratis.estaffing.Controller.TalentFlow.TalentSingleController;
import com.stratis.estaffing.Controller.TalentFlowV3.TalentSingleController;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.Talent;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 18/06/20
 */

public class TalentAdapter extends ArrayAdapter<Talent> {

    public TalentAdapter(Context context, ArrayList<Talent> talents) {
        super(context, R.layout.activity_talent_controller_item, talents);
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final Talent talent = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final TalentViewHolder holder;

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.activity_talent_controller_item, parent, false);
            holder = new TalentViewHolder(convertView);
            convertView.setTag(holder);

            holder.getImage().setClipToOutline(true);

        } else {
            holder = (TalentViewHolder) convertView.getTag();
        }

        // -- Image
        if (!talent.getUserImg().equals("")) {

            Glide
                .with(getContext())
                .load(talent.getUserImg())
                .centerCrop()
                .thumbnail(.35f)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                // .placeholder(R.drawable.loading_spinner)
                .into( holder.getImage() );

                /*holder.getImage().getViewTreeObserver()
                        .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {

                                //holder.getImage().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                Picasso.get().
                                        load(talent.getUserImg()).
                                        networkPolicy(NetworkPolicy.OFFLINE).
                                        resize(0, holder.getImage().getHeight()).
                                        into( holder.getImage() );

                            }
                        });*/
        }

        // -- Fields
        // ONM: Se quita el capitalize para que se vea como en base
        holder.getName().setText( /*App.capitalize(*/talent.getUserFullNameShort()/*)*/ );
        holder.getSpeciality().setText( talent.getPosition() );

        /*
         * 20201-08-16: Se deja el elemento de Sr invisible por cambios
         * en la definicion del maquetado por parte de UX
         */
//        if (talent.getJobSeniority().equals("null") || talent.getJobSeniority().equals("Null") || talent.getJobSeniority().equals("NULL")) {
            holder.getSeniority().setVisibility(View.GONE);
//        } else  {
//            holder.getSeniority().setText( talent.getJobSeniority().toUpperCase() );
//        }

        // -- Click
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Intent intent = new Intent(getContext(), TalentSingleController.class);
                intent.putExtra("talentId", talent.getId());
                getContext().startActivity(intent);
            }
        });

        // --
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class TalentViewHolder {

        private View row;
        private RelativeLayout shape = null;
        private ImageView image = null;
        private TextView name = null;
        private TextView speciality = null;
        private TextView seniority = null;

        public TalentViewHolder(View row) {
            this.row = row;
        }

        public RelativeLayout getShape() {

            if (this.shape == null) {
                this.shape = row.findViewById(R.id.shape);
            }
            return this.shape;
        }

        public ImageView getImage() {

            if (this.image == null) {
                this.image = row.findViewById(R.id.image);
            }
            return this.image;
        }

        public TextView getName() {

            if (this.name == null) {
                this.name = row.findViewById(R.id.name);
            }
            return this.name;
        }

        public TextView getSpeciality() {

            if (this.speciality == null) {
                this.speciality = row.findViewById(R.id.speciality);
            }
            return this.speciality;
        }

        public TextView getSeniority() {

            if (this.seniority == null) {
                this.seniority = row.findViewById(R.id.seniority);
            }
            App.setViewAsPrimaryColor(this.seniority, getContext());
            return this.seniority;
        }

    }
}
