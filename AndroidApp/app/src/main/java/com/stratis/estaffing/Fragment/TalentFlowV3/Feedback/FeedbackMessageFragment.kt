package com.stratis.estaffing.Fragment.TalentFlowV3.Feedback

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.R

class FeedbackMessageFragment : ABFragment(), View.OnClickListener {

    var rate: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mView = inflater.inflate(R.layout.fragment_talent_feedback_message, container, false)

        initView()

        return mView;
    }
    private fun initView(){

        val app_screen_return_arrow = mView.findViewById<ImageButton>(R.id.app_screen_return_arrow)
        app_screen_return_arrow.setOnClickListener { v: View? -> backStack("initialFragment") }

        val txtMessage: TextView = mView.findViewById(R.id.message)
        val image : ImageView = mView.findViewById(R.id.image)

        if(rate!! >= 3) {
            txtMessage.text = getString(R.string.feedback_fragment_message_positive)
            image.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.feedback_positive))
        } else {
            txtMessage.text = getString(R.string.feedback_fragment_message_negative)
            image.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.feedback_negative))
        }

        val continueButton: Button = mView.findViewById(R.id.continueButton)
        continueButton.tag = 10
        continueButton.setOnClickListener(this)

        // Custom
        setViewAsPrimaryColor(continueButton)
    }

    override fun onClick(v: View?) {
        when (v!!.tag.toString()) {
            "10" -> {
                backStack("initialFragment")
            }
        }
    }
}