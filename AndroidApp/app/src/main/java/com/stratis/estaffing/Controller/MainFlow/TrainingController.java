package com.stratis.estaffing.Controller.MainFlow;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.R;

public class TrainingController extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // --
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_training_controller);

        // -- Set header
        this.setHeader(this.getResources().getString(R.string.training_header_title));

        // --
        final ImageView image = this.findViewById(R.id.image);
        image.setZ(100);
        image.bringToFront();

        // --
        final Button cancelButton = this.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setHeader(String title) {

        // --
        final ImageButton app_screen_return_arrow = this.findViewById(R.id.app_screen_return_arrow);
        app_screen_return_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // --
        final TextView titleHeader = this.findViewById(R.id.app_screen_title_header);
        titleHeader.setText(title);
    }
}
