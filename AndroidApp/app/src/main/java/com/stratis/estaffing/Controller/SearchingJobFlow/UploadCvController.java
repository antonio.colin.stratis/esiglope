package com.stratis.estaffing.Controller.SearchingJobFlow;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Control.UriUtils;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Login.LoginService;

import java.io.File;
import java.util.ArrayList;


public class UploadCvController extends AppCompatActivity {

    final private static int FILE_REQUEST_CODE = 0;
    private LoadingButton uploadButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_upload_cv_controller);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName());

        // -- Set header
        this.setHeader(this.getResources().getString(R.string.support_header_title));

        // --
        this.initViews();
    }

    private void initViews()  {

        this.uploadButton = new LoadingButton(this.findViewById(R.id.uploadButton));
        this.uploadButton.setText(this.getResources().getString(R.string.upload_cv_controller_upload_button));
        //this.uploadButton.getButton().setBackground(getResources().getDrawable(R.drawable.button_paused_gray));
        //this.uploadButton.getButton().setClickable(false);
        //this.uploadButton.getButton().setEnabled(false);
        this.uploadButton.getButton().setTag(10);
        this.uploadButton.getButton().setOnClickListener(v -> {

            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
            Permissions.check(UploadCvController.this, permissions, null, null, new PermissionHandler() {
                @Override
                public void onGranted() {

                    uploadButton.showLoading();
                    try {
                        final Intent intent = createIntent();
                        startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), FILE_REQUEST_CODE);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(UploadCvController.this, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        });
    }

    private Intent createIntent() {

        final String[] mimeTypes = {"application/pdf", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
        if (mimeTypes.length > 0) {
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        return intent;
    }

    /**
     * Method to handle the returned URI from the intent
     * **/
    private void processFile(final Uri uri) {

        if (uri != null) {

            Log.d("DXGOP", "FILE URI RETURNED: " + uri.toString());

            final String url = UriUtils.getPathFromUri(this, uri);
            Log.d("DXGOP", "FILE URI PARSED: " + uri.toString());

            final String fileName = this.getFileName(uri);
            this.paintFile(fileName);
            Log.d("DXGOP", "FILE NAME: " + fileName);

            File file = new File(url);
            this.uploadFile(file);
            Log.d("DXGOP",  "FILE FILE NAME :::: " + file.getName());
        }
    }

    /**
     * Method to get the name of the file by the URI
     * **/
    private String getFileName(Uri uri) {

        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    /**
     * Method paint into the UI the name of the file
     * **/
    private void paintFile(String name) {

        final RelativeLayout uploader = this.findViewById(R.id.uploader);
        final ImageView fileIcon = uploader.findViewById(R.id.fileIcon);
        final TextView fileName = uploader.findViewById(R.id.fileName);
        final ImageView removeButton = uploader.findViewById(R.id.removeButton);
        if (!name.equals("")) {

            // -- Container
            this.findViewById(R.id.thanks).setVisibility(View.VISIBLE);
            final ScrollView sc = this.findViewById(R.id.root);
            //sc.scrollTo(0, sc.getBottom());
            sc.post(new Runnable() {
                @Override
                public void run() {
                    sc.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });

            // -- Buttons logic
            fileIcon.setVisibility(View.VISIBLE);
            fileName.setText(name);
            fileName.setAlpha(1);
            fileName.setTextColor(Color.parseColor("#001330"));
            removeButton.setVisibility(View.VISIBLE);
            this.uploadButton.setText(this.getResources().getString(R.string.upload_cv_controller_reload_button));

            // -- Notification
            final Notification notification = new Notification(this, R.layout.notification_light);
            notification.setMessage(getResources().getString(R.string.upload_cv_controller_notification));
            notification.show(3000);
        }
    }

    /**
     * Method to upload the file
     * **/
    private void uploadFile(final File file) {

        LoginService.uploadCV(Session.get(this), file, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {
                // --
                Bundle parameters = new Bundle();
                final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(UploadCvController.this);;
                mFirebaseAnalytics.logEvent("CV_Enviados", parameters);
                FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName().concat("success"));
                // --
                uploadButton.hideLoading();
            }

            @Override
            public void onError(ReSTResponse response) {
                FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName().concat("error"));
                uploadButton.hideLoading();
            }
        });
    }

    private void setHeader(String title) {

        // --
        final ImageButton app_screen_return_arrow = this.findViewById(R.id.app_screen_return_arrow);
        app_screen_return_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // --
        final TextView titleHeader = this.findViewById(R.id.app_screen_title_header);
        titleHeader.setText(title);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                this.processFile(data.getData());
            }

        }
    }
}