package com.stratis.estaffing.Fragment.TalentFlowV3

import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate.RequestCandidateWorkExperienceAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateWorkExperienceModel
import com.stratis.estaffing.R
import java.util.*

class TalentExperienceFragment: ABFragment() {

    var experiences: ArrayList<CandidateWorkExperienceModel>? = null;

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_candidate_work_experience, container, false)
        setHeader("")
        this.initViews()

        return this.mView
    }

    private fun initViews() {

        experiences?.sortByDescending { it.startDate }
        val workExperienceList: RecyclerView = mView.findViewById(R.id.workExperienceList)
        App.createVerticalRecyclerList(workExperienceList, mContext)
        val adapter = RequestCandidateWorkExperienceAdapter(experiences)
        workExperienceList.adapter = adapter

    }
}