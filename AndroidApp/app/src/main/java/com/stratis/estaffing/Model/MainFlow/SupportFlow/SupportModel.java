package com.stratis.estaffing.Model.MainFlow.SupportFlow;

import org.json.JSONObject;

public class SupportModel {

    private String id = "";
    private String name = "";
    private String phone = "";
    private String email = "";

    public SupportModel() {}

    public SupportModel(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optString("id");
            this.name = obj.optString("name");
            this.phone = obj.optString("phone");
            this.email = obj.optString("email");
        }
    }

    // --
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
