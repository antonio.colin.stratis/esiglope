package com.stratis.estaffing.Core;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.stratis.estaffing.Control.JsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/14/19
 */

public class Preferences {

    private Context context;
    private SharedPreferences prefs;
    private String debugTag = "DXGOP";

    public Preferences(Context context) {

        this.context = context;
        this.prefs = this.context.getSharedPreferences(this.context.getPackageName(), Context.MODE_PRIVATE);
    }

    public Preferences(Context context, String tag) {

        this.context = context;
        this.prefs = this.context.getSharedPreferences(this.context.getPackageName(), Context.MODE_PRIVATE);
        this.debugTag = tag;
    }

    public Object getPreference(String preference) {

        Map<String,?> keys = this.prefs.getAll();

        for (Map.Entry<String,?> entry : keys.entrySet()) {
            if (entry.getKey().equals("preference")) {
                return entry.getValue();
            }
        }
        return "";
    }

    public String getStringPreference(String preference) {

        return this.prefs.getString(preference, "");
    }

    public JSONObject getJSONObjectPreference(String preference) {
        return JsonBuilder.stringToJson(this.prefs.getString(preference, new JSONObject().toString()));
    }

    public JSONArray getJSONArrayPreference(String preference) {
        return JsonBuilder.stringToJsonArray(this.prefs.getString(preference, new JSONArray().toString()));
    }

    public Integer getIntegerPreference(String preference) {
        return this.prefs.getInt(preference, -99999);
    }

    public Float getFloatPreference(String preference) {
        return this.prefs.getFloat(preference, -9999.99999f);
    }

    public Long getLongPreference(String preference) {
        return this.prefs.getLong(preference, -99999);
    }

    public boolean getBooleanPreference(String preference) {
        return this.prefs.getBoolean(preference, false);
    }

    @SuppressLint("ApplySharedPref")
    public void savePreference(String key, String value) {

        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    @SuppressLint("ApplySharedPref")
    public void savePreference(String key, JSONObject value) {

        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString(key, value.toString());
        editor.commit();
    }

    @SuppressLint("ApplySharedPref")
    public void savePreference(String key, JSONArray value) {

        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString(key, value.toString());
        editor.commit();
    }

    @SuppressLint("ApplySharedPref")
    public void savePreference(String key, Integer value) {

        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    @SuppressLint("ApplySharedPref")
    public void savePreference(String key, Float value) {

        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    @SuppressLint("ApplySharedPref")
    public void savePreference(String key, Long value) {

        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    @SuppressLint("ApplySharedPref")
    public void savePreference(String key, boolean value) {

        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    @SuppressLint("ApplySharedPref")
    public void removePreference(String key) {

        if (existsPreference(key)) {
            SharedPreferences.Editor editor = this.prefs.edit();
            editor.remove(key);
            editor.commit();
        }
    }

    public boolean existsPreference(String preference) {

        return this.prefs.contains(preference);
    }

    public void printExistedPreferences() {

        Map<String,?> keys = this.prefs.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            Log.d(this.debugTag, entry.getKey() + ": " + entry.getValue().toString());
        }
    }

    public void removePreferences() {

        Map<String,?> keys = this.prefs.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            this.removePreference(entry.getKey());
        }
    }
}