package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.ChooseCandidate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal.Adapter.NoCandidateInterestAdapter;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.NoCandidateInterestModal.Model.NoCandidateInterestModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.CatalogService;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.CandidateService;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 09/12/20
 */

public class ChooseCandidateModal extends BottomSheetDialog implements View.OnClickListener {

    private RequestCandidate candidate;

    private LoadingButton continueButton;
    private Button cancelButton;

    private ChooseCandidateListener listener;

    public ChooseCandidateModal(Context context, final RequestCandidate candidate) {

        super(context);
        this.candidate = candidate;
        this.commonInit();
    }

    public ChooseCandidateModal(Context context, int themeResId) {

        super(context, themeResId);
        this.commonInit();
    }

    protected ChooseCandidateModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.commonInit();
    }

    private void commonInit() {

        // -- R styles
        this.setContentView(R.layout.fragment_recruitment_request_candidate_choose_candidate_modal);
        this.setCanceledOnTouchOutside(false);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // -- Title
        final String name = String.format(getContext().
                getString(R.string.recruitment_request_candidate_single_choose_candidate_modal_title),
                this.candidate.getFullNameShort(), this.candidate.getFullNameShort());
        ((TextView) this.findViewById(R.id.title)).setText( name );

        // -- Buttons
        this.cancelButton = this.findViewById(R.id.cancelButton);
        cancelButton.setTag("31");
        cancelButton.setOnClickListener(this);

        this.continueButton = new LoadingButton(this.findViewById(R.id.continueButton));
        continueButton.getButton().setTag("32");
        continueButton.getButton().setOnClickListener(this);
        continueButton.setText(getContext().getString(R.string.recruitment_request_candidate_single_choose_candidate_modal_yes));
        continueButton.getButton().setBackground(App.getDrawable(getContext(), R.drawable.interview_selected_button));
        //continueButton.getButton().setClickable(false);
    }

    @Override
    public void show() {
        super.show();
    }

    public void setListener(ChooseCandidateListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {

        final String tag = String.valueOf(v.getTag());
        if (tag.equals("31")) {
            cancel();
        } else {
            this.continueButton.showLoading();
            this.changeStatus();
        }
    }

    private void changeStatus() {

        final Session session = Session.get(getContext());
        this.cancelButton.setOnClickListener(null);
        CandidateService.updateStatus(this.candidate, "Candidato seleccionado",
                RequestCandidate.statusCandidateChoose, session,
                new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                if (listener != null) {
                    listener.onCandidateChoose();
                }
                FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName());
                cancel();
            }

            @Override
            public void onError(ReSTResponse response) {}
        });
    }

    public interface ChooseCandidateListener {
        void onCandidateChoose();
    }
}