package com.stratis.estaffing.Adapter.TalentV3

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.TalentFlowV3.Feedback
import com.stratis.estaffing.R
import com.stratis.estaffing.View.Talent.FeedbackView

open class FeedbackAdapter(context: Context, items: List<Feedback>) : RecyclerView.Adapter<FeedbackView>() {

    private var items: List<Feedback>? = null
    private var context: Context? = null

    private var listener:FeedbackAdapterListener? = null

    init {
        this.context = context
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackView {
        val layoutView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.fragment_talent_feedback_detail_item, parent, false)
        return FeedbackView(layoutView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: FeedbackView, position: Int) {
        holder.setIsRecyclable(false)
        val feedback : Feedback = this.items!![position]

        // --
        holder.interviewLayoutParent?.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                listener?.onFeedbackClick(feedback)
            }
        })
        holder.interviewDate?.text = App.getDateCourse(feedback.dateStr)
        holder.interviewStrength?.text = String.format(context?.getString(R.string.feedback_fragment_detail_strength)!!, feedback.strengths!!.size)
        holder.interviewOportunity?.text = String.format(context?.getString(R.string.feedback_fragment_detail_oportunity)!!, feedback.opportunities!!.size)
        holder.interviewRate?.text = feedback.rateValue.toString()
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    fun setListener(listener: FeedbackAdapterListener) {
        this.listener = listener
    }

    interface FeedbackAdapterListener {
        fun onFeedbackClick(feedback: Feedback)
    }
}