package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.LoadingButton;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.Adapter.RequestPausedCancelFeedbackAdapter;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.Model.RequestPausedCancelFeedbackModel;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.CatalogService;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.RequestsService;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 09/12/20
 */

public class RequestPausedCancelModal extends BottomSheetDialog implements View.OnClickListener {

    public static final String otherOptionHardcode = "Otro";
    public enum RequestPausedCancelModalType { paused, cancel }

    private LoadingButton continueButton;
    private Button cancelButton;

    private RequestPausedCancelModalType type = RequestPausedCancelModalType.paused;
    private RequestPausedCancelModalListener listener;
    private String requestId = "";
    private ArrayList<RequestPausedCancelFeedbackModel> options;
    private String optionTextSelected = "";

    public RequestPausedCancelModal(Context context, final RequestPausedCancelModalType type) {

        super(context);
        this.type = type;
        this.commonInit();
    }

    public RequestPausedCancelModal(Context context, int themeResId) {

        super(context, themeResId);
        this.commonInit();
    }

    protected RequestPausedCancelModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.commonInit();
    }

    private void commonInit() {

        // -- R styles
        this.setContentView(R.layout.fragment_recruitment_request_single_modal);
        this.setCanceledOnTouchOutside(false);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // -- Get options
        if (this.type == RequestPausedCancelModalType.paused) {

            // --
            ((TextView) this.findViewById(R.id.title)).setText(getContext().
                    getString(R.string.recruitment_actions_request_single_modal_pause_title));

            // --
            CatalogService.getPauseFeedbackCatalog(new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {

                    JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                    options = RequestPausedCancelFeedbackModel.parse(res);
                    handleActions();
                }

                @Override
                public void onError(ReSTResponse response) {}
            });
        } else {

            CatalogService.getCancelFeedbackCatalog(new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {

                    JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                    options = RequestPausedCancelFeedbackModel.parse(res);
                    handleActions();
                }

                @Override
                public void onError(ReSTResponse response) {}
            });
        }

        // -- Buttons
        this.cancelButton = this.findViewById(R.id.cancelButton);
        cancelButton.setTag("31");
        cancelButton.setOnClickListener(this);

        this.continueButton = new LoadingButton(this.findViewById(R.id.continueButton));
        continueButton.getButton().setTag("32");
        continueButton.getButton().setOnClickListener(this);
        continueButton.setText( (this.type.equals(RequestPausedCancelModalType.cancel)) ?
                getContext().getString(R.string.recruitment_actions_request_single_cancel_button) :
                getContext().getString(R.string.recruitment_actions_request_single_paused_button));
        continueButton.getButton().setBackground(App.getDrawable(getContext(), R.drawable.button_paused_gray));
        continueButton.getButton().setClickable(false);
    }

    @Override
    public void show() {

        super.show();

        // --
        //this.handleActions();
    }

    @SuppressLint("ResourceType")
    private void handleActions() {

        // -- Options
        final RecyclerView optionsList = this.findViewById(R.id.options);
        App.createVerticalRecyclerList(optionsList, getContext());
        final RequestPausedCancelFeedbackAdapter requestPausedCancelFeedbackAdapter = new RequestPausedCancelFeedbackAdapter(
                this.options, getContext()
        );
        optionsList.setAdapter(requestPausedCancelFeedbackAdapter);
        requestPausedCancelFeedbackAdapter.setListener(option -> {

            optionTextSelected = option;
            if (optionTextSelected.equals(otherOptionHardcode)) {
                findViewById(R.id.otherAvailable).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.otherAvailable).setVisibility(View.GONE);
            }
            // --
            continueButton.getButton().setBackground(App.getDrawable(getContext(), R.drawable.activity_login_controller_login_button));
            App.setViewAsPrimaryColor(continueButton.getButton(), getContext());
            continueButton.getButton().setClickable(true);
        });
    }

    public void setListener(RequestPausedCancelModalListener listener) {
        this.listener = listener;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public void onClick(View v) {

        final String tag = String.valueOf(v.getTag());
        if (tag.equals("31")) {
            cancel();
        } else {

            final String otherCommentText = String.valueOf(((TextInputEditText) this.findViewById(R.id.otherCommentText)).getText());
            if (this.optionTextSelected.equals(otherOptionHardcode) && otherCommentText.equals("")) {
                optionTextSelected = otherOptionHardcode;
            } else if (this.optionTextSelected.equals(otherOptionHardcode) && !otherCommentText.equals("")) {
                optionTextSelected = otherCommentText;
            }

            // --
            //Log.d("DXGOP", "COMMMENT :::: " + optionTextSelected);
            this.changeStatus();
            this.continueButton.showLoading();
        }
    }

    private void changeStatus() {

        final Session session = Session.get(getContext());
        this.cancelButton.setOnClickListener(null);
        if (this.type == RequestPausedCancelModalType.paused) {

            RequestsService.pauseRequest(this.requestId, this.optionTextSelected, session, new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {

                    // --
                    Bundle parameters = new Bundle();
                    parameters.putString("motivo_pausa", optionTextSelected);
                    final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
                    mFirebaseAnalytics.logEvent("Solicitudes_Pausadas", parameters);

                    if (listener != null) {
                        listener.onStatusChange();
                    }
                    cancel();
                }

                @Override
                public void onError(ReSTResponse response) {}
            });

        } else {

            RequestsService.cancelRequest(this.requestId, this.optionTextSelected, session, new ReSTCallback() {
                @Override
                public void onSuccess(ReSTResponse response) {

                    // --
                    Bundle parameters = new Bundle();
                    parameters.putString("motivo_anulacion", optionTextSelected);
                    final FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());;
                    mFirebaseAnalytics.logEvent("solicitud_anulada", parameters);

                    // --
                    if (listener != null) {
                        listener.onStatusChange();
                    }
                    cancel();
                }

                @Override
                public void onError(ReSTResponse response) {}
            });
        }
    }

    public interface RequestPausedCancelModalListener {
        void onStatusChange();
    }
}