package com.stratis.estaffing.Broadcast;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.stratis.estaffing.Controller.LoginFlow.LoginController;
import com.stratis.estaffing.Controller.MainFlow.HomeController;
import com.stratis.estaffing.Core.Preferences;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Notification.NotificationService;

import java.util.Map;

/**
 * Created by Erick Sanchez
 * Revision 1 - 19/05/21
 */

public class MessageReceiver extends FirebaseMessagingService {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {

        Log.i("DXGOP", "FIREBASE NEW TOKENNNNNN :: Refreshed token: " + token);
        final Session session = Session.get(getApplicationContext());
        session.setFcmToken(token);

        // --
        if (session.isLogged()) {
            NotificationService.saveConfiguration(session);
        }
    }

    /**
     * Called when message is received.
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);
        final Map<String, String> data = remoteMessage.getData();
        final String title = data.get("title");
        final String message = data.get("message");

        // --
        Log.i("DXGOP", "From: " + remoteMessage.getFrom());
        Log.i("DXGOP", "PN DATA ::: " + data.get("notificationBody"));

        final String notificationJSON = data.get("notificationBody");
        Intent intent = new Intent(getApplicationContext(), LoginController.class);
        intent.putExtra("pendingPushNotification", notificationJSON);
        intent.setAction("pendingPushNotification");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                intent, PendingIntent.FLAG_ONE_SHOT);

        // --
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.icon_notification_android)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0, notificationBuilder.build());
    }
}