package com.stratis.estaffing.Adapter.TalentV3

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.ButtonArrowAdapter
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.CatalogItem
import com.stratis.estaffing.Model.TalentFlowV3.FeedbackDetail
import com.stratis.estaffing.R
import com.stratis.estaffing.View.Talent.FeedbackSkillsView
import java.util.ArrayList

class FeedbackSkillsAdapter(context: Context?, items: LinkedHashMap<CatalogItem, ArrayList<FeedbackDetail>>?) : RecyclerView.Adapter<FeedbackSkillsView>() {

    private var items: LinkedHashMap<CatalogItem, ArrayList<FeedbackDetail>>? = null
    private var listener: FeedbackSkillClickListener? = null
    private var context: Context? = null

    init {
        this.items = items
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackSkillsView {
        val layoutView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.fragment_feedback_skill_btn, parent, false)
        return FeedbackSkillsView(layoutView)
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: FeedbackSkillsView, position: Int) {
        holder.setIsRecyclable(false)
        val catalogItem : CatalogItem = this.items!!.keys.elementAt(position)
        val description : String = catalogItem.description
        val catalogItems : ArrayList<FeedbackDetail> = this.items!!.getValue(catalogItem)

        if(catalogItems.isEmpty()) {
            holder.textStatusInit!!.text = description
            if(listener!!.chooseSkills()) {
                holder.layoutStatusInit!!.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View?) {
                        listener?.onClick(catalogItem)
                    }
                })
            }
        } else {
            holder.layoutStatusInit!!.visibility = View.GONE
            holder.parentLayoutStatusLoad!!.visibility = View.VISIBLE
            if(listener!!.chooseSkills()) {
                holder.parentLayoutStatusLoad!!.setOnClickListener (object : View.OnClickListener {
                    override fun onClick(v: View?) {
                        listener?.onClick(catalogItem)
                    }
                })
            }
            holder.textStatusLoad!!.text = description
            holder.textValueLoad!!.text = catalogItems.size.toString()
            holder.itemList!!.visibility = View.VISIBLE
            App.createVerticalRecyclerList(holder.itemList, context)
            val itemAdapter= FeedbackSkillItemAdapter(catalogItems)
            itemAdapter.setListener(object : ButtonArrowAdapter.ButtonArrowClickListener {
                override fun onClick(position: Int) {
                    listener?.onDelete(catalogItems[position])
                }
            })
            holder.itemList!!.adapter = itemAdapter

        }
    }

    fun removeItem() {
        this.notifyDataSetChanged()
    }

    fun setListener(listener: FeedbackSkillClickListener) {
        this.listener = listener
    }

    interface FeedbackSkillClickListener {
        fun onClick(catalogItem: CatalogItem)
        fun onDelete(skill: FeedbackDetail)
        fun chooseSkills() : Boolean
        fun refreshButton()
    }
}