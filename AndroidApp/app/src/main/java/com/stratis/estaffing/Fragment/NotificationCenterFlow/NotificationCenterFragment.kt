package com.stratis.estaffing.Fragment.NotificationCenterFlow

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.NotificationCenter.NotificationCenterAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Preferences
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Fragment.MasterAdminFlow.MARequestSingleCandidatesFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidateMessagesFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidateSingleFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestCandidatesFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestSingleFragment
import com.stratis.estaffing.Listener.NotificationCenterFragmentListener
import com.stratis.estaffing.Modal.NotificationCenterFlow.Model.NotificationCenterFilterModel
import com.stratis.estaffing.Modal.NotificationCenterFlow.NotificationCenterModal
import com.stratis.estaffing.Model.NotificationCenterFlow.NotificationCenterModel
import com.stratis.estaffing.Model.Profile
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Interview.RequestCandidateInterview
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.Notification.NotificationService

open class NotificationCenterFragment : ABFragment(),
        NotificationCenterAdapter.NotificationCenterListener, View.OnClickListener,
        NotificationCenterModal.NotificationCenterFilterListener, NotificationCenterFragmentListener {

    var notifications:ArrayList<NotificationCenterModel>? = null
    var currentNotification:NotificationCenterModel? = null

    private var readAllNotifications:TextView? = null
    private var notificationsList:RecyclerView? = null
    private var filtersContainer:ConstraintLayout? = null
    private var message:ConstraintLayout? = null

    private var deleteFiltersButton:Button? = null
    private var filterButton:RelativeLayout? = null

    private var notificationCenterAdapter:NotificationCenterAdapter? = null
    private var currentFilterId = "0"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_notification_center, container, false)

        // -- Set header
        this.setHeader(mContext.getString(R.string.home_item_notifications_title2))

        // -- Init views
        this.initViews()

        // -- Get notifications
        if (this.notifications != null) {
            this.notifications = NotificationCenterModel.sort(this.notifications)
            this.parseNotifications(this.notifications)
        }

        // --
        return this.mView
    }

    private fun initViews() {

        this.readAllNotifications = this.findView(R.id.readAllNotifications)
        this.readAllNotifications?.tag = "9"
        this.readAllNotifications?.setOnClickListener(this)

        this.notificationsList = this.findView(R.id.notificationsList)
        this.filtersContainer = this.findView(R.id.filtersContainer)
        this.message = this.findView(R.id.message)

        this.deleteFiltersButton = this.findView(R.id.deleteFiltersButton)
        this.deleteFiltersButton?.tag = "10"
        this.deleteFiltersButton?.setOnClickListener(this)

        this.filterButton = this.findView(R.id.filterButton)
        this.filterButton?.tag = "11"
        this.filterButton?.setOnClickListener(this)
    }

    private fun parseNotifications(nots:ArrayList<NotificationCenterModel>?) {

        if (this.notificationCenterAdapter == null) {

            App.createVerticalRecyclerList(this.notificationsList, context)
            this.notificationCenterAdapter = NotificationCenterAdapter(context, nots)
            this.notificationCenterAdapter!!.listener = this

            if (nots?.size!! > 0) {

                this.readAllNotifications?.visibility = View.VISIBLE
                this.filtersContainer?.visibility = View.VISIBLE
                this.notificationsList?.adapter = this.notificationCenterAdapter

            } else {

                this.message?.visibility = View.VISIBLE
                this.message?.findViewById<Button>(R.id.actionButton)?.setOnClickListener {
                    activity?.onBackPressed()
                }
            }
        }
    }

    private fun removeFilters() {

        this.currentFilterId = "0"
        this.message?.visibility = View.GONE
        this.readAllNotifications?.visibility = View.VISIBLE
        this.notificationsList?.visibility = View.VISIBLE
        this.notificationCenterAdapter?.removeFilters()
    }

    override fun onClick(p0: View?) {

        when(p0?.tag) {

            "9" -> {

                NotificationService.readAll(Session.get(context))
                this.notificationCenterAdapter?.forceAllViewed = true
                this.notificationCenterAdapter?.notifyDataSetChanged()
            }

            "10" -> {
                this.removeFilters()
            }

            "11" -> {

                val modal = NotificationCenterModal(context)
                modal.setListener(this)
                modal.setCurrentFilterId(this.currentFilterId)
                modal.show()
            }
        }
    }

    override fun onNotificationClick(notification: NotificationCenterModel) {

        // --
        this.currentNotification = notification

        // -- Reading notification
        NotificationService.read(Session.get(context), notification)

        // --
        val profile = Profile()
        profile.provisionalId = notification.getParameter("vacancyId")

        // --
        when(notification.clickActionName) {

            NotificationCenterModel.toVacancyDetail -> {

                val recruitmentRequestSingleFragment = RecruitmentRequestSingleFragment(profile)
                recruitmentRequestSingleFragment.setNotificationCenterListener(this)
                continueSegue(recruitmentRequestSingleFragment)
            }

            NotificationCenterModel.toCandidateList -> {

                val session = Session.get(mContext)
                if (session.type == Session.SessionRolType.masterAdmin) {

                    val candidatesMAFragment = MARequestSingleCandidatesFragment()
                    candidatesMAFragment.profile = profile
                    candidatesMAFragment.notificationCenterListener = this
                    continueSegue(candidatesMAFragment)

                } else {

                    val candidatesFragment = RecruitmentRequestCandidatesFragment()
                    candidatesFragment.setProfile(profile)
                    candidatesFragment.setNotificationCenterListener(this)
                    continueSegue(candidatesFragment)
                }
            }

            NotificationCenterModel.toCandidateProfile -> {

                val candiate = RequestCandidate()
                candiate.id = notification.getParameter("talentId")

                val candidateSingleFragment = RecruitmentRequestCandidateSingleFragment()
                candidateSingleFragment.setProfile(profile)
                candidateSingleFragment.setCandidate(candiate)
                candidateSingleFragment.setNotificationCenterListener(this)
                continueSegue(candidateSingleFragment)
            }

            NotificationCenterModel.toBoard -> {

                val candiate = RequestCandidate()
                val interview = RequestCandidateInterview()

                candiate.id = notification.getParameter("talentId")
                candiate.vacancyCandidateId = notification.getParameter("vacancyCandidateId")
                interview.candidate = candiate

                val messagesFragment = RecruitmentRequestCandidateMessagesFragment()
                messagesFragment.setInterview(interview)
                continueSegue(messagesFragment)
            }
        }
    }

    override fun onFilterClick(filter: NotificationCenterFilterModel?) {

        this.currentFilterId = filter?.id ?: "0"
        when(filter?.id) {

            NotificationCenterModel.notificationCategoryTalent -> {}
            NotificationCenterModel.notificationCategoryVacancies -> { this.notificationCenterAdapter?.filterByCategoryId(filter.id) }
            NotificationCenterModel.notificationCategoryInterviews -> { this.notificationCenterAdapter?.filterByCategoryId(filter.id) }
            NotificationCenterModel.notificationCategoryCandidate -> { this.notificationCenterAdapter?.filterByCategoryId(filter.id) }
            "999" -> { this.notificationCenterAdapter?.filterByNotSeen() } // -- Not seen
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onEmptyResultsAtFilter(filterName: String) {

        this.message?.visibility = View.VISIBLE
        this.readAllNotifications?.visibility = View.GONE
        this.notificationsList?.visibility = View.GONE

        this.message?.findViewById<ImageView>(R.id.image)?.setImageBitmap(App.getImage(context, "sad_bell"))
        this.message?.findViewById<TextView>(R.id.title)?.text = "No tienes notificaciones pendientes del tipo ${filterName}"
        this.message?.findViewById<Button>(R.id.actionButton)?.text = "Ver todas las notificaciones"
        this.message?.findViewById<Button>(R.id.actionButton)?.setOnClickListener {
            this.removeFilters()
        }
    }

    override fun onFilterResults() {

        // -- Removing empty results
        if (this.message?.visibility == View.VISIBLE) {
            this.message?.visibility = View.GONE
            this.readAllNotifications?.visibility = View.VISIBLE
            this.notificationsList?.visibility = View.VISIBLE
        }
    }

    // --
    override fun onVacancyNotAvailable() {

        val preferences = Preferences(context)
        val message = String.format(mContext.getString(R.string.notification_center_vacancy_not_available),
                currentNotification?.getParameter("vacancyId"))
        preferences.savePreference("pendingErrorNotification", message)
        this.activity?.finish()
    }

    override fun onCandidateListNotAvailable() {

        val preferences = Preferences(context)
        val message = mContext.getString(R.string.notification_center_candidates_not_available)
        preferences.savePreference("pendingErrorNotification", message)
        this.activity?.finish()
    }

    override fun onCandidateNotAvailable() {

        val preferences = Preferences(context)
        val message = mContext.getString(R.string.notification_center_candidate_not_available)
        preferences.savePreference("pendingErrorNotification", message)
        this.activity?.finish()
    }

    /** REMOVE TODO
    override fun onVacancyPaused() {

        val preferences = Preferences(context)
        val message = String.format(mContext.getString(R.string.notification_center_vacancy_paused),
                currentNotification?.getParameter("vacancyId"))
        preferences.savePreference("pendingNotification", message)
        this.activity?.finish()
    }

    override fun onVacancyStopped() {

        val preferences = Preferences(context)
        val message = String.format(mContext.getString(R.string.notification_center_vacancy_cancel),
                currentNotification?.getParameter("vacancyId"))
        preferences.savePreference("pendingErrorNotification", message)
        this.activity?.finish()
    }
    **/
}