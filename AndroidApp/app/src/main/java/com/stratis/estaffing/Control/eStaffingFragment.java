package com.stratis.estaffing.Control;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.stratis.estaffing.Controller.RecruitmentFlow.RecruitmentFlowController;
import com.stratis.estaffing.Controller.RecruitmentFlow.RecruitmentFlowEditorController;
import com.stratis.estaffing.Model.Profile;

/**
 * Created by Erick Sanchez - App Builders CTO
 * Revision 1 - 09/09/20
 */

public abstract class eStaffingFragment extends ABFragment {

    private Profile profilePassed;

    public void setProfile(Profile profile) {
        if( getActivity() instanceof  RecruitmentFlowController)
            ((RecruitmentFlowController) getActivity()).profile = profile;
        else if( getActivity() instanceof RecruitmentFlowEditorController) {
            ((RecruitmentFlowEditorController) getActivity()).profile = profile;
            if (profilePassed != null) {
                setProfilePassed(profile);
            }
        }
    }

    public Profile getProfile() {

        if (profilePassed == null) {
            if( getActivity() instanceof  RecruitmentFlowController)
                return ((RecruitmentFlowController) getActivity()).profile;
            else if( getActivity() instanceof RecruitmentFlowEditorController)
                return ((RecruitmentFlowEditorController) getActivity()).profile;
            return null;
        } else {
            return this.profilePassed;
        }
    }

    public void setProfilePassed(Profile profilePassed) {
        this.profilePassed = profilePassed;
    }

    //  --
    public void onFooterContinueButtonClick() {}

    /**  KEYBOARD HELPERS **/
    // --
    public void hideSoftKeyboard(Activity activity, View view) {

        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    public void showSoftKeyboard(final Activity activity, final EditText editText) {

        editText.requestFocus();
        editText.postDelayed(
                new Runnable() {
                    @Override public void run() {

                        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editText,0);
                    }
                }
                ,200);
    }
}
