package com.stratis.estaffing.Controller.RecruitmentFlow;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;

import com.stratis.estaffing.Control.ABActivity;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.R;

public class FirstTimeController extends ABActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_first_time_controller);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName());

        this.setHeader("");
        this.setStatusBarColor(Color.WHITE);
        this.setStatusBarTextLight();

        this.initViews();
    }

    private void initViews() {

        final Button continueButton = this.findViewById(R.id.continueButton);
        continueButton.setOnClickListener(v -> {
            startActivity(new Intent(FirstTimeController.this, NewRequestController.class));
            finish();
        });
        this.setViewAsSecondaryColor(continueButton);

        // --
        final Button cancelButton = this.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(v -> finish());
    }
}