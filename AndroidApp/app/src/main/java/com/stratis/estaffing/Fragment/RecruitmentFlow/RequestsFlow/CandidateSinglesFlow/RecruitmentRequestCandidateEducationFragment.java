package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow;

import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate.RequestCandidateEducationAdapter;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandiateEducationModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.CandidateService;

import org.json.JSONArray;

import java.util.ArrayList;

public class RecruitmentRequestCandidateEducationFragment extends ABFragment implements
        RequestCandidateEducationAdapter.RequestCandidateEducationAdapterListener {

    private RequestCandidate candidate;

    public RecruitmentRequestCandidateEducationFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_candidate_education, container, false);

        this.setHeader("");
        this.showLoader();
        this.getEducation();

        return this.mView;
    }

    private void getEducation() {

        CandidateService.getCandidateEducation(this.candidate.getId(), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                hideLoader();
                final JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                parseEducation(res);
            }

            @Override
            public void onError(ReSTResponse response) {
                hideLoader();
            }
        });
    }

    private void parseEducation(JSONArray res) {

        // --
        final ArrayList<CandiateEducationModel> educationModels = CandiateEducationModel.parse(res);
        ArrayList<CandiateEducationModel> education = new ArrayList<>();
        ArrayList<CandiateEducationModel> certifications = new ArrayList<>();
        for (CandiateEducationModel item : educationModels) {

            switch(item.getCategoryId()) {

                case CandiateEducationModel.typeCertification:
                case CandiateEducationModel.typeDiploma:
                default:
                    certifications.add(item);
                    break;

                case CandiateEducationModel.typeGraded:
                case CandiateEducationModel.typeMaster:
                case CandiateEducationModel.typePhD:
                    education.add(item);
                    break;
            }
        }

        // -- EDUCATION
        final RecyclerView educationList = this.mView.findViewById(R.id.educationList);
        if (education.size() > 0) {
            App.createVerticalRecyclerList(educationList, getContext());
            final RequestCandidateEducationAdapter educationListAdapter = new RequestCandidateEducationAdapter(education, getContext());
            educationListAdapter.setListener(this);
            educationList.setAdapter(educationListAdapter);
        } else {
            this.mView.findViewById(R.id.textView).setVisibility(View.GONE);
            educationList.setVisibility(View.GONE);
        }

        // -- CERTIFICATIONS
        final RecyclerView certificationsList = this.mView.findViewById(R.id.certificationsList);
        if (certifications.size() > 0) {
            App.createVerticalRecyclerList(certificationsList, getContext());
            final RequestCandidateEducationAdapter certificationsListAdapter =
                    new RequestCandidateEducationAdapter(certifications, getContext(),
                            RequestCandidateEducationAdapter.RequestCandidateEducationAdapterType.course);
            certificationsListAdapter.setListener(this);
            certificationsList.setAdapter(certificationsListAdapter);
        } else {
            this.mView.findViewById(R.id.certificationsTitle).setVisibility(View.GONE);
            certificationsList.setVisibility(View.GONE);
        }
    }

    public void setCandidate(RequestCandidate candidate) {
        this.candidate = candidate;
    }

    @Override
    public void onDetail(CandiateEducationModel item) {

        final RecruitmentRequestCandidateEducationSingleFragment fragment = new RecruitmentRequestCandidateEducationSingleFragment();
        fragment.setEducationModel(item);
        this.continueSegue(fragment);
    }
}