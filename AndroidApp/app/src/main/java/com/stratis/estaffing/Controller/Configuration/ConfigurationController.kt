package com.stratis.estaffing.Controller.Configuration

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.stratis.estaffing.Controller.LoginFlow.LoginController
import com.stratis.estaffing.Controller.TalentFlow.TalentController
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.R

class ConfigurationController: AppCompatActivity(), View.OnClickListener {

    private var session: Session? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuration_controller)

        initButtons()
    }

    private fun initButtons() {
        val cancelButton: ImageView = findViewById(R.id.app_screen_return_arrow)
        cancelButton.tag = "1"
        cancelButton.setOnClickListener(this)

        val deleteAccount: ConstraintLayout = findViewById(R.id.delete_account)
        deleteAccount.tag = "2"
        deleteAccount.setOnClickListener(this)

        val logOut: ConstraintLayout = findViewById(R.id.logout)
        logOut.tag = "3"
        logOut.setOnClickListener(this)

        session = Session.get(this)
    }

    override fun onClick(v: View?) {
        when (v!!.tag.toString()) {
            "1" -> finish()
            "2" -> {
                startActivity(Intent(this, DeleteController::class.java))
            }
            "3" -> {
                val builder = AlertDialog.Builder(this)
                //set title for alert dialog
                builder.setTitle(R.string.configuration_option_logout_alert_title)
                builder.setIcon(android.R.drawable.ic_dialog_alert)

                //performing positive action
                builder.setPositiveButton(R.string.configuration_option_logout_alert_done){ dialogInterface, which ->
                    session?.logout()
                    startActivity(Intent(this, LoginController::class.java))
                    finishAffinity()
                }
                //performing cancel action
                builder.setNeutralButton(R.string.configuration_option_logout_alert_cancel){ dialogInterface , which ->

                }

                // Create the AlertDialog
                val alertDialog: AlertDialog = builder.create()
                // Set other dialog properties
                alertDialog.setCancelable(false)
                alertDialog.show()
            }
        }
    }

}