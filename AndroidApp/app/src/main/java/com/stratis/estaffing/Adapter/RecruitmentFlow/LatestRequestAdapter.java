package com.stratis.estaffing.Adapter.RecruitmentFlow;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.LatestRequestView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 21/08/20
 */

public class LatestRequestAdapter extends RecyclerView.Adapter<LatestRequestView> {

    private ArrayList<LatestVacancyRequest> items;

    public LatestRequestAdapter(ArrayList<LatestVacancyRequest> items) {

        Log.d("adaptaer", items.toString());
        ArrayList<LatestVacancyRequest> newItems = new ArrayList<>();
        int length = Math.min(items.size(), 20);
        for (int i=0; i<length; i++) {
            newItems.add(items.get(i));
        }

        this.items = newItems;
    }

    @NonNull
    @Override
    public LatestRequestView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_new_request_item, parent, false);
        return new LatestRequestView(layoutView);
    }

    @Override
    public void onBindViewHolder(final LatestRequestView holder, int position) {

        holder.setIsRecyclable(false);
        final LatestVacancyRequest item = this.items.get(position);

        // -- Setting the views
        holder.getVacancyName().setText( item.getName() );
        holder.getSeniority().setText( item.getJobSeniority() );
        holder.getDate().setText( item.getDate() );
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public ArrayList<LatestVacancyRequest> getItems() {
        return items;
    }

    public void reload(ArrayList<LatestVacancyRequest> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            this.items.subList(0, size).clear();
            this.notifyItemRangeRemoved(0, size);
        }
    }
}