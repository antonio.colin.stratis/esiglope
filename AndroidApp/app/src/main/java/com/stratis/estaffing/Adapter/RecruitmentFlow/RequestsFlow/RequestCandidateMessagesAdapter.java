package com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandiateEducationModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidateMessage;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate.CandidateEducationView;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.RequestCandidateMessageView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class RequestCandidateMessagesAdapter extends RecyclerView.Adapter<RequestCandidateMessageView> {

    private final static int outMessage = 1;
    private final static int inMessage = 2;

    protected ArrayList<RequestCandidateMessage> items;

    public RequestCandidateMessagesAdapter(ArrayList<RequestCandidateMessage> items) {

        this.items = items;
    }

    @Override
    public RequestCandidateMessageView onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView;
        if (viewType == outMessage) {
            layoutView  = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recruitment_request_candidate_messages_item_out,
                    parent, false);
        } else {
            layoutView  = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recruitment_request_candidate_messages_item_in,
                    parent, false);
        }
        return new RequestCandidateMessageView(layoutView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RequestCandidateMessageView holder, final int position) {

        holder.setIsRecyclable(false);
        final RequestCandidateMessage message = this.items.get(position);

        // --
        holder.getMessage().setText( message.getMessage() );
    }

    @Override
    public int getItemViewType(int position) {

        if (this.items.get(position).getType() == RequestCandidateMessage.RequestCandidateMessageType.out) {
            return outMessage;
        } else {
            return inMessage;
        }
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<RequestCandidateMessage> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<RequestCandidateMessage> getItems() {
        return items;
    }
}