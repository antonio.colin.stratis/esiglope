package com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate;

import android.content.Context;

import androidx.core.content.res.ResourcesCompat;

import com.amulyakhare.textdrawable.TextDrawable;
import com.stratis.estaffing.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 28/07/20
 *
 * Catalog 19
 *
 * 1001900001	Certificaciones
 * 1001900002	Diplomas
 * 1001900003	Licenciatura
 * 1001900004	Maestria
 * 1001900005	Doctorados
 */

public class CandiateEducationModel {

    public final static String typeCertification = "1001900001";
    public final static String typeDiploma = "1001900002";
    public final static String typeGraded = "1001900003";
    public final static String typeMaster = "1001900004";
    public final static String typePhD = "1001900005";

    private String id = "";
    private String talentCourseId = "";
    private String talentId = "";
    private Boolean hasCertificate = false;
    private String certificateNumber = "";
    private String certificateFileUrl = "";
    private String validityDate = "";
    private String talentCourseDescription = "";
    private String courseName = "";
    private String courseDescription = "";
    private String courseDate = "";
    private String courseSchool = "";
    private String categoryId = "";
    private String categoryName = "";

    public CandiateEducationModel(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optString("courseId");
            this.talentCourseId = obj.optString("talentCourseId", "");
            this.talentId = obj.optString("talentId", "");
            this.hasCertificate = obj.optBoolean("hasCertificate", false);
            this.certificateNumber = obj.optString("certificateNumber", "");
            this.certificateFileUrl = obj.optString("certificateFileUrl", "");
            this.validityDate = obj.optString("validityDate", "");
            this.talentCourseDescription = obj.optString("talentCourseDescription", "");
            this.courseName = obj.optString("courseName", "");
            this.courseDescription = obj.optString("courseDescription", "");
            this.courseDate = obj.optString("courseDate", "");
            this.courseSchool = obj.optString("courseSchool", "");
            this.categoryId = obj.optString("categoryId", "");
            this.categoryName = obj.optString("categoryName", "");

            // -- Cleaning
            /*if (this.school.equals("null")) {
                this.school = "N/A";
            }

            if (this.validityDate.equals("null")) {
                this.validityDate = "N/A";
            }

            if (this.certificateNumber.equals("null")) {
                this.certificateNumber = "N/A";
            }*/
        }
    }

    public static ArrayList<CandiateEducationModel> parse(JSONArray array) {

        if (array != null) {

            ArrayList<CandiateEducationModel> courses = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                courses.add(new CandiateEducationModel(obj));
            }
            return courses;
        }
        return null;
    }

    public TextDrawable getInitials(int color, final Context context) {

        String initials = "-";
        if (!this.courseName.equals("")) {

            if (this.courseName.charAt(0) == ' ') {
                this.courseName = this.courseName.replaceFirst(" ", "");
            }

            String[] nameParts = this.courseName.split(" ");
            if (nameParts.length > 0) {
                initials = String.valueOf(nameParts[0].charAt(0));
            }
            if (nameParts.length > 1) {
                initials = initials + nameParts[1].charAt(0);
            }
        }
        return TextDrawable.builder().beginConfig()
                .bold()
                .toUpperCase()
                .fontSize(40)
                .useFont(ResourcesCompat.getFont(context, R.font.os_bold))
                .bold()
                .endConfig()
                .buildRound(initials, color);
    }

    //  --
    public String getId() {
        return id;
    }

    public String getTalentCourseId() {
        return talentCourseId;
    }

    public String getTalentId() {
        return talentId;
    }

    public Boolean getHasCertificate() {
        return hasCertificate;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public String getCertificateFileUrl() {
        return certificateFileUrl;
    }

    public String getValidityDate() {
        return validityDate;
    }

    public String getTalentCourseDescription() {
        return talentCourseDescription;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public String getCourseDate() {
        return courseDate;
    }

    public String getCourseSchool() {
        return courseSchool;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }
}