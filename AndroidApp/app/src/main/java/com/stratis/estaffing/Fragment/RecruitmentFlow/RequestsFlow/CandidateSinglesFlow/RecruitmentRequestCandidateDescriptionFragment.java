package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandiateEducationModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Talent.TalentService;

public class RecruitmentRequestCandidateDescriptionFragment extends ABFragment {

    private RequestCandidate candidate;

    public RecruitmentRequestCandidateDescriptionFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_candidate_description, container, false);

        this.setHeader("");

        this.getCandidateDescription();
        this.initViews();

        return this.mView;
    }

    private void initViews() {

        // -- Title
        final TextView title = this.mView.findViewById(R.id.title);
        final String name = String.format(
                getString(R.string.recruitment_request_candidate_description_title),
                App.capitalize(this.candidate.getFirstNameShort()));
        title.setText( name );
    }

    private void getCandidateDescription() {

        final TextView content = this.mView.findViewById(R.id.content);
        TalentService.getDescription(this.candidate.getId(), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {
                content.setText( response.body );
            }

            @Override
            public void onError(ReSTResponse response) {
                content.setText( candidate.getDescription() );
            }
        });
    }

    // --
    public void setCandidate(RequestCandidate candidate) {
        this.candidate = candidate;
    }
}