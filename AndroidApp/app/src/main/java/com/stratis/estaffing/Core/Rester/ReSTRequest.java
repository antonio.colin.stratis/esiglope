package com.stratis.estaffing.Core.Rester;

import android.net.Uri;
import androidx.collection.ArrayMap;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class ReSTRequest {

    protected String mEndpoint;
    protected String mMethod;
    protected ArrayMap<String, String> mHeaders;
    protected ArrayMap<String, String> mParameters;
    protected ArrayMap<String, String> mFields;
    protected ArrayMap<String, File> mFiles = null;

    public static final int REST_REQUEST_METHOD_GET = 0;
    public static final int REST_REQUEST_METHOD_POST = 1;
    public static final int REST_REQUEST_METHOD_PUT = 2;

    public static final int REST_REQUEST_QUERY_PARAMETERS = 0;
    public static final int REST_REQUEST_QUERY_FIELDS = 1;

    //final private String boundary =  "*****";
    final private String boundary = UUID.randomUUID().toString();
    final private String crlf = "\r\n";
    final private String twoHyphens = "--";

    // -- Body json
    protected String bodyJson = null;

    public ReSTRequest(int method, String endpoint) {

        switch (method) {
            case REST_REQUEST_METHOD_POST:
                mMethod = "POST";
            break;
            case REST_REQUEST_METHOD_PUT:
                mMethod = "PUT";
                break;
            case REST_REQUEST_METHOD_GET:
            default:
                mMethod = "GET";
            break;
        }
        mEndpoint = endpoint;
        mParameters = new ArrayMap<>();
        mFields = new ArrayMap<>();
        mHeaders = new ArrayMap<>();
    }

    public void addHeader(String name, String value) {
        mHeaders.put(name, value);
    }

    public void addHeaders(ArrayMap<String, String> headers) {

        if (this.mHeaders == null || this.mHeaders.size() == 0) {
            this.mHeaders = headers;
        } else {
            for (Map.Entry<String, String> entry : this.mHeaders.entrySet()) {
                this.addHeader(entry.getKey(), entry.getValue());
            }
        }
    }

    public void addParameter(String name, String value) {
        mParameters.put(name, value);
    }

    public void addField(String name, String value) {
        mFields.put(name, value);
    }

    public void addFile(String name, File file) {

        if (file != null) {

            if (this.mFiles == null) {
                this.mFiles = new ArrayMap<>();
                this.addHeader("Connection", "Keep-Alive");
                this.addHeader("Content-Type", "multipart/form-data;boundary=" + boundary);
            }
            this.mFiles.put(name, file);
        }
    }

    public void addBodyJson(String json) {
        this.bodyJson = json;
    }

    public String buildQuery(int type) {

        String query = "";
        Uri.Builder builder = new Uri.Builder();
        ArrayMap<String, String> map = null;
        switch (type) {
            case REST_REQUEST_QUERY_PARAMETERS:
                map = mParameters;
                break;
            case REST_REQUEST_QUERY_FIELDS:
                map = mFields;
                break;
        }
        if (map != null && map.size() > 0) {
            String name, value;
            for (int i = 0; i < map.size(); i++) {
                name = map.keyAt(i);
                value = map.valueAt(i);
                builder.appendQueryParameter(name, value);
            }
            query = builder.build().getEncodedQuery();
        }
        return query;
    }

    public void buildPost(DataOutputStream writer, int type) {

        try {

            if (this.mFiles != null && type == ReSTRequest.REST_REQUEST_QUERY_FIELDS) {

                // -- Build fields
                this.buildFields(writer);
                this.buildFiles(writer);

                // -- Write tne finish of the file
                writer.writeBytes(crlf);
                writer.writeBytes(twoHyphens + boundary + twoHyphens + crlf);
                writer.flush();

            } else {

                // --  Writing the only parameters
                writer.writeBytes(this.buildQuery(type));
                writer.flush();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void buildFields(DataOutputStream writer) {

        try {

            for (Map.Entry<String, String> entry : mFields.entrySet()) {

                writer.writeBytes(this.twoHyphens + this.boundary + this.crlf);
                writer.writeBytes("Content-Disposition: form-data; name=\"" + entry.getKey() + "\""+ this.crlf);
                writer.writeBytes("Content-Type: text/plain; charset=UTF-8" + this.crlf);
                writer.writeBytes(this.crlf);
                writer.writeBytes(entry.getValue() + this.crlf);
                writer.flush();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void buildFiles(DataOutputStream writer) {

        if (mFiles != null) {

            try {

                for (Map.Entry<String, File> entry : mFiles.entrySet()) {

                    String fileName = entry.getValue().getName();

                    writer.writeBytes(twoHyphens + boundary + crlf);
                    writer.writeBytes("Content-Disposition: form-data; name=\"" + entry.getKey() + "\";filename=\"" + fileName + "\"" + crlf + crlf);
                    byte[] bytes = this.getFileBytes(entry.getValue());
                    writer.write(bytes);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private byte[] getFileBytes(File file) {

        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {

            /*InputStream is = new FileInputStream(file);
            is.read(bytes);
            is.close();*/


            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();

        } catch (FileNotFoundException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.d("DXGOP", "Error in file 1 :: " + e.getLocalizedMessage());

        } catch (IOException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.d("DXGOP", "Error in file 2 :: " + e.getLocalizedMessage());

        }
        return bytes;
    }

    /** DEPRECIETED AND NOT WORKING **/
    private byte[] getByes(InputStream i) {

        byte[] bytes = new byte[2048];

        try {

            BufferedInputStream buf = new BufferedInputStream(i);
            buf.read(bytes, 0, bytes.length);
            buf.close();

        } catch (IOException e) {

            Log.d("DXGOP", "Error ImputSream -> bytes[] :: " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return bytes;
    }

    private String getRandomName() {

        Random r = new Random();
        int i = r.nextInt(80 - 1) + 1;
        char c = (char)(r.nextInt(26) + 'a');
        char e = (char)(r.nextInt(26) + 'a');
        return String.format("%c%c%x%c%x%c", c, c, i, c, i, c);
    }
}