package com.stratis.estaffing.Core.Rester;

public interface ReSTCallback {

    public void onSuccess(ReSTResponse response);
    public void onError(ReSTResponse response);
}
