package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.Model.RequestPausedCancelFeedbackModel;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.PausedCancelFeedback.View.RequestPausedCancelFeedbackView;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class RequestPausedCancelFeedbackAdapter extends RecyclerView.Adapter<RequestPausedCancelFeedbackView> {

    protected ArrayList<RequestPausedCancelFeedbackModel> items;
    protected Context context;
    protected RequestPausedCancelFeedbackAdapterListener listener;
    protected int previousHolder = -1;

    public RequestPausedCancelFeedbackAdapter(ArrayList<RequestPausedCancelFeedbackModel> items, Context context) {

        this.context = context;
        this.items = items;
    }

    @Override
    public RequestPausedCancelFeedbackView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recruitment_request_single_modal_item, parent, false);
        return new RequestPausedCancelFeedbackView(layoutView);
    }

    @Override
    public void onBindViewHolder(final RequestPausedCancelFeedbackView holder, final int position) {

        holder.setIsRecyclable(false);
        final RequestPausedCancelFeedbackModel item = this.items.get(position);

        // --
        holder.getName().setText(item.getValueItem());
        if (item.getSelected()) {

            holder.getBox().setBackground(context.getResources().getDrawable(R.drawable.request_paused_cancel_feedback_modal_item_selected));
            App.setViewAsPrimaryColor(holder.getBox(), context);
            holder.getName().setTextColor(Color.WHITE);
        }

        // --
        holder.getBox().setOnClickListener(v -> {

            // --
            if (this.previousHolder != -1) {
                this.items.get(previousHolder).setSelected(false);
            }
            this.items.get(position).setSelected(true);
            previousHolder = position;

            // --
            if (listener != null) {
                listener.onOptionClick(item.getValueItem());
            }
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void setListener(RequestPausedCancelFeedbackAdapterListener listener) {
        this.listener = listener;
    }

    public interface RequestPausedCancelFeedbackAdapterListener {
        public void onOptionClick(String option);
    }
}