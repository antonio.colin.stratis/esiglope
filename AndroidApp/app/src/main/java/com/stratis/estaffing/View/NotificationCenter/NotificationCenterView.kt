package com.stratis.estaffing.View.NotificationCenter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.R

/**
 * Created by Erick Sanchez
 * Revision 1 - 24/05/21
 */
open class NotificationCenterView(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var shape:ConstraintLayout? = null
    var iconContainer:ConstraintLayout? = null
    var icon:ImageView? = null
    var title:TextView? = null
    var content:TextView? = null
    var dateTime:TextView? = null

    init {

        this.shape = itemView.findViewById(R.id.shape)
        this.iconContainer = itemView.findViewById(R.id.backgroundIcon)
        this.icon = itemView.findViewById(R.id.icon)
        this.title = itemView.findViewById(R.id.title)
        this.content = itemView.findViewById(R.id.content)
        this.dateTime = itemView.findViewById(R.id.dateTime)
    }
}