package com.stratis.estaffing.Controller.Configuration

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Fragment.DeleteAccount.DeleteDisclaimerFragment
import com.stratis.estaffing.R

class DeleteController: AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_controller)

        val deleteDisclaimerFragment = DeleteDisclaimerFragment()
        this.loadFragment(deleteDisclaimerFragment)
    }

    override fun onClick(v: View?) {

    }

    private fun loadFragment(fragment: ABFragment) {

        this.supportFragmentManager.
        beginTransaction().
        replace(R.id.fragment_container, fragment).
        commit()
        return
    }
}
