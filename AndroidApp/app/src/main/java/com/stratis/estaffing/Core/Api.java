package com.stratis.estaffing.Core;

import android.annotation.SuppressLint;

import androidx.collection.ArrayMap;

import com.stratis.estaffing.BuildConfig;

/**
 * Created by Erick Sanchez
 * Revision 1 - 24/07/20
 */

public class Api {

    public final static String QAFlavor = "QA";
    public final static String DEVFlavor = "DEV";
    public final static String PRODFlavor = "PROD";

    @SuppressLint("StaticFieldLeak")
    private static Api mInstance;
    private Boolean log = true;

    private final Boolean isDev = true;
    private String flavor = "DEV";

    public static Api getInstance() {

        if ( mInstance == null ) {
            Class clazz = Api.class;
            synchronized (clazz) {
                mInstance = new Api();
            }
        }
        return mInstance;
    }

    private Api() {
        this.flavor = BuildConfig.FLAVOR;
    }

    public String getLoginUrl(String endpoint) {

        String url = "";
        switch(this.flavor) {
            case "DEV":
                url = "http://192.168.0.7:8080/cognitoauth/rest/auth";
                break;
            case "QA":
                url = "https://be-qa-estaffing.mystratis.com:23412/cognitoauth/rest/auth";
                break;
            case "PROD":
                url = "https://be-prod-estaffing.mystratis.com:34523/cognitoauth/rest/auth";
                break;
        }
        return url + endpoint;
    }

    public String getUrl(String endpoint) {

        String url = "";
        switch(this.flavor) {
            case "DEV":
                url = "http://192.168.0.7:8080/eStaffingDraftService/api";
                break;
            case "QA":
                url = "https://be-qa-estaffing.mystratis.com:23412/eStaffingDraftService/api";
                break;
            case "PROD":
                url = "https://be-prod-estaffing.mystratis.com:34523/eStaffingDraftService/api";
                break;
        }
        return url + endpoint;
    }

    public Boolean isDev() {
        return isDev;
    }

    public Boolean canLog() {
        return log;
    }

    public String getFlavor() {
        return flavor;
    }

    public static ArrayMap<String, String> getBasicHeaders() {

        ArrayMap<String, String> headers = new ArrayMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "application/json");
        String local = Session.get(App.getAppContext()).getLocalIdentifier();
        headers.put("Accept-Language", App.getAppContext().getResources().getConfiguration().locale.toString() );

        /*if (!token.equals("")) {
            headers.put("Authorization", "Bearer " + token);
        }*/
        return headers;
    }

    public static ArrayMap<String, String> getSecureHeaders() {

        ArrayMap<String, String> headers = Api.getBasicHeaders();
        headers.put("Authorization", "Bearer " + Session.get(App.getAppContext()).getIdToken() );
        return headers;
    }
}