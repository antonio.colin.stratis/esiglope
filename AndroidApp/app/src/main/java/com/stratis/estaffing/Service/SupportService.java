package com.stratis.estaffing.Service;


import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Core.eStaffingClient;

/**
 * Created by Erick Sanchez
 * Revision 1 - 15/12/20
 */

public class SupportService {

    public static void getSupportInfo(final Session session, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/user/" + session.getDataSessionId() + "/supportInfo");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("userId", session.getDataSessionId());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }
}