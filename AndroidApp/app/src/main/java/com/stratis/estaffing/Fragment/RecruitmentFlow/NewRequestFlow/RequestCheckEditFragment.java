package com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stratis.estaffing.Control.eStaffingFragment;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.R;

public class RequestCheckEditFragment extends eStaffingFragment {

    public enum RequestCheckEditType {
        vacancies, profile, language, modality, budget
    }

    private RequestCheckEditType type = RequestCheckEditType.vacancies;
    private Profile profile;

    public RequestCheckEditFragment() {}

    public RequestCheckEditFragment(final RequestCheckEditType type, final Profile profile) {
        this.type = type;
        this.profile  = profile;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_request_check_edit, container, false);;

        return this.mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        eStaffingFragment fragment = null;
        switch(this.type)  {

            case vacancies:
                fragment = new RequestVacancyFragment();
                break;
        }
        transaction.replace(R.id.fragment_container, fragment).commit();
    }
}