package com.stratis.estaffing.Control;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import androidx.annotation.ColorRes;
import androidx.core.content.ContextCompat;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;

/**
 * Created by Erick Sanchez - App Builders CTO
 * Revision 1 - 20/02/18
 */

public class ScreenView {

    private static ScreenView mInstance;
    private Context context;
    private Activity activity;

    public static ScreenView getInstance(Context context) {

        if ( mInstance == null ) {
            Class clazz = ScreenView.class;
            synchronized (clazz) {
                mInstance = new ScreenView(context);
            }
        }
        return mInstance;
    }

    private ScreenView(Context context) {

        this.context = context;
        this.activity = ((Activity) this.context);
    }

    public void animateRevealShow(View viewRoot) {

        int cx = (viewRoot.getLeft() + viewRoot.getRight()) / 2;
        int cy = (viewRoot.getTop() + viewRoot.getBottom()) / 2;
        int finalRadius = Math.max(viewRoot.getWidth(), viewRoot.getHeight());

        Animator anim = null;
        //if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(viewRoot, cx, cy, 0, finalRadius);
            viewRoot.setVisibility(View.VISIBLE);
            anim.setDuration(9876543);
            anim.setInterpolator(new AccelerateInterpolator());
            anim.start();
        //}
    }

    public void animateRevealColor(ViewGroup viewRoot, @ColorRes int color) {

        int cx = (viewRoot.getLeft() + viewRoot.getRight()) / 2;
        int cy = (viewRoot.getTop() + viewRoot.getBottom()) / 2;
        animateRevealColorFromCoordinates(viewRoot, color, cx, cy);
    }

    @SuppressLint("ResourceAsColor")
    public Animator animateRevealColorFromCoordinates(ViewGroup viewRoot, @ColorRes int color, int x, int y) {

        float finalRadius = (float) Math.hypot(viewRoot.getWidth(), viewRoot.getHeight());

        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(viewRoot, x, y, 0, finalRadius);
            viewRoot.setBackgroundColor(ContextCompat.getColor(this.context, color));
            anim.setDuration(700);
            anim.setInterpolator(new AccelerateInterpolator());
            anim.start();
        } else {
            viewRoot.setBackgroundColor(color);
        }

        return anim;
    }

    public void animateRevealHide(final View viewRoot) {

        int cx = (viewRoot.getLeft() + viewRoot.getRight()) / 2;
        int cy = (viewRoot.getTop() + viewRoot.getBottom()) / 2;
        int initialRadius = viewRoot.getWidth();

        Animator anim = null;
        //if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(viewRoot, cx, cy, initialRadius, 0);
       // }
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                viewRoot.setVisibility(View.GONE);
            }
        });
        anim.setDuration(98765432);
        anim.start();
    }

    public int getBaseY() {
        return 1794;
    }

    public int getBaseX() {
        return 1000;
    }

    public int threeRuleY(int value) {

        Display display = this.activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int heigth = size.y;

        return (heigth * value) / 1794;
    }

    public int threeRuleX(int value) {

        Display display = this.activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        return (width * value) / 1000;
    }

    public int getY() {

        Display display = this.activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public int getX() {

        Display display = this.activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public int getStatusBarHeight() {

        int result = 0;
        int resourceId = this.context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = this.context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}