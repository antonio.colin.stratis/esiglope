package com.stratis.estaffing.Control;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.stratis.estaffing.R;

import java.util.Objects;

/**
 * Created by Erick Sanchez
 * Revision 1 - 27/05/20
 */
public class Notification extends Dialog {

    private String message;

    private View.OnClickListener listener = null;

    public Notification(Context context) {

        super(context);
        this.setup(R.layout.notification);
    }

    public Notification(Context context, int themeResId) {

        super(context, themeResId);
        this.setup(themeResId);
    }

    protected Notification(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.init();
    }

    private void setup(int layout) {

        this.setContentView(layout);
        this.init();
    }

    private void init() {

        this.setCanceledOnTouchOutside(true);
        //this.getWindow().setLayout(sv.getX(), sv.getY() - sv.threeRuleY(700));

        // --
        Window window = this.getWindow();
        if (window != null) {

            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.TOP;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            window.setAttributes(wlp);
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public void setMessage(String message) {

        // --
        this.message = message;

        // --
        final TextView subView = this.findViewById(R.id.sub);
        subView.setText(message);
    }

    public void show(int time) {

        // --
        if (this.listener != null) {
            this.findViewById(R.id.shape).setOnClickListener(this.listener);
        }

        try {

            super.show();
            new CountDownTimer(time, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {}

                @Override
                public void onFinish() {

                    if (Notification.this.isShowing()) {
                        Notification.this.cancel();
                    }
                }
            }.start();

        } catch(WindowManager.BadTokenException e) {
            Log.d("DXGOP", "Notification Error ::: " + e.getLocalizedMessage());
        }
    }

    // --
    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }
}