package com.stratis.estaffing.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Modal.LanguagesModal;
import com.stratis.estaffing.Model.Language;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 18/06/20
 */

public class LanguageAdapter extends ArrayAdapter<Language> {

    public LanguageAdapter(Context context, ArrayList<Language> languages) {
        super(context, R.layout.fragment_talent_language_item, languages);
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final Language language = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(getContext());

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.fragment_talent_language_item, parent, false);
            final LanguageViewHolder holder = new LanguageViewHolder(convertView);
            convertView.setTag(holder);

            // -- Logic for views
            holder.getLanguageName().setText( language.getName() );
            holder.getLanguageLevel().setText( language.getLevel() );
            holder.getLanguageBar().setProgress( language.getPercentage() );
            holder.getLanguageBar().setProgressColor(Color.parseColor(App.fixWrongColorATuDePiglek(Session.get(getContext()).getPreferences().getSecondaryColor())));
            holder.getLanguageInfo().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final LanguagesModal languagesModal = new LanguagesModal(getContext());
                    languagesModal.show();
                }
            });

        } else {

            final LanguageViewHolder holder = (LanguageViewHolder) convertView.getTag();
            convertView.setTag(holder);
        }

        // --
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class LanguageViewHolder {

        private View row;
        private TextView language_name = null;
        private RoundCornerProgressBar language_bar = null;
        private TextView language_level = null;
        private ImageButton language_info = null;

        public LanguageViewHolder(View row) {
            this.row = row;
        }

        public TextView getLanguageName() {

            if (this.language_name == null) {
                this.language_name = row.findViewById(R.id.language_name);
            }
            return this.language_name;
        }

        public RoundCornerProgressBar getLanguageBar() {

            if (this.language_bar == null) {
                this.language_bar = row.findViewById(R.id.language_bar);
            }
            return this.language_bar;
        }

        public TextView getLanguageLevel() {

            if (this.language_level == null) {
                this.language_level = row.findViewById(R.id.language_level);
            }
            return this.language_level;
        }

        public ImageButton getLanguageInfo() {

            if (this.language_info == null) {
                this.language_info = row.findViewById(R.id.language_info);
            }
            this.language_info.setBackgroundResource(R.drawable.interrogation_symbol);
            App.setViewAsSecondaryColor(this.language_info, getContext());
            return this.language_info;
        }

    }
}
