package com.stratis.estaffing.Adapter.TalentV3

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Model.Language
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidateSkill
import com.stratis.estaffing.R
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.RequestCandidateSkillView
import java.util.*

open class LanguageAdapter(items: ArrayList<Language>?) : RecyclerView.Adapter<RequestCandidateSkillView>() {

    private var items: ArrayList<Language>? = null

    init {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestCandidateSkillView {
        val layoutView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.fragment_recruitment_request_candidate_single_item, parent, false)
        return RequestCandidateSkillView(layoutView)
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RequestCandidateSkillView, position: Int) {
        holder.setIsRecyclable(false)
        val language : Language = this.items!![position]

        // --
        holder.name?.text = language.name
        holder.percentBar.progress = language.percentage.toFloat()
        holder.percentBar.progressBarColor = Color.parseColor(App.fixWrongColorATuDePiglek(Session.get(holder.itemView.context).preferences.secondaryColor))
        holder.percent.text = "${language.percentage} %"
        holder.languageLevel.setText(language.level)
    }
}