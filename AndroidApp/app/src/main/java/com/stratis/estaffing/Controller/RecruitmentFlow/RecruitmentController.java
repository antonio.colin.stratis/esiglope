package com.stratis.estaffing.Controller.RecruitmentFlow;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.stratis.estaffing.Control.ABActivity;
import com.stratis.estaffing.Controller.RecruitmentFlow.RequestsFlow.RecruitmentRequestsController;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.R;

public class RecruitmentController extends ABActivity {

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_recruitment_controller);
        //setContentView(R.layout.activity_recruitment_controller_disabled);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName());

        // -- Set header
        this.setHeader(this.getResources().getString(R.string.recruitment_header_title), true);

        // -- Init Firebase
        this.mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        // --  Init views
        this.initViews();
    }

    private void initViews() {

        final Button continueButton = this.findViewById(R.id.continueButton);
        continueButton.setOnClickListener(v -> {
            mFirebaseAnalytics.logEvent("Solicitudes_Iniciadas", new Bundle());
            startActivity(new Intent(RecruitmentController.this, FirstTimeController.class));
        });
        this.setViewAsPrimaryColor(continueButton);

        // --
        final Button requestsButton = this.findViewById(R.id.requestsButton);
        requestsButton.setOnClickListener(v -> startActivity(new Intent(RecruitmentController.this, RecruitmentRequestsController.class)));
    }

}