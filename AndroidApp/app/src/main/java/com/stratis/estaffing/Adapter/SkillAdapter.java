package com.stratis.estaffing.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.Model.Language;
import com.stratis.estaffing.Model.Skill;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 18/06/20
 */

public class SkillAdapter extends ArrayAdapter<Skill> {

    public SkillAdapter(Context context, ArrayList<Skill> skills) {
        super(context, R.layout.modal_skills_item, skills);
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final Skill skill = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(getContext());

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.modal_skills_item, parent, false);
            final SkillViewHolder holder = new SkillViewHolder(convertView);
            convertView.setTag(holder);

            // -- Logic for views
            holder.getName().setText( skill.getName() );

        } else {

            final SkillViewHolder holder = (SkillViewHolder) convertView.getTag();
            convertView.setTag(holder);
        }

        convertView.setOnClickListener(null);

        // --
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class SkillViewHolder {

        private View row;
        private TextView name = null;

        public SkillViewHolder(View row) {
            this.row = row;
        }

        public TextView getName() {

            if (this.name == null) {
                this.name = row.findViewById(R.id.name);
            }
            return this.name;
        }
    }
}
