package com.stratis.estaffing.Fragment.MasterAdminFlow

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.MasterAdminFlow.MALeadersAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.FirebaseEvent
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Model.MasterAdminFlow.Leader
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.MasterAdmin.MasterAdminService

open class MALeadersFragment : ABFragment(), View.OnClickListener {

    var leaders:ArrayList<Leader>? = null
    var list:RecyclerView? = null
    var btnSeeActive:Button? = null
    var btnSeeAll:Button? = null
    var noItemsRequest: ConstraintLayout? = null
    var noItemsLeaders: ConstraintLayout? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_m_a_leaders, container, false)
        this.setHeader(Session.get(mContext).clientName)
        this.noItemsRequest = findView(R.id.no_request)
        this.noItemsLeaders = findView(R.id.no_leaders)
        this.getLeaders()
        this.initViews()
        return this.mView
    }

    private fun initViews() {

        this.list = this.findView(R.id.list)
        App.createVerticalRecyclerList(this.list, mContext)

        btnSeeActive = mView.findViewById(R.id.btnSeeActive)
        setViewAsSecondaryColor(btnSeeActive)
        btnSeeActive!!.setTextColor(ContextCompat.getColor(mContext, R.color.white))
        btnSeeActive!!.tag = 10
        btnSeeActive!!.setOnClickListener(this)

        btnSeeAll = mView.findViewById(R.id.btnSeeAll)
        btnSeeAll!!.background.setTint(ContextCompat.getColor(mContext,R.color.white))
        btnSeeAll!!.tag = 11
        btnSeeAll!!.setOnClickListener(this)
    }

    private fun getLeaders() {

        val session:Session = Session.get(mContext)
        MasterAdminService.getLeaders(session, object :ReSTCallback {

            override fun onSuccess(response: ReSTResponse?) {

                val res = JsonBuilder.stringToJsonArray(response?.body)
                if( res.length() != 0) {
                    leaders = Leader.parse(res)
                    if(leaders?.filter { it.vacancyCount == 0 }?.size == leaders?.size) {
                        paintNoItems(true)
                    } else {
                        setLeaders(true, false, true)
                    }
                } else {
                    paintNoItems(true)
                }
            }

            override fun onError(response: ReSTResponse?) {
                paintNoItems(true)
            }
        })
    }

    private fun setLeaders(filtred: Boolean, handleView: Boolean, isRequest: Boolean) {
        var leadersFiltred : ArrayList<Leader>
        if(filtred)
            leadersFiltred = leaders!!.filter { it.vacancyCount > 0 } as ArrayList<Leader>
        else
            leadersFiltred = leaders!!

        // -- Check for empty items
        if (leadersFiltred?.size!! <= 0) {
            this.paintNoItems(isRequest)
            return
        }

        if(handleView){
            this.list?.visibility = View.VISIBLE
            this.noItemsRequest?.visibility = View.GONE
            this.noItemsLeaders?.visibility = View.GONE
        }

        val adapter = MALeadersAdapter(leadersFiltred)
        adapter.listener = object: MALeadersAdapter.MALeadersAdapterListener {
            override fun onDetail(leader: Leader) {

                val requestsFragment = MARequestsFragment()
                requestsFragment.leader = leader
                continueSegue(requestsFragment)
            }
        }
        this.list?.adapter = adapter
    }

    @SuppressLint("SetTextI18n")
    private fun paintNoItems(isRequest: Boolean) {

        // -- Hide all
        this.list?.visibility = View.GONE

        // --
//        val actionButton = findView<Button>(R.id.actionButton)
        if(isRequest) {
            this.noItemsRequest?.visibility = View.VISIBLE
            if(this.noItemsLeaders!!.isVisible)
                this.noItemsLeaders?.visibility = View.GONE
        } else {
            this.noItemsLeaders?.visibility = View.VISIBLE
            if(this.noItemsRequest!!.isVisible)
                this.noItemsRequest?.visibility = View.GONE
        }

//        (noItems.findViewById<View>(R.id.content) as TextView).text = getString(R.string.messages_no_request_items_content)
//        actionButton.background = ContextCompat.getDrawable(mContext, R.drawable.fragment_recruitment_request_candidate_education_bk)
//        actionButton.text = "Habla con soporte"
//        actionButton.setTextColor(Color.parseColor("#767676"))
        // --
//        actionButton.setOnClickListener { v: View? -> startActivity(Intent(mContext, SupportController::class.java)) }

        FirebaseEvent.logEvent(mContext, this::class.simpleName.plus("no_items"))
    }

    override fun onClick(v: View?) {
        when(v!!.tag.toString()) {
            "10" -> {
                setViewAsSecondaryColor(btnSeeActive)
                btnSeeActive!!.setTextColor(ContextCompat.getColor(mContext, R.color.white))
                btnSeeAll!!.background.setTint(ContextCompat.getColor(mContext,R.color.white))
                btnSeeAll!!.setTextColor(ContextCompat.getColor(mContext, R.color.home_title))
                if(leaders!=null)
                    setLeaders(true, true, true)
                else
                    paintNoItems(true)
            }
            "11" -> {
                setViewAsSecondaryColor(btnSeeAll)
                btnSeeAll!!.setTextColor(ContextCompat.getColor(mContext, R.color.white))
                btnSeeActive!!.background.setTint(ContextCompat.getColor(mContext,R.color.white))
                btnSeeActive!!.setTextColor(ContextCompat.getColor(mContext, R.color.home_title))
                if(leaders!=null && !leaders!!.isEmpty())
                    setLeaders(false, true, false)
                else
                    paintNoItems(false)
            }
        }
    }
}