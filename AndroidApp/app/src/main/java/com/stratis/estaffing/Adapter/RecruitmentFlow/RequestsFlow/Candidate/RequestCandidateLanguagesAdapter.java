package com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateLanguageModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateWorkExperienceModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidateSkill;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate.CandidateLanguageView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class RequestCandidateLanguagesAdapter extends RecyclerView.Adapter<CandidateLanguageView> {

    protected ArrayList<CandidateLanguageModel> items;
    private RequestCandidateLanguagesAdapterListener listener;

    public RequestCandidateLanguagesAdapter(ArrayList<CandidateLanguageModel> items) {
        this.items = items;
    }

    @Override
    public CandidateLanguageView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recruitment_request_candidate_languages_item,
                parent, false);
        return new CandidateLanguageView(layoutView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final CandidateLanguageView holder, final int position) {

        holder.setIsRecyclable(false);
        final CandidateLanguageModel language = this.items.get(position);

        // --
        holder.getName().setText( language.getLangName() );
        holder.getPercentBar().setProgress(Float.parseFloat(language.getLangLevelPercentage()));
        holder.getPercentBar().setProgressBarColor(Color.parseColor(App.fixWrongColorATuDePiglek(Session.get(holder.itemView.getContext()).getPreferences().getSecondaryColor())));
        holder.getPercent().setText( language.getLangLevelPercentage() + "%" );
        holder.getLanguageLevel().setText( language.getLangLevelName() );

        // --
        final boolean cl = language.getVideoUrl().equals("");
        if (cl) {
            holder.getPlayButtonItem().setVisibility(View.GONE);
        }

        holder.getShape().setOnClickListener(v -> {
          if (listener != null && !cl) {
              listener.onDetail(position);
          }
        });
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<CandidateLanguageModel> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<CandidateLanguageModel> getItems() {
        return items;
    }

    public void setListener(RequestCandidateLanguagesAdapterListener listener) {
        this.listener = listener;
    }

    public interface RequestCandidateLanguagesAdapterListener {
        public void onDetail(int position);
    }
}