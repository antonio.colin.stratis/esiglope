package com.stratis.estaffing.Adapter.RecruitmentFlow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Model.RecruitmentFlow.SoftwareCatalog;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.NewRequestFlow.SelectedSoftwareView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class SelectedSoftwareAdapter extends RecyclerView.Adapter<SelectedSoftwareView> {

    protected ArrayList<SoftwareCatalog> items;
    protected Context context;
    private int currentAddedSoftPointer = 0;
    protected SelectedSkillAdapter.OnCloseButtonListerner listener;

    public SelectedSoftwareAdapter(ArrayList<SoftwareCatalog> items, Context context) {

        this.context = context;
        this.items = items;

        if (this.items.size() >= 1 && !this.items.get(0).getName().equals("")) {
            this.currentAddedSoftPointer = this.items.size()-1;
        }
    }

    @Override
    public SelectedSoftwareView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_request_budget_item, parent, false);
        return new SelectedSoftwareView(layoutView);
    }

    @Override
    public void onBindViewHolder(final SelectedSoftwareView holder, final int position) {

        holder.setIsRecyclable(false);
        final SoftwareCatalog software = this.items.get(position);
        final Boolean isFilled = !software.getName().equals("");

        // --
        holder.getTitle().setVisibility( isFilled ? View.VISIBLE : View.INVISIBLE);
        holder.getCloseButton().setVisibility( isFilled ? View.VISIBLE : View.INVISIBLE );

        if (!isFilled) {

            holder.getBox().setBackgroundDrawable( ContextCompat.getDrawable(context, R.drawable.rectangle_dotty) );

        } else {

            holder.getBox().setBackgroundDrawable( ContextCompat.getDrawable(context, R.drawable.request_vacancy_selected_button) );
            holder.getTitle().setText( software.getName() );
            holder.getCloseButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onCloseButton(position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void addElement(SoftwareCatalog skill) {

        this.items.set(this.currentAddedSoftPointer, skill);
        //this.items.add(language);
        this.currentAddedSoftPointer++;

        if (this.items.size() >= 1 && !this.items.get(this.items.size() -1).getName().equals("")) {
            this.items.add(new SoftwareCatalog());
        }
    }

    public void removeElement(int index) {

        this.items.remove(index);
        this.currentAddedSoftPointer--;

        if (this.items.size() <= 2) {
            this.items.add(new SoftwareCatalog());
        }
    }

    public void reload(ArrayList<SoftwareCatalog> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<SoftwareCatalog> getItems() {
        return items;
    }

    public SelectedSkillAdapter.OnCloseButtonListerner getListener() {
        return listener;
    }

    public void setListener(SelectedSkillAdapter.OnCloseButtonListerner listener) {
        this.listener = listener;
    }
}
