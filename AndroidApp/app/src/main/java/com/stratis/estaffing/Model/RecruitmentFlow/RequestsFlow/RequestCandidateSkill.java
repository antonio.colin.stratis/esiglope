package com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 18/12/20
 *
 */

public class RequestCandidateSkill {

    public enum RequestCandidateSkillType { soft, hard, language, skill }

    private String id = "";
    private String name = "";
    private String percentage = "";
    private String langLevelName = "";
    private RequestCandidateSkillType type = RequestCandidateSkillType.hard;

    public RequestCandidateSkill() {}

    public RequestCandidateSkill(boolean isSkill){
        if(isSkill)
            this.type = RequestCandidateSkillType.skill;
    }

    public RequestCandidateSkill(JSONObject obj, Boolean asLanguage) {

        if (obj != null) {

            this.id = obj.optString("id");
            if (asLanguage) {

                this.type = RequestCandidateSkillType.language;
                this.name = obj.optString("langName");
                this.percentage = obj.optString("langLevelPercentage");
                this.langLevelName = obj.optString("langLevelName");

            } else {

                final String categoryId = obj.optString("categoryId", "1001100001");
                this.type = (categoryId.equals("1001100001")) ? RequestCandidateSkillType.hard : RequestCandidateSkillType.soft;
                this.name = obj.optString("name");
                this.percentage = obj.optString("skillLevelPercentage");
            }
        }
    }

    public static ArrayList<RequestCandidateSkill> parse(JSONArray array, Boolean asLanguage) {

        ArrayList<RequestCandidateSkill> skills = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {

            JSONObject obj = array.optJSONObject(i);
            skills.add(new RequestCandidateSkill(obj, asLanguage));
        }
        return skills;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getLangLevelName() {
        return langLevelName;
    }

    public void setLangLevelName(String langLevelName) {
        this.langLevelName = langLevelName;
    }

    public RequestCandidateSkillType getType() {
        return type;
    }
}