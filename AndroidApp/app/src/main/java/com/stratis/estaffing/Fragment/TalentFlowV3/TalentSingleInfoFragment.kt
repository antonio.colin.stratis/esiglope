package com.stratis.estaffing.Fragment.TalentFlowV3

import android.Manifest
import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.analytics.FirebaseAnalytics
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate.RequestCandidateMenuAdapter
import com.stratis.estaffing.Adapter.TalentV3.LanguageAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Control.Loader
import com.stratis.estaffing.Control.Notification
import com.stratis.estaffing.Core.Api
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.FirebaseEvent
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateDescriptionFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateLanguagesFragment
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateShare
import com.stratis.estaffing.Fragment.TalentFlowV3.Feedback.FeedbackDetailFragment
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateLanguageModel
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateMenuModel
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate
import com.stratis.estaffing.Model.Talent
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.CandidateService
import com.stratis.estaffing.Service.Talent.TalentService
import com.thin.downloadmanager.DefaultRetryPolicy
import com.thin.downloadmanager.DownloadRequest
import com.thin.downloadmanager.DownloadStatusListenerV1
import com.thin.downloadmanager.ThinDownloadManager
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class TalentSingleInfoFragment : ABFragment(), View.OnClickListener, RecruitmentRequestCandidateShare.RecruitmentRequestCandidateShareListener {

    var talentId : String? = null
    var talent : Talent? = null

    private var loader : Loader? = null
    private var dotsMenuOpened = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_talent_single, container, false)

        setHeader(getString(R.string.talent_single_header_title), true)
        loader = Loader(mContext)
        loader!!.show()
        this.initViews()
        this.getTalent()
        this.paintMenu()

        val fm: FragmentManager? = activity?.supportFragmentManager
        fm?.addOnBackStackChangedListener {
            if (fm.backStackEntryCount == 0) {
                loader = Loader(mContext)
                loader!!.show()
                getTalent()
            }
        }
        return this.mView

    }

    private fun initViews() {
        setViewAsPrimaryColor(mView.findViewById(R.id.backHeader))
        setViewAsPrimaryColor(mView.findViewById(R.id.seniorityView))
        setViewAsSecondaryColor(mView.findViewById(R.id.imageProfile))
        setViewAsSecondaryColor(mView.findViewById(R.id.aboutContainerButton))
        setViewAsSecondaryColor(mView.findViewById(R.id.languagesContainerMoreInfoButton))
    }

    fun getTalent(){
        TalentService.get(talentId, object :ReSTCallback {
            override fun onSuccess(response: ReSTResponse?) {
                val res = JsonBuilder.stringToJson(response!!.body)
                loader!!.cancel()
                checkResponse(res)
            }

            override fun onError(response: ReSTResponse?) {
                // --
                Log.d("DXGOP", "ERROR :: " + response!!.body)
                loader!!.cancel()
            }
        })
    }

    private fun checkResponse(json: JSONObject?) {
        if(json != null) {
            talent = Talent(json)
            paintTalentInfo()
            paintAboutTalent()
            paintLanguagesTalent()
            getLanguages()
        }

    }

    @SuppressLint("CheckResult")
    private fun paintTalentInfo() {
        val imageProfile = mView.findViewById<ImageView>(R.id.imageProfile)
        imageProfile.clipToOutline = true
        if (talent!!.userImg != "") {
            Glide.with(this)
                    .load(talent!!.userImg)
                    .centerCrop()
                    .thumbnail(.50f)
                    .diskCacheStrategy(DiskCacheStrategy.NONE) // .placeholder(R.drawable.loading_spinner)
                    .skipMemoryCache(true)
                    .apply(RequestOptions.circleCropTransform())
                    .into(imageProfile)
        }

        val seniorityView = mView.findViewById<TextView>(R.id.seniorityView)
        val s = talent!!.jobSeniority
        if (s == "" || s == "null" || s == "Null" || s == "NULL") {
            seniorityView.visibility = View.GONE
        } else {
            seniorityView.text = App.capitalize(s)
        }
        /** Name and job  */
        val nameView = mView.findViewById<TextView>(R.id.nameView)
        // ONM: Se quita el capitalize para que se vea como en base
        nameView.text = talent!!.userFullName //App.capitalize(talent!!.userFullName)

        val positionView = mView.findViewById<TextView>(R.id.positionView)
        // ONM: Se quita el capitalize para que se vea como en base
        positionView.text = talent!!.position //App.capitalize(talent!!.position)

        val rateView = mView.findViewById<TextView>(R.id.rateValue)
        if(talent!!.rating != null && talent!!.rating!! != "0.0") {
            if(talent!!.rating.toString().endsWith(".0") || talent!!.rating.toString().endsWith(".00"))
                rateView.text = talent!!.rating!!.substring(0, talent!!.rating!!.lastIndexOf("."))
            else
                rateView.text = talent!!.rating.toString()
        } else {
            rateView.text = "5"
        }
        rateView.tag = "142"
        rateView.setOnClickListener(this)

        val oldView = mView.findViewById<TextView>(R.id.hireValue)
        oldView.text = App.hanledStringNull(talent!!.oldDate.plus(" (").plus(App.getDateFormatted(talent!!.hireDate, "dd/MMMM/yyyy").uppercase()).plus(")"))

        val leaderView = mView.findViewById<TextView>(R.id.leaderValue)
        leaderView.text = App.hanledStringNull(talent!!.currentLeader)

        val areaView = mView.findViewById<TextView>(R.id.areaValue)
        areaView.text = App.hanledStringNull(talent!!.positionArea)

        val periodView = mView.findViewById<TextView>(R.id.asignedValue)
        periodView.text = App.hanledStringNull(talent!!.assigmentPeriodFormatted)

        val skillsButton = mView.findViewById<Button>(R.id.skillsButton)
        skillsButton.tag = "11"
        skillsButton.setOnClickListener(this)

        val coursesButton = mView.findViewById<Button>(R.id.coursesButton)
        coursesButton.tag = "12"
        coursesButton.setOnClickListener(this)

        val experienceButton = mView.findViewById<Button>(R.id.experienceButton)
        experienceButton.tag = "13"
        experienceButton.setOnClickListener(this)

        val feedbackButton = mView.findViewById<Button>(R.id.feedbackButton)
        feedbackButton.tag = "14"
        feedbackButton.setOnClickListener(this)
    }

    private fun getLanguages() {
        CandidateService.getCandidateLanguages(talent?.id, object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {
                hideLoader()
                val res = JsonBuilder.stringToJsonArray(response.body)
                parseLanguages(res)
            }
            override fun onError(response: ReSTResponse) {
                hideLoader()
                parseLanguages(null)
            }
        })
    }

    private fun parseLanguages(res: JSONArray?) {
        val languageModels = CandidateLanguageModel.parse(res)

        var isVideo = false
        languageModels.forEach {
            Log.d("videoURL", it.toString())
            if (it.videoUrl != "" && it.videoUrl != "null") { isVideo = true }
        }

        if (isVideo) {
            mView.findViewById<LottieAnimationView>(R.id.header_animation).visibility = View.VISIBLE
            val imageProfile = mView.findViewById<ImageView>(R.id.imageProfile)
            imageProfile.tag = "15"
            imageProfile.setOnClickListener(this)
        }

    }

    @SuppressLint("SetTextI18n")
    private fun paintAboutTalent() {

        val aboutContainerTitle = mView.findViewById<TextView>(R.id.aboutContainerTitle)
        val aboutContainerTitleString = String.format(
                getString(R.string.recruitment_request_candidate_single_about_container_title),
                talent!!.firstName.substring(0, 1).uppercase(Locale.getDefault()) +
                        talent!!.firstName.substring(1).lowercase(Locale.getDefault()).split(" ")[0]
        )
        aboutContainerTitle.text = aboutContainerTitleString

        // --
        val aboutContainerContent = mView.findViewById<TextView>(R.id.aboutContainerContent)

        if (talent!!.description.length > 190) {
            val shortDescription = talent!!.description.subSequence(0, 190);
            aboutContainerContent.text = "${shortDescription}..."
            val aboutContainerButton = mView.findViewById<Button>(R.id.aboutContainerButton)
            aboutContainerButton.visibility = View.VISIBLE
            aboutContainerButton.tag = "40"
            aboutContainerButton.setOnClickListener(this)
        } else {
            aboutContainerContent.text = talent!!.description
        }
    }

    private fun paintLanguagesTalent() {

        // -- SOFT
//        val softSkillsContainerList: RecyclerView = mView.findViewById(R.id.softSkillsContainerList)
//        App.createVerticalRecyclerList(softSkillsContainerList, context)
//        val softAdapter = RequestCandidateSkillsAdapter(candidateLegacy?.softSkills)
//        softSkillsContainerList.adapter = softAdapter

        // -- HARD
//        val hardSkillsContainerList: RecyclerView = mView.findViewById(R.id.hardSkillsContainerList)
//        App.createVerticalRecyclerList(hardSkillsContainerList, context)
//        val hardAdapter = RequestCandidateSkillsAdapter(candidateLegacy?.hardSkills)
//        hardSkillsContainerList.adapter = hardAdapter

        // -- LANGUAGE
        val languagesContainerList: RecyclerView = mView.findViewById(R.id.languagesContainerList)
        App.createVerticalRecyclerList(languagesContainerList, mContext)
        val langAdapter = LanguageAdapter(talent?.languages)
        languagesContainerList.adapter = langAdapter

        // --
        val languagesContainerMoreInfoButton = mView.findViewById<Button>(R.id.languagesContainerMoreInfoButton)
        languagesContainerMoreInfoButton.tag = "16"
        languagesContainerMoreInfoButton.setOnClickListener(this)
    }

    private fun paintMenu() {

        // -- Init view for menu dots
        val dotsMenu = findView<ImageView>(R.id.dotsMenu)
        dotsMenu.tag = "41"
        dotsMenu.setOnClickListener(this)

        /** Menu Options  */
        val dotsMenuOpened = findView<ConstraintLayout>(R.id.dotsMenuOpened)
        val optionsMenu: RecyclerView = dotsMenuOpened.findViewById(R.id.options)
        val adapter = RequestCandidateMenuAdapter(CandidateMenuModel.getMenuTalent(context))
        adapter.setListener { option: CandidateMenuModel ->
            when (option.id) {
                1 -> this.downloadCV()
                2 -> this.shareCV()
            }
        }
        App.createVerticalRecyclerList(optionsMenu, mContext)
        optionsMenu.adapter = adapter
    }

    private fun downloadCV() {
        val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        Permissions.check(mContext, permissions, null, null, object : PermissionHandler() {
            override fun onGranted() {
                val currentDateTime = App.getCurrentDateTime()
                //final String destinationUrl = getApp().getExternalCacheDir().toString()+ "/" + candidate.getFirstName() + "_CV_" + currentDateTime + ".pdf";
                //val destinationUrl = app.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/" + talent!!.firstName + "_CV_" + currentDateTime + ".pdf"
                val destinationUrl = App.getDownloadPath() + "/" + talent!!.firstName + "_CV_" + currentDateTime + ".pdf"
                Log.d("DXGOP", "CV :: $destinationUrl")
                val uri = Uri.parse(Api.getInstance().getUrl("/talent/" + talent!!.id + "/downloadcv"))
                val destinationUri = Uri.parse(destinationUrl)
                val downloadRequest = DownloadRequest(uri)
                        .addCustomHeader("Authorization", "Bearer " + Session.get(App.getAppContext()).idToken)
                        .setRetryPolicy(DefaultRetryPolicy(150000, 1, 1f))
                        .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                        .setDownloadContext(this)
                        .setStatusListener(object : DownloadStatusListenerV1 {
                            override fun onDownloadComplete(downloadRequest: DownloadRequest) {
                                Log.d("DXGOP", "BAJADO EL PDF en :: $destinationUrl")
                                val notification = Notification(mContext, R.layout.notification_light)
                                val msg = getString(R.string.recruitment_request_candidate_resume_donwload_title)
                                notification.setMessage("$msg $destinationUrl")
                                notification.show(3000)
                                //hareCVLocal(destinationUri);

                                // --
                                val parameters = Bundle()
                                val mFirebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
                                mFirebaseAnalytics.logEvent("CV_Descargado", parameters)
                            }

                            override fun onDownloadFailed(downloadRequest: DownloadRequest, errorCode: Int, errorMessage: String) {
                                val notification = Notification(mContext, R.layout.notification_light_error)
                                notification.setMessage("$errorMessage -- Code :: $errorCode")
                                notification.show()
                                Log.d("DXGOP", "ERROR BAJANDO EL PDF")
                            }

                            override fun onProgress(downloadRequest: DownloadRequest, totalBytes: Long, downloadedBytes: Long, progress: Int) {
                                Log.d("DXGOP", "PROGRESS ::: $progress")
                            }
                        })
                val downloadManager = ThinDownloadManager()
                downloadManager.add(downloadRequest)
            }
        })
    }

    private fun shareCV() {
        val candidateShare = RecruitmentRequestCandidateShare()
        candidateShare.talentId = this.talentId
        candidateShare.listener = this
        this.continueSegueUp(candidateShare)
    }

    override fun onClick(v: View?) {
        when (v!!.tag.toString()) {
            "11" -> {
                val skillsFragment = TalentSkillsFragment()
                skillsFragment.skills = talent!!.skills
                this.continueSegue(skillsFragment)
            }
            "12" -> {
                val coursesFragment = TalentCoursesFragment()
                coursesFragment.courses = talent!!.courses
                this.continueSegue(coursesFragment)
            }
            "13" -> {
                val experienceFragment = TalentExperienceFragment()
                experienceFragment.experiences = talent!!.experiences
                this.continueSegue(experienceFragment)
            }
            "14", "142" -> {
                val feedbackFragment = FeedbackDetailFragment()
                feedbackFragment.talentName = this.talent?.firstName
                feedbackFragment.rating = talent!!.rating
                feedbackFragment.talentId = this.talentId
                this.continueSegueName(feedbackFragment, "initialFragment")
                //this.continueSegue(feedbackFragment)
            }
            "15" -> {
                FirebaseEvent.logEvent(mContext, "video_talento")
                val languagesFragment = RecruitmentRequestCandidateLanguagesFragment()
                val candidate = RequestCandidate()
                candidate.id = talent!!.id
                languagesFragment.setCandidate(candidate)
                this.continueSegue(languagesFragment)
            }
            "16" -> this.continueSegue(TalentLanguageInfoFragment())
            "40" -> {
                val descriptionFragment = RecruitmentRequestCandidateDescriptionFragment()
                val candidate = RequestCandidate()
                candidate.id = talent!!.id
                descriptionFragment.setCandidate(candidate)
                this.continueSegue(descriptionFragment)
            }
            "41" -> this.openCloseDotsMenu()
        }
    }

    private fun openCloseDotsMenu() {
        Log.d("DXGOP", "Before click a menu, dotMenuOpened? $dotsMenuOpened")
        findView<View>(R.id.dotsMenuOpened).visibility = if (dotsMenuOpened) View.VISIBLE else View.INVISIBLE
        dotsMenuOpened = !dotsMenuOpened
        Log.d("DXGOP", "After click a menu, dotMenuOpened? $dotsMenuOpened")
    }

    override fun onSingleCVShare() {
        val notification = Notification(mContext, R.layout.notification_light)
        notification.setMessage(getString(R.string.recruitment_request_candidate_share_success_notification))
        notification.show(1500)
    }

    override fun onMultipleCVShare() {}

    override fun onErrorSharing(error: String) {
        val notification = Notification(mContext, R.layout.notification_light_error)
        notification.setMessage(error)
        notification.show(1500)
    }
}