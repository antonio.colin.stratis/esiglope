package com.stratis.estaffing.Control.NumberSelector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import androidx.core.content.ContextCompat;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 21/08/20
 */

public class NumberSelector {

    private RelativeLayout mView = null;
    private Context context;
    public NumberSelectorListener listener = null;

    public EditText numberField = null;
    private ImageButton removeButton = null;
    private ImageButton addButton = null;

    public NumberSelector(RelativeLayout view, Context context) {

        this.mView = view;
        this.context = context;
        this.initViews();
    }

    public void initViews() {

        // --
        this.numberField = this.mView.findViewById(R.id.number);
        this.numberField.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {

                    // --
                    if (listener != null) {
                        listener.onSelectedNumberSelector();
                    }
                    // --
                    return false;
                }
                return false;
            }
        });

        // --
        this.numberField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != "") {
                    final Integer nu = Integer.valueOf(String.valueOf(numberField.getText()));
                    if (listener != null) {
                        listener.vacanciesChange(nu);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // --
        this.removeButton = this.mView.findViewById(R.id.remove);
        this.removeButton.setOnClickListener(v -> {

            // --
            if (listener != null) {
                listener.onSelectedNumberSelector();
            }

            // --
            final Integer nu = Integer.valueOf(String.valueOf(numberField.getText()));
            if (nu > 4) {
                numberField.setText("" + (nu - 1) );
                if (listener != null) {
                    listener.vacanciesChange(nu-1);
                }
            }
        });

        // --
        this.addButton = this.mView.findViewById(R.id.add);
        this.addButton.setOnClickListener(v -> {

            // --
            if (listener != null) {
                listener.onSelectedNumberSelector();
            }

            // --
            final Integer nu = Integer.valueOf(String.valueOf(numberField.getText()));
            Log.d("DXGOP", "SUMANDOOOO ::: " + nu);
            numberField.setText("" + (nu + 1) );
            if (listener != null) {
                listener.vacanciesChange(nu+1);
            }
        });
    }

    public void activeState(Boolean state) {

        if (state) {

            this.mView.setBackgroundDrawable( ContextCompat.getDrawable(this.context, R.drawable.request_vacancy_selected_button) );
            this.numberField.setTextColor( ContextCompat.getColor(this.context, R.color.white) );

            // --
            removeButton.setColorFilter(Color.argb(255, 255, 255, 255));

            // --
            addButton.setColorFilter(Color.argb(255, 255, 255, 255));


        } else {

            this.mView.setBackgroundDrawable( ContextCompat.getDrawable(this.context, R.drawable.request_vacancy_unselected_button) );
            this.numberField.setTextColor( ContextCompat.getColor(this.context, R.color.recruitment_request_vacancy_unselected_text_button) );

            // --
            removeButton.clearColorFilter();
            // --
            addButton.clearColorFilter();
        }
    }

    // --
    public void setVacancies(int vacancies) {
        this.numberField.setText( String.valueOf(vacancies) );
    }

    public int getVacancies() {
        return Integer.parseInt(String.valueOf(this.numberField.getText()));
    }
}

