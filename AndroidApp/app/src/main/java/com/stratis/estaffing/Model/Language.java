package com.stratis.estaffing.Model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 27/07/20
 */

public class Language {

    private String level = "";
    private String name = "";
    private Integer percentage = 0;

    public Language(JSONObject obj) {

        if (obj != null) {

            this.level = obj.optString("level", "");
            this.name = obj.optString("name", "");
            this.percentage =  obj.optInt("percentage", 0);
        }
    }

    public static ArrayList<Language> parse(JSONArray array) {

        if (array != null) {

            ArrayList<Language> languages = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                languages.add(new Language(obj));
            }
            return languages;
        }
        return null;
    }

    // --

    public String getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    public Integer getPercentage() {
        return percentage;
    }
}