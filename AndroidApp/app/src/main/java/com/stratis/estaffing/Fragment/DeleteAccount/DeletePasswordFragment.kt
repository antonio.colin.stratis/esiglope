package com.stratis.estaffing.Fragment.DeleteAccount

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.AutoResizeTextView
import com.stratis.estaffing.Model.CatalogItem
import com.stratis.estaffing.R

class DeletePasswordFragment: ABFragment(), View.OnClickListener {

    private lateinit var buttonContinue: RelativeLayout
    private lateinit var password: TextInputLayout
    private lateinit var passwordTitle: AutoResizeTextView
    private lateinit var passwordText: TextInputEditText
    private lateinit var passwordErrorText: TextView
    lateinit var reason: CatalogItem
    lateinit var comment: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_delete_password, container, false)
        setHeader("")

        this.initViews()
        return this.mView
    }

    private fun initViews() {

        val buttonCancel: Button = mView.findViewById(R.id.footerCancelButton)
        buttonCancel.tag = "11"
        buttonCancel.setOnClickListener(this)

        password = mView.findViewById(R.id.password)
        passwordText = mView.findViewById(R.id.passwordText)
        passwordTitle = mView.findViewById(R.id.passwordTitle)
        
        buttonContinue = mView.findViewById(R.id.footerContinueButton)
        buttonContinue.tag = "13"
        buttonContinue.setOnClickListener(this)

        passwordErrorText = mView.findViewById(R.id.textViewError)

    }

    @SuppressLint("ResourceAsColor")
    override fun onClick(v: View?) {
        when(v!!.tag.toString()) {
            "11" -> {
                this.activity?.finish()
                return
            }
            "13" -> {
                val deleteFragment = DeleteFragment()
                deleteFragment.reason = reason
                deleteFragment.comment = comment
                deleteFragment.password = passwordText.text.toString()
                deleteFragment.setListener(object: DeleteFragment.DeleteFragmentListener {
                    override fun onBackPassword() {
                        passwordErrorText.visibility = View.VISIBLE
                        passwordText.setTextColor(ContextCompat.getColor(mContext, R.color.red))
                        passwordTitle.setTextColor(ContextCompat.getColor(mContext, R.color.red))
                    }
                })
                this.continueSegue(deleteFragment)
            }
        }
    }

}