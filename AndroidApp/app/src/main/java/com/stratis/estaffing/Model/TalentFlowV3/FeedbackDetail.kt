package com.stratis.estaffing.Model.TalentFlowV3

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

class FeedbackDetail:Serializable {

    var tagId:Long = 0
    var skillTagId:Long = 0
    var createdById:Long = 0
    var isPublic:Boolean = false
    var isEnable:Boolean = false
    var vacancyId:Long = 0
    var id:Long = 0
    var name:String = ""
    var description:String = ""
    var categoryId:Long = 0
    var categoryName:String = ""
    var skillLevelPercentage: Int = 0
    var expertiseYears:Int = 0
    var talentId:Long = 0

    constructor(obj: JSONObject?) {
        if (obj != null) {

            this.tagId = obj.optLong("tagId")
            this.skillTagId = obj.optLong("skillTagId")
            this.createdById = obj.optLong("createdById")
            this.isPublic = obj.optBoolean("isPublic")
            this.isEnable = obj.optBoolean("isEnable")
            this.vacancyId = obj.optLong("vacancyId")
            this.id = obj.optLong("id")
            this.name = obj.optString("name")
            this.description = obj.optString("description")
            this.categoryId = obj.optLong("categoryId")
            this.categoryName = obj.optString("categoryName")
            this.skillLevelPercentage = obj.optInt("skillLevelPercentage")
            this.expertiseYears = obj.optInt("expertiseYears")
            this.talentId = obj.optLong("talentId")
        }
    }

    companion object {

        fun parse(array: JSONArray?): ArrayList<FeedbackDetail>? {

            if (array != null) {
                val feedbackDetails = ArrayList<FeedbackDetail>()
                for (i in 0 until array.length()) {
                    val obj = array.optJSONObject(i)
                    feedbackDetails.add(FeedbackDetail(obj))
                }
                return feedbackDetails
            }
            return null
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is FeedbackDetail) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun toString(): String {
        return "FeedbackDetail(id=$id, name='$name', description='$description', categoryId=$categoryId)"
    }

    fun getObject(): JSONObject {
        val obj = JSONObject()
        obj.put("skillTagId", skillTagId)
        obj.put("createdById", createdById)
        obj.put("isPublic", isPublic)
        obj.put("isEnable", isEnable)
        obj.put("name", name)
        obj.put("description", description)
        obj.put("categoryId", categoryId)
        obj.put("categoryName", categoryName)

        return obj
    }

}