package com.stratis.estaffing.Adapter.MasterAdminFlow

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.RequestAdapter
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest
import com.stratis.estaffing.R
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.RequestView
import java.util.*

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/07/21
 */
open class MARequestAdapter(context: Context?, items: ArrayList<LatestVacancyRequest>?) : RequestAdapter(context, items) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestView {

        val layoutView: View = LayoutInflater.from(parent.context).inflate(R.layout.fragment_m_a_requests_item, parent, false)
        return RequestView(layoutView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RequestView, position: Int) {

        holder.setIsRecyclable(false)
        val item = items[position]

        // -- Setting the views
        if (item.folioClient == "0") {

            val params: ViewGroup.LayoutParams = holder.requestVacancy.layoutParams
            params.height = 0
            holder.requestVacancy.layoutParams = params

        } else {
            holder.requestVacancy.text = String.format(
                context.getString(R.string.recruitment_actions_request_title),
                item.folioClient
            )
        }

        holder.requestOrder.text =
            String.format(context.getString(R.string.recruitment_actions_order_title), item.id)
        holder.requestTitle.text = item.name + " " + item.jobSeniorityShort
        holder.requestDate.text = App.getDateFormatted(item.date, "dd/MMM")
            .uppercase(Locale.ROOT).replace("\\.".toRegex(), "")
        holder.requestButton.setOnClickListener {
            if (listener != null) {
                listener.onDetail(item)
            }
        }

        // -- Icon and Text
        setIcon(item, holder)
    }
}