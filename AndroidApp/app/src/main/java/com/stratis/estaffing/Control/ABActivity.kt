package com.stratis.estaffing.Control

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.ExceptionHandler
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.R

/**
 * Created by Erick Sanchez
 * Revision 1 - 4/13/21
 */
abstract class ABActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        Thread.setDefaultUncaughtExceptionHandler(ExceptionHandler(this))
    }

    protected fun setHeader(title: String?) {
        this.setHeader(title, false)
    }

    protected fun setHeader(title: String?, custom: Boolean) {

        // -- Shape and status bar
        val session = Session.get(this)
        val primaryColor = App.fixWrongColorATuDePiglek(session.preferences.primaryColor)

        if (primaryColor != "" && custom) {
            findViewById<RelativeLayout>(R.id.header).setBackgroundColor(Color.parseColor(primaryColor))
            this.setStatusBarColor(Color.parseColor(primaryColor))
        }

        // --
        val app_screen_return_arrow = findViewById<ImageButton>(R.id.app_screen_return_arrow)
        app_screen_return_arrow.setOnClickListener { v: View? -> finish() }

        // --
        val titleHeader = findViewById<TextView>(R.id.app_screen_title_header)
        titleHeader.text = title
    }

    protected fun setStatusBarColor(color:Int) {
        window.statusBarColor = color
    }

    protected fun setStatusBarTextLight() {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }

    open fun setViewAsPrimaryColor(view:View) {

        val session = Session.get(this)
        val primaryColor = App.fixWrongColorATuDePiglek(session.preferences.primaryColor)
        if (primaryColor != "") {
            this.setViewColor(view, Color.parseColor(primaryColor))
        }
    }

    open fun setViewAsSecondaryColor(view:View) {

        val session = Session.get(this)
        val secondaryColor = App.fixWrongColorATuDePiglek(session.preferences.secondaryColor)

        if (secondaryColor != "") {
            this.setViewColor(view, Color.parseColor(secondaryColor))
        }
    }

    private fun setViewColor(view:View, color:Int) {

        val drawable: Drawable = view.getBackground()
        drawable.setTint(color)
    }
}