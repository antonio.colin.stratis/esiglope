package com.stratis.estaffing.Controller.TalentFlow;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.stratis.estaffing.Adapter.TalentAdapter;
import com.stratis.estaffing.Control.ABActivity;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Loader;
import com.stratis.estaffing.Controller.RecruitmentFlow.RecruitmentController;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.Talent;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Talent.TalentService;

import org.json.JSONArray;

import java.util.ArrayList;

public class TalentController extends ABActivity  {

    private Loader loader = null;
    private ListView talentList = null;

    private TalentAdapter adapter;
    private static ArrayList<Talent> talents;
    private static ArrayList<Talent> talentsFiltred = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // --
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_talent_controller);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName());

        // -- Set header
        this.setHeader(this.getResources().getString(R.string.talent_header_title), true);

        // -- Init views
        this.initViews();

        // -- Get talents
        this.getTalents();

        // -- Show loader
        this.loader = new Loader(this);
        this.loader.show();
    }

    private void initViews() {
        this.talentList = this.findViewById(R.id.talentList);

        RelativeLayout searchComponent = this.findViewById(R.id.searchComponent);
        EditText searcher = searchComponent.findViewById(R.id.searchComponentBoxInput);
        searcher.setHint(R.string.search_talent_list);
        searcher.addTextChangedListener(seacherEditTexttListener());

    }

    private void getTalents() {

        // -- Talent logic
        TalentService.findByHierarchy(Session.get(this), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                loader.cancel();
                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                checkResponse(res);
            }

            @Override
            public void onError(ReSTResponse response) {

                loader.cancel();
                checkResponse(null);
            }
        });
    }

    private void checkResponse(final JSONArray res) {

        if (res != null) {

            talents = Talent.parse(res);
            talentsFiltred = (ArrayList<Talent>) talents.clone();
            if (talentsFiltred.size() > 0) {
                this.findViewById(R.id.no_items).setVisibility(View.GONE);
                this.findViewById(R.id.searchComponent).setVisibility(View.VISIBLE);
                adapter = new TalentAdapter(this, talentsFiltred);
                this.talentList.setAdapter(adapter);
            } else {
                this.setNoItems();
            }
        } else {
            this.setNoItems();
        }
    }

    private void setNoItems() {

        this.findViewById(R.id.no_items).setVisibility(View.VISIBLE);
        this.findViewById(R.id.searchComponent).setVisibility(View.GONE);
        this.talentList.setVisibility(View.GONE);


        // --
        final Button actionButton = this.findViewById(R.id.actionButton);
        actionButton.setOnClickListener(v -> startActivity(
                new Intent(TalentController.this, RecruitmentController.class)));
    }

    private TextWatcher seacherEditTexttListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                talentsFiltred.clear();
                for(Talent t : talents) {
                    if(t.getUserFullNameShort().toLowerCase().contains(charSequence.toString().toLowerCase())
                        || charSequence.toString().equals("")) {
                        talentsFiltred.add(t);
                    }
                }

                adapter.notifyDataSetChanged();
                if(!talentsFiltred.isEmpty()) {
                    findViewById(R.id.no_items_search).setVisibility(View.GONE);
                } else {
                    findViewById(R.id.no_items_search).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
    }
}