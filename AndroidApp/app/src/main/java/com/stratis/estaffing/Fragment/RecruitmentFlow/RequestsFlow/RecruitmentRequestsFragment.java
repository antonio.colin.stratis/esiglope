package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow;

import android.content.Intent;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.faltenreich.skeletonlayout.Skeleton;
import com.faltenreich.skeletonlayout.SkeletonLayoutUtils;
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.RequestAdapter;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Controller.RecruitmentFlow.RecruitmentController;
import com.stratis.estaffing.Controller.TalentFlow.TalentController;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.Adapter.RequestFilterStatusAdapter;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.RequestsFilterModal;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.RequestsService;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Objects;

public class RecruitmentRequestsFragment extends ABFragment implements RequestAdapter.RequestAdapterListener,
        View.OnClickListener, RequestFilterStatusAdapter.RequestFilterStatusAdapterListener {

    private RecyclerView activeRequest;
    private RequestAdapter activeRequestAdapter;

    private RecyclerView pausedRequest;
    private RequestAdapter pausedRequestAdapter;

    private RecyclerView cancelRequest;
    private RequestAdapter cancelRequestAdapter;

    private RecyclerView doneRequest;
    private RequestAdapter doneRequestAdapter;

    private String currentFilterId = "0";
    private String currentDateFilterId = "10";

    private Boolean activeVisible = false;
    private Boolean pausedVisible = false;
    private Boolean cancelVisible = false;
    private Boolean doneVisible = false;

    public RecruitmentRequestsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_recruitment_requests, container, false);

        // -- Set header
        this.setHeader(getString(R.string.recruitment_actions_request_main_title), true);

        // -- Init views
        this.initViews();

        // -- Get requests
        this.getRequests();

        // --
        return this.mView;
    }

    private void initViews() {

        final DividerItemDecoration itemDecorator = new DividerItemDecoration(this.mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this.mContext, R.drawable.recycler_devider_20));

        this.pausedRequest = this.mView.findViewById(R.id.paused_elements_list);
        App.createVerticalRecyclerList(this.pausedRequest, getContext());
        this.pausedRequest.addItemDecoration(itemDecorator);

        this.cancelRequest = this.mView.findViewById(R.id.canceled_elements_list);
        App.createVerticalRecyclerList(this.cancelRequest, getContext());
        this.cancelRequest.addItemDecoration(itemDecorator);

        this.doneRequest = this.mView.findViewById(R.id.done_elements_list);
        App.createVerticalRecyclerList(this.doneRequest, getContext());
        this.doneRequest.addItemDecoration(itemDecorator);

        // -- Buttons
        final Button deleteFiltersButton = this.mView.findViewById(R.id.deleteFiltersButton);
        deleteFiltersButton.setTag("10");
        deleteFiltersButton.setOnClickListener(this);

        final RelativeLayout filterButton = this.mView.findViewById(R.id.filterButton);
        filterButton.setTag("11");
        filterButton.setOnClickListener(this);
        this.setViewAsSecondaryColor(filterButton);
    }

    public void getRequests() {

        final DividerItemDecoration itemDecorator = new DividerItemDecoration(this.mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(this.mContext, R.drawable.recycler_devider_20)));

        // --
        this.activeRequest = this.mView.findViewById(R.id.recyclerView);
        App.createVerticalRecyclerList(this.activeRequest, getContext());
        this.activeRequest.addItemDecoration(itemDecorator);

        // --
        Skeleton skeleton = SkeletonLayoutUtils.applySkeleton(activeRequest, R.layout.fragment_recruitment_requests_item);
        skeleton.showSkeleton();

        // --
        RequestsService.getRequests(Session.get(getContext()), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                parseRequests(res);
                mView.findViewById(R.id.filtersContainer).setVisibility(View.VISIBLE);

                // -- Apply current filters
                onFilterClick(currentFilterId);
            }

            @Override
            public void onError(ReSTResponse response) {
                paintNoItems();
            }
        });
    }

    private void parseRequests(JSONArray res) {

        // -- Hide all
        mView.findViewById(R.id.activeGroup).setVisibility(View.GONE);
        mView.findViewById(R.id.pausedGroup).setVisibility(View.GONE);
        mView.findViewById(R.id.cancelGroup).setVisibility(View.GONE);
        mView.findViewById(R.id.doneGroup).setVisibility(View.GONE);

        if (res.length() <= 0) {

            this.paintNoItems();
            return;
        }

        ArrayList<LatestVacancyRequest> activeRequests = new ArrayList<>();
        ArrayList<LatestVacancyRequest> pausedRequests = new ArrayList<>();
        ArrayList<LatestVacancyRequest> nullRequests = new ArrayList<>();
        ArrayList<LatestVacancyRequest> doneRequests = new ArrayList<>();
        final ArrayList<LatestVacancyRequest> requests = LatestVacancyRequest.parse(res);
        for (LatestVacancyRequest last : requests)  {

            switch(last.getStatusId()) {

                case LatestVacancyRequest.statusSent:
                case LatestVacancyRequest.statusConfirm:
                case LatestVacancyRequest.statusSearch:
                case LatestVacancyRequest.statusCandidates:
                case LatestVacancyRequest.statusInterviews:
                case LatestVacancyRequest.statusHire:
                default:
                    activeRequests.add(last);
                    break;

                case LatestVacancyRequest.statusPaused:
                    pausedRequests.add(last);
                    break;

                case LatestVacancyRequest.statusCancel:
                    nullRequests.add(last);
                    break;

                case LatestVacancyRequest.statusDone:
                    doneRequests.add(last);
                    break;
            }
        }

        // -- Painting
        this.activeRequestAdapter = new RequestAdapter(getContext(), activeRequests);
        this.activeRequestAdapter.setListener(RecruitmentRequestsFragment.this);
        this.activeRequest.setAdapter(this.activeRequestAdapter);
        if (activeRequests.size() > 0) {
            activeVisible = true;
            this.mView.findViewById(R.id.activeGroup).setVisibility(View.VISIBLE);
        }

        this.pausedRequestAdapter = new RequestAdapter(getContext(), pausedRequests);
        this.pausedRequestAdapter.setListener(RecruitmentRequestsFragment.this);
        this.pausedRequest.setAdapter(this.pausedRequestAdapter);
        if (pausedRequests.size() > 0) {
            pausedVisible = true;
            this.mView.findViewById(R.id.pausedGroup).setVisibility(View.VISIBLE);
        }

        this.cancelRequestAdapter = new RequestAdapter(getContext(), nullRequests);
        this.cancelRequestAdapter.setListener(RecruitmentRequestsFragment.this);
        this.cancelRequest.setAdapter(this.cancelRequestAdapter);
        if (nullRequests.size() > 0) {
            cancelVisible = true;
            this.mView.findViewById(R.id.cancelGroup).setVisibility(View.VISIBLE);
        }

        this.doneRequestAdapter = new RequestAdapter(getContext(), doneRequests);
        this.doneRequestAdapter.setListener(RecruitmentRequestsFragment.this);
        this.doneRequest.setAdapter(this.doneRequestAdapter);
        if (doneRequests.size() > 0) {
            doneVisible = true;
            this.mView.findViewById(R.id.doneGroup).setVisibility(View.VISIBLE);
        }
    }

    private void paintNoItems() {

        // -- Hide all
        mView.findViewById(R.id.activeGroup).setVisibility(View.GONE);
        mView.findViewById(R.id.pausedGroup).setVisibility(View.GONE);
        mView.findViewById(R.id.cancelGroup).setVisibility(View.GONE);
        mView.findViewById(R.id.doneGroup).setVisibility(View.GONE);

        // --
        final ConstraintLayout noItems = this.findView(R.id.no_items);
        noItems.setVisibility(View.VISIBLE);
        ((ImageView) noItems.findViewById(R.id.image)).setImageBitmap(
                App.getImage(mContext, "person_icon")
        );
        ((TextView) noItems.findViewById(R.id.title)).setText(getString(R.string.messages_no_request_items_title));
        ((TextView) noItems.findViewById(R.id.content)).setText(getString(R.string.messages_no_request_items_content));
        ((Button) noItems.findViewById(R.id.actionButton)).setText(getString(R.string.messages_no_request_items_button));


        this.findView(R.id.interviews_elements_title).setVisibility(View.GONE);

        // --
        final Button actionButton = this.findView(R.id.actionButton);
        actionButton.setOnClickListener(v -> startActivity(
                new Intent(getActivity(), RecruitmentController.class)));
    }

    // --
    @Override
    public void onDetail(LatestVacancyRequest item) {

        final Profile profile = new Profile();
        profile.setProvisionalId(item.getId());
        this.performSegue(new RecruitmentRequestSingleFragment(profile), "RecruitmentRequestsFragmentTag");
    }

    @Override
    public void onClick(View v) {

        if (v.getTag().equals("10")) {

            if (!this.currentFilterId.equals("0") || !this.currentDateFilterId.equals("10")) {
                this.removeAllDateFilter();
                this.currentDateFilterId = "10";
                onFilterClick("0");
            }

        } else {

            final RequestsFilterModal modal = new RequestsFilterModal(getContext());
            modal.setListener(this);
            modal.setCurrentFilterId(this.currentFilterId);
            modal.setCurrentDateFilterId(this.currentDateFilterId);
            modal.show();
        }
    }

    @Override
    public void onFilterClick(String filterId) {

        final Group activeGroup = this.mView.findViewById(R.id.activeGroup);
        final Group pausedGroup = this.mView.findViewById(R.id.pausedGroup);
        final Group cancelGroup = this.mView.findViewById(R.id.cancelGroup);
        final Group doneGroup = this.mView.findViewById(R.id.doneGroup);

        activeGroup.setVisibility(View.GONE);
        pausedGroup.setVisibility(View.GONE);
        cancelGroup.setVisibility(View.GONE);
        doneGroup.setVisibility(View.GONE);


        switch(filterId) {

            case "0":
                this.currentFilterId = filterId;
                activeGroup.setVisibility((!this.activeVisible) ? View.GONE : View.VISIBLE);
                pausedGroup.setVisibility((!this.pausedVisible) ? View.GONE : View.VISIBLE);
                cancelGroup.setVisibility((!this.cancelVisible) ? View.GONE : View.VISIBLE);
                doneGroup.setVisibility((!this.doneVisible) ? View.GONE : View.VISIBLE);
                break;

            case LatestVacancyRequest.statusConfirm:
                this.currentFilterId = filterId;
                activeGroup.setVisibility(View.VISIBLE);
                break;

            case LatestVacancyRequest.statusPaused:
                this.currentFilterId = filterId;
                pausedGroup.setVisibility(View.VISIBLE);
                break;

            case LatestVacancyRequest.statusCancel:
                this.currentFilterId = filterId;
                cancelGroup.setVisibility(View.VISIBLE);
                break;

            case LatestVacancyRequest.statusDone:
                this.currentFilterId = filterId;
                doneGroup.setVisibility(View.VISIBLE);
                break;

            case "10":
                this.currentDateFilterId = filterId;
                this.removeAllDateFilter();
                this.onFilterClick(this.currentFilterId);
                break;

            case "11":
            case "12":
                this.currentDateFilterId = filterId;
                this.filterByDate(filterId);
                this.onFilterClick(this.currentFilterId);
                break;
        }
    }

    private void filterByDate(String dateFilterId) {

        if (dateFilterId.equals("11")) {

            switch(this.currentFilterId) {

                case "0":
                    activeRequestAdapter.filterCurrentMonth();
                    pausedRequestAdapter.filterCurrentMonth();
                    cancelRequestAdapter.filterCurrentMonth();
                    doneRequestAdapter.filterCurrentMonth();
                    break;

                case LatestVacancyRequest.statusConfirm:
                    activeRequestAdapter.filterCurrentMonth();
                    break;

                case LatestVacancyRequest.statusPaused:
                    pausedRequestAdapter.filterCurrentMonth();
                    break;

                case LatestVacancyRequest.statusCancel:
                    cancelRequestAdapter.filterCurrentMonth();
                    break;

                case LatestVacancyRequest.statusDone:
                    doneRequestAdapter.filterCurrentMonth();
                    break;
            }

        } else {

            switch(this.currentFilterId) {

                case "0":
                    activeRequestAdapter.filterTwoCurrentMonth();
                    pausedRequestAdapter.filterTwoCurrentMonth();
                    cancelRequestAdapter.filterTwoCurrentMonth();
                    doneRequestAdapter.filterTwoCurrentMonth();
                    break;

                case LatestVacancyRequest.statusConfirm:
                    activeRequestAdapter.filterTwoCurrentMonth();
                    break;

                case LatestVacancyRequest.statusPaused:
                    pausedRequestAdapter.filterTwoCurrentMonth();
                    break;

                case LatestVacancyRequest.statusCancel:
                    cancelRequestAdapter.filterTwoCurrentMonth();
                    break;

                case LatestVacancyRequest.statusDone:
                    doneRequestAdapter.filterTwoCurrentMonth();
                    break;
            }
        }
    }

    private void removeAllDateFilter() {

        this.activeRequestAdapter.removeDateFilter();
        this.pausedRequestAdapter.removeDateFilter();
        this.cancelRequestAdapter.removeDateFilter();
        this.doneRequestAdapter.removeDateFilter();
    }

    public void scrollTToTop() {
        ((NestedScrollView)this.mView.findViewById(R.id.scrollRoot)).scrollTo(0, 0);
    }
}