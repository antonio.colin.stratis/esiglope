package com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Interview;

import android.content.Context;
import android.util.Log;

import com.stratis.estaffing.R;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.Course;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 05/01/21
 *
 * interview Catalog
 *
 * 1001000001	Programada
 * 1001000002	Confirmada
 * 1001000003	Re-programada
 * 1001000004	Realizada
 * 1001000005	No realizada
 *
 */

public class RequestCandidateInterview {

    public final static String interviewScheduled = "1001000001";
    public final static String interviewConfirmed = "1001000002";
    public final static String interviewReScheduled = "1001000003";
    public final static String interviewDone = "1001000004";
    public final static String interviewNotMade = "1001000005";
    public final static String interviewCancelled = "1001000006";

    private String id = "";

    private String status = "";
    private Boolean isRemote = null;
    private String vacancyId = "";
    private String talentId = "";
    private String asignee = "";
    private String date = "";
    private String locationName = "";
    private String locationAddress = "";
    private String interviewerName = "";
    private String interviewerEmail = "";

    private String rating = "";
    private String review = "";
    private String comment = "";

    private String dateDate = "";
    private String dateTime = "";

    private RequestCandidate candidate;

    public RequestCandidateInterview() {}

    public RequestCandidateInterview(JSONObject obj, Context context) {

        if (obj != null) {

            this.id = obj.optString("id");
            this.status = obj.optString("status");
            this.isRemote = obj.optBoolean("isRemote");
            this.vacancyId = obj.optString("vacancyId");
            this.talentId = obj.optString("talentId");
            this.asignee = obj.optString("asignee");
            this.date = obj.optString("date");
            this.locationName = obj.optString("locationName");
            this.locationAddress = obj.optString("locationAddress");
            this.interviewerName = obj.optString("interviewerName");
            this.interviewerEmail = obj.optString("interviewerEmail");

            this.rating = obj.optString("rating", "");
            if (this.rating.equals("null")) {
                this.rating = "";
            }

            this.review = obj.optString("review");
            if (this.review.equals("") || this.review.equals("null") || this.review == null) {
                this.review = context.getString(R.string.recuritment_request_candidate_rate_interview_not_comment);
            }


            this.comment = obj.optString("comment");

            // -- Dividing date in parts
            this.dateDate = App.getDateFormatted(this.date,"dd/M/yyyy","yyyy-MM-dd HH:mm:ss");
            this.dateTime = App.getDateFormatted(this.date,"HH:mm","yyyy-MM-dd HH:mm:ss");
        }
    }

    public static ArrayList<RequestCandidateInterview> parse(JSONArray array, String vacancyId, Context context) {

        ArrayList<RequestCandidateInterview> interviews = new ArrayList<>();
        if (array != null) {

            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                RequestCandidateInterview temp = new RequestCandidateInterview(obj, context);
                if (temp.getVacancyId().equals(vacancyId)) {
                    interviews.add(temp);
                    Log.i("DXGOP", "INTERVIEW IN ARRAY [] ::: " + obj);
                }
            }
            return interviews;
        }
        return interviews;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getRemote() {
        return isRemote;
    }

    public void setRemote(Boolean remote) {
        isRemote = remote;
    }

    public String getVacancyId() {
        return vacancyId;
    }

    public void setVacancyId(String vacancyId) {
        this.vacancyId = vacancyId;
    }

    public String getTalentId() {
        return talentId;
    }

    public void setTalentId(String talentId) {
        this.talentId = talentId;
    }

    public String getAsignee() {
        return asignee;
    }

    public void setAsignee(String asignee) {
        this.asignee = asignee;
    }

    public String getDate() {
        return date;
    }

    public String getDateServer() {

        String dateFormatted = App.getDateFormatted(this.dateDate,"yyyy-MM-dd","dd/M/yyyy");
        dateFormatted = dateFormatted + " " + this.dateTime + ":00";
        this.date =  dateFormatted;
        return dateFormatted;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public RequestCandidate getCandidate() {
        return candidate;
    }

    public void setCandidate(RequestCandidate candidate) {
        this.candidate = candidate;
    }

    public String getDateDate() {
        return dateDate;
    }

    public void setDateDate(String dateDate) {
        this.dateDate = dateDate;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getId() {
        return id;
    }

    public String getRating() {
        return rating;
    }

    public String getRatingNumber() {
        return (rating == null || rating == "" || rating == "null") ? "0" : rating;
    }

    public String getReview() {
        return review;
    }

    public String getComment() {
        return comment;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getInterviewerName() {
        return interviewerName;
    }

    public String getInterviewerNameShort() {

        String[] parts = this.interviewerName.split(" ");
        String ret = "";
        if (parts.length == 1) {
            ret = parts[0];
        }  else if (parts.length > 1) {
            ret = parts[0] + " " + parts[1];
        }
        return ret;
    }

    public void setInterviewerName(String interviewerName) {
        this.interviewerName = interviewerName;
    }

    public String getInterviewerEmail() {
        return interviewerEmail;
    }

    public void setInterviewerEmail(String interviewerEmail) {
        this.interviewerEmail = interviewerEmail;
    }

    // --
    public static boolean hasGradedInterviews(final ArrayList<RequestCandidateInterview> interviews,
                                              final String vacancyId) {

        for(RequestCandidateInterview interview: interviews) {
            final String status = interview.getStatus();

            if (status.equals(RequestCandidateInterview.interviewNotMade)) { return true; }

            if ( status.equals(RequestCandidateInterview.interviewDone) &&
                    interview.getVacancyId().equals(vacancyId)) {
                if (interview.getRating() != null && !interview.getRating().equals("")) {
                    Log.i("DXGOP", "CALIFICADA CONNN :::: " + interview.getRating());
                    return true;
                }
            }
        }
        return false;
    }

    public static ArrayList<RequestCandidateInterview> getGradedInterviews(final ArrayList<RequestCandidateInterview> interviews,
                                                                           final String vacancyId) {

        ArrayList<RequestCandidateInterview> ret = new ArrayList<>();
        for(RequestCandidateInterview interview: interviews) {
            if ((interview.getStatus().equals(RequestCandidateInterview.interviewDone) ||
                    interview.getStatus().equals(RequestCandidateInterview.interviewNotMade)) &&
                    interview.getVacancyId().equals(vacancyId)) {
                ret.add(interview);
            }
        }
        return ret;
    }

    public static RequestCandidateInterview getLastInterview(final ArrayList<RequestCandidateInterview> interviews,
                                                                        final String vacancyId) {

        RequestCandidateInterview lastInterview = null;
        //for (int i = (interviews.size() - 1); i >= 0; i--) {
        /*for (RequestCandidateInterview interview: interviews) {
            //if (interviews.get(i).getVacancyId().equals(vacancyId)) {
            if (interview.getVacancyId().equals(vacancyId)) {
                lastInterview = interview;
                Log.i("DXGOP", "LAST INTERVIEW ::: " + lastInterview.getId());
                return lastInterview;
            }
        }*/
        if (interviews.size() > 0) {
            lastInterview = interviews.get( interviews.size() - 1 );
            //lastInterview = interviews.get(0);
        }
        return lastInterview;
    }
}