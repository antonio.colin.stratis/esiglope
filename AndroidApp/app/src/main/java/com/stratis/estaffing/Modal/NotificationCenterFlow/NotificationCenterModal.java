package com.stratis.estaffing.Modal.NotificationCenterFlow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Modal.NotificationCenterFlow.Adapter.NotificationCenterFilterAdapter;
import com.stratis.estaffing.Modal.NotificationCenterFlow.Model.NotificationCenterFilterModel;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequestCandidateFilter.Adapter.RequestCandidatesFilterAdapter;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequestCandidateFilter.Model.RequestCandidateFilterModel;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 09/12/20
 */

public class NotificationCenterModal extends BottomSheetDialog {

    private String currentFilterId = "";
    private NotificationCenterFilterListener listener;

    public NotificationCenterModal(Context context) {

        super(context);
        this.commonInit();
    }

    public NotificationCenterModal(Context context, int themeResId) {

        super(context, themeResId);
        this.commonInit();
    }

    protected NotificationCenterModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.commonInit();
    }

    private void commonInit() {

        // -- R styles
        this.setContentView(R.layout.fragment_notification_center_modal);
        this.setCanceledOnTouchOutside(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void show() {

        super.show();

        // --
        this.handleActions();
    }

    @SuppressLint("ResourceType")
    private void handleActions() {

        final DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.recycler_devider_20));

        // -- Status Filter
        final RecyclerView stateFilters = this.findViewById(R.id.stateFilters);
        App.createRecyclerHorizontalList(stateFilters, getContext());
        stateFilters.addItemDecoration(itemDecorator);
        final NotificationCenterFilterAdapter notificationCenterFilterAdapter = new NotificationCenterFilterAdapter(
                NotificationCenterFilterModel.getFilter(),
                getContext());


        stateFilters.setAdapter(notificationCenterFilterAdapter);
        notificationCenterFilterAdapter.setListener(this.listener);
        notificationCenterFilterAdapter.setParent(this);
        notificationCenterFilterAdapter.setCurrentFilter(this.currentFilterId);
    }

    public void setCurrentFilterId(String currentFilterId) {
        this.currentFilterId = currentFilterId;
    }

    public void setListener(NotificationCenterFilterListener listener) {
        this.listener = listener;
    }

    public interface NotificationCenterFilterListener {
        void onFilterClick(NotificationCenterFilterModel filter);
    }
}