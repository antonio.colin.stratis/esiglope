package com.stratis.estaffing.View.Talent;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.AutoResizeTextView;
import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 29/07/20
 */

public class CourseView extends RecyclerView.ViewHolder {

    //private ImageView courseInitials = null;
    private TextView courseInitials = null;
    private TextView courseTitle = null;
    private TextView courseEndDate = null;
    private ImageButton arrow = null;

    private RelativeLayout courseMoreInfo = null;
    private AutoResizeTextView schoolView = null;
    private AutoResizeTextView validityView = null;
    private AutoResizeTextView certificateView = null;
    private TextView course_description = null;
    private Button show_certificate_button = null;

    public CourseView(View row) {

        // --
        super(row);

        // --
        this.courseInitials = row.findViewById(R.id.courseInitials);
        this.courseTitle = row.findViewById(R.id.courseTitle);
        this.courseEndDate = row.findViewById(R.id.courseEndDate);
        this.arrow = row.findViewById(R.id.arrow);
        this.courseMoreInfo = row.findViewById(R.id.courseMoreInfo);
        this.schoolView = row.findViewById(R.id.schoolView);
        this.validityView = row.findViewById(R.id.validityView);
        this.certificateView = row.findViewById(R.id.certificateView);
        this.course_description = row.findViewById(R.id.course_description);
        this.show_certificate_button = row.findViewById(R.id.show_certificate_button);
    }

    /*public ImageView getCourseInitials() {
        return courseInitials;
    }*/
    public TextView getCourseInitials(){
        return courseInitials;
    }

    public TextView getCourseTitle() {
        return courseTitle;
    }

    public TextView getCourseEndDate() {
        return courseEndDate;
    }

    public ImageButton getArrow() {
        return arrow;
    }

    public RelativeLayout getCourseMoreInfo() {
        return courseMoreInfo;
    }

    public AutoResizeTextView getSchoolView() {
        return schoolView;
    }

    public AutoResizeTextView getValidityView() {
        return validityView;
    }

    public AutoResizeTextView getCertificateView() {
        return certificateView;
    }

    public TextView getCourse_description() {
        return course_description;
    }

    public Button getShow_certificate_button() {
        return show_certificate_button;
    }
}