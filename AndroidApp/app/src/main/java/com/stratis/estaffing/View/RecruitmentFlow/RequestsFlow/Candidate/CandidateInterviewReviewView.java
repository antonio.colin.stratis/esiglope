package com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 21/08/20
 */

public class CandidateInterviewReviewView extends RecyclerView.ViewHolder {

    private TextView interviewNumber;
    private TextView interviewDate;
    private LinearLayout ratedContainerStars;
    private TextView ratedNoDone;
    private TextView interviewPerson;
    private TextView interviewReview;

    public CandidateInterviewReviewView(View itemView) {

        super(itemView);
        this.interviewNumber = itemView.findViewById(R.id.interviewNumber);
        this.interviewDate = itemView.findViewById(R.id.interviewDate);
        this.ratedContainerStars = itemView.findViewById(R.id.ratedContainerStars);
        this.interviewPerson = itemView.findViewById(R.id.interviewPerson);
        this.interviewReview = itemView.findViewById(R.id.interviewReview);
        this.ratedNoDone = itemView.findViewById(R.id.ratedNoDone);
    }

    // --
    public TextView getInterviewNumber() {
        return interviewNumber;
    }

    public TextView getInterviewDate() {
        return interviewDate;
    }

    public LinearLayout getRatedContainerStars() {
        return ratedContainerStars;
    }

    public TextView getInterviewPerson() {
        return interviewPerson;
    }

    public TextView getInterviewReview() {
        return interviewReview;
    }

    public TextView getRatedNoDone() {
        return ratedNoDone;
    }
}
