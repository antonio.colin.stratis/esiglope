package com.stratis.estaffing.Fragment.MasterAdminFlow

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.MasterAdminFlow.MARequestAdapter
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.RequestAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Controller.MainFlow.SupportFlow.SupportController
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestSingleFragment
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.Adapter.RequestFilterStatusAdapter.RequestFilterStatusAdapterListener
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequstsFilter.RequestsFilterModal
import com.stratis.estaffing.Model.MasterAdminFlow.Leader
import com.stratis.estaffing.Model.Profile
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.RequestsService
import org.json.JSONArray
import java.util.*

open class MARequestsFragment : ABFragment(), View.OnClickListener, RequestAdapter.RequestAdapterListener,
        RequestFilterStatusAdapterListener {

    open var leader:Leader? = null

    private var activeRequest: RecyclerView? = null
    private var activeRequestAdapter: MARequestAdapter? = null

    private var pausedRequest: RecyclerView? = null
    private var pausedRequestAdapter: MARequestAdapter? = null

    private var cancelRequest: RecyclerView? = null
    private var cancelRequestAdapter: MARequestAdapter? = null

    private var doneRequest: RecyclerView? = null
    private var doneRequestAdapter: MARequestAdapter? = null

    private var currentFilterId = "0"
    private var currentDateFilterId = "10"

    private var activeVisible = false
    private var pausedVisible = false
    private var cancelVisible = false
    private var doneVisible = false

    private var miniShortNameTitle: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_m_a_requests, container, false)

        // -- Set header
        miniShortNameTitle = App.getMiniShorName("${this.leader?.firstName} ${this.leader?.lastName}")
        setHeader(String.format(
                getString(R.string.fragment_m_a_request_single_candidate_request_title),
                miniShortNameTitle)) //${this.leader?.getPoliteName()}")

        // -- Init views
        initViews()

        // -- Get requests
        this.getRequests()

        // --
        return this.mView
    }

    private fun initViews() {

        val itemDecorator = DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(mContext, R.drawable.recycler_devider_20)!!)

        pausedRequest = mView.findViewById(R.id.paused_elements_list)
        App.createVerticalRecyclerList(pausedRequest, context)
        pausedRequest?.addItemDecoration(itemDecorator)

        cancelRequest = mView.findViewById(R.id.canceled_elements_list)
        App.createVerticalRecyclerList(cancelRequest, context)
        cancelRequest?.addItemDecoration(itemDecorator)

        doneRequest = mView.findViewById(R.id.done_elements_list)
        App.createVerticalRecyclerList(doneRequest, context)
        doneRequest?.addItemDecoration(itemDecorator)

        // -- Buttons
        val deleteFiltersButton = mView.findViewById<Button>(R.id.deleteFiltersButton)
        deleteFiltersButton.tag = "10"
        deleteFiltersButton.setOnClickListener(this)

        val filterButton = mView.findViewById<RelativeLayout>(R.id.filterButton)
        filterButton.tag = "11"
        filterButton.setOnClickListener(this)
    }

    open fun getRequests() {

        val itemDecorator = DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(mContext, R.drawable.recycler_devider_20))!!)

        // --
        activeRequest = mView.findViewById(R.id.recyclerView)
        App.createVerticalRecyclerList(activeRequest, context)
        activeRequest?.addItemDecoration(itemDecorator)

        // --
        val session = Session.get(mContext)
        RequestsService.getRequests(this.leader?.id, session.dataSessionId, object : ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {
                val res = JsonBuilder.stringToJsonArray(response.body)
                parseRequests(res)
                mView.findViewById<View>(R.id.filtersContainer).visibility = View.VISIBLE

                // -- Apply current filters
                onFilterClick(currentFilterId)
            }

            override fun onError(response: ReSTResponse) {
                paintNoItems()
            }
        })
    }

    private fun parseRequests(res: JSONArray) {

        // -- Hide all
        mView.findViewById<View>(R.id.activeGroup).visibility = View.GONE
        mView.findViewById<View>(R.id.pausedGroup).visibility = View.GONE
        mView.findViewById<View>(R.id.cancelGroup).visibility = View.GONE
        mView.findViewById<View>(R.id.doneGroup).visibility = View.GONE

        if (res.length() <= 0) {
            this.paintNoItems()
            return
        }
        val activeRequests = ArrayList<LatestVacancyRequest>()
        val pausedRequests = ArrayList<LatestVacancyRequest>()
        val nullRequests = ArrayList<LatestVacancyRequest>()
        val doneRequests = ArrayList<LatestVacancyRequest>()
        val requests = LatestVacancyRequest.parse(res)
        for (last in requests) {
            when (last.statusId) {
                LatestVacancyRequest.statusSent, LatestVacancyRequest.statusConfirm, LatestVacancyRequest.statusSearch, LatestVacancyRequest.statusCandidates, LatestVacancyRequest.statusInterviews, LatestVacancyRequest.statusHire -> activeRequests.add(last)
                LatestVacancyRequest.statusPaused -> pausedRequests.add(last)
                LatestVacancyRequest.statusCancel -> nullRequests.add(last)
                LatestVacancyRequest.statusDone -> doneRequests.add(last)
                else -> activeRequests.add(last)
            }
        }

        // -- Painting
        activeRequestAdapter = MARequestAdapter(context, activeRequests)
        activeRequestAdapter!!.setListener(this)
        activeRequest!!.adapter = activeRequestAdapter
        if (activeRequests.size > 0) {
            activeVisible = true
            mView.findViewById<View>(R.id.activeGroup).visibility = View.VISIBLE
        }

        pausedRequestAdapter = MARequestAdapter(context, pausedRequests)
        pausedRequestAdapter!!.setListener(this)
        pausedRequest!!.adapter = pausedRequestAdapter
        if (pausedRequests.size > 0) {
            pausedVisible = true
            mView.findViewById<View>(R.id.pausedGroup).visibility = View.VISIBLE
        }

        cancelRequestAdapter = MARequestAdapter(context, nullRequests)
        cancelRequestAdapter!!.setListener(this)
        cancelRequest!!.adapter = cancelRequestAdapter
        if (nullRequests.size > 0) {
            cancelVisible = true
            mView.findViewById<View>(R.id.cancelGroup).visibility = View.VISIBLE
        }

        doneRequestAdapter = MARequestAdapter(context, doneRequests)
        doneRequestAdapter!!.setListener(this)
        doneRequest!!.adapter = doneRequestAdapter
        if (doneRequests.size > 0) {
            doneVisible = true
            mView.findViewById<View>(R.id.doneGroup).visibility = View.VISIBLE
        }
    }

    private fun paintNoItems() {

        // -- Hide all
        mView.findViewById<View>(R.id.activeGroup).visibility = View.GONE
        mView.findViewById<View>(R.id.pausedGroup).visibility = View.GONE
        mView.findViewById<View>(R.id.cancelGroup).visibility = View.GONE
        mView.findViewById<View>(R.id.doneGroup).visibility = View.GONE

        // --
        val stringName = String.format(getString(R.string.recruitment_actions_request_candidates_no_vacancies_title), miniShortNameTitle) //leader?.firstName)
        val noItems = findView<ConstraintLayout>(R.id.no_items)
//        val actionButton = findView<Button>(R.id.actionButton)

        noItems.visibility = View.VISIBLE
        (noItems.findViewById<View>(R.id.title) as TextView).text = stringName
//        (noItems.findViewById<View>(R.id.content) as TextView).text = getString(R.string.messages_no_request_items_content)
//        actionButton.background = ContextCompat.getDrawable(mContext, R.drawable.fragment_recruitment_request_candidate_education_bk)
//        actionButton.text = "Hablar con soporte"
//        actionButton.setTextColor(Color.parseColor("#767676"))
        findView<View>(R.id.interviews_elements_title).visibility = View.GONE

        // --
//        actionButton.setOnClickListener { v: View? -> startActivity(Intent(mContext, SupportController::class.java)) }
    }

    /** Implementations **/

    override fun onClick(v: View?) {

        if (v!!.tag == "10") {
            if (currentFilterId != "0" || currentDateFilterId != "10") {
                this.removeAllDateFilter()
                currentDateFilterId = "10"
                onFilterClick("0")
            }
        } else {
            val modal = RequestsFilterModal(context)
            modal.setListener(this)
            modal.setCurrentFilterId(currentFilterId)
            modal.setCurrentDateFilterId(currentDateFilterId)
            modal.show()
        }
    }

    override fun onDetail(item: LatestVacancyRequest?) {

        val profile = Profile()
        profile.provisionalId = item!!.id

        val fragment = MARequestSingleFragment()
        fragment.profile = profile
        fragment.latestVacancyRequest = item
        fragment.leader = this.leader
        this.performSegue(fragment, "MARecruitmentRequestsFragmentTag")
    }

    override fun onFilterClick(filterId: String?) {

        val activeGroup: Group = mView.findViewById(R.id.activeGroup)
        val pausedGroup: Group = mView.findViewById(R.id.pausedGroup)
        val cancelGroup: Group = mView.findViewById(R.id.cancelGroup)
        val doneGroup: Group = mView.findViewById(R.id.doneGroup)

        activeGroup.visibility = View.GONE
        pausedGroup.visibility = View.GONE
        cancelGroup.visibility = View.GONE
        doneGroup.visibility = View.GONE

        when (filterId) {
            "0" -> {
                currentFilterId = filterId
                activeGroup.visibility = if (!activeVisible) View.GONE else View.VISIBLE
                pausedGroup.visibility = if (!pausedVisible) View.GONE else View.VISIBLE
                cancelGroup.visibility = if (!cancelVisible) View.GONE else View.VISIBLE
                doneGroup.visibility = if (!doneVisible) View.GONE else View.VISIBLE
            }
            LatestVacancyRequest.statusConfirm -> {
                currentFilterId = filterId
                activeGroup.visibility = View.VISIBLE
            }
            LatestVacancyRequest.statusPaused -> {
                currentFilterId = filterId
                pausedGroup.visibility = View.VISIBLE
            }
            LatestVacancyRequest.statusCancel -> {
                currentFilterId = filterId
                cancelGroup.visibility = View.VISIBLE
            }
            LatestVacancyRequest.statusDone -> {
                currentFilterId = filterId
                doneGroup.visibility = View.VISIBLE
            }
            "10" -> {
                currentDateFilterId = filterId
                this.removeAllDateFilter()
                onFilterClick(currentFilterId)
            }
            "11", "12" -> {
                currentDateFilterId = filterId
                this.filterByDate(filterId)
                onFilterClick(currentFilterId)
            }
        }
    }

    /** Filter other methods **/

    private fun filterByDate(dateFilterId: String) {

        if (dateFilterId == "11") {
            when (currentFilterId) {
                "0" -> {
                    activeRequestAdapter!!.filterCurrentMonth()
                    pausedRequestAdapter!!.filterCurrentMonth()
                    cancelRequestAdapter!!.filterCurrentMonth()
                    doneRequestAdapter!!.filterCurrentMonth()
                }
                LatestVacancyRequest.statusConfirm -> activeRequestAdapter!!.filterCurrentMonth()
                LatestVacancyRequest.statusPaused -> pausedRequestAdapter!!.filterCurrentMonth()
                LatestVacancyRequest.statusCancel -> cancelRequestAdapter!!.filterCurrentMonth()
                LatestVacancyRequest.statusDone -> doneRequestAdapter!!.filterCurrentMonth()
            }
        } else {
            when (currentFilterId) {
                "0" -> {
                    activeRequestAdapter!!.filterTwoCurrentMonth()
                    pausedRequestAdapter!!.filterTwoCurrentMonth()
                    cancelRequestAdapter!!.filterTwoCurrentMonth()
                    doneRequestAdapter!!.filterTwoCurrentMonth()
                }
                LatestVacancyRequest.statusConfirm -> activeRequestAdapter!!.filterTwoCurrentMonth()
                LatestVacancyRequest.statusPaused -> pausedRequestAdapter!!.filterTwoCurrentMonth()
                LatestVacancyRequest.statusCancel -> cancelRequestAdapter!!.filterTwoCurrentMonth()
                LatestVacancyRequest.statusDone -> doneRequestAdapter!!.filterTwoCurrentMonth()
            }
        }
    }

    private fun removeAllDateFilter() {

        activeRequestAdapter!!.removeDateFilter()
        pausedRequestAdapter!!.removeDateFilter()
        cancelRequestAdapter!!.removeDateFilter()
        doneRequestAdapter!!.removeDateFilter()
    }
}