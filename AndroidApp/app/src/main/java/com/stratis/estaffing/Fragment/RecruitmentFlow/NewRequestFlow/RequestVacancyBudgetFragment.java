package com.stratis.estaffing.Fragment.RecruitmentFlow.NewRequestFlow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Adapter.RecruitmentFlow.SelectedSkillAdapter;
import com.stratis.estaffing.Adapter.RecruitmentFlow.SelectedSoftwareAdapter;
import com.stratis.estaffing.Control.FluidSlider.FluidSlider;
import com.stratis.estaffing.Control.Searcher.Searcher;
import com.stratis.estaffing.Control.Searcher.SearcherOpen;
import com.stratis.estaffing.Control.eStaffingFragment;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.RecruitmentFlow.SoftwareCatalog;
import com.stratis.estaffing.R;

import java.util.ArrayList;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;

public class RequestVacancyBudgetFragment extends eStaffingFragment implements Searcher.SearcherListener {

    private SelectedSoftwareAdapter adapter;

    private Boolean assignEquipmentState = false;
    private Boolean assignSoftwareState = false;

    private FluidSlider slider;
    private EditText sliderContent;
    private Button assignEquipmentButton;
    private Button assignSoftwareButton;
    private SearcherOpen componentSearch;
    private RecyclerView softwareTable;

    final Integer min = 0;
    final Integer max = 200;
    final int total = max - min;

    public RequestVacancyBudgetFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        this.mView = inflater.inflate(R.layout.fragment_request_vacancy_budget, container, false);
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName());
        this.initViews();
        return this.mView;
    }

    @Override
    public void onResume() {

        super.onResume();
        this.checkProfile();
    }

    @SuppressLint("SetTextI18n")
    private void initViews() {

        this.sliderContent = this.mView.findViewById(R.id.sliderContent);

        // -- Slider
        this.slider = this.mView.findViewById(R.id.slider);
        slider.setPosition(0.5f);
        slider.setBubbleText(String.valueOf(max/2));
        slider.setStartText(String.valueOf(min));
        slider.setEndText(String.valueOf(max));
        slider.setColorBar(Color.parseColor(App.fixWrongColorATuDePiglek(Session.get(mContext).getPreferences().getPrimaryColor())));
        sliderContent.setText("$" + max/2 + ",000");


        // -- Slider Content
        this.sliderContent.setFocusable(false);
        this.sliderContent.setOnEditorActionListener((v, actionId, event) -> {

            if (actionId == EditorInfo.IME_ACTION_DONE || event != null
                    && event.getAction() == KeyEvent.ACTION_DOWN
                    && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                if (event == null || !event.isShiftPressed()) {

                    InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(sliderContent.getApplicationWindowToken(), 0);
                    double money = Double.parseDouble(
                            String.valueOf(sliderContent.getText()).replaceAll("\\$","")
                                    .replaceAll(",", ""));
                    double moneyAssign = ((money*1)/200000);
                    slider.setPosition((float) moneyAssign);
                    sliderContent.setFocusable(false);
                    //slider.setFocusableInTouchMode(false);
                    return true;
                }
            }
            return false;
        });

        // --
        slider.setBeginTrackingListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {
                //textView.setVisibility(View.INVISIBLE);
                return Unit.INSTANCE;
            }
        });

        slider.setEndTrackingListener(new Function0<Unit>() {
            @Override
            public Unit invoke() {
                //textView.setVisibility(View.VISIBLE);
                return Unit.INSTANCE;
            }
        });

        // --
        slider.setPositionListener(new Function1<Float, Unit>() {
            @Override
            public Unit invoke(Float aFloat) {

                final String value = String.valueOf( (int)(min + total * aFloat) );
                getProfile().setVacancyBudget( Integer.parseInt(value) * 1000 );
                slider.setBubbleText(value);
                sliderContent.setText( "$" + value + ",000" );
                return Unit.INSTANCE;
            }
        });

        // -- Budget EditText
        final ImageButton editEnable = this.mView.findViewById(R.id.sliderEditButton);
        editEnable.setOnClickListener(v -> new Handler().post(() -> {

            sliderContent.setFocusableInTouchMode(true);
            sliderContent.setFocusable(true);
            sliderContent.requestFocus();
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }));

        // --
        this.initEquipmentRequirement();
    }

    private void checkProfile() {

        if (this.getProfile() != null) {

            if (this.getProfile().getVacancyBudget() != 0.0 && this.slider != null) {

                int value = (int) (getProfile().getVacancyBudget() / 1000);
                float position = Float.parseFloat(String.valueOf(value))/this.max;
                this.slider.setPosition(position);
                this.sliderContent.setText( "$" + value + ",000" );
            }

            this.assignEquipmentState = this.getProfile().getVacancyEquipment();
            this.changeAssignEquipmentButtonState();

            this.assignSoftwareState = this.getProfile().getVacancyEquipmentSoftware();
            this.changeAssignSoftwareButtonState();

            if (this.getProfile().getSoftwareCatalogs() != null) {
                componentSearch.setData(this.getProfile().getSkillCatalogs());
            }
            this.adapter =  new SelectedSoftwareAdapter( (this.getProfile().getCurrentSoftware() == null || this.getProfile().getCurrentSoftware().size() == 0) ?
                    SoftwareCatalog.getBase() :
                    this.getProfile().getCurrentSoftware(),
                    this.mContext);
            this.adapter.setListener(new SelectedSkillAdapter.OnCloseButtonListerner() {
                @Override
                public void onCloseButton(Integer position) {

                    adapter.removeElement(position);
                    adapter.notifyDataSetChanged();
                }
            });
            this.softwareTable.setAdapter(this.adapter);
        }
    }

    private void initEquipmentRequirement() {

        //  --
        this.assignEquipmentButton = this.mView.findViewById(R.id.assignEquipmentButton);
        this.assignEquipmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                assignEquipmentState = !assignEquipmentState;
                changeAssignEquipmentButtonState();
            }
        });

        // --
        this.assignSoftwareButton = this.mView.findViewById(R.id.assignSoftwareButton);
        this.assignSoftwareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                assignSoftwareState = !assignSoftwareState;
                changeAssignSoftwareButtonState();
            }
        });

        // --
        this.componentSearch = new SearcherOpen(getContext(), (ConstraintLayout) this.mView.findViewById(R.id.searchComponent));
        this.componentSearch.setListener(this, "90");
        this.componentSearch.setType(SearcherOpen.SearcherOpenType.soft);
        this.componentSearch.setDataSoftware(this.getProfile().getSoftwareCatalogs());
        this.componentSearch.setInputHint(getResources().getString(R.string.request_vacancy_budget_software_box_hint));
        this.componentSearch.setSearchComponentListBoxTitle(getResources().getString(R.string.request_vacancy_budget_software_box_title));

        // -- Software Table
        this.softwareTable = this.mView.findViewById(R.id.softwareTable);
        final DividerItemDecoration itemDecorator = new DividerItemDecoration(this.mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this.mContext, R.drawable.recycler_devider_5));
        LinearLayoutManager layout = new LinearLayoutManager(this.mContext, LinearLayoutManager.VERTICAL, false);

        // --
        softwareTable.getRecycledViewPool().setMaxRecycledViews(0, 3);
        softwareTable.setLayoutManager(layout);
        softwareTable.addItemDecoration(itemDecorator);
        softwareTable.setItemViewCacheSize(8);
        softwareTable.setDrawingCacheEnabled(true);
        softwareTable.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // --
        this.adapter = new SelectedSoftwareAdapter(new ArrayList<SoftwareCatalog>(), this.mContext);
        softwareTable.setAdapter(this.adapter);
        // --
        this.adapter.setListener(new SelectedSkillAdapter.OnCloseButtonListerner() {
            @Override
            public void onCloseButton(Integer position) {

                adapter.removeElement(position);
                adapter.notifyDataSetChanged();
            }
        });
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeAssignEquipmentButtonState() {

        this.getProfile().setVacancyEquipment(assignEquipmentState);
        if (assignEquipmentState) {

            this.assignEquipmentButton.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_selected_button) );
            this.setViewAsSecondaryColor(this.assignEquipmentButton);

            this.assignEquipmentButton.setTextColor(Color.WHITE);
            this.assignEquipmentButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getContext().getResources().getDrawable( R.drawable.select_white_option ), null);

            // -- Show elements
            this.assignSoftwareButton.setVisibility(View.VISIBLE);
            this.assignSoftwareButton.animate().alpha(1.0f).setDuration(500);

        } else {

            this.assignEquipmentButton.setBackground( ContextCompat.getDrawable(mContext, R.drawable.request_vacancy_unselected_button) );
            this.assignEquipmentButton.setTextColor(Color.parseColor("#001330"));
            this.assignEquipmentButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getContext().getResources().getDrawable( R.drawable.empty_circle ), null);

            // -- Hide elements
            this.assignSoftwareButton.animate().alpha(0.0f).setDuration(350);
            this.assignSoftwareButton.setVisibility(View.GONE);

            // --
            this.assignSoftwareState  = false;
            this.changeAssignSoftwareButtonState();
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void changeAssignSoftwareButtonState() {

        this.getProfile().setVacancyEquipmentSoftware(assignSoftwareState);
        if (assignSoftwareState) {

            this.assignSoftwareButton.setBackgroundDrawable( ContextCompat.getDrawable(getContext(), R.drawable.selected_stroke_button) );
            this.assignSoftwareButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getContext().getResources().getDrawable( R.drawable.check_orange ), null);

            // -- Show elements
            this.mView.findViewById(R.id.assignSoftwareTitle).animate().alpha(1.0f).setDuration(500);
            this.componentSearch.getRoot().setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.searchComponentSubTitle).setVisibility(View.VISIBLE);
            this.mView.findViewById(R.id.softwareTable).setVisibility(View.VISIBLE);

            componentSearch.getSearchComponentBoxInput().requestFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(componentSearch.getSearchComponentBoxInput(), InputMethodManager.SHOW_IMPLICIT);

        } else {

            this.assignSoftwareButton.setBackgroundDrawable( ContextCompat.getDrawable(getContext(), R.drawable.request_vacancy_unselected_button) );
            this.assignSoftwareButton.setCompoundDrawablesWithIntrinsicBounds(null, null, getContext().getResources().getDrawable( R.drawable.rectangle_selecction ), null);

            // -- Hide elements
            this.mView.findViewById(R.id.assignSoftwareTitle).animate().alpha(0.0f).setDuration(350);
            this.componentSearch.getRoot().setVisibility(View.GONE);
            this.mView.findViewById(R.id.searchComponentSubTitle).setVisibility(View.GONE);
            this.mView.findViewById(R.id.softwareTable).setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemSelected(Object item, ConstraintLayout box, RecyclerView recyclerView, EditText editText, ImageView icon) {

        // --
        final SoftwareCatalog softwareCatalog = (SoftwareCatalog) item;

        // -- Hide keyboard
        editText.setText("");
        editText.clearFocus();
        hideSoftKeyboard(getActivity(), editText);

        // --
        this.adapter.addElement(softwareCatalog);
        this.adapter.notifyDataSetChanged();
        // --
        this.componentSearch.setSelectedSoftware(this.adapter.getItems());

        // --  Software
        this.getProfile().setCurrentSoftware(this.adapter.getItems());
    }

    @Override
    public void onItemSelected(Object item, RelativeLayout box, RecyclerView recyclerView, EditText editText, ImageView icon) {}

    // -- Abstract methods
    @Override
    public void onFooterContinueButtonClick() {

        //--
        super.onFooterContinueButtonClick();

        // --  Equipment
        this.getProfile().setVacancyEquipment(this.assignEquipmentState);

        // --  Software
        this.getProfile().setCurrentSoftware(this.adapter.getItems());
    }
}