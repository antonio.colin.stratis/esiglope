package com.stratis.estaffing.Service.Notification;

import android.util.Log;

import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Core.eStaffingClient;
import com.stratis.estaffing.Model.NotificationCenterFlow.NotificationCenterModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Erick Sanchez
 * Revision 1 - 2/25/21
 */

public class NotificationService {

    public static void get(final Session session, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/notification/findByUser");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("userId", session.getDataSessionId());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getAll(final Session session, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/notification/find");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("userId", session.getDataSessionId());
        //request.addParameter("page", "1");
        request.addParameter("size", "100");
        request.addParameter("sort", "viewed:asc,createdDate:desc");

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void save(final Session session) {

        final String url = Api.getInstance().getUrl("/notification/save");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        JSONObject obj = new JSONObject();
        try {

            obj.put("userId", session.getDataSessionId());
            obj.put("message", "Tu solicitud de “Desarrollador Backend” ha sido atendida y finalizada con éxito. Toca aquí para más información. 1");
            obj.put("parameters", "vacancyId:1053,talentId:4");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // --
        request.addBodyJson(JsonBuilder.jsonToString(obj));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {}

            @Override
            public void onError(ReSTResponse response) {}
        });
    }

    public static void read(final Session session, final NotificationCenterModel notification) {

        final String url = Api.getInstance().getUrl("/notification/viewed");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        JSONObject obj = new JSONObject();
        try {

            obj.put("userId", session.getDataSessionId());
            final JSONArray notifications = new JSONArray();
            final JSONObject notificationObject = new JSONObject();

            notificationObject.put("notificationId", notification.getId());
            notifications.put(notificationObject);
            obj.put("notificationList", notifications);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // --
        request.addBodyJson(JsonBuilder.jsonToString(obj));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {}

            @Override
            public void onError(ReSTResponse response) {}
        });
    }

    public static void readAll(final Session session) {

        final String url = Api.getInstance().getUrl("/notification/" + session.getDataSessionId() + "/allSeen");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {}

            @Override
            public void onError(ReSTResponse response) {}
        });
    }

    public static void saveConfiguration(final Session session) {

        final String url = Api.getInstance().getUrl("/notification/configuration/save");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        JSONObject obj = new JSONObject();
        try {

            obj.put("userId", session.getDataSessionId());
            obj.put("fcmToken", session.getFcmToken());
            //obj.put("globalSetting", 4);
            //obj.put("notificationList", JSONObject.NULL);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // --
        Log.i("DXGOP", "CONFIGURATION SEND ::: " + JsonBuilder.jsonToString(obj));
        request.addBodyJson(JsonBuilder.jsonToString(obj));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {}

            @Override
            public void onError(ReSTResponse response) {}
        });
    }
}