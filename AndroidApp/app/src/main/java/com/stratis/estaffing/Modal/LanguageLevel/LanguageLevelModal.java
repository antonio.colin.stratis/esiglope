package com.stratis.estaffing.Modal.LanguageLevel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.stratis.estaffing.Modal.LanguageLevel.Adapter.LanguageLevelAdapter;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageCatalog;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageLevel;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 28/07/20
 */

public class LanguageLevelModal extends BottomSheetDialog {

    private TextView titleView;
    private RecyclerView languageTable;

    private LanguageCatalog languageCatalog;
    private ArrayList<LanguageLevel> levels;
    private RecyclerView.OnItemTouchListener listener;

    public LanguageLevelModal(Context context) {

        super(context);
        this.commonInit();
    }

    public LanguageLevelModal(Context context, int themeResId) {

        super(context, themeResId);
        this.commonInit();
    }

    protected LanguageLevelModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.commonInit();
    }

    private void commonInit() {

        // -- R styles
        this.setContentView(R.layout.modal_language_level);
        this.setCanceledOnTouchOutside(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void show() {

        super.show();

        // --
        this.handleActions();
    }

    @SuppressLint("ResourceType")
    private void handleActions() {

        // --
        final TextView title = this.findViewById(R.id.textView2);
        title.setText(String.format( getContext().getResources().getString(R.string.modal_language_level_title), languageCatalog.getName() ));

        // --
        final LanguageLevelAdapter adapter = new LanguageLevelAdapter(this.levels, getContext());

        // --
        final RecyclerView languageTable = this.findViewById(R.id.languageLevelTable);

        // --
        final LinearLayoutManager layout = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        // --
        languageTable.getRecycledViewPool().setMaxRecycledViews(0, 3);
        languageTable.setLayoutManager(layout);
        //languageTable.addItemDecoration(itemDecorator);
        languageTable.setItemViewCacheSize(8);
        languageTable.setDrawingCacheEnabled(true);
        languageTable.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // --
        adapter.setLanguageCatalog(this.languageCatalog);
        languageTable.setAdapter(adapter);

        // --
        languageTable.addOnItemTouchListener(this.listener);
    }

    public void setLevels(ArrayList<LanguageLevel> levels) {
        this.levels = levels;
    }

    public void setLanguageCatalog(LanguageCatalog languageCatalog) {
        this.languageCatalog = languageCatalog;
    }

    public void setListener(RecyclerView.OnItemTouchListener listener) {
        this.listener = listener;
    }
}