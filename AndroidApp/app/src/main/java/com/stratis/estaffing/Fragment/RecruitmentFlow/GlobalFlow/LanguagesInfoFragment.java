package com.stratis.estaffing.Fragment.RecruitmentFlow.GlobalFlow;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.R;

public class LanguagesInfoFragment extends ABFragment {

    public LanguagesInfoFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.mView = inflater.inflate(R.layout.fragment_languages_info, container, false);

        this.setHeader("");
        this.initViews();

        return this.mView;
    }

    private void initViews() {

        // -- Basic
        // --
        final ConstraintLayout basic_one = this.mView.findViewById(R.id.basic_one);
        final TextView basic_one_level = basic_one.findViewById(R.id.textView4);
        final TextView basic_one_desc = basic_one.findViewById(R.id.textView5);
        basic_one_level.setText( this.getStr(R.string.modal_languages_basic_level_one) );
        basic_one_desc.setText( this.getStr(R.string.modal_languages_basic_desc_one) );

        // --
        final ConstraintLayout basic_two = this.mView.findViewById(R.id.basic_two);
        final TextView basic_two_level = basic_two.findViewById(R.id.textView4);
        final TextView basic_two_desc = basic_two.findViewById(R.id.textView5);
        basic_two_level.setText( this.getStr(R.string.modal_languages_basic_level_two) );
        basic_two_desc.setText( this.getStr(R.string.modal_languages_basic_desc_two) );


        // -- Medium
        // --
        final ConstraintLayout medium_one = this.mView.findViewById(R.id.medium_one);
        final TextView medium_one_level = medium_one.findViewById(R.id.textView4);
        final TextView medium_one_desc = medium_one.findViewById(R.id.textView5);
        medium_one_level.setText( this.getStr(R.string.modal_languages_medium_level_one) );
        medium_one_level.setBackgroundColor(getContext().getColor(R.color.modal_languages_medium_color));
        medium_one_desc.setText( this.getStr(R.string.modal_languages_medium_desc_one) );
        medium_one_desc.setBackground(getContext().getResources().getDrawable(R.drawable.modal_languages_medium));

        // --
        final ConstraintLayout medium_two = this.mView.findViewById(R.id.medium_two);
        final TextView medium_two_level = medium_two.findViewById(R.id.textView4);
        final TextView medium_two_desc = medium_two.findViewById(R.id.textView5);
        medium_two_level.setText( this.getStr(R.string.modal_languages_medium_level_two) );
        medium_two_level.setBackgroundColor(getContext().getColor(R.color.modal_languages_medium_color));
        medium_two_desc.setText( this.getStr(R.string.modal_languages_medium_desc_two) );
        medium_two_desc.setBackground(getContext().getResources().getDrawable(R.drawable.modal_languages_medium));


        // -- Advance
        // --
        final ConstraintLayout advance_one = this.mView.findViewById(R.id.advance_one);
        final TextView advance_one_level = advance_one.findViewById(R.id.textView4);
        final TextView advance_one_desc = advance_one.findViewById(R.id.textView5);
        advance_one_level.setText( this.getStr(R.string.modal_languages_advance_level_one) );
        advance_one_level.setBackgroundColor(getContext().getColor(R.color.modal_languages_advance_color));
        advance_one_desc.setText( this.getStr(R.string.modal_languages_advance_desc_one) );
        advance_one_desc.setBackground(getContext().getResources().getDrawable(R.drawable.modal_languages_advance));

        // --
        final ConstraintLayout advance_two = this.mView.findViewById(R.id.advance_two);
        final TextView advance_two_level = advance_two.findViewById(R.id.textView4);
        final TextView advance_two_desc = advance_two.findViewById(R.id.textView5);
        advance_two_level.setText( this.getStr(R.string.modal_languages_advance_level_two) );
        advance_two_level.setBackgroundColor(getContext().getColor(R.color.modal_languages_advance_color));
        advance_two_desc.setText( this.getStr(R.string.modal_languages_advance_desc_two) );
        advance_two_desc.setBackground(getContext().getResources().getDrawable(R.drawable.modal_languages_advance));
    }
}