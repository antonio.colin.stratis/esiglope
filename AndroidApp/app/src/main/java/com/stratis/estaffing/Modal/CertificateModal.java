package com.stratis.estaffing.Modal;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Model.Course;
import com.stratis.estaffing.R;


/**
 * Created by Erick Sanchez
 * Revision 1 - 28/07/20
 */

public class CertificateModal extends Dialog {

    private TextView title = null;
    private ImageView image = null;
    private Button closeButton = null;

    private Course course = null;

    public CertificateModal(Context context) {

        super(context);
        this.init();
    }

    public CertificateModal(Context context, int themeResId) {

        super(context, themeResId);
        this.init();
    }

    protected CertificateModal(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {

        super(context, cancelable, cancelListener);
        this.init();
    }

    @SuppressLint("ResourceType")
    private void init() {

        // -- R styles
        this.setContentView(R.layout.modal_certificate);
        FirebaseEvent.logEvent(getContext(), this.getClass().getSimpleName());
        this.setCanceledOnTouchOutside(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow()
                .setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );

        // --
        this.title = this.findViewById(R.id.courseName);
        this.image = this.findViewById(R.id.image);
        this.closeButton = this.findViewById(R.id.closeButton);

        // --
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        App.setViewAsSecondaryColor(this.closeButton, getContext());
    }

    public void setCourse(Course course) {

        // --
        this.course = course;

        // --
        this.title.setText( this.course.getName() );

        // -- Image
        if (this.course.getHasCertificate() && !this.course.getCertificateFileUrl().equals("") && !this.course.getCertificateFileUrl().equals("null")) {

            this.image.setImageBitmap(null);
            this.image.setVisibility(View.VISIBLE);

            Glide.with(getContext())
                    .load(this.course.getCertificateFileUrl())
                    .thumbnail(.40f)
                    .override(Target.SIZE_ORIGINAL, 206)
                    .centerCrop()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    // .placeholder(R.drawable.loading_spinner)
                    .into( this.image );

        } else {
            // -- Show NO - CERTIFICATE IMAGE
            this.image.setVisibility(View.VISIBLE);
        }
    }
}
