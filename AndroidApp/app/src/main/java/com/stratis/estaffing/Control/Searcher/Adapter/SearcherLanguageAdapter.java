package com.stratis.estaffing.Control.Searcher.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.Searcher.Filter.LanguageSearchFilter;
import com.stratis.estaffing.Control.Searcher.View.SearchLanguageView;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageCatalog;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class SearcherLanguageAdapter extends RecyclerView.Adapter<SearchLanguageView> implements Filterable {

    protected ArrayList<LanguageCatalog> items;
    protected ArrayList<LanguageCatalog> original;
    protected ArrayList<LanguageCatalog> selected = new ArrayList<>();
    protected Context context;

    public SearcherLanguageAdapter(ArrayList<LanguageCatalog> items, Context context) {

        this.context = context;
        this.items = items;
        this.original = items;
    }

    @Override
    public SearchLanguageView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.component_searcher_language_item, parent, false);
        return new SearchLanguageView(layoutView);
    }

    @Override
    public void onBindViewHolder(final SearchLanguageView holder, int position) {

        holder.setIsRecyclable(false);
        final LanguageCatalog language = this.items.get(position);

        // -- Setting the views
        holder.getSearchComponentBoxItemText().setText( language.getName() );
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<LanguageCatalog> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void reload(Object values) {
        this.reload((ArrayList<LanguageCatalog>) values);
    }

    public ArrayList<LanguageCatalog> getItems() {
        return items;
    }

    public ArrayList<LanguageCatalog> getOriginal() {
        return original;
    }

    public ArrayList<LanguageCatalog> getSelected() {
        return selected;
    }

    public void setSelected(ArrayList<LanguageCatalog> selected) {
        this.selected = selected;
    }

    // --

    @NonNull
    @Override
    public Filter getFilter() {

        LanguageSearchFilter filter = new LanguageSearchFilter(this);
        return filter;
    }
}
