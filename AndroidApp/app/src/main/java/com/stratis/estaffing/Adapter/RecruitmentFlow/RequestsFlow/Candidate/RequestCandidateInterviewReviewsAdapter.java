package com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.RatingStar.RatingStarComplete;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Interview.RequestCandidateInterview;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate.CandidateInterviewReviewView;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.RequestView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * Created by Erick Sanchez
 * Revision 1 - 21/08/20
 */

public class RequestCandidateInterviewReviewsAdapter extends RecyclerView.Adapter<CandidateInterviewReviewView> {

    private ArrayList<RequestCandidateInterview> items;

    private Context context;

    public RequestCandidateInterviewReviewsAdapter(final Context context, ArrayList<RequestCandidateInterview> items) {

        this.items = items;
        this.context = context;
    }

    @Override
    public CandidateInterviewReviewView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.fragment_recruitment_request_candidate_single_multi_rated_component_item,
                        parent, false);
        return new CandidateInterviewReviewView(layoutView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final CandidateInterviewReviewView holder, int position) {

        holder.setIsRecyclable(false);
        final RequestCandidateInterview item = this.items.get(position);

        // -- Setting the views
        holder.getInterviewNumber().setText( (items.size() - position) + "ª Entrevista" );
        holder.getInterviewDate().setText( App.getDateFormatted(item.getDate(), "dd/MMM/YY")
                .replaceAll("\\.","") );

        holder.getInterviewPerson().setText( item.getInterviewerNameShort() );
        holder.getInterviewReview().setText( item.getReview() );

        // -- Starts and not done
        final RatingStarComplete ratedContainerStars = new RatingStarComplete(holder.getRatedContainerStars(), this.context);
        if (item.getStatus().equals(RequestCandidateInterview.interviewNotMade)) {

            ratedContainerStars.getBaseLayout().setVerticalGravity(View.GONE);
            holder.getRatedNoDone().setVisibility(View.VISIBLE);

        } else {

            final String rateString = item.getRating().substring(0, 1);
            final int rateInt = Integer.parseInt( App.isNumeric(rateString) ? rateString : "0");
            ratedContainerStars.setRating(rateInt);

        }
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<RequestCandidateInterview> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }
}