package com.stratis.estaffing.Adapter.RecruitmentFlow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Model.RecruitmentFlow.ProfileCheck;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.NewRequestFlow.ProfileCheckView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class ProfileCheckAdapter extends RecyclerView.Adapter<ProfileCheckView> {

    protected ArrayList<ProfileCheck> items;
    protected Context context;
    protected ProfileCheckClickListener listener;

    public ProfileCheckAdapter(ArrayList<ProfileCheck> items, Context context) {

        this.context = context;
        this.items = items;
    }

    @Override
    public ProfileCheckView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_request_check_item, parent, false);
        return new ProfileCheckView(layoutView);
    }

    @Override
    public void onBindViewHolder(final ProfileCheckView holder, final int position) {

        holder.setIsRecyclable(false);
        final ProfileCheck check = this.items.get(position);

        holder.getIcon().setImageBitmap( check.getIcon() );
        holder.getTitle().setText( check.getTitle() );
        holder.getContent().setText( check.getContent() );

        // -- Button
        holder.getEditButton().setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                if (listener != null) {
                    listener.onCheckButtonClicked(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<ProfileCheck> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<ProfileCheck> getItems() {
        return items;
    }

    public void setListener(ProfileCheckClickListener listener) {
        this.listener = listener;
    }

    public interface ProfileCheckClickListener {
        void onCheckButtonClicked(int position);
    }
}
