package com.stratis.estaffing.Controller.RecruitmentFlow;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.stratis.estaffing.Adapter.RecruitmentFlow.LatestRequestAdapter;
import com.stratis.estaffing.Control.ABActivity;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Searcher.Listener.RecyclerItemClickListener;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Core.FirebaseEvent;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.LatestVacancyRequest;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.RequestsService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Objects;

public class  NewRequestController extends ABActivity {

    private RecyclerView content_section_view;
    private ConstraintLayout noRequestContainer;
    private RelativeLayout searchProfile;
    private ConstraintLayout noRequestSearchContainer;
    private LatestRequestAdapter adapter;
    private EditText editSearachProfile;

    private ArrayList<LatestVacancyRequest> profiles;
    private ArrayList<LatestVacancyRequest> filterProfiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_new_request_controller);
        FirebaseEvent.logEvent(getApplicationContext(), this.getClass().getSimpleName());

        // -- Set header
        this.setHeader("");
        this.setStatusBarColor(Color.parseColor("#f5f5f6"));
        this.setStatusBarTextLight();

        this.initViews();
    }

    @Override
    protected void onResume() {

        super.onResume();
        this.getLatestRequests();
    }

    private void initViews() {

        // -- New Request
        final RelativeLayout newRequest = this.findViewById(R.id.newRequest);
        newRequest.setOnClickListener(v -> startActivity(new Intent(NewRequestController.this, RecruitmentFlowController.class)));
        this.setViewAsPrimaryColor(newRequest);

        /** DividerItemDecoration instance **/
        final DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL);
        itemDecorator.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(this, R.drawable.latest_request_devider)));

        /** LinearLayoutManager instances **/
        final LinearLayoutManager contentRecycleViewLayout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        this.noRequestContainer = this.findViewById(R.id.noRequestContainer);
        this.searchProfile = this.findViewById(R.id.searchProfile);
        this.noRequestSearchContainer = this.findViewById(R.id.noRequestSearchContainer);
        this.editSearachProfile = this.searchProfile.findViewById(R.id.searchComponentBoxInput);

        /** EditText **/
        this.editSearachProfile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterProfiles.clear();

                for (int i=0;i<profiles.size();i++) {
                    if (s.toString().equals("") || profiles.get(i).getName().toLowerCase().contains(s.toString().toLowerCase())) {
                        filterProfiles.add(profiles.get(i));
                    }
                }

                if (filterProfiles.size() == 0) {
                    content_section_view.setVisibility(View.GONE);
                    noRequestSearchContainer.setVisibility(View.VISIBLE);
                } else {
                    content_section_view.setVisibility(View.VISIBLE);
                    noRequestSearchContainer.setVisibility(View.GONE);
                }

                adapter.getItems().clear();
                adapter.getItems().addAll(filterProfiles);
                adapter.notifyDataSetChanged(); 
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        /** Content Recycle View **/
        this.content_section_view = this.findViewById(R.id.latestRequestView);
        this.content_section_view.getRecycledViewPool().setMaxRecycledViews(0, 5);
        this.content_section_view.setLayoutManager(contentRecycleViewLayout);
        this.content_section_view.addItemDecoration(itemDecorator);
        this.content_section_view.setItemViewCacheSize(5);
        this.content_section_view.setDrawingCacheEnabled(true);
        this.content_section_view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        this.content_section_view.addOnItemTouchListener(
                new RecyclerItemClickListener(this, this.content_section_view, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        getRequestDetail(adapter.getItems().get(position));
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {}
                }));

        // -- Templates
        this.getLatestRequests();
    }

    private void getLatestRequests() {

        RequestsService.getRequests(Session.get(this), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                if (res.length() > 0) {
                    profiles = LatestVacancyRequest.parse(res);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        profiles.sort(Comparator. comparing(o -> o.getDate()));
                    }
                    Collections.reverse(profiles);
                    filterProfiles = LatestVacancyRequest.parse(res);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        filterProfiles.sort(Comparator.comparing(o -> o.getDate()));
                    }
                    Collections.reverse(filterProfiles);
                    adapter = new LatestRequestAdapter(filterProfiles);
                    if (content_section_view != null) {
                        content_section_view.setAdapter(adapter);
                    }
                    content_section_view.setVisibility(View.VISIBLE);
                    searchProfile.setVisibility(View.VISIBLE);
                } else {
                    noRequestContainer.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(ReSTResponse response) {}
        });
    }

    private void getRequestDetail(final LatestVacancyRequest item) {

        RequestsService.getRequest(item.getId(), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                // -- Response
                final JSONObject res = JsonBuilder.stringToJson(response.body);
                Log.d("RESTER", res.toString());
                Profile profile = new Profile();

                profile.loadFromJSON(res);
                profile.setStatus("Solicitud creada");

                if (profile.getJobLocation().getId() == 0) {
                    profile.setJobLocation(null);
                }

                // -- Intent
                final Intent intent = new Intent(NewRequestController.this,
                        RecruitmentFlowController.class);
                intent.putExtra("profile", profile);
                startActivity(intent);
            }

            @Override
            public void onError(ReSTResponse response) {}
        });
    }

}