package com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequestCandidateFilter.Model;

import android.content.Context;
import android.graphics.Bitmap;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 09/12/20
 */

public class RequestCandidateFilterModel {

    private String id = "";
    private String name = "";
    private String icon = "";

    public static ArrayList<RequestCandidateFilterModel> getFilter(Context context) {

        ArrayList<RequestCandidateFilterModel> filters = new ArrayList<>();

        //  -- All
        final RequestCandidateFilterModel allFilter = new RequestCandidateFilterModel();
        allFilter.setId("0");
        allFilter.setName(context.getString(R.string.recruitment_actions_request_candidates_filter_modal_all));
        allFilter.setIcon("icon_request_filter_all");
        filters.add(allFilter);

        //  -- All
        final RequestCandidateFilterModel activeFilter = new RequestCandidateFilterModel();
        activeFilter.setId(RequestCandidate.statusCandidateInterviewDateAssigned);
        activeFilter.setName(context.getString(R.string.recruitment_actions_request_candidates_filter_modal_scheduled));
        activeFilter.setIcon("icon_request_active");
        filters.add(activeFilter);

        //  -- All
        final RequestCandidateFilterModel pausedFilter = new RequestCandidateFilterModel();
        pausedFilter.setId(RequestCandidate.statusCandidate);
        pausedFilter.setName(context.getString(R.string.recruitment_actions_request_candidates_filter_modal_candidate));
        pausedFilter.setIcon("icon_candidates_candidates_filter_modal");
        filters.add(pausedFilter);

        //  -- All
        final RequestCandidateFilterModel cancelFilter = new RequestCandidateFilterModel();
        cancelFilter.setId("999");
        cancelFilter.setName(context.getString(R.string.recruitment_actions_request_candidates_filter_modal_fav));
        cancelFilter.setIcon("icon_candidates_graded_filter_modal");
        filters.add(cancelFilter);

        //  -- All
        final RequestCandidateFilterModel doneFilter = new RequestCandidateFilterModel();
        doneFilter.setId(RequestCandidate.statusCandidateNotInterest);
        doneFilter.setName(context.getString(R.string.recruitment_actions_request_candidates_filter_modal_notint));
        doneFilter.setIcon("icon_request_cancel");
        filters.add(doneFilter);

        // --
        return filters;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return this.icon;
    }

    public Bitmap getIcon(Context context) {
        return App.getImage(context, this.icon);
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}