package com.stratis.estaffing.View.Talent

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.R

class FeedbackView(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var interviewLayoutParent: ConstraintLayout? = null
    var interviewDate: Button? = null
    var interviewStrength: TextView? = null
    var interviewOportunity: TextView? = null
    var interviewRate: TextView? = null

    init {
        this.interviewLayoutParent = itemView.findViewById(R.id.interviewLayoutParent)
        this.interviewDate = itemView.findViewById(R.id.interviewDate)
        this.interviewStrength = itemView.findViewById(R.id.interviewStrength)
        this.interviewOportunity = itemView.findViewById(R.id.interviewOportunity)
        this.interviewRate = itemView.findViewById(R.id.interviewRate)
    }
}