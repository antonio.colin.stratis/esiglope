package com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidateSkill;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.RequestCandidateSkillView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class RequestCandidateSkillsAdapter extends RecyclerView.Adapter<RequestCandidateSkillView> {

    protected ArrayList<RequestCandidateSkill> items;
    protected RequestCandidatesClickListener listener;

    public RequestCandidateSkillsAdapter(ArrayList<RequestCandidateSkill> items) {
        this.items = items;
    }

    @Override
    public RequestCandidateSkillView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recruitment_request_candidate_single_item,
                parent, false);
        return new RequestCandidateSkillView(layoutView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final RequestCandidateSkillView holder, final int position) {

        holder.setIsRecyclable(false);
        final RequestCandidateSkill skill = this.items.get(position);

        // --
        holder.getName().setText( skill.getName() );
        if(skill.getType().equals(RequestCandidateSkill.RequestCandidateSkillType.skill)){
            holder.getPercentBar().setVisibility(View.GONE);
        }
        else {
            holder.getPercentBar().setProgress(Float.parseFloat(skill.getPercentage()));
            holder.getPercentBar().setProgressBarColor(Color.parseColor(App.fixWrongColorATuDePiglek(Session.get(holder.itemView.getContext()).getPreferences().getSecondaryColor())));
        }
        holder.getPercent().setText( skill.getPercentage() + "%" );

        if (skill.getType() == RequestCandidateSkill.RequestCandidateSkillType.language) {
            holder.getLanguageLevel().setText( skill.getLangLevelName() );
        } else {
            holder.getLanguageLevel().setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<RequestCandidateSkill> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<RequestCandidateSkill> getItems() {
        return items;
    }

    public void setListener(RequestCandidatesClickListener listener) {
        this.listener = listener;
    }

    public interface RequestCandidatesClickListener {
        void onDetail(RequestCandidate item);
    }
}