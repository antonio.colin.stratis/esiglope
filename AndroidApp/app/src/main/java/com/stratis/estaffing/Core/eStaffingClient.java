package com.stratis.estaffing.Core;

import android.content.Intent;
import android.util.Log;

import com.stratis.estaffing.Controller.LoginFlow.LoginController;
import com.stratis.estaffing.Core.Rester.ReSTClient;

/**
 * Created by Erick Sanchez
 * Revision 1 - 3/31/21
 */

public class eStaffingClient extends ReSTClient {

    private boolean reExecuted = false;

    public eStaffingClient() {

        super();
        this.customWorker = true;
    }

    public eStaffingClient(String serviceUrl) {

        super(serviceUrl);
        this.customWorker = true;
    }

    @Override
    public void checkStatusCode(Integer result) {

        super.checkStatusCode(result);

        // -- Check worker
        if (this.mWorker != null) {

            switch(this.mWorker.getResponse().statusCode) {

                case 403:

                    if (!this.reExecuted) {

                        Log.d(TAG, "RESTER -- SENDING REFRESH TOKEN -- ");
                        this.reExecuted = true;
                        final String refreshToken = Session.get( App.getAppContext() ).getRefreshToken();
                        this.mWorker.getRequest().addHeader("refreshtoken", refreshToken);
                        this.mWorker.cancel(true);

                        // -- New instance
                        this.mWorker = new ReSTWorker(this.mWorker.getRequest(), this.mWorker.getCallback());
                        this.mWorker.execute();

                    } else {

                        final Intent intent = new Intent(App.getAppContext(), LoginController.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        App.getAppContext().startActivity(intent);
                    }
                    break;

                default:

                    if (this.reExecuted && !this.mWorker.getResponse().getHeader("token").equals("")) {
                        Log.d(TAG, "RESTER -- SAVING NEW ID TOKEN -- ");
                        Session.get(App.getAppContext()).setIdToken( this.mWorker.getResponse().getHeader("token") );
                    }
                    this.mWorker.onValidate(result);
                    break;
            }
        }
    }
}
