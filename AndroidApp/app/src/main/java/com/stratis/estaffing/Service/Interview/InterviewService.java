package com.stratis.estaffing.Service.Interview;

import android.util.Log;

import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Core.eStaffingClient;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Interview.RequestCandidateInterview;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Erick Sanchez
 * Revision 1 - 06/01/21
 */

public class InterviewService {

    public static void get(final String candidateId, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/interview/findByCandidate");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("candidateId", candidateId);

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getRated(final RequestCandidate candidate, final ReSTCallback listener) {

        final String url = Api.getInstance().getUrl("/interview/find/" + candidate.getVacancyId() + "/" + candidate.getTalentId() );
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void create(final RequestCandidateInterview interview,
                              final Session session, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/interview/save");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_POST, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("status", RequestCandidateInterview.interviewScheduled);
            body.put("isRemote", interview.getRemote());
            body.put("vacancyId", interview.getCandidate().getVacancyId());
            body.put("talentId", interview.getCandidate().getTalentId());
            body.put("asignee", session.getDataSessionId());
            body.put("date", interview.getDateServer());
            body.put("locationName", interview.getLocationName());
            body.put("locationAddress", interview.getLocationAddress());
            body.put("interviewerName", interview.getInterviewerName());
            body.put("interviewerEmail", interview.getInterviewerEmail());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.setSuccessCode(201);
        rest.execute(request, listener);
    }

    public static void reschedule(final RequestCandidateInterview interview, String cause,
                                  final String comment, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/interview/reschedule");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_PUT, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("interviewId", interview.getId());
            body.put("date", interview.getDateServer());
            body.put("isRemote", interview.getRemote());
            body.put("locationName", interview.getLocationName());
            body.put("locationAddress", interview.getLocationAddress());
            body.put("cause", cause);
            body.put("comment", comment);
            body.put("interviewerName", interview.getInterviewerName());
            body.put("interviewerEmail", interview.getInterviewerEmail());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void evaluate(final RequestCandidateInterview interview, String review,
                                  int rating, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/interview/evaluate");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_PUT, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("interviewId", interview.getId());
            body.put("rating", rating);
            body.put("review", review);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void updateStatus(final RequestCandidateInterview interview,
                                    String review, final String comment,
                                    String status, final ReSTCallback listener) {

        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/interview/updateStatus");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_PUT, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // -- JSON Body
        final JSONObject body = new JSONObject();
        try {

            body.put("id", interview.getId());
            body.put("status", status);
            body.put("review", review);
            body.put("comment", comment);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.addBodyJson(JsonBuilder.jsonToString(body));
        Log.d("DXGOP", "SEND ::: " + JsonBuilder.jsonToString(body));

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }
}
