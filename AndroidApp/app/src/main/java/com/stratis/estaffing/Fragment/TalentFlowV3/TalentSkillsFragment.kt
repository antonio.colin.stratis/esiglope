package com.stratis.estaffing.Fragment.TalentFlowV3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.RequestCandidateSkillsAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidateSkill
import com.stratis.estaffing.Model.Skill
import com.stratis.estaffing.R
import java.util.*

class TalentSkillsFragment: ABFragment() {

    open var skills : ArrayList<Skill>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mView = inflater.inflate(R.layout.fragment_talent_single_skills, container, false)

        setHeader("")
        this.initViews()

        return this.mView
    }

    private fun initViews() {

        val hardSkills: RecyclerView = mView.findViewById(R.id.hardSkillsContainerList)
        App.createVerticalRecyclerList(hardSkills, context)
        val hardSkillAdapter = RequestCandidateSkillsAdapter(parseSkills(1001100001, 1001100004))
        hardSkills.adapter = hardSkillAdapter

        val softSkills: RecyclerView = mView.findViewById(R.id.softSkillsContainerList)
        App.createVerticalRecyclerList(softSkills, context)
        val softSkillAdapter = RequestCandidateSkillsAdapter(parseSkills(1001100002, 1001100003))
        softSkills.adapter = softSkillAdapter

    }

    private fun parseSkills(categoryId1: Int, categoryId2: Int): ArrayList<RequestCandidateSkill>? {
        var skillsParsed: ArrayList<RequestCandidateSkill>? = ArrayList()
        skills?.forEach{
            if( it.categoryId.equals(categoryId1) || it.categoryId.equals(categoryId2) ) {
                var skillParsed: RequestCandidateSkill? = RequestCandidateSkill(true)
                skillParsed!!.name = it.name
                skillParsed!!.percentage = it.skillLevelPercentage.toString()
                skillsParsed!!.add(skillParsed)
            }
        }
        return skillsParsed
    }

}