package com.stratis.estaffing.Controller.LoginFlow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.stratis.estaffing.R;

public class RegisterThanksController extends AppCompatActivity {

    private String emailRegister = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_thanks_controller);

        // -- Get intent data
        this.getIntentData();
    }

    private void getIntentData() {

        if (getIntent().hasExtra("emailRegister")) {
            this.emailRegister = getIntent().getStringExtra("emailRegister");
        }

        // -- Init views
        this.initViews();
    }

    private void initViews() {

        // --
        final String txt = String.format( getResources().getString(R.string.register_thanks_subtitle), this.emailRegister );
        ((TextView) this.findViewById(R.id.subtitle)).setText(txt);

        // --
        final Button backButton = this.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Button webButton = this.findViewById(R.id.webButton);
        webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.mystratis.com/"));
                startActivity(browserIntent);
            }
        });
    }
}