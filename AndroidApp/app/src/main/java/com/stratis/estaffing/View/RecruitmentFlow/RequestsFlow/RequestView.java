package com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.R;

/**
 * Created by Erick Sanchez
 * Revision 1 - 21/08/20
 */

public class RequestView extends RecyclerView.ViewHolder {

    private ImageView requestIcon;
    private TextView requestVacancy;
    private TextView requestOrder;
    private TextView requestTitle;
    private TextView requestDescription;
    private TextView requestDate;
    private Button requestButton;

    public RequestView(View itemView) {

        super(itemView);
        this.requestIcon = itemView.findViewById(R.id.requestIcon);
        this.requestOrder = itemView.findViewById(R.id.requestOrder);
        this.requestTitle = itemView.findViewById(R.id.requestTitle);
        this.requestDescription = itemView.findViewById(R.id.requestDescription);
        this.requestDate = itemView.findViewById(R.id.requestDate);
        this.requestButton = itemView.findViewById(R.id.requestButton);
    }

    // --

    public ImageView getRequestIcon() {
        return requestIcon;
    }

    public TextView getRequestOrder() {
        return requestOrder;
    }

    public TextView getRequestTitle() {
        return requestTitle;
    }

    public TextView getRequestDescription() {
        return requestDescription;
    }

    public TextView getRequestDate() {
        return requestDate;
    }

    public Button getRequestButton() {
        return requestButton;
    }

    public TextView getRequestVacancy() {

        if (this.requestVacancy == null) {
            this.requestVacancy = itemView.findViewById(R.id.requestVacancy);
        }
        return requestVacancy;
    }
}
