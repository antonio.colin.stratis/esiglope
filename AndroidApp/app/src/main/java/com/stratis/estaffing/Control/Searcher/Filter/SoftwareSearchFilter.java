package com.stratis.estaffing.Control.Searcher.Filter;

import android.widget.Filter;

import com.stratis.estaffing.Control.Searcher.Adapter.SearcherSoftwareAdapter;
import com.stratis.estaffing.Model.RecruitmentFlow.SoftwareCatalog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erick Sanchez - App Builders CTO
 * Revision 1 - 01/09/20
 */

public class SoftwareSearchFilter extends Filter {

    SearcherSoftwareAdapter adapter;

    public SoftwareSearchFilter(SearcherSoftwareAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

        FilterResults results = new FilterResults();
        // We implement here the filter logic
        if (constraint == null || constraint.length() == 0) {

            // No filter implemented we return all the list
            results.values = this.adapter.getOriginal();
            results.count = this.adapter.getOriginal().size();

        } else {

            // We perform filtering operation
            List<SoftwareCatalog> nArrayList = new ArrayList<SoftwareCatalog>();
            if (nArrayList.size() > 1) {
                nArrayList.set(0, new SoftwareCatalog((String) constraint));
            } else {
                nArrayList.add(0, new SoftwareCatalog((String) constraint));
            }

            // --
            for (SoftwareCatalog item : this.adapter.getOriginal()) {

                if (item.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    if (nArrayList.size() < 4 && !this.adapter.getSelected().contains(item)) {
                        nArrayList.add(item);
                    }
                }

                if (item.getName().toLowerCase().equals(constraint.toString().toLowerCase())) {
                    nArrayList.remove(0);
                }
            }

            results.values = nArrayList;
            results.count = nArrayList.size();
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        // Now we have to inform the adapter about the new list filtered
        this.adapter.reload((ArrayList<SoftwareCatalog>) results.values);
        this.adapter.notifyDataSetChanged();
    }
}
