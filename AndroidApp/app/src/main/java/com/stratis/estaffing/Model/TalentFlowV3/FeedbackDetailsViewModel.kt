package com.stratis.estaffing.Model.TalentFlowV3

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class FeedbackDetailsViewModel : ViewModel() {
    private val feedbackDetails = MutableLiveData<ArrayList<FeedbackDetail>>()

    fun setFeedbackDetails(feedbackDetails: ArrayList<FeedbackDetail>) {
        this.feedbackDetails.value = feedbackDetails
    }

    fun setFeedbackDetails(feedbackDetailsMutable: MutableList<FeedbackDetail>) {
        val feedbackDetails: ArrayList<FeedbackDetail> = ArrayList()
        feedbackDetailsMutable.forEach{
            feedbackDetails.add(it)
        }
        this.setFeedbackDetails(feedbackDetails)
    }

    fun getFeedbackDetails() : LiveData<ArrayList<FeedbackDetail>> {
        return feedbackDetails
    }

}