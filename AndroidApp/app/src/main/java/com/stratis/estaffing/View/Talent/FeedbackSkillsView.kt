package com.stratis.estaffing.View.Talent

import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.R

class FeedbackSkillsView(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var shape: RelativeLayout? = null
    var layoutStatusInit: ConstraintLayout? = null
    var textStatusInit: TextView? = null
    var parentLayoutStatusLoad: RelativeLayout? = null
    var textStatusLoad: TextView? = null
    var textValueLoad: TextView? = null
    var itemList: RecyclerView? = null

    init {
        this.shape = itemView.findViewById(R.id.shape)
        this.layoutStatusInit = itemView.findViewById(R.id.layoutStatusInit)
        this.textStatusInit = itemView.findViewById(R.id.textStatusInit)
        this.parentLayoutStatusLoad = itemView.findViewById(R.id.parentLayoutStatusLoad)
        this.textStatusLoad = itemView.findViewById(R.id.textStatusLoad)
        this.textValueLoad = itemView.findViewById(R.id.textValueLoad)
        this.itemList = itemView.findViewById(R.id.itemList)
    }
}