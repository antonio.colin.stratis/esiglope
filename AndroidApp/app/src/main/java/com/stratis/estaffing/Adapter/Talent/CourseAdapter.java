package com.stratis.estaffing.Adapter.Talent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Modal.CertificateModal;
import com.stratis.estaffing.Model.Course;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.Talent.CourseView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 21/08/20
 */

public class CourseAdapter extends RecyclerView.Adapter<CourseView> {

    private ArrayList<Course> items;
    private Context context;

    public CourseView clickedView = null;

    public CourseAdapter(ArrayList<Course> items) {
        this.items = items;
    }

    @Override
    public CourseView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_talent_courses_item, parent, false);
        return new CourseView(layoutView);
    }

    @Override
    public void onBindViewHolder(final CourseView holder, int position) {

        holder.setIsRecyclable(false);
        final Course course = this.items.get(position);

        // -- Setting the views
        // --
        //holder.getCourseInitials().setImageDrawable( course.getInitials(context.getColor(R.color.colorPrimary), context) );
        App.setViewAsPrimaryColor(holder.getCourseInitials(), context);
        holder.getCourseInitials().setText(App.getTwoFirstChars(course.getName()));
        holder.getCourseTitle().setText( course.getName() );
        //holder.getCourseEndDate().setText( "Concluido el: " + App.getDateCourse(course.getDate()) );
        holder.getCourseEndDate().setText( App.getDateCourse(course.getDate()) );

        holder.getSchoolView().setText( course.getSchool() );
        holder.getValidityView().setText( course.getValidityDate() );
        holder.getCertificateView().setText( course.getCertificateNumber() );
        holder.getCourse_description().setText( course.getDescription() );

        holder.getArrow().setEnabled(false);
        holder.getArrow().setClickable(false);
        holder.getCourseMoreInfo().setVisibility(View.GONE);

        // -- Cert Image
        holder.getShow_certificate_button().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final CertificateModal modal = new CertificateModal(context);
                modal.setCourse(course);
                modal.show();
            }
        });

        // --
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (clickedView != null && clickedView != holder) {

                    clickedView.getCourseMoreInfo().setVisibility(View.GONE);
                    clickedView.getArrow().setRotation(0);
                }
                clickedView = holder;

                // --
                if (holder.getCourseMoreInfo().getVisibility() == View.GONE) {

                    holder.getCourseMoreInfo().setVisibility(View.VISIBLE);
                    holder.getArrow().setRotation(180);

                } else {

                    holder.getCourseMoreInfo().setVisibility(View.GONE);
                    holder.getArrow().setRotation(0);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<Course> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
