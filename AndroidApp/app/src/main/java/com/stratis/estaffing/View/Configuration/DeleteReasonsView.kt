package com.stratis.estaffing.View.Configuration

import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.R

class DeleteReasonsView(itemView: View) : RecyclerView.ViewHolder(itemView)  {

    var content: RelativeLayout? = null
    var textView: TextView? = null
    var reasonIconImageView: ImageView? = null

    init {
        this.content = itemView.findViewById(R.id.reasonContainer)
        this.textView = itemView.findViewById(R.id.reasonTextView)
        this.reasonIconImageView = itemView.findViewById(R.id.reasonIconImageView)
    }

}