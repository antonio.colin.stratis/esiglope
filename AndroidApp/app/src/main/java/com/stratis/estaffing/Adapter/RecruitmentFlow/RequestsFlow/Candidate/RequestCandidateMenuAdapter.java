package com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.Candidate;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateLanguageModel;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.Candidate.CandidateMenuModel;
import com.stratis.estaffing.R;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate.CandidateLanguageView;
import com.stratis.estaffing.View.RecruitmentFlow.RequestsFlow.Candidate.CandidateMenuView;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class RequestCandidateMenuAdapter extends RecyclerView.Adapter<CandidateMenuView> {

    protected ArrayList<CandidateMenuModel> items;
    private RequestCandidateMenuAdapterListener listener;

    private Integer messagesWithOutRead = 0;

    public RequestCandidateMenuAdapter(ArrayList<CandidateMenuModel> items) {
        this.items = items;
    }

    @Override
    public CandidateMenuView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.fragment_recruitment_request_candidate_single_menu_item,
                parent, false);
        return new CandidateMenuView(layoutView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final CandidateMenuView holder, final int position) {

        holder.setIsRecyclable(false);
        final CandidateMenuModel option = this.items.get(position);

        // --
        holder.getIcon().setImageBitmap(App.getImage(holder.itemView.getContext(), option.getIcon()));
        if (option.getId() == 1 && messagesWithOutRead > 0) {

            holder.getBubble().setVisibility(View.VISIBLE);
            holder.getBubble().setText("" + messagesWithOutRead);
        }

        // --
        holder.getTitle().setText( option.getTitle() );
        holder.getShape().setOnClickListener(v -> {
          if (listener != null) { listener.onDetail(option); }
        });
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<CandidateMenuModel> items) {

        this.items = items;
        this.notifyItemRangeInserted(0, this.items.size() - 1);
    }

    public void clear() {

        int size = this.items.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.items.remove(0);
            }
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public ArrayList<CandidateMenuModel> getItems() {
        return items;
    }

    public void setListener(RequestCandidateMenuAdapterListener listener) {
        this.listener = listener;
    }

    public void setMessagesWithOutRead(Integer messagesWithOutRead) {
        this.messagesWithOutRead = messagesWithOutRead;
    }

    public interface RequestCandidateMenuAdapterListener {
        void onDetail(CandidateMenuModel option);
    }
}