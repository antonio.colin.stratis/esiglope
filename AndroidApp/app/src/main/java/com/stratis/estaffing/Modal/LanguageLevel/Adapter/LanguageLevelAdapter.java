package com.stratis.estaffing.Modal.LanguageLevel.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.ABActivity;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Modal.LanguageLevel.View.LanguageLevelView;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageCatalog;
import com.stratis.estaffing.Model.RecruitmentFlow.LanguageLevel;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 02/09/20
 */

public class LanguageLevelAdapter extends RecyclerView.Adapter<LanguageLevelView> {

    protected ArrayList<LanguageLevel> items;
    protected Context context;
    protected LanguageCatalog languageCatalog;

    public LanguageLevelAdapter(ArrayList<LanguageLevel> items, Context context) {

        this.context = context;
        this.items = items;
    }

    @Override
    public LanguageLevelView onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.modal_language_level_item, parent, false);
        return new LanguageLevelView(layoutView);
    }

    @Override
    public void onBindViewHolder(final LanguageLevelView holder, final int position) {

        holder.setIsRecyclable(false);
        final LanguageLevel language = this.items.get(position);

        // --
        if (this.languageCatalog != null)  {
            if (this.languageCatalog.getLevelType() == language.getLevelType())  {

                holder.getBox().setBackground( ContextCompat.getDrawable(context, R.drawable.modal_language_level_item) );
                App.setViewAsSecondaryColor(holder.getBox(), context);

                holder.getTitle().setTextColor(Color.WHITE);
                holder.getIcon().setImageBitmap(App.getImage(context, "check_white_button"));
            }
        }

        // --
        holder.getTitle().setText(language.getLevel(context));
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    // --

    public void setLanguageCatalog(LanguageCatalog languageCatalog) {
        this.languageCatalog = languageCatalog;
    }


}
