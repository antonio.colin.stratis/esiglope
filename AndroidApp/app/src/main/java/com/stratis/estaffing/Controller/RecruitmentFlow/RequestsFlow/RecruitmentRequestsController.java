package com.stratis.estaffing.Controller.RecruitmentFlow.RequestsFlow;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Core.ExceptionHandler;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.RecruitmentRequestsFragment;
import com.stratis.estaffing.R;

public class RecruitmentRequestsController extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // ONM: ExceptionHandler
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_recruitment_request_actions);

        // -- Init views
        this.initViews();
    }

    private void initViews() {
        this.loadFragment(new RecruitmentRequestsFragment());
    }

    private boolean loadFragment(Fragment fragment) {

        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment, "RequestTop")
                    .commit();
            return true;
        }
        return false;
    }

    private void continueSegue(ABFragment fragment) {
        this.continueSegue(fragment,  "");
    }

    private void continueSegue(ABFragment fragment,  String tag) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.add(R.id.fragment_container, fragment, tag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {

        /*ABFragment previous = ((ABFragment) this.getSupportFragmentManager().getFragments().get(
                this.getSupportFragmentManager().getFragments().size() - 1));*/

        super.onBackPressed();

        /*ABFragment current = ((ABFragment) this.getSupportFragmentManager().getFragments().get(
                this.getSupportFragmentManager().getFragments().size() - 1));

        // --
        if (previous != current) {

            if (current instanceof RecruitmentRequestsFragment) {
                ((RecruitmentRequestsFragment) current).getRequests();
                ((RecruitmentRequestsFragment) current).scrollTToTop();
            }
        }*/
    }
}