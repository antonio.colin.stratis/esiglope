package com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow;

import android.os.Bundle;

import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stratis.estaffing.Adapter.RecruitmentFlow.RequestsFlow.RequestCandidatesAdapter;
import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.JsonBuilder;
import com.stratis.estaffing.Control.Loader;
import com.stratis.estaffing.Control.Notification;
import com.stratis.estaffing.Core.App;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTResponse;
import com.stratis.estaffing.Core.Session;
import com.stratis.estaffing.Fragment.RecruitmentFlow.RequestsFlow.CandidateSinglesFlow.RecruitmentRequestCandidateShare;
import com.stratis.estaffing.Listener.NotificationCenterFragmentListener;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequestCandidateFilter.Adapter.RequestCandidatesFilterAdapter;
import com.stratis.estaffing.Modal.RecruitmentFlow.RequestsFlows.RequestCandidateFilter.RequestCandidateFilterModal;
import com.stratis.estaffing.Model.Profile;
import com.stratis.estaffing.Model.RecruitmentFlow.RequestsFlow.RequestCandidate;
import com.stratis.estaffing.R;
import com.stratis.estaffing.Service.Recruitment.RequestsFlow.RequestsService;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;

import java.util.ArrayList;

public class RecruitmentRequestCandidatesFragment extends ABFragment implements View.OnClickListener,
        RequestCandidatesFilterAdapter.RequestCandidatesFilterAdapterListener,
        RequestCandidatesAdapter.RequestCandidatesClickListener, RecruitmentRequestCandidateShare.RecruitmentRequestCandidateShareListener {

    private RecyclerView gradedCandidates;
    private RecyclerView interviewCandidates;
    private RecyclerView candidates;
    private RecyclerView notInterestCandidates;

    private RequestCandidatesAdapter candidatesAdapter;
    private RequestCandidatesAdapter gradedCandidatesAdapter;
    private RequestCandidatesAdapter interviewCandidatesAdapter;
    private RequestCandidatesAdapter notInterestCandidatesAdapter;

    private Profile profile;
    private Loader loader = null;

    private String currentFilterId = "0";
    private Boolean candidatesVisible = false;
    private Boolean gradedCandidatesVisible = false;
    private Boolean interviewCandidatesVisible = false;
    private Boolean notInterestCandidatesVisible = false;

    // -- Listener for NotificationCenter
    private NotificationCenterFragmentListener notificationCenterListener;

    public RecruitmentRequestCandidatesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // --
        this.mView = inflater.inflate(R.layout.fragment_recruitment_request_candidates, container, false);

        // -- Set header
        this.setHeader("");

        // -- Init views
        this.initViews();

        // -- Get talents
        this.getCandidates();

        // -- Show loader
        this.loader = new Loader(getContext());
        this.loader.show();

        // --
        return this.mView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initViews() {

        this.gradedCandidates = this.mView.findViewById(R.id.gradedList);
        App.createVerticalRecyclerList(this.gradedCandidates, getContext());

        this.interviewCandidates = this.mView.findViewById(R.id.recyclerView);
        App.createVerticalRecyclerList(this.interviewCandidates, getContext());

        this.notInterestCandidates = this.mView.findViewById(R.id.not_elements_list);
        App.createVerticalRecyclerList(this.notInterestCandidates, getContext());

        // -- Buttons
        final Button deleteFiltersButton = this.mView.findViewById(R.id.deleteFiltersButton);
        deleteFiltersButton.setTag("10");
        deleteFiltersButton.setOnClickListener(this);

        final RelativeLayout filterButton = this.mView.findViewById(R.id.filterButton);
        filterButton.setTag("11");
        filterButton.setOnClickListener(this);
        this.setViewAsSecondaryColor(filterButton);
    }

    public void getCandidates() {

        // --
        this.candidates = this.mView.findViewById(R.id.elements_list);
        App.createVerticalRecyclerList(this.candidates, getContext());

        // -- Get talents
        RequestsService.getCandidates(this.profile.getProvisionalId(), new ReSTCallback() {
            @Override
            public void onSuccess(ReSTResponse response) {

                // --
                JSONArray res = JsonBuilder.stringToJsonArray(response.body);
                parseRequests(res);

                // -- Hide loader
                loader.cancel();
            }

            @Override
            public void onError(ReSTResponse response) {

                mView.findViewById(R.id.filtersContainer).setVisibility(View.GONE);
                loader.cancel();

                if (notificationCenterListener != null) {
                    notificationCenterListener.onCandidateListNotAvailable();
                }
            }
        });
    }

    private void parseRequests(JSONArray res) {

        ArrayList<RequestCandidate> gradedCandidatesItems = new ArrayList<>();
        ArrayList<RequestCandidate> interviewCandidatesItems = new ArrayList<>();
        ArrayList<RequestCandidate> candidatesItems = new ArrayList<>();
        ArrayList<RequestCandidate> notInterestCandidatesItems = new ArrayList<>();

        final ArrayList<RequestCandidate> items = RequestCandidate.parse(res);
        for (RequestCandidate item : items)  {

            switch(item.getStatusId()) {

                case RequestCandidate.statusCandidate:
                case RequestCandidate.statusCandidateFavorite:
                default:
                    candidatesItems.add(item);
                    break;

                case RequestCandidate.statusCandidateGraded:
                    gradedCandidatesItems.add(item);
                    break;

                case RequestCandidate.statusCandidateInterviews:
                case RequestCandidate.statusCandidateInterviewDateAssigned:
                    interviewCandidatesItems.add(item);
                    break;

                case RequestCandidate.statusCandidateNotChoose:
                case RequestCandidate.statusCandidateNotInterest:
                    notInterestCandidatesItems.add(item);
                    break;
            }
        }

        // -- Painting
        this.candidatesAdapter = new RequestCandidatesAdapter(candidatesItems, getContext());
        this.candidatesAdapter.setListener(this);
        this.candidates.setAdapter(candidatesAdapter);
        if (candidatesItems.size() > 0) {
            this.candidatesVisible = true;
            this.mView.findViewById(R.id.candidatesGroup).setVisibility(View.VISIBLE);
        }

        this.gradedCandidatesAdapter = new RequestCandidatesAdapter(gradedCandidatesItems, getContext());
        this.gradedCandidatesAdapter.setListener(this);
        this.gradedCandidates.setAdapter(gradedCandidatesAdapter);
        if (gradedCandidatesItems.size() > 0) {
            this.gradedCandidatesVisible = true;
            this.mView.findViewById(R.id.gradedGroup).setVisibility(View.VISIBLE);
        }

        this.interviewCandidatesAdapter = new RequestCandidatesAdapter(interviewCandidatesItems, getContext());
        this.interviewCandidatesAdapter.setListener(this);
        this.interviewCandidates.setAdapter(interviewCandidatesAdapter);
        if (interviewCandidatesItems.size() > 0) {
            this.interviewCandidatesVisible = true;
            this.mView.findViewById(R.id.interviewGroup).setVisibility(View.VISIBLE);
        }

        this.notInterestCandidatesAdapter = new RequestCandidatesAdapter(notInterestCandidatesItems, getContext());
        this.notInterestCandidatesAdapter.setListener(this);
        this.notInterestCandidatesAdapter.setNotInterested(true);
        this.notInterestCandidates.setAdapter(notInterestCandidatesAdapter);
        if (notInterestCandidatesItems.size() > 0) {
            this.notInterestCandidatesVisible = true;
            this.mView.findViewById(R.id.notInterestGroup).setVisibility(View.VISIBLE);
        }

        // -- Sharing CV's
        if (candidatesItems.size() > 0 || gradedCandidatesItems.size() > 0 ||
                interviewCandidatesItems.size() > 0 || notInterestCandidatesItems.size() > 0) {

            final TextView shareCV = findView(R.id.shareCV);
            shareCV.setVisibility(View.VISIBLE);
            shareCV.setTag("20");
            shareCV.setOnClickListener(this);
        }
    }

    @Override
    public void onDetail(RequestCandidate item) {

        final RecruitmentRequestCandidateSingleFragment candidateSingleFragment =
                new RecruitmentRequestCandidateSingleFragment();
        candidateSingleFragment.setProfile(this.profile);
        candidateSingleFragment.setCandidate(item);
        this.performSegue(candidateSingleFragment);
    }

    @Override
    public void onClick(View v) {

        final String tag = String.valueOf(v.getTag());
        switch (tag) {

            case "10":
                if (!this.currentFilterId.equals("0")) {
                    onFilterClick("0");
                }
                break;

            case "11":
                final RequestCandidateFilterModal modal = new RequestCandidateFilterModal(getContext());
                modal.setListener(this);
                modal.setCurrentFilterId(this.currentFilterId);
                modal.show();
                break;

            case "20":

                final RecruitmentRequestCandidateShare shareFragment = new RecruitmentRequestCandidateShare();
                shareFragment.setType(RecruitmentRequestCandidateShare.RecruitmentRequestCandidateShareType.all);
                shareFragment.setRequestId(this.profile.getProvisionalId());
                shareFragment.setListener(this);
                continueSegueUp(shareFragment);
                break;
        }
    }

    @Override
    public void onFilterClick(String filterId) {

        final Group gradedCandidatesGroup = this.mView.findViewById(R.id.gradedGroup);
        final Group interviewCandidatesGroup = this.mView.findViewById(R.id.interviewGroup);
        final Group candidatesGroup = this.mView.findViewById(R.id.candidatesGroup);
        final Group notInterestCandidatesGroup = this.mView.findViewById(R.id.notInterestGroup);

        gradedCandidatesGroup.setVisibility(View.GONE);
        interviewCandidatesGroup.setVisibility(View.GONE);
        candidatesGroup.setVisibility(View.GONE);
        notInterestCandidatesGroup.setVisibility(View.GONE);

        this.removeAllFavoriteFilter();

        this.currentFilterId = filterId;
        switch(filterId) {

            case "0":
                gradedCandidatesGroup.setVisibility((!this.gradedCandidatesVisible) ? View.GONE : View.VISIBLE);
                interviewCandidatesGroup.setVisibility((!this.interviewCandidatesVisible) ? View.GONE : View.VISIBLE);
                candidatesGroup.setVisibility((!this.candidatesVisible) ? View.GONE : View.VISIBLE);
                notInterestCandidatesGroup.setVisibility((!this.notInterestCandidatesVisible) ? View.GONE : View.VISIBLE);
                break;

            case RequestCandidate.statusCandidateInterviewDateAssigned:
                interviewCandidatesGroup.setVisibility(View.VISIBLE);
                break;

            case RequestCandidate.statusCandidate:
                candidatesGroup.setVisibility(View.VISIBLE);
                break;

            case "999":

                if (this.gradedCandidatesVisible) {
                    this.gradedCandidatesAdapter.filterByFavorite();
                    gradedCandidatesGroup.setVisibility( (this.gradedCandidatesAdapter.isEmpty()) ? View.GONE : View.VISIBLE);
                }

                if (this.interviewCandidatesVisible) {
                    this.interviewCandidatesAdapter.filterByFavorite();
                    interviewCandidatesGroup.setVisibility( (this.interviewCandidatesAdapter.isEmpty()) ? View.GONE : View.VISIBLE);
                }

                if (this.candidatesVisible) {
                    this.candidatesAdapter.filterByFavorite();
                    candidatesGroup.setVisibility( (this.candidatesAdapter.isEmpty()) ? View.GONE : View.VISIBLE);
                }

                if (this.notInterestCandidatesVisible) {
                    this.notInterestCandidatesAdapter.filterByFavorite();
                    notInterestCandidatesGroup.setVisibility( (this.notInterestCandidatesAdapter.isEmpty()) ? View.GONE : View.VISIBLE);
                }

                break;

            case RequestCandidate.statusCandidateNotInterest:
                notInterestCandidatesGroup.setVisibility(View.VISIBLE);
                break;
        }

    }

    private void removeAllFavoriteFilter() {

        this.candidatesAdapter.removeFavoriteFilter();
        this.gradedCandidatesAdapter.removeFavoriteFilter();
        this.interviewCandidatesAdapter.removeFavoriteFilter();
        this.notInterestCandidatesAdapter.removeFavoriteFilter();
    }

    // --
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public void setNotificationCenterListener(NotificationCenterFragmentListener notificationCenterListener) {
        this.notificationCenterListener = notificationCenterListener;
    }

    @Override
    public void onSingleCVShare() {}

    @Override
    public void onMultipleCVShare() {

        final Notification notification = new Notification(mContext, R.layout.notification_light);
        notification.setMessage("Candidatos compartidos");
        notification.show(2500);
    }

    @Override
    public void onErrorSharing(@NotNull String error) {

        final Notification notification = new Notification(mContext, R.layout.notification_light_error);
        notification.setMessage(error);
        notification.show(3000);
    }
}