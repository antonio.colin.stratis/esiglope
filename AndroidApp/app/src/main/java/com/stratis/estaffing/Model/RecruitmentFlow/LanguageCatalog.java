package com.stratis.estaffing.Model.RecruitmentFlow;

import android.content.Context;

import com.stratis.estaffing.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class LanguageCatalog implements Serializable {

    private Integer id = 0;
    private String name = "";
    private String description = "";
    private LanguageCatalogLevel level = LanguageCatalogLevel.basic;

    public enum LanguageCatalogLevel {
        basic, medium, advance
    }

    public LanguageCatalog() {}

    public LanguageCatalog(String name) {

        this.id = 0;
        this.name = name;
        this.description = "";
    }

    public LanguageCatalog(JSONObject obj) {

        if (obj != null) {

            this.id = obj.optInt("id", 0);
            this.name = obj.optString("name", "");
            this.description =  obj.optString("description", "");
        }
    }

    public static ArrayList<LanguageCatalog> parse(JSONArray array) {

        if (array != null) {

            ArrayList<LanguageCatalog> languages = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                JSONObject obj = array.optJSONObject(i);
                languages.add(new LanguageCatalog(obj));
            }
            return languages;
        }
        return null;
    }

    public static ArrayList<LanguageCatalog> getBase() {

        // --
        ArrayList<LanguageCatalog> languages = new ArrayList<>();

        // 1
        final LanguageCatalog languageOne = new LanguageCatalog();
        languages.add(languageOne);

        // 2
        final LanguageCatalog languageTwo = new LanguageCatalog();
        languages.add(languageTwo);

        // 3
        final LanguageCatalog languageThree = new LanguageCatalog();
        languages.add(languageThree);

        // --
        return  languages;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LanguageCatalogLevel getLevelType() {
        return this.level;
    }

    public void setLevel(LanguageCatalogLevel level) {
        this.level = level;
    }

    public void setLevelById(int id) {

        if (id == 1 || id == 2) {
            this.level = LanguageCatalogLevel.basic;
        } else if (id == 3 || id == 4) {
            this.level = LanguageCatalogLevel.medium;
        } else {
            this.level = LanguageCatalogLevel.advance;
        }
    }

    public String getLevel(Context context) {

        if (this.level ==  LanguageCatalogLevel.basic) {
            return context.getString(R.string.modal_language_level_basic);
        } else if (this.level == LanguageCatalogLevel.medium) {
            return context.getString(R.string.modal_language_level_medium);
        } else {
            return context.getString(R.string.modal_language_level_advanced);
        }
    }
    public int getLevelId() {

        if (this.level ==  LanguageCatalogLevel.basic) {
            return 2;
        } else if (this.level == LanguageCatalogLevel.medium) {
            return 4;
        } else {
            return 6;
        }
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
