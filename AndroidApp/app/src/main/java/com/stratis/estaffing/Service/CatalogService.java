package com.stratis.estaffing.Service;

import com.stratis.estaffing.Core.Api;
import com.stratis.estaffing.Core.Rester.ReSTCallback;
import com.stratis.estaffing.Core.Rester.ReSTRequest;
import com.stratis.estaffing.Core.eStaffingClient;

/**
 * Created by Erick Sanchez
 * Revision 1 - 15/12/20
 */

public class CatalogService {

    /**
     * LIST OF CATALOGS
     * 1000020001	Job Modality
     * 1000020002	Status de vacantes
     * 1000030003	Seniority
     * 1000030004	Contacts
     * 1000020005	Feedback Cancelación
     * 1000020006	Feedback Pausa
     * 1000050007	Stage Nutshell
     * 1000030008	Estatus Registro de Usuarios
     * 1000010009	Estatus De Candidatos/Talentos
     * 1000050010	Estatus Entrevistas
     * 1000010011	Categoria Skills
     * 1000010012	Motivos Descartar Candidatos
     * 1000020013	Motivos Anulación Vacantes
     * 1000020014	Motivos Pausa Vacantes
     * 1000030015	Reclutadores
     *
     * 1000020016	Paises
     * 1000020017	Tipo de contratacion
     * 1000020018	Areas
     * 1000010019	Tipo de Cursos/Educacion
     * 1000020036   Razones para borrar la cuenta
     * **/
    private final static String cancelFeedbackCatalog = "1000020005";
    private final static String pauseFeedbackCatalog = "1000020006";
    private final static String noInterestInCandidateCatalog = "1000010012";

    public static void findById(final String catalogId, final ReSTCallback listener) {
        // -- Single Talent logic
        final String url = Api.getInstance().getUrl("/catalog/findById");
        eStaffingClient rest = new eStaffingClient(url);
        ReSTRequest request = new ReSTRequest(ReSTRequest.REST_REQUEST_METHOD_GET, "");

        // --
        request.addHeaders(Api.getSecureHeaders());

        // --
        request.addParameter("catalogId", catalogId);

        // --
        rest.setDebugMode(Api.getInstance().canLog());
        rest.execute(request, listener);
    }

    public static void getCancelFeedbackCatalog(final ReSTCallback listener) {
        findById(CatalogService.cancelFeedbackCatalog, listener);
    }

    public static void getPauseFeedbackCatalog(final ReSTCallback listener) {
        findById(CatalogService.pauseFeedbackCatalog, listener);
    }

    public static void getNoInterestInCandidateCatalog(final ReSTCallback listener) {
        findById(CatalogService.noInterestInCandidateCatalog, listener);
    }
}