package com.stratis.estaffing.Control.Searcher;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.Searcher.Adapter.SearcherSkillAdapter;
import com.stratis.estaffing.Control.Searcher.Adapter.SearcherSoftwareAdapter;
import com.stratis.estaffing.Control.Searcher.Listener.RecyclerItemClickListener;
import com.stratis.estaffing.Model.RecruitmentFlow.SkillCatalog;
import com.stratis.estaffing.Model.RecruitmentFlow.SoftwareCatalog;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 01/09/20
 */

public class SearcherOpen {

    private ConstraintLayout root;
    private Context mContext;
    private Searcher.SearcherListener listener;
    private Object tag;
    private boolean initOpened = true;

    // -- Skills profile
    private ArrayList<SkillCatalog> data = new ArrayList<>();
    private ArrayList<SoftwareCatalog> dataSoftware = new ArrayList<>();

    private SearcherSkillAdapter adapter;
    private SearcherSoftwareAdapter adapterSoftware;

    private SearcherOpen.SearcherOpenType type = SearcherOpenType.normal;

    // --
    private ConstraintLayout searchComponentBox;
    private EditText searchComponentBoxInput;
    private ImageView searchComponentBoxIcon;

    private ConstraintLayout searchComponentListBox;
    private TextView searchComponentListBoxTitle;
    private RecyclerView searchComponentList;

    public SearcherOpen(Context context, ConstraintLayout root) {

        this.mContext = context;
        this.root = root;
    }

    private void initViews() {

        this.searchComponentBox = this.root.findViewById(R.id.searchComponentBox);
        this.searchComponentBoxInput = this.root.findViewById(R.id.searchComponentBoxInput);
        this.searchComponentBoxIcon = this.root.findViewById(R.id.searchComponentBoxIcon);

        this.searchComponentListBox = this.root.findViewById(R.id.searchComponentListBox);
        this.searchComponentListBoxTitle = this.root.findViewById(R.id.searchComponentListBoxTitle);
        this.searchComponentList = this.root.findViewById(R.id.searchComponentList);

        // --
        /** DividerItemDecoration instance **/
        final DividerItemDecoration itemDecorator = new DividerItemDecoration(this.mContext, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this.mContext, R.drawable.recycler_devider_1_gray));

        /** LinearLayoutManager instances **/
        LinearLayoutManager layout = new LinearLayoutManager(this.mContext, LinearLayoutManager.VERTICAL, false);

        // --
        this.searchComponentList.getRecycledViewPool().setMaxRecycledViews(0, 4);
        this.searchComponentList.setLayoutManager(layout);
        this.searchComponentList.addItemDecoration(itemDecorator);
        this.searchComponentList.setItemViewCacheSize(8);
        this.searchComponentList.setDrawingCacheEnabled(true);
        this.searchComponentList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // --  Adapter magics
        if (this.type == SearcherOpenType.normal) {

            this.adapter = new SearcherSkillAdapter( this.data, this.mContext);
            this.searchComponentList.setAdapter(adapter);

        } else {

            this.adapterSoftware = new SearcherSoftwareAdapter(this.dataSoftware, this.mContext);
            this.searchComponentList.setAdapter(this.adapterSoftware);
        }

        // --
        if ( this.searchComponentBoxInput.getText().equals("") && this.data.size() > 0 ) {
            this.searchComponentListBox.setVisibility(View.GONE);
        } else {
            this.searchComponentListBox.setVisibility(View.VISIBLE);
        }

        // --
        if (!this.initOpened) {
            this.searchComponentListBox.setVisibility(View.GONE);
        }

        // --
        this.setItemClick();

        // --
        this.setInputListener();
    }

    private void setItemClick() {

        this.searchComponentList.addOnItemTouchListener(
                new RecyclerItemClickListener(this.mContext, this.searchComponentList, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Object object;
                //  --
                if (type == SearcherOpenType.normal) {
                    object = adapter.getItems().get(position);
                } else {
                    object = adapterSoftware.getItems().get(position);
                }

                // --
                if (listener != null) {
                    listener.onItemSelected(object, searchComponentBox, searchComponentList,
                            searchComponentBoxInput, searchComponentBoxIcon);
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));
    }

    private void setInputListener() {

        this.searchComponentBoxInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                searchComponentListBox.setVisibility( (s.length() > 0) ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                searchComponentListBox.setVisibility( (s.length() > 0) ? View.VISIBLE : View.GONE);

                // -- Check filter
                if (type == SearcherOpenType.normal) {
                    adapter.getFilter().filter(String.valueOf(s));
                } else {
                    adapterSoftware.getFilter().filter(String.valueOf(s));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchComponentListBox.setVisibility( (s.length() > 0) ? View.VISIBLE : View.GONE);
            }
        });
    }

    public void setData(ArrayList<SkillCatalog> data) {

        this.data = data;
        this.initViews();
    }

    public void setDataSoftware(ArrayList<SoftwareCatalog> dataSoftware) {

        this.dataSoftware = dataSoftware;
        this.initViews();
    }

    // --
    public void setInputHint(String string) {

        if (this.searchComponentBoxInput != null) {
            this.searchComponentBoxInput.setHint(string);
        }
    }

    public void setSearchComponentListBoxTitle(String searchComponentListBoxTitle) {
        if (this.searchComponentListBoxTitle != null) {
            this.searchComponentListBoxTitle.setText(searchComponentListBoxTitle);
        }
    }

    public void setListener(Searcher.SearcherListener listener, Object tag) {

        this.listener = listener;
        this.setTag(tag);
        if (this.searchComponentBox != null) {
            this.searchComponentBox.setTag(tag);
        }
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public Object getTag() {
        return tag;
    }

    public ConstraintLayout getRoot() {
        return root;
    }

    public void setSelected(ArrayList<SkillCatalog> array) {
        this.adapter.setSelected(array);
    }

    public void setSelectedSoftware(ArrayList<SoftwareCatalog> arraySoftware) {
        this.adapterSoftware.setSelected(arraySoftware);
    }

    public void setType(SearcherOpenType type) {
        this.type = type;
    }

    public EditText getSearchComponentBoxInput() {
        return searchComponentBoxInput;
    }

    public ImageView getSearchComponentBoxIcon() {
        return searchComponentBoxIcon;
    }

    public RecyclerView getSearchComponentList() {
        return searchComponentList;
    }

    public enum SearcherOpenType {
        normal, soft
    }

    public void setInitOpened(boolean initOpened) {
        this.initOpened = initOpened;
    }
}
