package com.stratis.estaffing.Adapter;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.stratis.estaffing.Control.ABFragment;
import com.stratis.estaffing.Control.WrapHeightViewPager;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez
 * Revision 1 - 27/07/20
 */

public class HomePageAdapter extends FragmentStatePagerAdapter {

    ArrayList<ABFragment> mFragments;
    ArrayList<String> mTitles;
    FragmentManager mFragmentManager;
    private int mCurrentPosition = -1;

    public HomePageAdapter(FragmentManager fm) {

        super(fm);
        this.mFragmentManager = fm;
        this.mFragments = new ArrayList<>();
        this.mTitles = new ArrayList<>();
    }

    public HomePageAdapter(FragmentManager fm, ArrayList<ABFragment> fragments) {

        super(fm);
        this.mFragmentManager = fm;
        this.mFragments = fragments;
        this.mTitles = new ArrayList<>();
    }

    @Override
    public ABFragment getItem(int position) {
        return this.mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    public void addFragment(ABFragment fragment) {
        this.addFragment(fragment, "");
    }

    public void addFragment(ABFragment fragment, String title) {

        mFragments.add(fragment);
        mTitles.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {

        super.setPrimaryItem(container, position, object);
        if (position != mCurrentPosition) {
            Fragment fragment = (Fragment) object;
            WrapHeightViewPager pager = (WrapHeightViewPager) container;
            if (fragment != null && fragment.getView() != null) {
                mCurrentPosition = position;
                pager.measureCurrentView(fragment.getView());
            }
        }
    }
}