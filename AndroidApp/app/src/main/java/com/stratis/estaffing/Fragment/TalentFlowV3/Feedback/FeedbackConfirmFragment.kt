package com.stratis.estaffing.Fragment.TalentFlowV3.Feedback

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.analytics.FirebaseAnalytics
import com.stratis.estaffing.Adapter.ListButtonAdapter
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Control.JsonBuilder
import com.stratis.estaffing.Control.RatingStar.RatingStarComplete
import com.stratis.estaffing.Core.App
import com.stratis.estaffing.Core.Rester.ReSTCallback
import com.stratis.estaffing.Core.Rester.ReSTResponse
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.Modal.FeedbackModal
import com.stratis.estaffing.Model.TalentFlowV3.FeedbackDetail
import com.stratis.estaffing.R
import com.stratis.estaffing.Service.Talent.TalentService
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class FeedbackConfirmFragment : ABFragment(), View.OnClickListener {

    private var isLoading: Boolean = false
    var rate: Int? = null
    var talentId: String? = null
    var talentName: String? = null
    var skills: ArrayList<FeedbackDetail>? = null
    var strenghtsSkills: ArrayList<FeedbackDetail> = ArrayList()
    var opportunitiesSkills: ArrayList<FeedbackDetail> = ArrayList()
    var strenghtsComment: String? = null
    var opportunitiesComment: String? = null

    var strengthsHardAdapter: ListButtonAdapter? = null
    var strengthsSoftAdapter: ListButtonAdapter? = null
    var strengthsHabitAdapter: ListButtonAdapter? = null
    var strengthsToolsAdapter: ListButtonAdapter? = null
    var opportunitiesHardAdapter: ListButtonAdapter? = null
    var opportunitiesSoftAdapter: ListButtonAdapter? = null
    var opportunitiesHabitAdapter: ListButtonAdapter? = null
    var opportunitiesToolsAdapter: ListButtonAdapter? = null

    var strengthsHardSubtitle: TextView? = null
    var strengthsSoftSubtitle: TextView? = null
    var strengthsHabitSubtitle: TextView? = null
    var strengthsToolsSubtitle: TextView? = null
    var opportunitiesHardSubtitle: TextView? = null
    var opportunitiesSoftSubtitle: TextView? = null
    var opportunitiesHabitSubtitle: TextView? = null
    var opportunitiesToolsSubtitle: TextView? = null

    var strengthsHardList: RecyclerView? = null
    var strengthsSoftList: RecyclerView? = null
    var strengthsHabitList: RecyclerView? = null
    var strengthsToolsList: RecyclerView? = null
    var opportunitiesHardList: RecyclerView? = null
    var opportunitiesSoftList: RecyclerView? = null
    var opportunitiesHabitList: RecyclerView? = null
    var opportunitiesToolsList: RecyclerView? = null

    var hardStrenghtsSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var softStrenghtsSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var habitsStrenghtsSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var toolsStrenghtsSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var hardOpportunitiesSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var softOpportunitiesSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var habitsOpportunitiesSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()
    var toolsOpportunitiesSkills: List<FeedbackDetail> = emptyArray<FeedbackDetail>().toMutableList()

    var strengthsCommentValue: TextView? = null
    var opportunitiesCommentValue: TextView? = null

    var buttonContainer: ConstraintLayout? = null
    var buttonTextView: TextView? = null
    var buttonImage: ImageView? = null
    var loadingContainer: ConstraintLayout? = null
    var progressBar: ProgressBar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mView = inflater.inflate(R.layout.fragment_talent_feedback_confirm, container, false)

        setHeader("")
        initView()

        return mView
    }

    @SuppressLint("CutPasteId")
    private fun initView(){
        // Buttons
        val editStrenghtsButton: Button = mView.findViewById(R.id.editStrenghtsButton)
        val editOpportunitiesButton: Button = mView.findViewById(R.id.editOpportunitiesButton)
        buttonContainer = mView.findViewById(R.id.buttonContainer)
        buttonTextView = mView.findViewById(R.id.buttonTextView)
        buttonImage = mView.findViewById(R.id.buttonImage)
        loadingContainer = mView.findViewById(R.id.loadingContainer)
        progressBar = mView.findViewById(R.id.progress_bar)

        editStrenghtsButton.tag = 11
        editStrenghtsButton.setOnClickListener(this)
        editOpportunitiesButton.tag = 12
        editOpportunitiesButton.setOnClickListener(this)
        buttonContainer?.tag = 10
        buttonContainer?.setOnClickListener(this)

        // Custom
        setViewAsPrimaryColor(buttonContainer)

        // Strings
        mView.findViewById<TextView>(R.id.title).text = String.format(mContext.getString(R.string.feedback_fragment_confirm_title),  App.capitalize(talentName))
        mView.findViewById<TextView>(R.id.title3).text = String.format(mContext.getString(R.string.feedback_fragment_confirm_review),  App.capitalize(talentName))

        // -- Stars
        val ratedContainerStars = RatingStarComplete(mView.findViewById(R.id.ratedContainerStars), context)
        rate?.let { ratedContainerStars.setRating(it) }
        mView.findViewById<TextView>(R.id.ratedDescription).text = ratedContainerStars.geFeedbackRateDescription(rate)

        // Comments
        strengthsCommentValue = mView.findViewById(R.id.strengthsCommentValue)
        if (strenghtsComment != "") {
            strengthsCommentValue?.text = strenghtsComment
        } else {
            strengthsCommentValue?.isGone = true
        }

        opportunitiesCommentValue = mView.findViewById(R.id.opportunitiesCommentValue)
        if (opportunitiesComment != "") {
            opportunitiesCommentValue?.text = opportunitiesComment
        } else {
            opportunitiesCommentValue?.isGone = true
        }

        // Adapters
        strengthsHardList = mView.findViewById(R.id.strengthsHardList)
        strengthsHardSubtitle = mView.findViewById(R.id.strengthsHardSubtitle)
        strengthsSoftList = mView.findViewById(R.id.strengthsSoftList)
        strengthsSoftSubtitle = mView.findViewById(R.id.strengthsSoftSubtitle)
        strengthsHabitList = mView.findViewById(R.id.strengthsHabitList)
        strengthsHabitSubtitle = mView.findViewById(R.id.strengthsHabitSubtitle)
        strengthsToolsList = mView.findViewById(R.id.strengthsToolsList)
        strengthsToolsSubtitle = mView.findViewById(R.id.strengthsToolsSubtitle)

        App.createVerticalRecyclerList(strengthsHardList, context)
        App.createVerticalRecyclerList(strengthsSoftList, context)
        App.createVerticalRecyclerList(strengthsHabitList, context)
        App.createVerticalRecyclerList(strengthsToolsList, context)

        opportunitiesHardList = mView.findViewById(R.id.opportunitiesHardList)
        opportunitiesHardSubtitle = mView.findViewById(R.id.opportunitiesHardSubtitle)
        opportunitiesSoftList = mView.findViewById(R.id.opportunitiesSoftList)
        opportunitiesSoftSubtitle = mView.findViewById(R.id.opportunitiesSoftSubtitle)
        opportunitiesHabitList = mView.findViewById(R.id.opportunitiesHabitList)
        opportunitiesHabitSubtitle = mView.findViewById(R.id.opportunitiesHabitSubtitle)
        opportunitiesToolsList = mView.findViewById(R.id.opportunitiesToolsList)
        opportunitiesToolsSubtitle = mView.findViewById(R.id.opportunitiesToolsSubtitle)

        App.createVerticalRecyclerList(opportunitiesHardList, context)
        App.createVerticalRecyclerList(opportunitiesSoftList, context)
        App.createVerticalRecyclerList(opportunitiesHabitList, context)
        App.createVerticalRecyclerList(opportunitiesToolsList, context)

        updateView()
    }

    private fun parseTalents(catalogs: List<FeedbackDetail>): ArrayList<String> {
        val descriptionParsed: ArrayList<String> = ArrayList()
        catalogs.forEach{ descriptionParsed.add(it.name) }

        return descriptionParsed
    }

    private fun updateView() {
        hardStrenghtsSkills = strenghtsSkills.filter { it.categoryId == 1001100001L }
        if (hardStrenghtsSkills.count() > 0) {
            strengthsHardSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_hard),
                hardStrenghtsSkills.count())
            strengthsHardAdapter = ListButtonAdapter(parseTalents(hardStrenghtsSkills))
            strengthsHardList?.adapter = strengthsHardAdapter
            strengthsHardList?.isGone = false
            strengthsHardSubtitle?.isGone = false
            strengthsHardAdapter?.notifyDataSetChanged()
        } else {
            strengthsHardList?.isGone = true
            strengthsHardSubtitle?.isGone = true
        }

        softStrenghtsSkills = strenghtsSkills.filter { it.categoryId == 1001100002L }
        if (softStrenghtsSkills.count() > 0) {
            strengthsSoftSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_soft),
                softStrenghtsSkills.count())
            strengthsSoftAdapter = ListButtonAdapter(parseTalents(softStrenghtsSkills))
            strengthsSoftList?.adapter = strengthsSoftAdapter
            strengthsSoftList?.isGone = false
            strengthsSoftSubtitle?.isGone = false
            strengthsSoftAdapter?.notifyDataSetChanged()
        } else {
            strengthsSoftList?.isGone = true
            strengthsSoftSubtitle?.isGone = true
        }

        habitsStrenghtsSkills = strenghtsSkills.filter { it.categoryId == 1001100003L }
        if (habitsStrenghtsSkills.count() > 0) {
            strengthsHabitSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_habits),
                habitsStrenghtsSkills.count())
            strengthsHabitAdapter = ListButtonAdapter(parseTalents(habitsStrenghtsSkills))
            strengthsHabitList?.adapter = strengthsHabitAdapter
            strengthsHabitList?.isGone = false
            strengthsHabitSubtitle?.isGone = false
            strengthsHabitAdapter?.notifyDataSetChanged()
        } else {
            strengthsHabitList?.isGone = true
            strengthsHabitSubtitle?.isGone = true
        }

        toolsStrenghtsSkills = strenghtsSkills.filter { it.categoryId == 1001100004L }
        if (toolsStrenghtsSkills.count() > 0) {
            strengthsToolsSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_tools),
                toolsStrenghtsSkills.count())
            strengthsToolsAdapter = ListButtonAdapter(parseTalents(toolsStrenghtsSkills))
            strengthsToolsList?.adapter = strengthsToolsAdapter
            strengthsToolsList?.isGone = false
            strengthsToolsSubtitle?.isGone = false
            strengthsToolsAdapter?.notifyDataSetChanged()
        } else {
            strengthsToolsList?.isGone = true
            strengthsToolsSubtitle?.isGone = true
        }

        hardOpportunitiesSkills = opportunitiesSkills.filter { it.categoryId == 1001100001L }
        if (hardOpportunitiesSkills.count() > 0) {
            opportunitiesHardSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_hard),
                hardOpportunitiesSkills.count())
            opportunitiesHardAdapter = ListButtonAdapter(parseTalents(hardOpportunitiesSkills))
            opportunitiesHardList?.adapter = opportunitiesHardAdapter
            opportunitiesHardList?.isGone = false
            opportunitiesHardSubtitle?.isGone = false
            opportunitiesHardAdapter?.notifyDataSetChanged()
        } else {
            opportunitiesHardList?.isGone = true
            opportunitiesHardSubtitle?.isGone = true
        }

        softOpportunitiesSkills = opportunitiesSkills.filter { it.categoryId == 1001100002L }
        if (softOpportunitiesSkills.count() > 0) {
            opportunitiesSoftSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_soft),
                softOpportunitiesSkills.count())
            opportunitiesSoftAdapter = ListButtonAdapter(parseTalents(softOpportunitiesSkills))
            opportunitiesSoftList?.adapter = opportunitiesSoftAdapter
            opportunitiesSoftList?.isGone = false
            opportunitiesSoftSubtitle?.isGone = false
            opportunitiesSoftAdapter?.notifyDataSetChanged()
        } else {
            opportunitiesSoftList?.isGone = true
            opportunitiesSoftSubtitle?.isGone = true
        }

        habitsOpportunitiesSkills = opportunitiesSkills.filter { it.categoryId == 1001100003L }
        if (habitsOpportunitiesSkills.count() > 0) {
            opportunitiesHabitSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_habits),
                habitsOpportunitiesSkills.count())
            opportunitiesHabitAdapter = ListButtonAdapter(parseTalents(habitsOpportunitiesSkills))
            opportunitiesHabitList?.adapter = opportunitiesHabitAdapter
            opportunitiesHabitList?.isGone = false
            opportunitiesHabitSubtitle?.isGone = false
            opportunitiesHabitAdapter?.notifyDataSetChanged()
        } else {
            opportunitiesHabitList?.isGone = true
            opportunitiesHabitSubtitle?.isGone = true
        }

        toolsOpportunitiesSkills = opportunitiesSkills.filter { it.categoryId == 1001100004L }
        if (toolsOpportunitiesSkills.count() > 0) {
            opportunitiesToolsSubtitle?.text = String.format(mContext.getString(R.string.feedback_fragment_detail_item_tools),
                toolsOpportunitiesSkills.count())
            opportunitiesToolsAdapter = ListButtonAdapter(parseTalents(toolsOpportunitiesSkills))
            opportunitiesToolsList?.adapter = opportunitiesToolsAdapter
            opportunitiesToolsList?.isGone = false
            opportunitiesToolsSubtitle?.isGone = false
            opportunitiesToolsAdapter?.notifyDataSetChanged()
        } else {
            opportunitiesToolsList?.isGone = true
            opportunitiesToolsSubtitle?.isGone = true
        }
    }

    private fun toogleButton() {
        buttonTextView?.visibility = if (isLoading) View.VISIBLE else View.GONE
        buttonImage?.visibility = if (isLoading) View.VISIBLE else View.GONE
        loadingContainer?.visibility = if (isLoading) View.GONE else View.VISIBLE

        if (!isLoading) {
            setViewColor(buttonContainer, ContextCompat.getColor(mContext, R.color.recruitment_request_vacancy_unselected_text_button))
            progressBar?.animate()
        }

        isLoading = !isLoading
    }

    private fun sendFeedback() {
        // --
        val params = JSONObject()
        params.put("authorId", Session.get(context).dataSessionId)
        params.put("talentId", talentId)
        params.put("rate", rate)
        params.put("strengths", strenghtsComment)
        params.put("opportunities", opportunitiesComment)
        params.put("strengthsSkills", getArray(strenghtsSkills))
        params.put("opportunitiesSkills", getArray(opportunitiesSkills))

        Log.d("DXGOP", "JSON FEEDBACK SAVE ENVIADO: " + JsonBuilder.jsonToString(params))

        toogleButton()

        TalentService.sendFeedback(params, object: ReSTCallback {
            override fun onSuccess(response: ReSTResponse) {
                toogleButton()
                // --
                Log.d("DXGOP", "TALENT FEEDBACK SAVE RESPONSE = " + response.body)
                val res = JsonBuilder.stringToJson(response.body)

                // --
                val code = res.optInt("code", 500)
                if (code == 201) {
                    // --
                    val parameters = Bundle()
                    val mFirebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
                    mFirebaseAnalytics.logEvent("Feedbacks_Enviados", parameters)
                    val messageFramgent = FeedbackMessageFragment()
                    messageFramgent.rate = rate
                    continueSegue(messageFramgent)
                } else {
                    // -- Show
                    val modal = FeedbackModal(context)
                    modal.setWarning()
                    modal.show()
                }
            }

            override fun onError(response: ReSTResponse) {
                toogleButton()
                // --
                Log.d("DXGOP", """ERROR :: ${response.body} CODDE :: ${response.statusCode}""")

                // -- Show
                val modal = FeedbackModal(context)
                modal.setWarning()
                modal.show()
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.tag.toString()) {
            "10" -> {
                sendFeedback()
            }
            "11" -> {
                val feedbackNewOportunityFragment = FeedbackNewFragment()
                feedbackNewOportunityFragment.type = FeedbackNewFragment.FeedbackType.STRENGTH
                feedbackNewOportunityFragment.talentId = this.talentId
                feedbackNewOportunityFragment.talentName = this.talentName
                feedbackNewOportunityFragment.allSkills = skills
                feedbackNewOportunityFragment.strenghtsComment = strenghtsComment
                feedbackNewOportunityFragment.strenghtsSkills = strenghtsSkills
                feedbackNewOportunityFragment.opportunitiesComment = opportunitiesComment
                feedbackNewOportunityFragment.opportunitiesSkills = opportunitiesSkills
                feedbackNewOportunityFragment.selectedSkills = strenghtsSkills.toMutableList() as ArrayList<FeedbackDetail>
                feedbackNewOportunityFragment.rate = rate
                feedbackNewOportunityFragment.isEdit = true
                feedbackNewOportunityFragment.setListener(object : FeedbackNewFragment.FeedbackConfirmListener {
                    override fun onEdit(
                        selectedSkills: ArrayList<FeedbackDetail>,
                        comment: String
                    ) {
                        strenghtsComment = comment
                        if (strenghtsComment != "") {
                            strengthsCommentValue?.text = strenghtsComment
                            strengthsCommentValue?.isGone = false
                        } else {
                            strengthsCommentValue?.isGone = true
                        }
                        strenghtsSkills = selectedSkills
                        updateView()
                    }
                })

                this.continueSegue(feedbackNewOportunityFragment)
            }
            "12" -> {
                val feedbackNewOportunityFragment = FeedbackNewFragment()
                feedbackNewOportunityFragment.type = FeedbackNewFragment.FeedbackType.OPORTUNITY
                feedbackNewOportunityFragment.talentId = this.talentId
                feedbackNewOportunityFragment.talentName = this.talentName
                feedbackNewOportunityFragment.allSkills = skills
                feedbackNewOportunityFragment.strenghtsComment = strenghtsComment
                feedbackNewOportunityFragment.strenghtsSkills = strenghtsSkills
                feedbackNewOportunityFragment.opportunitiesComment = opportunitiesComment
                feedbackNewOportunityFragment.opportunitiesSkills = opportunitiesSkills
                feedbackNewOportunityFragment.selectedSkills = opportunitiesSkills.toMutableList() as ArrayList<FeedbackDetail>
                feedbackNewOportunityFragment.rate = rate
                feedbackNewOportunityFragment.isEdit = true
                feedbackNewOportunityFragment.setListener(object : FeedbackNewFragment.FeedbackConfirmListener {
                    override fun onEdit(
                        selectedSkills: ArrayList<FeedbackDetail>,
                        comment: String
                    ) {
                        opportunitiesComment = comment
                        if (opportunitiesComment != "") {
                            opportunitiesCommentValue?.text = opportunitiesComment
                            opportunitiesCommentValue?.isGone = false
                        } else {
                            opportunitiesCommentValue?.isGone = true
                        }
                        opportunitiesSkills = selectedSkills
                        updateView()
                    }
                })
                this.continueSegue(feedbackNewOportunityFragment)
            }
        }
    }

    private fun getArray(arraySkills: MutableList<FeedbackDetail>): JSONArray {
        val array = JSONArray()
        for (skill in arraySkills) {  array.put(skill.getObject()) }

        return array
    }

}