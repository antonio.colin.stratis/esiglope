package com.stratis.estaffing.Adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stratis.estaffing.R
import com.stratis.estaffing.View.ButtonArrowView
import java.util.ArrayList

class ListButtonAdapter(items: ArrayList<String>?) : RecyclerView.Adapter<ButtonArrowView>() {

    private var items: ArrayList<String>? = null

    init {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ButtonArrowView {
        val layoutView: View = LayoutInflater.from(parent.context).
        inflate(R.layout.list_button_item, parent, false)
        return ButtonArrowView(layoutView)
    }

    override fun getItemCount(): Int {
        return if (items == null) 0 else items!!.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ButtonArrowView, position: Int) {
        holder.setIsRecyclable(false)
        val description : String = this.items!![position]
        holder.layoutButton!!.text = description
    }
}