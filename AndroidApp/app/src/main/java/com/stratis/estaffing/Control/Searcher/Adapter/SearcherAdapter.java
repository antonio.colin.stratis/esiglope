package com.stratis.estaffing.Control.Searcher.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.stratis.estaffing.Control.Searcher.View.SearchView;
import com.stratis.estaffing.Control.Searcher.Filter.SimpleProfileSearchFilter;
import com.stratis.estaffing.Model.RecruitmentFlow.SimpleProfile;
import com.stratis.estaffing.Model.RecruitmentFlow.SkillCatalog;
import com.stratis.estaffing.R;

import java.util.ArrayList;

/**
 * Created by Erick Sanchez - App Builders CTO
 * Revision 1 - 01/09/20
 */

public class SearcherAdapter extends RecyclerView.Adapter<SearchView> implements Filterable {

    protected ArrayList<SimpleProfile> items;
    protected ArrayList<SimpleProfile> original = new ArrayList<>();
    protected ArrayList<SkillCatalog> selected = new ArrayList<>();
    protected Context context;
    private SearcherAdapterListener listener;

    public SearcherAdapter(ArrayList<SimpleProfile> items, Context context) {
        this.context = context;
        this.items = items;
        this.original.addAll(items);
    }

    @NonNull
    @Override
    public SearchView onCreateViewHolder(ViewGroup parent, int viewType) {
        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.component_search_item, parent, false);
        return new SearchView(layoutView);
    }

    @Override
    public void onBindViewHolder(final SearchView holder, int position) {

        holder.setIsRecyclable(false);
        final SimpleProfile profile = this.items.get(position);

        // -- Setting the views
        holder.getSearchComponentBoxItemText().setText( profile.getName() );
    }

    @Override
    public int getItemCount() {
        return this.items == null ? 0 : this.items.size();
    }

    public void reload(ArrayList<SimpleProfile> items) {

        if (items != null) {
            this.items = items;
            this.notifyItemRangeInserted(0, this.items.size() - 1);
        }
    }

    public void clear() {
        int size = this.items.size();
        if (size > 0) {
            this.items.subList(0, size).clear();
            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void reload(Object values) {
        this.reload((ArrayList<SimpleProfile>) values);
    }

    public ArrayList<SimpleProfile> getItems() {
        return items;
    }

    public ArrayList<SimpleProfile> getOriginal() {
        return original;
    }

    public ArrayList<SkillCatalog> getSelected() {
        return selected;
    }

    public void setSelected(ArrayList<SkillCatalog> selected) {
        this.selected = selected;
    }

    // --

    @NonNull
    @Override
    public Filter getFilter() {
        return new SimpleProfileSearchFilter(this);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void filter(String text) {
        Log.d("DXGI",  "TEXT :::: " + text);
        this.items.clear();
        if(text.isEmpty()) {
            this.items.addAll(this.original);
            listener.onClean();
        } else {
            int MAX_ITEMS = 5;
            int currentItems = 0;
            text = text.toLowerCase();
            for (SimpleProfile item: this.original) {
                if (currentItems == MAX_ITEMS) { break; }
                if (item.getName().toLowerCase().contains(text)) {
                    items.add(item);
                    currentItems++;
                }
            }
        }

        this.notifyDataSetChanged();
    }

    public void setListener(SearcherAdapterListener listener) {
        this.listener = listener;
    }

    public interface SearcherAdapterListener {
        void onClean();
    }
}
