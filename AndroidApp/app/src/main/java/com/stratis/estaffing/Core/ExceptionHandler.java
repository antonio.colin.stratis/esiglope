package com.stratis.estaffing.Core;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.stratis.estaffing.Controller.LoginFlow.LoginController;

import androidx.annotation.NonNull;

import java.util.Objects;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    private final Activity mContext;

    public ExceptionHandler(Activity context) {
        this.mContext = context;
    }

    @Override
    public void uncaughtException(@NonNull Thread t, @NonNull Throwable e) {

        Log.e("DXGOP", Objects.requireNonNull(e.getLocalizedMessage()));
        e.printStackTrace();
        FirebaseCrashlytics.getInstance().recordException(e);
        if (!(this.mContext instanceof LoginController)) {

            Intent intent = new Intent(this.mContext, LoginController.class);
            intent.putExtra("from", "killProcess");
            this.mContext.startActivity(intent);

            // --
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(10);
        }
    }
}