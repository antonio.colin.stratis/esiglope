package com.stratis.estaffing.Fragment.DeleteAccount

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.stratis.estaffing.Control.ABFragment
import com.stratis.estaffing.Controller.LoginFlow.LoginController
import com.stratis.estaffing.Core.Session
import com.stratis.estaffing.R

class DeleteConfirmFragment: ABFragment(), View.OnClickListener {

    private lateinit var buttonContinue: Button
    private var session: Session? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        this.mView = inflater.inflate(R.layout.fragment_delete_confirm, container, false)

        this.initViews()
        return this.mView
    }

    private fun initViews() {

        buttonContinue = mView.findViewById(R.id.done)
        buttonContinue.tag = "13"
        buttonContinue.setOnClickListener(this)

        session = Session.get(this.activity)
    }

    @SuppressLint("ResourceAsColor")
    override fun onClick(v: View?) {
        when(v!!.tag.toString()) {
            "13" -> {
                activity?.let{
                    session?.logout()
                    val intent = Intent (it, LoginController::class.java)
                    it.startActivity(intent)
                    activity?.finishAffinity()
                }
            }
        }
    }

}